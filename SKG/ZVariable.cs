﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Sep-19 22:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections;
using System.IO;

namespace SKG
{
    using Ext;

    /// <summary>
    /// Environment variable
    /// </summary>
    public class ZVariable
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZVariable() { }

        /// <summary>
        /// Get variable value
        /// </summary>
        /// <param name="name">Variable name</param>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        /// <returns>Return the result</returns>
        protected static int GetEnv(string name, double min, double max)
        {
            var t = Environment.GetEnvironmentVariable(name);
            var res = t.ToDouble();

            if (res < min)
            {
                res = min;
            }
            if (res > max)
            {
                res = max;
            }

            return (int)res;
        }

        /// <summary>
        /// Get variable value
        /// </summary>
        /// <param name="name">Variable name</param>
        /// <returns>Return the result</returns>
        protected static bool GetEnv(string name)
        {
            var t = Environment.GetEnvironmentVariable(name);
            var res = t.ToBool();
            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Build version
        /// </summary>
        public static string BuildVersion
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("BUILD_VERSION").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// JWT signing
        /// </summary>
        public static string JwtSigning
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "JWT_SIGNING").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// JWT issuer
        /// </summary>
        public static string JwtIssuer
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "JWT_ISSUER").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// JWT audience
        /// </summary>
        public static string JwtAudience
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "JWT_AUDIENCE").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// System administrator user name
        /// </summary>
        public static string SystemAdmin
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("SYSTEM_ADMIN").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = nameof(SystemAdmin);
                }

                return res;
            }
        }

        /// <summary>
        /// Customer
        /// </summary>
        public static string AspCustomer
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("ASPNETCORE_CUSTOMER").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }
                else
                {
                    res = "-" + res;
                }

                return res;
            }
        }

        /// <summary>
        /// Environment
        /// </summary>
        public static string AspEnvironment
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Base path
        /// </summary>
        public static string AspBasepath
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("ASPNETCORE_BASEPATH").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Server environment
        /// </summary>
        public static string ServerEnvironment
        {
            get
            {
                if (_serverEnvironment == null)
                {
                    var t = "";

                    var customer = Environment.GetEnvironmentVariable("ASPNETCORE_CUSTOMER").Trimz();
                    if (string.IsNullOrWhiteSpace(customer))
                    {
                        t = "local";
                    }
                    else if (customer.Contains("dev") || customer.Contains("toan"))
                    {
                        t = "dev";
                    }
                    else if (customer.Contains("test"))
                    {
                        t = "test";
                    }
                    else if (customer.Contains("qa"))
                    {
                        t = "qa";
                    }
                    else if (customer.Contains("stg"))
                    {
                        t = "stg";
                    }
                    else if (customer.Contains("uat"))
                    {
                        t = "uat";
                    }
                    _serverEnvironment = t;
                }

                return _serverEnvironment;
            }
        }

        /// <summary>
        /// MA (Matcha Analytics) API production port
        /// </summary>
        public static string MaApiPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("MA_API_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = DebugMode ? ZConst.MaApiDebugPort : ZConst.MaApiPort;
                }

                return res;
            }
        }

        /// <summary>
        /// MS (Matcha Site) API production port
        /// </summary>
        public static string MsApiPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("MS_API_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = DebugMode ? ZConst.MsApiDebugPort : ZConst.MsApiPort;
                }

                return res;
            }
        }

        /// <summary>
        /// MT (Matcha Tools) API production port
        /// </summary>
        public static string MtApiPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("MT_API_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = DebugMode ? ZConst.MtApiDebugPort : ZConst.MtApiPort;
                }

                return res;
            }
        }

        /// <summary>
        /// MA (Matcha Analytics) production port
        /// </summary>
        public static string MaPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("MA_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = DebugMode ? ZConst.MaPort : "80";
                }

                return res;
            }
        }

        /// <summary>
        /// MS (Matcha Site) production port
        /// </summary>
        public static string MsPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable("MS_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = DebugMode ? ZConst.MsPort : "80";
                }

                return res;
            }
        }

        /// <summary>
        /// RSA private key
        /// </summary>
        public static string RsaPrivateKey
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "RSA_PRIVATE_KEY").Trimz();
                return res;
            }
        }

        /// <summary>
        /// RSA public key
        /// </summary>
        public static string RsaPublicKey
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "RSA_PUBLIC_KEY").Trimz();
                return res;
            }
        }

        /// <summary>
        /// File upload
        /// </summary>
        public static string FileUpload
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "FILE_UPLOAD").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Site title
        /// </summary>
        public static string SiteTitle
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "SITE_TITLE").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Site UID
        /// </summary>
        public static string SiteUid
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "SITE_UID").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// API URL
        /// </summary>
        public static string ApiUrl
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "API_URL").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// API key
        /// </summary>
        public static string ApiKey
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "API_KEY").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// API hash
        /// </summary>
        public static string ApiHash
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "API_HASH").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        #region -- Database --
        /// <summary>
        /// Database server
        /// </summary>
        public static string DbServer
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "DB_SERVER").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Database port
        /// </summary>
        public static string DbPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "DB_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Database user
        /// </summary>
        public static string DbUser
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "DB_USER").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Database password
        /// </summary>
        public static string DbPassword
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "DB_PASSWORD").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Database analysis
        /// </summary>
        public static string DbDatabaseAnalysis
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "DB_DATABASE_ANALYSIS").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Database home
        /// </summary>
        public static string DbDatabaseHome
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "DB_DATABASE_HOME").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }
        #endregion

        #region -- Redis --
        /// <summary>
        /// Redis server
        /// </summary>
        public static string RdServer
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "RD_SERVER").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Redis password
        /// </summary>
        public static string RdPassword
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "RD_PASSWORD").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }

        /// <summary>
        /// Redis port
        /// </summary>
        public static string RdPort
        {
            get
            {
                var res = Environment.GetEnvironmentVariable(Prefix + "RD_PORT").Trimz();

                if (string.IsNullOrEmpty(res))
                {
                    res = string.Empty;
                }

                return res;
            }
        }
        #endregion

        /// <summary>
        /// Time to live of JWT [2 - 1440] minutes
        /// </summary>
        public static int JwtTime
        {
            get
            {
                var res = GetEnv(Prefix + "JWT_TIME", 2, 1440);
                return res;
            }
        }

        /// <summary>
        /// Time to auto logout [2 - 60] minutes
        /// </summary>
        public static int LogoutTime
        {
            get
            {
                var res = GetEnv(Prefix + "LOGOUT_TIME", 2, 60);
                return res;
            }
        }

        /// <summary>
        /// Number of days password will be expired [2 - 90] days
        /// </summary>
        public static int PasswordExpired
        {
            get
            {
                var res = GetEnv(Prefix + "PASSWORD_EXPIRED", 2, 90);
                return res;
            }
        }

        /// <summary>
        /// Number of times password will be allowed [2 - 10] times
        /// </summary>
        public static int PasswordHistory
        {
            get
            {
                var res = GetEnv(Prefix + "PASSWORD_HISTORY", 2, 10);
                return res;
            }
        }

        /// <summary>
        /// Number of attempts to enter a wrong password [2 - 10] times
        /// </summary>
        public static int LockoutThreshold
        {
            get
            {
                var res = GetEnv(Prefix + "LOCKOUT_THRESHOLD", 2, 10);
                return res;
            }
        }

        /// <summary>
        /// How long an account will be locked [2 - 30] minutes
        /// </summary>
        public static int LockoutDuration
        {
            get
            {
                var res = GetEnv(Prefix + "LOCKOUT_DURATION", 2, 30);
                return res;
            }
        }

        /// <summary>
        /// Single session
        /// </summary>
        public static bool SingleSession
        {
            get
            {
                var res = GetEnv(Prefix + "SINGLE_SESSION");
                return res;
            }
        }

        /// <summary>
        /// Debug mode
        /// </summary>
        public static bool DebugMode
        {
            get
            {
                var res = GetEnv(Prefix + "DEBUG_MODE");
                return res;
            }
        }

        /// <summary>
        /// RSA mode
        /// </summary>
        public static bool RsaMode
        {
            get
            {
                var res = GetEnv(Prefix + "RSA_MODE");
                return res;
            }
        }

        /// <summary>
        /// Use development cloud API
        /// </summary>
        public static bool UseCloudApi
        {
            get
            {
                var res = GetEnv(Prefix + "USE_CLOUD_API");
                return res;
            }
        }

        /// <summary>
        /// Enable Swagger
        /// </summary>
        public static bool EnableSwagger
        {
            get
            {
                var res = GetEnv(Prefix + "ENABLE_SWAGGER");
                return res;
            }
        }

        /// <summary>
        /// Allow to write log
        /// </summary>
        public static bool AllowWriteLog
        {
            get
            {
                var res = GetEnv(Prefix + "ALLOW_WRITE_LOG");
                return res;
            }
        }

        /// <summary>
        /// Variable prefix
        /// </summary>
        public static string Prefix
        {
            get
            {
                if (prefix == null)
                {
                    prefix = string.Empty;
                }

                return prefix;
            }
            set
            {
                prefix = value;
            }
        }

        /// <summary>
        /// Get environment variables
        /// </summary>
        public static string Variables
        {
            get
            {
                var res = "\n";

                var t = Environment.GetEnvironmentVariables();
                foreach (DictionaryEntry de in t)
                {
                    res += string.Format("  {0} = {1}\n", de.Key, de.Value);
                }

                res += "\n********# # # # # # # # # # # # # # # # # # # # # #********\n";
                res += $"Path.DirectorySeparatorChar: '{Path.DirectorySeparatorChar}'\n";
                res += $"Path.AltDirectorySeparatorChar: '{Path.AltDirectorySeparatorChar}'\n";
                res += $"Path.PathSeparator: '{Path.PathSeparator}'\n";
                res += $"Path.VolumeSeparatorChar: '{Path.VolumeSeparatorChar}'\n";

                var invalidChars = Path.GetInvalidPathChars();
                res += $"Path.GetInvalidPathChars:\n";
                for (int ctr = 0; ctr < invalidChars.Length; ctr++)
                {
                    res += $"  U+{Convert.ToUInt16(invalidChars[ctr]):X4} ";
                    if ((ctr + 1) % 10 == 0)
                    {
                        res += "\n";
                    }
                }

                res += "\n\nASPNETCORE_CUSTOMER = " + AspCustomer + "\n";
                res += "ASPNETCORE_ENVIRONMENT = " + AspEnvironment + "\n";

                return res;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Variable prefix
        /// </summary>
        private static string prefix;

        /// <summary>
        /// Server environment
        /// </summary>
        private static string _serverEnvironment;

        #endregion
    }
}
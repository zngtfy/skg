﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-24 06:54
 * Update       : 2020-Sep-23 08:34
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Quartz;
using Quartz.Spi;
using System;

namespace SKG
{
    /// <summary>
    /// Job factory
    /// </summary>
    public class ZFactory : IJobFactory
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="serviceProvider">Service provider</param>
        public ZFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// New job
        /// </summary>
        /// <param name="bundle">bundle</param>
        /// <param name="scheduler">scheduler</param>
        /// <returns>Return the result</returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _serviceProvider.GetService(bundle.JobDetail.JobType) as IJob;
        }

        /// <summary>
        /// Return job
        /// </summary>
        /// <param name="job">Job</param>
        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Service provider
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        #endregion
    }
}
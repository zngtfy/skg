﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Sep-11 09:09
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.ComponentModel.DataAnnotations;

namespace SKG
{
    using Att;

    /// <summary>
    /// Enumeration
    /// </summary>
    public class ZEnum
    {
        /// <summary>
        /// Text format
        /// </summary>
        public enum Format
        {
            /// <summary>
            /// Sentence case
            /// </summary>
            Sentence,

            /// <summary>
            /// lower case
            /// </summary>
            Lower,

            /// <summary>
            /// UPPER CASE
            /// </summary>
            Upper,

            /// <summary>
            /// Capitalized Case
            /// </summary>
            Capitalized,

            /// <summary>
            /// Original string
            /// </summary>
            Original
        }

        /// <summary>
        /// Traffic
        /// </summary>
        public enum Traffic
        {
            /// <summary>
            /// None
            /// </summary>
            [Css("direct", "secondary")]
            Direct = 0,

            /// <summary>
            /// Google
            /// </summary>
            [Css("google-ads", "yellow")]
            Google = 1,

            /// <summary>
            /// Other
            /// </summary>
            [Css("other", "gray-800")]
            Other = 2,

            /// <summary>
            /// Double Click
            /// </summary>
            DoubleClick = 3,

            /// <summary>
            /// Facebook
            /// </summary>
            [Css("facebook", "ocean")]
            Facebook = 4,

            /// <summary>
            /// Zanox
            /// </summary>
            Zanox = 5,

            /// <summary>
            /// Link
            /// </summary>
            [Css("link", "red")]
            Link = 6,

            /// <summary>
            /// DuckDuckGo
            /// </summary>
            [Css("duckduckgo", "orange")]
            DuckDuckGo = 7,

            /// <summary>
            /// Yandex
            /// </summary>
            [Css("yandex", "red")]
            Yandex = 8,

            /// <summary>
            /// Bing
            /// </summary>
            [Css("bing", "blue")]
            Bing = 9
        }

        /// <summary>
        /// Entity status
        /// </summary>
        public enum Status
        {
            /// <summary>
            /// None
            /// </summary>
            [Display(Name = "Default"), Css("default", "")]
            None = 0,

            /// <summary>
            /// Enabled
            /// </summary>
            [Display(Name = "Enabled"), Css("success", "")]
            Enabled,

            /// <summary>
            /// Disabled
            /// </summary>
            [Display(Name = "Disabled"), Css("warning", "")]
            Disabled,

            /// <summary>
            /// Blocked
            /// </summary>
            [Display(Name = "Blocked"), Css("danger", "")]
            Blocked,

            /// <summary>
            /// Deleted
            /// </summary>
            Deleted,

            /// <summary>
            /// Data for testing
            /// </summary>
            Tested,

            /// <summary>
            /// Developing
            /// </summary>
            Developing,

            /// <summary>
            /// Enquired
            /// </summary>
            [Display(Name = "Enquired"), Css("default", "")]
            Enquired,

            /// <summary>
            /// From CRM
            /// </summary>
            [Display(Name = "CRM"), Css("default", "")]
            CRM
        }

        /// <summary>
        /// User type
        /// </summary>
        public enum UserType
        {
            /// <summary>
            /// Standard
            /// </summary>
            [Display(Name = "Standard User"), Css("success", "")]
            Standard,

            /// <summary>
            /// Super
            /// </summary>
            [Display(Name = "Super User"), Css("warning", "")]
            Super,

            /// <summary>
            /// Master
            /// </summary>
            [Display(Name = "Master User"), Css("danger", "")]
            Master
        }

        /// <summary>
        /// User role
        /// </summary>
        public enum UserRole
        {
            /// <summary>
            /// General user
            /// </summary>
            [Display(Name = "General user")]
            General = 1,

            /// <summary>
            /// Sales suite user
            /// </summary>
            [Display(Name = "Sales user")]
            Sales,

            /// <summary>
            /// Master user
            /// </summary>
            [Display(Name = "Master user")]
            Master,

            /// <summary>
            /// Head user
            /// </summary>
            [Display(Name = "Head user")]
            Head,

            /// <summary>
            /// Super user
            /// </summary>
            [Display(Name = "Super user")]
            Super,

            /// <summary>
            /// Operator
            /// </summary>
            [Display(Name = "Operator")]
            Operator,

            /// <summary>
            /// Manager
            /// </summary>
            [Display(Name = "Manager")]
            Manager,

            /// <summary>
            /// Admin
            /// </summary>
            [Display(Name = "Admin")]
            Admin,

            /// <summary>
            /// Sub user
            /// </summary>
            [Display(Name = "Sub")]
            Sub
        }

        /// <summary>
        /// Synchronization
        /// </summary>
        public enum Sync
        {
            /// <summary>
            /// Synced (between MA and MS)
            /// </summary>
            Synced = 1,

            /// <summary>
            /// Skip synchronization
            /// </summary>
            Skip,

            /// <summary>
            /// Error
            /// </summary>
            Error,

            /// <summary>
            /// Synced (between MA and MA)
            /// </summary>
            MaMa
        }

        /// <summary>
        /// Base type of code
        /// </summary>
        public enum TypeZ
        {
            /// <summary>
            /// Standard objects
            /// </summary>
            ObjectStandard,

            /// <summary>
            /// Custom objects
            /// </summary>
            ObjectCustom,

            /// <summary>
            /// Administrative permissions
            /// </summary>
            PermissionAdministrative,

            /// <summary>
            /// General user permissions
            /// </summary>
            PermissionGeneralUser,

            /// <summary>
            /// Group object
            /// </summary>
            GroupObject,

            /// <summary>
            /// Development
            /// </summary>
            Development,

            /// <summary>
            /// Setting
            /// </summary>
            Setting,

            /// <summary>
            /// Function
            /// </summary>
            Function,

            /// <summary>
            /// Record type
            /// </summary>
            RecordType,

            /// <summary>
            /// Picklist
            /// </summary>
            Picklist
        }

        /// <summary>
        /// Type plan
        /// </summary>
        public enum TypePlan
        {
            /// <summary>
            /// None
            /// </summary>
            None = 0,

            /// <summary>
            /// Apartment
            /// </summary>
            [Display(Name = "Apartment")]
            Apartment = 1 << 0,

            /// <summary>
            /// Home plan
            /// </summary>
            [Display(Name = "Masterplan")]
            HomePlan = 1 << 1,

            /// <summary>
            /// Both
            /// </summary>
            [Display(Name = "Apartment/Masterplan")]
            Both = Apartment | HomePlan
        }

        /// <summary>
        /// External CRM and EDM
        /// </summary>
        public enum External
        {
            /// <summary>
            /// Don't have
            /// </summary>
            None,

            /// <summary>
            /// Salesforce
            /// </summary>
            Salesforce,

            /// <summary>
            /// Rexsoftware
            /// </summary>
            Rexsoftware,

            /// <summary>
            /// Agentbox
            /// </summary>
            Agentbox,

            /// <summary>
            /// Boxdice
            /// </summary>
            Boxdice,

            /// <summary>
            /// Mailchimp
            /// </summary>
            Mailchimp,

            /// <summary>
            /// Sendgrid
            /// </summary>
            Sendgrid
        }

        /// <summary>
        /// Form mode
        /// </summary>
        public enum FormMode
        {
            /// <summary>
            /// View
            /// </summary>
            View,

            /// <summary>
            /// Create
            /// </summary>
            Create,

            /// <summary>
            /// Update
            /// </summary>
            Update
        }

        /// <summary>
        /// Alert code
        /// </summary>
        public enum AlertCode
        {
            /// <summary>
            /// None
            /// </summary>
            None = 0,

            /// <summary>
            /// Success
            /// </summary>
            Success = 1,

            /// <summary>
            /// Failure
            /// </summary>
            Failure = 2,

            /// <summary>
            /// Not found
            /// </summary>
            NotFound = 3,

            /// <summary>
            /// Redirect URL
            /// </summary>
            RedirectUrl = 10
        }

        /// <summary>
        /// Auth type
        /// </summary>
        public enum AuthType
        {
            /// <summary>
            /// Matcha analytics
            /// </summary>
            [Display(Name = "MA")]
            Ma,

            /// <summary>
            /// Customer relationship management
            /// </summary>
            [Display(Name = "CRM")]
            Crm,

            /// <summary>
            /// Electronic direct mail
            /// </summary>
            [Display(Name = "EDM")]
            Edm
        }

        /// <summary>
        /// Profile matcha analytics
        /// </summary>
        public enum ProfileMa
        {
            /// <summary>
            /// Matcha System Administrator
            /// Full access in MA and MS
            /// </summary>
            MatchaSystemAdministrator,

            /// <summary>
            /// Matcha Site Administrator
            /// Full access MS and have standard permission in MA
            /// </summary>
            MatchaSiteAdministrator,

            /// <summary>
            /// Matcha Standard User
            /// Standard permission in MA
            /// </summary>
            MatchaStandardUser,

            /// <summary>
            /// Matcha Analyst View
            /// Allow analyst view data only
            /// </summary>
            MatchaAnalystView,

            /// <summary>
            /// Matcha Sync Data
            /// Allow user sync data between MA and MS
            /// </summary>
            MatchaSyncData
        }

        /// <summary>
        /// Profile matcha site
        /// </summary>
        public enum ProfileMs
        {
            /// <summary>
            /// Full access web application
            /// </summary>
            Administrator,

            /// <summary>
            /// Minimum access in Matcha.Home
            /// </summary>
            StandardUser
        }

        /// <summary>
        /// Time type
        /// </summary>
        public enum TimeType
        {
            /// <summary>
            /// Minute
            /// </summary>
            Minute,

            /// <summary>
            /// Hour
            /// </summary>
            Hour,

            /// <summary>
            /// Day
            /// </summary>
            Day
        }
    }
}
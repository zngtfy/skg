﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Sep-19 22:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace SKG
{
    using Dto;
    using Ext;

    /// <summary>
    /// Token
    /// </summary>
    public class ZToken
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="setting">JWT setting</param>
        /// <param name="payload">Payload</param>
        /// <param name="cid">Client ID</param>
        /// <param name="sid">Session ID</param>
        public ZToken(JwtDto setting, object payload, Guid? cid, Guid? sid)
        {
            if (setting == null)
            {
                setting = new JwtDto();
            }

            Secret = setting.Signing;
            Expires = setting.Time;
            Issuer = setting.Issuer;
            Audience = setting.Audience;

            Payload = payload;
            ClientId = cid;
            SessionId = sid;
        }

        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="auth">Bearer token</param>
        /// <param name="secret">Secret (if null or length less than 16 then use ZConst.MyCode)</param>
        public static JwtSecurityToken Validate(StringValues auth, string secret)
        {
            JwtSecurityToken res = null;

            var token = auth.FirstOrDefault()?.Split(" ").Last();
            if (string.IsNullOrWhiteSpace(token))
            {
                return res;
            }

            try
            {
                var key = Encoding.UTF8.GetBytes(secret);
                var hmac = new HMACSHA512(key);
                var param = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(hmac.Key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero // tokens expire exactly at token expiration time (instead of 5 minutes later)
                };

                var handler = new JwtSecurityTokenHandler();
                handler.ValidateToken(token, param, out SecurityToken jwt);

                res = (JwtSecurityToken)jwt;
            }
            catch (Exception ex)
            {
                ex.Message.LogError();
            }

            return res;
        }

        /// <summary>
        /// Read JWT
        /// </summary>
        /// <param name="auth">Bearer token</param>
        public static JwtSecurityToken ReadJwt(StringValues auth)
        {
            JwtSecurityToken res = null;

            var token = auth.FirstOrDefault()?.Split(" ").Last();
            if (string.IsNullOrWhiteSpace(token))
            {
                return res;
            }

            try
            {
                var handler = new JwtSecurityTokenHandler();
                res = handler.ReadJwtToken(token);
            }
            catch (Exception ex)
            {
                ex.Message.LogError();
            }

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Issuer
        /// </summary>
        public string Issuer { get; private set; }

        /// <summary>
        /// Audience
        /// </summary>
        public string Audience { get; private set; }

        /// <summary>
        /// Client ID
        /// </summary>
        public Guid? ClientId { get; private set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public Guid? SessionId { get; private set; }

        /// <summary>
        /// Secret
        /// </summary>
        public string Secret { get; private set; }

        /// <summary>
        /// Payload
        /// </summary>
        public object Payload { get; private set; }

        /// <summary>
        /// Expires (minute: 1 - 10000)
        /// </summary>
        public double Expires { get; private set; }

        /// <summary>
        /// JSON web token
        /// </summary>
        public string Jwt
        {
            get
            {
                var now = DateTime.UtcNow;
                var expires = now.AddMinutes(Expires);

                var jti = $"{ClientId}_{SessionId}";
                var sid = SessionId.ToString();
                var iat = now.ToUniversalTime().ToString();

                var claims = new Claim[] {
                    new Claim(JwtRegisteredClaimNames.Jti, jti),
                    new Claim(JwtRegisteredClaimNames.Sid, sid),
                    new Claim(JwtRegisteredClaimNames.Iat, iat)
                };

                var array = Encoding.UTF8.GetBytes(Secret);
                var key = new SymmetricSecurityKey(array);
                var signing = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);

                var token = new JwtSecurityToken(
                    issuer: Issuer,
                    audience: Audience,
                    claims: claims,
                    notBefore: now,
                    expires: expires,
                    signingCredentials: signing);
                token.Payload[ZConst.Payload] = Payload;

                var res = new JwtSecurityTokenHandler().WriteToken(token);
                return res;
            }
        }

        #endregion
    }
}
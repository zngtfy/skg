﻿using System.Collections.Generic;
using System.Text;
using System.Web;

namespace SKG
{
    /// <summary>
    /// String map
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    public class StringMap<T> : SortedList<string, T>
    {
        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Return the result</returns>
        public new T this[string key]
        {
            get
            {
                if (key != null && ContainsKey(key))
                {
                    return base[key];
                }
                else
                {
                    return default;
                }
            }
            set
            {
                base[key] = value;
            }
        }
    }

    /// <summary>
    /// String map
    /// </summary>
    public class StringMap : StringMap<string>
    {
        /// <summary>
        /// Build query string
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>Return the result</returns>
        public string BuildQueryString(string path)
        {
            var strBuilder = new StringBuilder();
            strBuilder.Append(path);
            var isFirst = true;

            foreach (var item in this)
            {
                if (isFirst)
                {
                    strBuilder.Append("?");
                    isFirst = false;
                }
                else
                {
                    strBuilder.Append("&");
                }

                var val = HttpUtility.UrlEncode(item.Value);
                strBuilder.Append($"{item.Key}={val}");
            }

            return strBuilder.ToString();
        }
    }

    /// <summary>
    /// Number map
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    public class NumberMap<T> : SortedList<int, T>
    {
        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Return the result</returns>
        public new T this[int key]
        {
            get
            {
                if (ContainsKey(key))
                {
                    return base[key];
                }
                else
                {
                    return default;
                }
            }
            set
            {
                base[key] = value;
            }
        }
    }

    /// <summary>
    /// Number map
    /// </summary>
    public class NumberMap : NumberMap<string>
    {
    }
}
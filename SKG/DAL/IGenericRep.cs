﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SKG.DAL
{
    using static ZEnum;

    /// <summary>
    /// Interface generic repository
    /// </summary>
    /// <typeparam name="T">Model class type</typeparam>
    public interface IGenericRep<T> where T : class
    {
        #region -- Methods --

        /// <summary>
        /// Create the model
        /// </summary>
        /// <param name="m">The model</param>
        void Create(T m);

        /// <summary>
        /// Create list model
        /// </summary>
        /// <param name="l">List model</param>
        void Create(List<T> l);

        /// <summary>
        /// Read by
        /// </summary>
        /// <param name="p">Predicate</param>
        /// <returns>Return query data</returns>
        IQueryable<T> Read(Expression<Func<T, bool>> p);

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <returns>Return the object</returns>
        T Read(uint id);

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="code">Secondary key</param>
        /// <param name="status">Status</param>
        /// <returns>Return the object</returns>
        T Read(string code, Status? status);

        /// <summary>
        /// Update the model
        /// </summary>
        /// <param name="m">The model</param>
        void Update(T m);

        /// <summary>
        /// Update list model
        /// </summary>
        /// <param name="l">List model</param>
        void Update(List<T> l);

        #endregion

        #region -- Properties --

        /// <summary>
        /// Return query all data
        /// </summary>
        IQueryable<T> All { get; }

        /// <summary>
        /// Return query all data
        /// </summary>
        IQueryable<T> AllNoTracking { get; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Sep-24 07:18
 * Update       : 2020-Sep-24 07:18
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.DAL.Models
{
    /// <summary>
    /// Group member model
    /// </summary>
    public class ZeroGroupMember : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroGroupMember() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Member ID
        /// </summary>
        public uint? MemberId { get; set; }

        /// <summary>
        /// Member ID (MA)
        /// </summary>
        public uint? MemberMaId { get; set; }

        /// <summary>
        /// Group ID
        /// </summary>
        public uint GroupId { get; set; }

        /// <summary>
        /// Group ID (MA)
        /// </summary>
        public uint? GroupMaId { get; set; }

        #endregion
    }
}
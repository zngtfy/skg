﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-14 13:11
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : Done
 */
#endregion

using System;
using static SKG.ZEnum;

namespace SKG.DAL.Models
{
    using Ext;

    /// <summary>
    /// Authentication model
    /// </summary>
    public class SysAuth : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysAuth() { }

        /// <summary>
        /// Create token
        /// </summary>
        /// <param name="d">Date and time create</param>
        /// <param name="id">Site UID</param>
        /// <returns>Return the result</returns>
        public string CreateToken(DateTime d, string id)
        {
            var h = new ZHash(ApiKey, ApiHash);
            return h.CreateToken(d, id);
        }

        /// <summary>
        /// Valid token
        /// </summary>
        /// <param name="s">Token needs to valid</param>
        /// <returns>Return the result</returns>
        public bool ValidToken(string s)
        {
            var h = new ZHash(ApiKey, ApiHash);
            return h.ValidToken(s);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="name">Site name</param>
        /// <param name="uid">Site UID</param>
        /// <param name="url">MA URL</param>
        /// <returns>Return the result</returns>
        public static SysAuth Create(string name, Guid uid, string url = "https://analytics.immexgroup.com")
        {
            var h = new ZHash(name);

            var m = new SysAuth
            {
                Name = name,
                DevName = name.ToPascalCase(),
                SiteUid = uid,
                ApiUrl = url,
                ApiKey = h.Value,
                ApiHash = h.Salt,
                Type = AuthType.Ma
            };

            return m;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Site UID
        /// </summary>
        public Guid SiteUid { get; set; }

        /// <summary>
        /// API URL (host)
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// API key (client ID)
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// API hash (secret)
        /// </summary>
        public string ApiHash { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Account ID (for Rexsoftware)
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public AuthType? Type { get; set; }

        #endregion
    }
}
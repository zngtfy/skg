﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using SKG.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKG.DAL.Models
{
    using Ext;

    /// <summary>
    /// Code model
    /// </summary>
    public class SysCode : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysCode()
        {
            Sequence = 0;
            Parents = new HashSet<SysCode>();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="dn">Developer name</param>
        public SysCode(string dn)
        {
            DevName = dn;
            Name = dn.ToAddSpace();
            Group = dn.ToPrefix();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Group
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Sequence
        /// </summary>
        public ushort Sequence { get; set; }

        /// <summary>
        /// Type ID
        /// </summary>
        public uint? TypeId { get; set; }

        /// <summary>
        /// Type navigation
        /// </summary>
        [JsonIgnore, ForeignKey("TypeId")]
        public virtual SysType Type { get; set; }

        /// <summary>
        /// Parent ID
        /// </summary>
        public uint? ParentId { get; set; }

        /// <summary>
        /// Parent navigation
        /// </summary>
        [JsonIgnore, ForeignKey("ParentId")]
        public virtual SysCode Parent { get; set; }

        /// <summary>
        /// Parents
        /// </summary>
        public virtual ICollection<SysCode> Parents { get; set; }

        /// <summary>
        /// Type name
        /// </summary>
        [NotMapped]
        public string TypeName
        {
            get { return Type == null ? string.Empty : Type.Name; }
        }

        /// <summary>
        /// Parent name
        /// </summary>
        [NotMapped]
        public string ParentName
        {
            get { return Parent == null ? string.Empty : Parent.Name; }
        }

        /// <summary>
        /// Expand data
        /// </summary>
        [NotMapped]
        public ExpandAuthDto Expand
        {
            get
            {
                return ExpandData.ToInst<ExpandAuthDto>();
            }
        }

        #endregion
    }
}
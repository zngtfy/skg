﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Oct-10 18:44
 * Update       : 2020-Oct-10 18:44
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections.Generic;

namespace SKG.DAL.Models
{
    /// <summary>
    /// Scheduler model
    /// </summary>
    public class SysScheduler : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysScheduler() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Last run time
        /// </summary>
        public DateTime? LastRunTime { get; set; }

        /// <summary>
        /// Next run time
        /// </summary>
        public DateTime? NextRunTime { get; set; }

        /// <summary>
        /// Codes navigation
        /// </summary>
        public virtual ICollection<SysSchedulerLog> Codes { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKG.DAL.Models
{
    using Dto;
    using Ext;

    /// <summary>
    /// Application model
    /// </summary>
    public class SysApp : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysApp() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="dn">Developer name</param>
        public SysApp(string dn)
        {
            DevName = dn;
            Name = dn.ToAddSpace();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">ID</param>
        public SysApp(string name, uint id) : this(name)
        {
            Id = id;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Is custom application
        /// </summary>
        public bool Custom { get; set; }

        /// <summary>
        /// Sequence
        /// </summary>
        public ushort Sequence { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Color
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Navigation (use splitter ;)
        /// </summary>
        public string Navigation { get; set; }

        /// <summary>
        /// Profiles (use splitter ;)
        /// </summary>
        public string Profile { get; set; }

        /// <summary>
        /// Navigations
        /// </summary>
        [NotMapped]
        public List<NavDto> Navigations
        {
            get
            {
                return Navigation.ToList<NavDto>();
            }
            set
            {
                if (value != null)
                {
                    Navigation = value.ToJson();
                }
            }
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion


namespace SKG.DAL.Models
{
    /// <summary>
    /// History for even ID  model
    /// </summary>
    public class HisCommon : ZeroHistory
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public HisCommon() : base() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="tn">Table name</param>
        /// <param name="new">New data</param>
        /// <param name="old">Old data</param>
        public HisCommon(string tn, object @new, object old) : base(tn, @new, old) { }

        #endregion
    }
}
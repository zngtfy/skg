﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-14 05:34
 * Update       : 2020-Nov-06 08:28
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.DAL.Models
{
    using static ZEnum;

    /// <summary>
    /// Interface normal model
    /// </summary>
    public interface INorModel
    {
        #region -- Properties --

        /// <summary>
        /// Status
        /// </summary>
        Status Status { get; set; }

        /// <summary>
        /// Created by
        /// </summary>
        string CreatedBy { get; set; }

        /// <summary>
        /// Created on
        /// </summary>
        DateTime CreatedOn { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Modified by
        /// </summary>
        string ModifiedBy { get; set; }

        /// <summary>
        /// Modified on
        /// </summary>
        DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// Expand data (JSON)
        /// </summary>
        string ExpandData { get; set; }

        /// <summary>
        /// Behalf
        /// </summary>
        string Behalf { get; set; }

        /// <summary>
        /// Synchronization UID
        /// </summary>
        Guid? SyncUid { get; set; }

        #endregion
    }
}
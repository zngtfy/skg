﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SKG.DAL.Models
{
    using Ext;
    using static ZEnum;

    /// <summary>
    /// Type model
    /// </summary>
    public class SysType : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysType()
        {
            Sequence = 0;
            Codes = new HashSet<SysCode>();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="type">Type of code</param>
        public SysType(TypeZ type)
        {
            var t = type.ToString();
            DevName = t;
            Name = t.ToAddSpace();
            Group = t.ToPrefix();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Group
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Sequence
        /// </summary>
        public ushort Sequence { get; set; }

        /// <summary>
        /// Codes navigation
        /// </summary>
        public virtual ICollection<SysCode> Codes { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-10 13:14
 * Update       : 2020-Sep-23 22:44
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.DAL.Models
{
    using static ZEnum;

    /// <summary>
    /// Interface Synchronization meta model
    /// </summary>
    public interface ISync
    {
        #region -- Properties --

        /// <summary>
        /// Synchronization status
        /// </summary>
        Sync? SyncStatus { get; set; }

        /// <summary>
        /// Synchronization message
        /// </summary>
        string SyncMessage { get; set; }

        /// <summary>
        /// Synced on
        /// </summary>
        DateTime? SyncedOn { get; set; }

        #endregion
    }
}
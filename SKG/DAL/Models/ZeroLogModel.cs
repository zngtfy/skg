﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2021-Aug-08 11:22
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.ComponentModel.DataAnnotations;

namespace SKG.DAL.Models
{
    using Dto;
    using static ZConst;
    using static ZEnum;

    /// <summary>
    /// Zero log model
    /// </summary>
    public abstract class ZeroLogModel : ZeroDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroLogModel() : base()
        {
            SetDefault();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        public ZeroLogModel(string n) : base(n)
        {
            SetDefault();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroLogModel(string n, string d) : base(n, d)
        {
            SetDefault();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroLogModel(uint i, string n, string d) : base(i, n, d)
        {
            SetDefault();
        }

        /// <summary>
        /// Set default data
        /// </summary>
        private void SetDefault()
        {
            var s = string.Empty;
            Status = Status.Enabled;
            CreatedBy = s;
            CreatedOn = DateTime.UtcNow;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Created by
        /// </summary>
        [StringLength(MaxSize.UserName)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Created on
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Matcha analytics ID (Matcha.Analysis)
        /// </summary>
        public uint? MaId { get; set; }

        #endregion
    }
}
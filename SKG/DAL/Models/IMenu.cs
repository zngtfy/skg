﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-13 20:54
 * Update       : 2020-Aug-13 20:54
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.DAL.Models
{
    /// <summary>
    /// Interface menu model
    /// </summary>
    public interface IMenu
    {
        #region -- Properties --

        /// <summary>
        /// Icon
        /// </summary>
        string Icon { get; set; }

        /// <summary>
        /// Controller name
        /// </summary>
        string Controller { get; set; }

        /// <summary>
        /// Action name
        /// </summary>
        string Action { get; set; }

        #endregion
    }
}
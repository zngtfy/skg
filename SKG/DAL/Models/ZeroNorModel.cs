﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Nov-06 08:28
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static SKG.ZEnum;

namespace SKG.DAL.Models
{
    using static ZConst;

    /// <summary>
    /// Zero normal model
    /// </summary>
    public class ZeroNorModel : ZeroLogModel, INorModel
    {
        #region -- Implements --

        /// <summary>
        /// Description
        /// </summary>
        [StringLength(MaxSize.Description)]
        public string Description { get; set; }

        /// <summary>
        /// Modified by
        /// </summary>
        [StringLength(MaxSize.UserName)]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Modified on
        /// </summary>
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// Expand data (JSON)
        /// </summary>
        public string ExpandData { get; set; }

        /// <summary>
        /// Behalf
        /// </summary>
        public string Behalf { get; set; }

        /// <summary>
        /// Synchronization UID
        /// </summary>
        public Guid? SyncUid { get; set; }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroNorModel() : base()
        {
            SetDefault();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        public ZeroNorModel(string n) : base(n) { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroNorModel(string n, string d) : base(n, d) { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroNorModel(uint i, string n, string d) : base(i, n, d)
        {
            SetDefault();
        }

        /// <summary>
        /// Set default data
        /// </summary>
        private void SetDefault()
        {
            Description = null;
            ModifiedBy = null;
            ModifiedOn = null;
            ExpandData = null;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Form mode
        /// </summary>
        [NotMapped]
        public FormMode? Mode { get; set; }

        /// <summary>
        /// Exclude unchanged columns
        /// </summary>
        [NotMapped]
        public string Skip { get; set; }

        /// <summary>
        /// If true return model else detail
        /// </summary>
        [NotMapped]
        public bool ReturnModel { get; set; }

        #endregion
    }
}
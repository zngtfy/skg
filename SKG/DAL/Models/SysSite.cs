﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKG.DAL.Models
{
    using Dto;
    using Ext;
    using static ZEnum;

    /// <summary>
    /// Site model
    /// </summary>
    public class SysSite : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysSite() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// UID
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Short name
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Base URL (production)
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Development URL
        /// </summary>
        public string DevUrl { get; set; }

        /// <summary>
        /// Staging URL
        /// </summary>
        public string StgUrl { get; set; }

        /// <summary>
        /// Tools URL
        /// </summary>
        public string ToolsUrl { get; set; }

        /// <summary>
        /// Special function
        /// </summary>
        public string Function { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Catergory
        /// </summary>
        public string Catergory { get; set; }

        /// <summary>
        /// Google code
        /// </summary>
        [StringLength(16)]
        public string GoogleCode { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Type plan
        /// </summary>
        public TypePlan TypePlan { get; set; }

        /// <summary>
        /// Expand data
        /// </summary>
        [NotMapped]
        public ExpandSiteDto Expand
        {
            get
            {
                return ExpandData.ToInst<ExpandSiteDto>();
            }
            set
            {
                if (value != null)
                {
                    ExpandData = value.ToJson();
                }
            }
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-14 13:11
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.ComponentModel.DataAnnotations.Schema;

namespace SKG.DAL.Models
{
    using Dto;
    using Ext;

    /// <summary>
    /// Profile model
    /// </summary>
    public class ZeroProfile : ZeroNorModel, IProfile
    {
        #region -- Implements --

        /// <summary>
        /// Privilege (store JSON string data)
        /// </summary>
        public string PrivilegeJson { get; set; }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroProfile() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">Name</param>
        public ZeroProfile(string name)
        {
            Name = name;
            DevName = name.ToPascalCase();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">ID</param>
        public ZeroProfile(string name, uint id) : this(name)
        {
            Id = id;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Privileges
        /// </summary>
        [NotMapped]
        public PrivilegeDto Privilege
        {
            get
            {
                return PrivilegeJson.ToInst<PrivilegeDto>();
            }
            set
            {
                if (value != null)
                {
                    PrivilegeJson = value.ToJson();
                }
            }
        }
        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-08 10:26
 * Update       : 2020-Aug-20 05:29
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using SKG.Dto;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKG.DAL.Models
{
    /// <summary>
    /// Zero site model
    /// </summary>
    public abstract class ZeroSiteModel : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroSiteModel() : base() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        public ZeroSiteModel(string n) : base(n) { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroSiteModel(string n, string d) : base(n, d) { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="o">Authority</param>
        public ZeroSiteModel(string n, AuthorityDto o) : base(n)
        {
            if (o != null)
            {
                SiteId = o.SiteId;
                CreatedBy = o.UserName;
            }
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        /// <param name="o">Authority</param>
        public ZeroSiteModel(string n, string d, AuthorityDto o) : base(n, d)
        {
            if (o != null)
            {
                SiteId = o.SiteId;
                CreatedBy = o.UserName;
            }
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        /// <param name="s">Site ID</param>
        public ZeroSiteModel(uint i, string n, string d, uint s) : base(i, n, d)
        {
            SiteId = s;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Site ID
        /// </summary>
        public uint SiteId { get; set; }

        /// <summary>
        /// Site
        /// </summary>
        [JsonIgnore, ForeignKey("SiteId")]
        public virtual SysSite Site { get; set; }

        #endregion
    }
}
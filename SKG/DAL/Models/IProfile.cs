﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-13 08:27
 * Update       : 2020-Aug-13 08:27
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.DAL.Models
{
    /// <summary>
    /// Interface profile model
    /// </summary>
    public interface IProfile
    {
        #region -- Properties --

        /// <summary>
        /// Privilege (store JSON string data)
        /// </summary>
        string PrivilegeJson { get; set; }

        #endregion
    }
}
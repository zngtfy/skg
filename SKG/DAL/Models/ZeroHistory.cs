﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using SKG.Ext;

namespace SKG.DAL.Models
{
    /// <summary>
    /// History common model
    /// </summary>
    public abstract class ZeroHistory : ZeroLogModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroHistory()
        {
            Old = string.Empty;
            New = string.Empty;
            Sequence = 0;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="tn">Table name</param>
        /// <param name="new">New data</param>
        /// <param name="old">Old data</param>
        public ZeroHistory(string tn, object @new, object old) : this()
        {
            Table = tn;
            New = @new.ToJson(true);
            Old = old.ToJson(true);
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Table name
        /// </summary>
        public string Table { get; set; }

        /// <summary>
        /// Old data JSON string
        /// </summary>
        public string Old { get; set; }

        /// <summary>
        /// New data JSON string
        /// </summary>
        public string New { get; set; }

        /// <summary>
        /// Sequence
        /// </summary>
        public ushort Sequence { get; set; }

        #endregion
    }
}
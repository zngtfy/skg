﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Sep-24 07:18
 * Update       : 2020-Sep-24 07:18
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.ComponentModel.DataAnnotations;

namespace SKG.DAL.Models
{
    /// <summary>
    /// Group model
    /// </summary>
    public class ZeroGroup : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroGroup() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Object
        /// </summary>
        public string Object { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.ComponentModel.DataAnnotations;

namespace SKG.DAL.Models
{
    using Ext;

    /// <summary>
    /// Tab model
    /// </summary>
    public class SysTab : ZeroNorModel, IMenu
    {
        #region -- Implements --

        /// <summary>
        /// Icon
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Controller name
        /// </summary>
        [StringLength(32)]
        public string Controller { get; set; }

        /// <summary>
        /// Action name
        /// </summary>
        [StringLength(32)]
        public string Action { get; set; }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysTab() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="dn">Developer name</param>
        public SysTab(string dn)
        {
            DevName = dn;
            Name = dn.ToAddSpace();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">ID</param>
        public SysTab(string name, uint id) : this(name)
        {
            Id = id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dn"></param>
        /// <param name="n">Object target (table, report or page name)</param>
        public SysTab(string dn, string n) : this(dn)
        {
            Object = n;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Is custom tab
        /// </summary>
        public bool Custom { get; set; }

        /// <summary>
        /// Object target - DevName of SysCode (table, report or page name)
        /// </summary>
        public string Object { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-08 10:26
 * Update       : 2020-Sep-23 22:44
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using SKG.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace SKG.DAL.Models
{
    using static ZEnum;

    /// <summary>
    /// Zero sync model
    /// </summary>
    public class ZeroSyncModel : ZeroSiteModel, ISync
    {
        #region -- Implements --

        /// <summary>
        /// Synchronization status
        /// </summary>
        public Sync? SyncStatus { get; set; }

        /// <summary>
        /// Synchronization message
        /// </summary>
        [StringLength(256)]
        public string SyncMessage { get; set; }

        /// <summary>
        /// Synced on
        /// </summary>
        public DateTime? SyncedOn { get; set; }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroSyncModel() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="o">Authority</param>
        public ZeroSyncModel(string n, AuthorityDto o) : base(n, o) { }

        /// <summary>
        /// Set site ID, copy Id to MsId and reset Id = 0
        /// </summary>
        /// <param name="siteId">Site ID</param>
        public void SetSiteMsId(uint siteId)
        {
            MsId = Id;
            Id = 0;
            SiteId = siteId;

            SyncStatus = Sync.Synced;
            SyncedOn = DateTime.UtcNow;
        }

        /// <summary>
        /// Set site ID, copy Id to MaId and reset Id = 0
        /// </summary>
        /// <param name="siteId">Site ID</param>
        public void SetSiteMaId(uint siteId)
        {
            MaId = Id;
            Id = 0;
            SiteId = siteId;

            SyncStatus = Sync.Synced;
            SyncedOn = DateTime.UtcNow;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Matcha site ID (Matcha.Home)
        /// </summary>
        public uint? MsId { get; set; }

        #endregion
    }
}
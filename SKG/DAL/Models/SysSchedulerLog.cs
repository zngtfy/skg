﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Oct-10 18:44
 * Update       : 2020-Oct-10 18:44
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.DAL.Models
{
    /// <summary>
    /// Scheduler log model
    /// </summary>
    public class SysSchedulerLog : ZeroNorModel
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SysSchedulerLog() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Type navigation
        /// </summary>
        public virtual SysScheduler Scheduler { get; set; }

        #endregion
    }
}
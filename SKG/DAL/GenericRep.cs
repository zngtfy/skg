﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2021-Jun-29 14:34
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SKG.DAL
{
    using Ext;
    using Models;
    using Rsp;
    using static ZConst;
    using static ZEnum;

    /// <summary>
    /// Generic repository
    /// </summary>
    /// <typeparam name="C">Database context class type</typeparam>
    /// <typeparam name="T">Model class type</typeparam>
    /// <typeparam name="TID">Model ID type</typeparam>
    public class GenericRep<C, T, TID> where T : class, new() where C : DbContext, new()
    {
        #region -- Implements --

        /// <summary>
        /// Create the model
        /// </summary>
        /// <param name="m">The model</param>
        protected void Create(T m)
        {
            // CreatedOn
            var t = m.GetPropertyValue(nameof(ZeroNorModel.CreatedBy));
            if (t == null)
            {
                m.SetPropertyValue(nameof(ZeroNorModel.CreatedBy), ZVariable.SystemAdmin);
            }
            m.SetPropertyValue(nameof(ZeroNorModel.CreatedOn), DateTime.UtcNow);

            // SyncUid
            t = m.GetPropertyValue(nameof(ZeroNorModel.SyncUid));
            if (t == null)
            {
                m.SetPropertyValue(nameof(ZeroNorModel.SyncUid), Guid.NewGuid());
            }

            _db.Set<T>().Add(m);
            _db.SaveChanges();
        }

        /// <summary>
        /// Create list model
        /// </summary>
        /// <param name="l">List model</param>
        protected void Create(List<T> l)
        {
            foreach (var i in l)
            {
                // CreatedOn
                var t = i.GetPropertyValue(nameof(ZeroNorModel.CreatedBy));
                if (t == null)
                {
                    i.SetPropertyValue(nameof(ZeroNorModel.CreatedBy), ZVariable.SystemAdmin);
                }
                i.SetPropertyValue(nameof(ZeroNorModel.CreatedOn), DateTime.UtcNow);

                // SyncUid
                t = i.GetPropertyValue(nameof(ZeroNorModel.SyncUid));
                if (t == null)
                {
                    i.SetPropertyValue(nameof(ZeroNorModel.SyncUid), Guid.NewGuid());
                }
            }

            _db.Set<T>().AddRange(l);
            _db.SaveChanges();
        }

        /// <summary>
        /// Read by
        /// </summary>
        /// <param name="p">Predicate</param>
        /// <returns>Return query data</returns>
        public IQueryable<T> Read(Expression<Func<T, bool>> p)
        {
            return _db.Set<T>().Where(p);
        }

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <returns>Return the object</returns>
        public virtual T Read(TID id)
        {
            var res = _db.Find<T>(id);
            return res;
        }

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="p">Predicate</param>
        /// <param name="status">Status</param>
        /// <returns>Return the object</returns>
        public virtual T Read(Expression<Func<T, bool>> p, Status? status)
        {
            return null;
        }

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="code">Secondary key</param>
        /// <param name="status">Status</param>
        /// <returns>Return the object</returns>
        public virtual T Read(string code, Status? status)
        {
            return null;
        }

        /// <summary>
        /// Update the model
        /// </summary>
        /// <param name="m">The model</param>
        protected void Update(T m)
        {
            // ModifiedOn
            var t = m.GetPropertyValue(nameof(ZeroNorModel.ModifiedBy));
            if (t == null)
            {
                m.SetPropertyValue(nameof(ZeroNorModel.ModifiedBy), ZVariable.SystemAdmin);
            }
            m.SetPropertyValue(nameof(ZeroNorModel.ModifiedOn), DateTime.UtcNow);

            // SyncStatus
            t = m.GetPropertyValue(nameof(ZeroSyncModel.SyncStatus));
            if (t != null)
            {
                var s = (Sync)t;
                if (s != Sync.Skip)
                {
                    m.SetPropertyValue(nameof(ZeroSyncModel.SyncStatus), null);
                }
            }

            _db.Set<T>().Update(m);
            _db.SaveChanges();
        }

        /// <summary>
        /// Update list model
        /// </summary>
        /// <param name="l">List model</param>
        protected void Update(List<T> l)
        {
            foreach (var i in l)
            {
                // ModifiedOn
                var t = i.GetPropertyValue(nameof(ZeroNorModel.ModifiedBy));
                if (t == null)
                {
                    i.SetPropertyValue(nameof(ZeroNorModel.ModifiedBy), ZVariable.SystemAdmin);
                }
                i.SetPropertyValue(nameof(ZeroNorModel.ModifiedOn), DateTime.UtcNow);

                // SyncStatus
                t = i.GetPropertyValue(nameof(ZeroSyncModel.SyncStatus));
                if (t != null)
                {
                    var s = (Sync)t;
                    if (s != Sync.Skip)
                    {
                        i.SetPropertyValue(nameof(ZeroSyncModel.SyncStatus), null);
                    }
                }
            }

            _db.Set<T>().UpdateRange(l);
            _db.SaveChanges();
        }

        /// <summary>
        /// Return query all data
        /// </summary>
        public IQueryable<T> All
        {
            get { return _db.Set<T>(); }
        }

        /// <summary>
        /// Return query all data (<see href="https://docs.microsoft.com/en-us/ef/core/querying/tracking">no tracking</see>)
        /// </summary>
        public IQueryable<T> AllNoTracking
        {
            get { return _db.Set<T>().AsNoTracking(); }
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public GenericRep()
        {
            _db = new C();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="db">The database context</param>
        public GenericRep(C db)
        {
            _db = db;
        }

        /// <summary>
        /// Update data with delegate
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <param name="a">Action delegate</param>
        /// <returns>Return the result</returns>
        public virtual SingleRsp UpdateRsp(TID id, Action<T> a)
        {
            return new SingleRsp();
        }

        /// <summary>
        /// Update the model
        /// </summary>
        /// <param name="old">The old model</param>
        /// <param name="new">The new model</param>
        protected object Update(T old, T @new)
        {
            _db.Entry(old).State = EntityState.Modified;
            var res = _db.Set<T>().Add(@new);
            return res;
        }

        /// <summary>
        /// Delete the model
        /// </summary>
        /// <param name="m">The model</param>
        /// <returns>Return the object</returns>
        protected T Delete(T m)
        {
            var t = _db.Set<T>().Remove(m);
            return t.Entity;
        }

        /// <summary>
        /// Make serial number
        /// </summary>
        /// <param name="p">Predicate</param>
        /// <param name="s">Selector</param>
        /// <param name="prefix">Prefix</param>
        /// <returns>Return the result</returns>
        protected string MakeNo(Expression<Func<T, bool>> p, Func<T, TID> s, string prefix)
        {
            // Prefix
            prefix = prefix.Trimz();
            if (string.IsNullOrEmpty(prefix))
            {
                prefix = "UN";
            }
            prefix += ZConst.String.Minus;

            // First
            var res = prefix + string.Format(Default.SerialFormat, 1);
            var m = _db.Set<T>().Where(p).OrderBy(s).LastOrDefault();
            if (m == null)
            {
                return res;
            }

            // Next
            var t = m.GetPropertyValue(nameof(ZeroNorModel.DevName));
            if (t != null)
            {
                var seq = t.ToNumber().ToIntU();
                res = prefix + string.Format(Default.SerialFormat, seq + 1);
            }

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// The database context
        /// </summary>
        protected C Db
        {
            get { return _db; }
            set { _db = value; }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// The database context
        /// </summary>
        private C _db;

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;

namespace SKG
{
    using Zefault = ZConst.Default;

    /// <summary>
    /// Encryption
    /// </summary>
    public class ZEncryption
    {
        /// <summary>
        /// Secret key
        /// </summary>
        public static string SecretKey = Zefault.Passphase;

        /// <summary>
        /// Encrypt string
        /// </summary>
        /// <param name="id">String value to encrypt</param>
        /// <returns>Return the result</returns>
        public static string UrlEncrypt(string id)
        {
            var aes = new ZAes(SecretKey);
            var tmp = aes.Encrypt(id);
            return tmp;
        }

        /// <summary>
        /// Encrypt integer
        /// </summary>
        /// <param name="Id">Integer value to encrypt</param>
        /// <returns>Return the result</returns>
        public static string UrlEncrypt(int Id)
        {
            var res = UrlEncrypt(Id.ToString());
            return res;
        }

        /// <summary>
        /// Encrypt a key value pair
        /// </summary>
        /// <param name="param">Key value pair to encrypt</param>
        /// <returns>Return the result</returns>
        public static string UrlEncrypt(string[,] param)
        {
            var aes = new ZAes(SecretKey);
            var res = aes.EncryptKeyValue(param);
            return res;
        }

        /// <summary>
        /// Decrypt URL to string value
        /// </summary>
        /// <param name="encrypted">Encrypted URL</param>
        /// <param name="isDecode">Use HttpUtility.UrlDecode</param>
        /// <returns>Return the string ID</returns>
        public static string UrlDecrypt(string encrypted, bool isDecode = false)
        {
            var res = string.Empty;
            try
            {
                var aes = new ZAes(SecretKey);
                res = aes.Decrypt(encrypted, isDecode);
            }
            catch (Exception ex)
            {
                var x = ex.StackTrace;
            }
            return res;
        }

        /// <summary>
        /// Decrypt key value pair URL
        /// </summary>
        /// <param name="encrypted">Encrypted URL</param>
        /// <returns>Return the result</returns>
        public static string[,] UrlDecryptKeyValue(string encrypted)
        {
            var aes = new ZAes(SecretKey);
            var tmp = HttpUtility.UrlDecode(encrypted);
            var res = aes.DecryptKeyValue(tmp);
            return res;
        }

        /// <summary>
        /// Decrypt key value pair URL
        /// </summary>
        /// <param name="s">Encrypted URL</param>
        /// <returns>Return the result</returns>
        public static Dictionary<string, object> UrlDecryptDictionary(string s)
        {
            var res = new Dictionary<string, object>();

            if (s != null)
            {
                var arr = UrlDecryptKeyValue(s);
                for (var i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    string key1 = arr[i, 0];
                    string val1 = arr[i, 1];
                    res.Add(key1, val1);
                }
            }

            return res;
        }
    }
}
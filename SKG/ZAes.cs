﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SKG
{
    using Zefault = ZConst.Default;

    /// <summary>
    /// AES encryption
    /// </summary>
    public class ZAes
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="key">Passphase</param>
        public ZAes(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                key = Zefault.Passphase;
            }

            _passphase = key;
        }

        /// <summary>
        /// Encrypt key value
        /// </summary>
        /// <param name="arParams"></param>
        /// <returns></returns>
        public string EncryptKeyValue(string[,] arParams)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < arParams.Length / 2; i++)
            {
                if (i > 0) sb.Append('&');
                sb.Append(arParams[i, 0]);
                sb.Append('=');
                sb.Append(arParams[i, 1]);
            }

            var tmp = EncryptString(sb.ToString());
            var res = HttpUtility.UrlEncode(tmp);

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encrypted"></param>
        /// <returns></returns>
        public string[,] DecryptKeyValue(string encrypted)
        {
            //encrypted = HttpUtility.UrlDecode(encrypted);

            if (encrypted != null)
            {
                var allValues = DecryptString(encrypted);
                var nameValuePairs = allValues.Split('&');
                var oriKeyValue = new string[nameValuePairs.Length, 2];

                for (int i = 0; i < nameValuePairs.Length; i++)
                {
                    var nameValue = nameValuePairs[i].Split('=');
                    if (nameValue.Length == 2)
                    {
                        oriKeyValue[i, 0] = nameValue[0];
                        oriKeyValue[i, 1] = nameValue[1];
                    }
                }
                return oriKeyValue;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string Encrypt(string text)
        {
            var tmp = EncryptString(text);
            var res = HttpUtility.UrlEncode(tmp);
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Encrypt(int id)
        {
            return Encrypt(id.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encrypted"></param>
        /// <param name="isDecode"></param>
        /// <returns></returns>
        public string Decrypt(string encrypted, bool isDecode)
        {
            if (isDecode)
            {
                encrypted = HttpUtility.UrlDecode(encrypted);
            }

            if (encrypted != null)
            {
                return DecryptString(encrypted);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string EncryptString(string text)
        {
            byte[] key, iv;
            var salt = new byte[8];
            var rng = new RNGCryptoServiceProvider();

            rng.GetNonZeroBytes(salt);
            DeriveKeyAndIV(_passphase, salt, out key, out iv);

            // Encrypt bytes
            var encryptedBytes = EncryptStringToBytesAes(text, key, iv);

            // Add salt as first 8 bytes
            var encryptedBytesWithSalt = new byte[salt.Length + encryptedBytes.Length + 8];

            Buffer.BlockCopy(Encoding.ASCII.GetBytes("Salted__"), 0, encryptedBytesWithSalt, 0, 8);
            Buffer.BlockCopy(salt, 0, encryptedBytesWithSalt, 8, salt.Length);
            Buffer.BlockCopy(encryptedBytes, 0, encryptedBytesWithSalt, salt.Length + 8, encryptedBytes.Length);

            // Base64 encode
            var res = Convert.ToBase64String(encryptedBytesWithSalt);
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encryptedString"></param>
        /// <returns></returns>
        private string DecryptString(string encryptedString)
        {
            // Base 64 decode
            var encryptedBytesWithSalt = Convert.FromBase64String(encryptedString);

            // Extract salt (first 8 bytes of encrypted)
            var saltPadding = new byte[8];
            var salt = new byte[8];
            var encryptedBytes = new byte[encryptedBytesWithSalt.Length - salt.Length - saltPadding.Length];

            Buffer.BlockCopy(encryptedBytesWithSalt, 0, saltPadding, 0, saltPadding.Length);
            Buffer.BlockCopy(encryptedBytesWithSalt, saltPadding.Length, salt, 0, salt.Length);
            Buffer.BlockCopy(encryptedBytesWithSalt, saltPadding.Length + salt.Length, encryptedBytes, 0, encryptedBytes.Length);
            // get key and iv

            byte[] key, iv;
            DeriveKeyAndIV(_passphase, salt, out key, out iv);

            var res = DecryptStringFromBytesAes(encryptedBytes, key, iv);
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="passphrase"></param>
        /// <param name="salt"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        private void DeriveKeyAndIV(string passphrase, byte[] salt, out byte[] key, out byte[] iv)
        {
            // Generate key and iv
            var concatenatedHashes = new List<byte>(48);

            var password = Encoding.UTF8.GetBytes(passphrase);
            var currentHash = new byte[0];
            var md5 = MD5.Create();
            var enoughBytesForKey = false;

            // See http://www.openssl.org/docs/crypto/EVP_BytesToKey.html#KEY_DERIVATION_ALGORITHM

            while (!enoughBytesForKey)
            {
                var preHashLength = currentHash.Length + password.Length + salt.Length;
                var preHash = new byte[preHashLength];

                Buffer.BlockCopy(currentHash, 0, preHash, 0, currentHash.Length);
                Buffer.BlockCopy(password, 0, preHash, currentHash.Length, password.Length);
                Buffer.BlockCopy(salt, 0, preHash, currentHash.Length + password.Length, salt.Length);

                currentHash = md5.ComputeHash(preHash);
                concatenatedHashes.AddRange(currentHash);

                if (concatenatedHashes.Count >= 48)
                {
                    enoughBytesForKey = true;
                }
            }

            key = new byte[32];
            iv = new byte[16];
            concatenatedHashes.CopyTo(0, key, 0, 32);
            concatenatedHashes.CopyTo(32, iv, 0, 16);

            md5.Clear();
            md5 = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        private byte[] EncryptStringToBytesAes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("iv");
            }

            // Declare the stream used to encrypt to an in memory
            // array of bytes.
            MemoryStream msEncrypt;

            // Declare the RijndaelManaged object
            // used to encrypt the data.
            RijndaelManaged aesAlg = null;

            try
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged
                {
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 128,
                    Key = key,
                    IV = iv
                };

                // Create an encryptor to perform the stream transform.
                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                msEncrypt = new MemoryStream();
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (var swEncrypt = new StreamWriter(csEncrypt))
                    {
                        // Write all data to the stream.
                        swEncrypt.Write(plainText);
                        swEncrypt.Flush();
                        swEncrypt.Close();
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                {
                    aesAlg.Clear();
                }
            }

            // Return the encrypted bytes from the memory stream.
            return msEncrypt.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        private string DecryptStringFromBytesAes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("iv");
            }

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext;

            try
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged
                {
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 128,
                    Key = key,
                    IV = iv
                };

                // Create a decrytor to perform the stream transform.
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                            srDecrypt.Close();
                        }
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                {
                    aesAlg.Clear();
                }
            }

            return plaintext;
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Passphase
        /// </summary>
        private string _passphase;

        #endregion
    }
}
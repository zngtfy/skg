﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Jun-04 09:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using SKG.Ext;

namespace SKG.Rsp
{
    /// <summary>
    /// Base response
    /// </summary>
    public class BaseRsp
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public BaseRsp()
        {
            Success = true;
            Message = string.Empty;
            Icon = "success";
            Title = "Success";
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="message">Success message</param>
        public BaseRsp(string message) : this()
        {
            Message = message;
        }

        /// <summary>
        /// Set error
        /// </summary>
        /// <param name="message">Error message</param>
        /// <param name="log">Allow to write log</param>
        public void SetError(string message, bool log = true)
        {
            Success = false;
            Message = message;

            if (log)
            {
                var msg = $"{Message} {Code}";
                msg.LogError();
            }

            Icon = "error";
            Title = "Failure";
        }

        /// <summary>
        /// Set error
        /// </summary>
        /// <param name="message">Error message</param>
        /// <param name="code">Error code</param>
        /// <param name="log">Allow to write log</param>
        public void SetError(string message, string code, bool log = true)
        {
            SetError(message);
            Code = code;

            if (log)
            {
                var msg = $"{Message} {Code}";
                msg.LogError();
            }
        }

        /// <summary>
        /// Set message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="code">Code</param>
        /// <param name="log">Allow to write log</param>
        public void SetMessage(string message, string code, bool log = true)
        {
            Message = message;
            Code = code;

            if (log)
            {
                var msg = $"{Message} {Code}";
                msg.LogInfor();
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Success
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Success or error code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Icon
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Return URL
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Request
        /// </summary>
        public object Request { get; set; }

        #endregion
    }
}
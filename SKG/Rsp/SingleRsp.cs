﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Jun-04 09:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Rsp
{
    /// <summary>
    /// Single response
    /// </summary>
    public class SingleRsp : BaseRsp
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SingleRsp() : base() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="req">Request</param>
        public SingleRsp(object data, object req = null)
        {
            Data = data;
            Request = req;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="message">Success message</param>
        public SingleRsp(string message) : base(message) { }

        /// <summary>
        /// Set data
        /// </summary>
        /// <param name="code">Success code</param>
        /// <param name="data">Data</param>
        public void SetData(string code, object data)
        {
            Code = code;
            Data = data;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Data
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Error, I like 'Eror'
        /// </summary>
        public object Eror { get; set; }

        #endregion
    }
}
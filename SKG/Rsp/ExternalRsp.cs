﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-Aug-26 12:18
 * Update       : 2021-Aug-26 12:18
 * Checklist    : 1.1
 * Status       : New
 */
#endregion


namespace SKG.Rsp
{
    /// <summary>
    /// External response
    /// </summary>
    public class ExternalRsp
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="req">Request</param>
        public ExternalRsp(object req)
        {
            Request = req;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Response
        /// </summary>
        public object Response { get; set; }

        /// <summary>
        /// Request
        /// </summary>
        public object Request { get; set; }

        #endregion
    }
}
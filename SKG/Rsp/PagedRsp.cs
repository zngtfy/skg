﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-04 06:09
 * Update       : 2020-Aug-04 06:09
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;

namespace SKG.Rsp
{
    /// <summary>
    /// Paged response
    /// </summary>
    /// <typeparam name="T">Model class type</typeparam>
    public class PagedRsp<T>
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="count">Total count</param>
        /// <param name="items">Items</param>
        public PagedRsp(int count, IReadOnlyList<T> items)
        {
            TotalCount = count;
            Items = items;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="count">Total count</param>
        /// <param name="items">Items</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="pageNum">Page number</param>
        public PagedRsp(int count, IReadOnlyList<T> items, int pageSize, int? pageNum) : this(count, items)
        {
            PageSize = pageSize;
            PageNum = pageNum ?? 1;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Total count
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int PageNum { get; private set; }

        /// <summary>
        /// Items
        /// </summary>
        public IReadOnlyList<T> Items { get; set; }

        #endregion
    }
}
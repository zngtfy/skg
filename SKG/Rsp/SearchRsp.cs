﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Jun-04 09:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Rsp
{
    using Req;

    /// <summary>
    /// Search response
    /// </summary>
    public sealed class SearchRsp : SingleRsp
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="req">Request</param>
        public SearchRsp(PagingReq req) : base()
        {
            PageNum = req.PageNum;
            PageSize = req.PageSize;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Total records
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int PageNum { get; private set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// Total pages
        /// </summary>
        public int TotalPages
        {
            get
            {
                var t = (double)TotalRecords / PageSize;
                var res = (int)Math.Ceiling(t);
                return res;
            }
        }

        /// <summary>
        /// Paging size information
        /// </summary>
        public string PagingSizeInfo
        {
            get
            {
                var fr = (PageNum - 1) * PageSize + 1;
                var to = PageSize * PageNum;

                if (to > TotalRecords)
                {
                    to = TotalRecords;
                }

                var res = $"Displaying {fr} - {to} of {TotalRecords}";
                return res;
            }
        }

        #endregion
    }
}
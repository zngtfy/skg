﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Jun-04 09:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;

namespace SKG.Rsp
{
    /// <summary>
    /// Multiple response
    /// </summary>
    public class MultipleRsp : BaseRsp
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public MultipleRsp() : base() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="req">Request</param>
        public MultipleRsp(object req)
        {
            Request = req;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="req">Request</param>
        public MultipleRsp(Dictionary<string, object> data, object req = null)
        {
            Data = data;
            Request = req;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="message">Message</param>
        public MultipleRsp(string message) : base(message) { }

        /// <summary>
        /// Set data
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="o">Data</param>
        public void SetData(string key, object o)
        {
            if (Data == null)
            {
                Data = new Dictionary<string, object>();
            }

            Data.Add(key, o);
        }

        /// <summary>
        /// Set success data
        /// </summary>
        /// <param name="o">Data</param>
        /// <param name="message">Message</param>
        /// <param name="code">Success code</param>
        public void SetSuccess(object o, string message, string code)
        {
            var t = new Dto(o, message, code);
            SetData("success", t);
        }

        /// <summary>
        /// Set failure data
        /// </summary>
        /// <param name="o">Data</param>
        /// <param name="message">Message</param>
        /// <param name="code">Error code</param>
        public void SetFailure(object o, string message, string code)
        {
            var t = new Dto(o, message, code);
            SetData("failure", t);
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Data
        /// </summary>
        public Dictionary<string, object> Data { get; private set; }

        #endregion

        #region -- Classes --

        /// <summary>
        /// Data transfer object
        /// </summary>
        private class Dto
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="data">Data</param>
            /// <param name="message">Message</param>
            public Dto(object data, string message)
            {
                Data = data;
                Message = message;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="data">Data</param>
            /// <param name="message">Message</param>
            /// <param name="code">Success or error code</param>
            public Dto(object data, string message, string code) : this(data, message)
            {
                Code = code;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Data
            /// </summary>
            public object Data { get; private set; }

            /// <summary>
            /// Success or error code
            /// </summary>
            public string Code { get; private set; }

            /// <summary>
            /// Message
            /// </summary>
            public string Message { get; private set; }

            #endregion
        }

        #endregion
    }
}
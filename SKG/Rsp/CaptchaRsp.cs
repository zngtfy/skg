﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Nov-10 16:28
 * Update       : 2020-Nov-10 16:28
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;

namespace SKG.Rsp
{
    /// <summary>
    /// Base response
    /// </summary>
    public class CaptchaRsp
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public CaptchaRsp() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Success
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Challenge time
        /// </summary>
        [JsonProperty("challenge_ts")]
        public DateTime ChallengeTime { get; set; }

        /// <summary>
        /// Host name
        /// </summary>
        [JsonProperty("hostname")]
        public string HostName { get; set; }

        /// <summary>
        /// Score
        /// </summary>
        public float Score { get; set; }

        /// <summary>
        /// Error codes
        /// </summary>
        [JsonProperty("error-codes")]
        public string[] ErrorCodes { get; set; }

        #endregion
    }
}
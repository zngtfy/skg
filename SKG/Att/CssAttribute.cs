﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-24 10:20
 * Update       : 2020-Jun-24 10:20
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Att
{
    /// <summary>
    /// Cascading style sheets attribute
    /// </summary>
    public class CssAttribute : Attribute
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">Icon name</param>
        /// <param name="class">Css class name</param>
        /// <param name="color">Hexa color</param>
        public CssAttribute(string name, string @class, string color = DefaultColor)
        {
            Name = name;
            Class = @class;
            Color = color;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Class
        /// </summary>
        public string Class { get; private set; }

        /// <summary>
        /// Color
        /// </summary>
        public string Color { get; set; }

        #endregion

        #region -- Constants --

        /// <summary>
        /// Default color
        /// </summary>
        private const string DefaultColor = "#000000";

        #endregion
    }
}
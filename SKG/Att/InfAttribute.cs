﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-24 10:20
 * Update       : 2020-Jun-24 10:20
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Att
{
    /// <summary>
    /// Information attribute
    /// </summary>
    public class InfAttribute : Attribute
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">Icon name</param>
        /// <param name="text">Css class name</param>
        /// <param name="value">Hexa color</param>
        public InfAttribute(string name, string text, string value = ZConst.String.Blank)
        {
            Name = name;
            Text = text;
            Value = value;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        #endregion
    }
}
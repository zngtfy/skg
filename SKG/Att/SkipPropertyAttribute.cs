﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-Oct-10 22:40
 * Update       : 2021-Oct-10 22:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Attributes
{
    /// <summary>
    /// Skip property attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SkipPropertyAttribute : Attribute
    {
    }
}
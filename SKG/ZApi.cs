﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-14 13:11
 * Update       : 2021-May-11 18:58
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SKG
{
    using DAL.Models;
    using Dto;
    using Ext;
    using Rsp;

    /// <summary>
    /// Call API
    /// </summary>
    public class ZApi
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZApi() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="s">Setting</param>
        public ZApi(ZSetting s)
        {
            if (s != null)
            {
                _site = s.Site;
                _authSalesforce = s.Salesforce;
                _authRexsoftware = s.Rexsoftware;
                _authAgentbox = s.Agentbox;
                _authMailchimp = s.Mailchimp;
            }
            SetHash(_site);
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="site">Site setting</param>
        public ZApi(SiteDto site)
        {
            _site = site;
            SetHash(_site);
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="m">Model</param>
        public ZApi(SysAuth m)
        {
            if (m != null)
            {
                var site = new SiteDto
                {
                    SiteUid = m.SiteUid.ToString(),
                    ApiUrl = m.ApiUrl,
                    ApiKey = m.ApiKey,
                    ApiHash = m.ApiHash
                };
                _site = site;
                SetHash(_site);
            }
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="o">Expand auth</param>
        public ZApi(ExpandAuthDto o)
        {
            if (o != null)
            {
                _authSalesforce = o.Salesforce;
                _authRexsoftware = o.Rexsoftware;
                _authAgentbox = o.Agentbox;
                _authMailchimp = o.Mailchimp;
            }
            _h = new ZHash(string.Empty);
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="salesforce">Salesforce setting</param>
        /// <param name="rexsoftware">Rexsoftware setting</param>
        /// <param name="agentbox">Agentbox setting</param>
        public ZApi(AuthenticationDto salesforce, AuthenticationDto rexsoftware, AuthenticationDto agentbox)
        {
            _authSalesforce = salesforce;
            _authRexsoftware = rexsoftware;
            _authAgentbox = agentbox;
            _h = new ZHash(string.Empty);
        }

        /// <summary>
        /// Clear token
        /// </summary>
        public static void ClearToken()
        {
            _eAuth = null;
            _project = null;
            _sToken = null;
            _rToken = null;
        }

        #region -- Salesforce --
        /// <summary>
        /// Post Salesforce async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="data">Data</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> PostSfxAsync(string path, object data)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToSfx)
            {
                res.SetError("Sfx sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_eAuth.HasPropertybase && (_authSalesforce == null || !_authSalesforce.HasPropertybase))
            {
                _authSalesforce = _eAuth.Salesforce;
            }

            if (_authSalesforce == null)
            {
                msg = "Authentication is not config";
                res.SetError(msg);
                return res;
            }
            if (string.IsNullOrWhiteSpace(_authSalesforce.Host))
            {
                msg = "Host is null or white space";
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = string.IsNullOrEmpty(_authSalesforce.Host) ? path : $"{_authSalesforce.Host}/{path}"; uri.LogInfor();
            var client = CreateClient();

            try
            {
                var content = CreateData(data);
                var rsp = await client.PostAsync(uri, content);
                if (rsp.IsSuccessStatusCode)
                {
                    res.Data = await rsp.Content.ReadAsStringAsync();
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }

        /// <summary>
        /// Post Salesforce async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="jsonData">JSON data</param>
        /// <param name="apex">Prefix APEX REST</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> PostSfxAsync(string path, string jsonData, string apex = ApexRest)
        {
            var res = new SingleRsp();

            if (ZSetting.DisableSyncToSfx)
            {
                res.SetError("Sfx sync is disabled", ZCode.EZ995);
                return res;
            }

        Begin:
            var token = SfxToken;
            if (token == null)
            {
                res.SetError("Token is null");
                return res;
            }
            if (string.IsNullOrWhiteSpace(token.AccessToken))
            {
                res.SetError("Salesforce 401 unauthorized", ZCode.EZ996);
                return res;
            }

            try
            {
                var url = token.InstanceUrl + $"/services/{apex}/{path}"; url.LogInfor();
                var req = new { jsonData };
                var data = CreateData(req);
                var client = CreateClient(token);

                var rsp = await client.PostAsync(url, data);
                res = await ReadContentSfx(rsp);
                if (res.Data.Value("TokenExpired").ToBool())
                {
                    ">>> Go begin <<<".LogInfor();
                    _sToken = null;
                    goto Begin;
                }
            }
            catch (Exception ex)
            {
                res.SetError(ex.Message);
            }

            return res;
        }

        /// <summary>
        /// Get Salesforce async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="apex">Prefix APEX REST</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> GetSfxAsync(string path, string apex = ApexRest)
        {
            var res = new SingleRsp();

            if (ZSetting.DisableSyncToSfx)
            {
                res.SetError("Salesforce sync is disabled", ZCode.EZ995);
                return res;
            }

        Begin:
            var token = SfxToken;
            if (token == null)
            {
                res.SetError("Token is null");
                return res;
            }
            if (string.IsNullOrWhiteSpace(token.AccessToken))
            {
                res.SetError("Salesforce 401 unauthorized", ZCode.EZ996);
                return res;
            }

            try
            {
                var uri = token.InstanceUrl + $"/services/{apex}/{path}"; uri.LogInfor();
                var client = CreateClient(token);

                var rsp = await client.GetAsync(uri);
                res = await ReadContentSfx(rsp);
                if (res.Data.Value("TokenExpired").ToBool())
                {
                    ">>> Go begin <<<".LogInfor();
                    _sToken = null;
                    goto Begin;
                }
            }
            catch (Exception ex)
            {
                res.SetError(ex.Message);
            }

            return res;
        }

        /// <summary>
        /// Update Salesforce async
        /// </summary>
        /// <param name="objectName">Object name</param>
        /// <param name="objectId">Object ID</param>
        /// <param name="req">Request data</param>
        /// <param name="isCreate">Is create</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> UpdateSfxAsync(string objectName, string objectId, object req, bool isCreate)
        {
            var res = new SingleRsp();

            if (ZSetting.DisableSyncToSfx)
            {
                res.SetError("Salesforce sync is disabled", ZCode.EZ995);
                return res;
            }

        Begin:
            var token = SfxToken;
            if (token == null)
            {
                res.SetError("Token is null");
                return res;
            }
            if (string.IsNullOrWhiteSpace(token.AccessToken))
            {
                res.SetError("Sfx 401 unauthorized", ZCode.EZ996);
                return res;
            }

            HttpResponseMessage rsp;
            try
            {
                var data = CreateData(req);
                var client = CreateClient(token);

                if (isCreate)
                {
                    var url = token.ApexUpdateUrl(objectName); url.LogInfor();
                    rsp = await client.PostAsync(url, data);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(objectId))
                    {
                        res.SetError("ObjectId is null or white space", ZCode.EZ103);
                        return res;
                    }

                    var url = token.ApexUpdateUrl(objectName) + objectId; url.LogInfor();
                    rsp = await client.PatchAsync(url, data);
                }
            }
            catch (Exception ex)
            {
                res.SetError(ex.Message);
                return res;
            }

            res = await ReadContentSfx(rsp);
            if (res.Data.Value("TokenExpired").ToBool())
            {
                ">>> Go begin <<<".LogInfor();
                _sToken = null;
                goto Begin;
            }

            return res;
        }

        /// <summary>
        /// Get total Salesforce async
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>Return the result</returns>
        public async Task<uint> GetTotalSfxAsync(string path)
        {
            var res = 0u;

            var uri = (path + "").Replace("paging=true", "paging=false");
            var rsp = await GetSfxAsync(uri);
            if (rsp.Success)
            {
                res = rsp.Data.Value("totalCount").ToIntU();
            }

            return res;
        }
        #endregion

        #region -- Rexsoftware --
        /// <summary>
        /// Post Rexsoftware async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="req">Request data</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> PostRexAsync(string path, object req)
        {
            var res = new SingleRsp();

            if (ZSetting.DisableSyncToRex)
            {
                res.SetError("Rexsoftware sync is disabled", ZCode.EZ995);
                return res;
            }

        Begin:
            var token = RexToken;
            if (token == null)
            {
                res.SetError("Token is null");
                return res;
            }

            try
            {
                var url = _authRexsoftware.Host + $"/{path}"; url.LogInfor();
                var data = CreateData(req);
                var client = CreateClient(token);

                var rsp = await client.PostAsync(url, data);
                res = await ReadContentRex(rsp);
                if (res.Data.Value("TokenExpired").ToBool())
                {
                    ">>> Go begin <<<".LogInfor();
                    _rToken = null;
                    goto Begin;
                }
            }
            catch (Exception ex)
            {
                res.SetError(ex.Message);
            }

            return res;
        }

        /// <summary>
        /// Update Rexsoftware async
        /// </summary>
        /// <param name="objectName">Object name</param>
        /// <param name="req">Request data</param>
        /// <param name="isCreate">Is create</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> UpdateRexAsync(string objectName, object req, bool isCreate)
        {
            var res = new SingleRsp();

            if (ZSetting.DisableSyncToRex)
            {
                res.SetError("Rexsoftware sync is disabled", ZCode.EZ995);
                return res;
            }

        Begin:
            var token = RexToken;
            if (token == null)
            {
                res.SetError("Token is null");
                return res;
            }

            try
            {
                var uri = objectName + "/" + (isCreate ? "create" : "update");
                var url = _authRexsoftware.Host + $"/{uri}"; url.LogInfor();
                var data = CreateData(req);
                var client = CreateClient(token);

                var rsp = await client.PostAsync(url, data);
                res = await ReadContentRex(rsp);
                if (res.Data.Value("TokenExpired").ToBool())
                {
                    ">>> Go begin <<<".LogInfor();
                    _rToken = null;
                    goto Begin;
                }
            }
            catch (Exception ex)
            {
                res.SetError(ex.Message);
            }

            return res;
        }

        /// <summary>
        /// Get IDs Rexsoftware async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="criteria">Criteria</param>
        /// <param name="page">Page</param>
        /// <returns>Return the result</returns>
        public async Task<List<string>> GetIdsRexAsync(string path, object criteria, uint page)
        {
            var res = new List<string>();

            var req = new
            {
                criteria = criteria,
                offset = page * MaxLimit,
                limit = MaxLimit,
                ids_only = true
            };

            var rsp = await PostRexAsync(path, req);
            if (rsp.Success)
            {
                res = rsp.Data.Value("rows").ToStr().ToInst<List<string>>();
            }

            return res;
        }

        /// <summary>
        /// Get total Rexsoftware async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="criteria">Criteria</param>
        /// <returns>Return the result</returns>
        public async Task<uint> GetTotalRexAsync(string path, object criteria)
        {
            var res = 0u;

            var req = new
            {
                criteria = criteria,
                ids_only = true
            };

            var rsp = await PostRexAsync(path, req);
            if (rsp.Success)
            {
                res = rsp.Data.Value("total").ToIntU();
            }

            return res;
        }
        #endregion

        #region -- Agentbox --
        /// <summary>
        /// Post Agentbox async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="data">Data</param>
        /// <param name="isStream">Is stream</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> PostAgbAsync(string path, object data, bool isStream = false)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToAgb)
            {
                res.SetError("Agb sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_eAuth.HasAgentbox && (_authAgentbox == null || !_authAgentbox.HasAgentbox))
            {
                _authAgentbox = _eAuth.Agentbox;
            }

            if (_authAgentbox == null)
            {
                msg = "Authentication is not config";
                res.SetError(msg);
                return res;
            }
            if (string.IsNullOrWhiteSpace(_authAgentbox.Host))
            {
                msg = "Host is null or white space";
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = $"{_authAgentbox.Host}/{path}"; uri.LogInfor();
            var client = CreateClient(_authAgentbox);

            try
            {
                var content = CreateData(data);
                var rsp = await client.PostAsync(uri, content);
                if (rsp.IsSuccessStatusCode)
                {
                    if (isStream)
                    {
                        res.Data = await rsp.Content.ReadAsStreamAsync();
                    }
                    else
                    {
                        res.Data = await rsp.Content.ReadAsStringAsync();
                    }
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }

        /// <summary>
        /// Get Agentbox async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="isStream">Is stream</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> GetAgbAsync(string path, bool isStream = false)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToAgb)
            {
                res.SetError("Agb sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_eAuth.HasAgentbox && (_authAgentbox == null || !_authAgentbox.HasAgentbox))
            {
                _authAgentbox = _eAuth.Agentbox;
            }

            if (_authAgentbox == null)
            {
                msg = "Authentication is not config";
                res.SetError(msg);
                return res;
            }
            if (string.IsNullOrWhiteSpace(_authAgentbox.Host))
            {
                msg = "Host is null or white space";
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = $"{_authAgentbox.Host}/{path}"; uri.LogInfor();
            var client = CreateClient(_authAgentbox);

            try
            {
                var rsp = await client.GetAsync(uri);
                if (rsp.IsSuccessStatusCode)
                {
                    if (isStream)
                    {
                        res.Data = await rsp.Content.ReadAsStreamAsync();
                    }
                    else
                    {
                        res.Data = await rsp.Content.ReadAsStringAsync();
                    }
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }
        #endregion

        #region -- Mailchimp --
        /// <summary>
        /// Post Mailchimp async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="data">Data</param>
        /// <param name="isStream">Is stream</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> PostMchAsync(string path, object data, bool isStream = false)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToMch)
            {
                res.SetError("Mch sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_eAuth.HasMailchimp && (_authMailchimp == null || !_authMailchimp.HasMailchimp))
            {
                _authMailchimp = _eAuth.Mailchimp;
            }

            if (_authMailchimp == null)
            {
                msg = "Authentication is not config";
                res.SetError(msg);
                return res;
            }
            if (string.IsNullOrWhiteSpace(_authMailchimp.Host))
            {
                msg = "Host is null or white space";
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = $"{_authMailchimp.Host}/{path}"; uri.LogInfor();
            var client = CreateClient(_authMailchimp);

            try
            {
                var content = CreateData(data);
                var rsp = await client.PostAsync(uri, content);
                if (rsp.IsSuccessStatusCode)
                {
                    if (isStream)
                    {
                        res.Data = await rsp.Content.ReadAsStreamAsync();
                    }
                    else
                    {
                        res.Data = await rsp.Content.ReadAsStringAsync();
                    }
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }

        /// <summary>
        /// Get Mailchimp async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="isStream">Is stream</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> GetMchAsync(string path, bool isStream = false)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToMch)
            {
                res.SetError("Mch sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_eAuth.HasMailchimp && (_authMailchimp == null || !_authMailchimp.HasMailchimp))
            {
                _authMailchimp = _eAuth.Mailchimp;
            }

            if (_authMailchimp == null)
            {
                msg = "Authentication is not config";
                res.SetError(msg);
                return res;
            }
            if (string.IsNullOrWhiteSpace(_authMailchimp.Host))
            {
                msg = "Host is null or white space";
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = $"{_authMailchimp.Host}/{path}"; uri.LogInfor();
            var client = CreateClient(_authMailchimp);

            try
            {
                var rsp = await client.GetAsync(uri);
                if (rsp.IsSuccessStatusCode)
                {
                    if (isStream)
                    {
                        res.Data = await rsp.Content.ReadAsStreamAsync();
                    }
                    else
                    {
                        res.Data = await rsp.Content.ReadAsStringAsync();
                    }
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }

        /// <summary>
        /// Update Mailchimp async
        /// </summary>
        /// <param name="objectName">Object name</param>
        /// <param name="objectId">Object ID</param>
        /// <param name="req">Request data</param>
        /// <param name="isCreate">Is create</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> UpdateMchAsync(string objectName, string objectId, object req, bool isCreate)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToMch)
            {
                res.SetError("Mch sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_eAuth.HasMailchimp && (_authMailchimp == null || !_authMailchimp.HasMailchimp))
            {
                _authMailchimp = _eAuth.Mailchimp;
            }

            if (_authMailchimp == null)
            {
                msg = "Authentication is not config";
                res.SetError(msg);
                return res;
            }
            if (string.IsNullOrWhiteSpace(_authMailchimp.Host))
            {
                msg = "Host is null or white space";
                res.SetError(msg);
                return res;
            }

            HttpResponseMessage rsp;
            try
            {
                // Call API
                var xx = (isCreate ? "" : $"/{objectId}");
                var uri = $"{_authMailchimp.Host}/{objectName}{xx}"; uri.LogInfor();
                var data = CreateData(req);
                var client = CreateClient(_authMailchimp);

                if (isCreate)
                {
                    rsp = await client.PostAsync(uri, data);
                }
                else
                {
                    rsp = await client.PatchAsync(uri, data);
                }
            }
            catch (Exception ex)
            {
                res.SetError(ex.Message);
                return res;
            }

            return res;
        }
        #endregion

        #region -- Site --
        /// <summary>
        /// Post async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="data">Data</param>
        /// <param name="user">User encrypted information</param>
        /// <param name="isStream">Is stream</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> PostAsync(string path, object data, string user, bool isStream = false)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToMa)
            {
                res.SetError("Ma sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_site == null)
            {
                msg = SiteNotConfig;
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = string.IsNullOrEmpty(_site.ApiUrl) ? path : $"{_site.ApiUrl}/{path}"; uri.LogInfor();
            var client = CreateClient(Token, user);

            try
            {
                var content = CreateData(data);
                var rsp = await client.PostAsync(uri, content);
                if (rsp.IsSuccessStatusCode)
                {
                    if (isStream)
                    {
                        res.Data = await rsp.Content.ReadAsStreamAsync();
                    }
                    else
                    {
                        res.Data = await rsp.Content.ReadAsStringAsync();
                    }
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }

        /// <summary>
        /// Get async
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="user">User encrypted information</param>
        /// <param name="isStream">Is stream</param>
        /// <returns>Return the result</returns>
        public async Task<SingleRsp> GetAsync(string path, string user, bool isStream = false)
        {
            var res = new SingleRsp();
            string msg;

            if (ZSetting.DisableSyncToMa)
            {
                res.SetError("Ma sync is disabled", ZCode.EZ995);
                return res;
            }

            if (_site == null)
            {
                msg = SiteNotConfig;
                res.SetError(msg);
                return res;
            }

            // Call API
            var uri = $"{_site.ApiUrl}/{path}"; uri.LogInfor();
            var client = CreateClient(Token, user);

            try
            {
                var rsp = await client.GetAsync(uri);
                if (rsp.IsSuccessStatusCode)
                {
                    if (isStream)
                    {
                        res.Data = await rsp.Content.ReadAsStreamAsync();
                    }
                    else
                    {
                        res.Data = await rsp.Content.ReadAsStringAsync();
                    }
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }
        #endregion

        /// <summary>
        /// Get async
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>Return the result</returns>
        public async static Task<SingleRsp> GetAsync(string path)
        {
            var res = new SingleRsp();
            string msg;

            var client = CreateClient();
            try
            {
                var rsp = await client.GetAsync(path);
                if (rsp.IsSuccessStatusCode)
                {
                    res.Data = await rsp.Content.ReadAsStringAsync();
                }
                else
                {
                    msg = rsp.ReasonPhrase;
                    res.SetError(msg);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                res.SetError(msg);
            }

            return res;
        }

        /// <summary>
        /// Valid token
        /// </summary>
        /// <param name="rt">Token needs to valid</param>
        /// <param name="st">Check with static token</param>
        /// <returns>Return the result</returns>
        public bool ValidToken(string rt, string st = null)
        {
            return rt == st ? true : _h.ValidToken(rt);
        }

        /// <summary>
        /// Create client
        /// </summary>
        /// <returns>Return the result</returns>
        private static HttpClient CreateClient()
        {
            var res = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(Timeout)
            };

            return res;
        }

        /// <summary>
        /// Create client
        /// </summary>
        /// <param name="o">Authentication token</param>
        /// <returns>Return the result</returns>
        private HttpClient CreateClient(BaseTokenDto o)
        {
            var res = CreateClient();

            if (o != null)
            {
                var t = new AuthenticationHeaderValue(o.TokenType, o.AccessToken);
                res.DefaultRequestHeaders.Authorization = t;
            }

            return res;
        }

        /// <summary>
        /// Create client
        /// </summary>
        /// <param name="o">Authentication token</param>
        /// <returns>Return the result</returns>
        private HttpClient CreateClient(AuthenticationDto o)
        {
            var res = CreateClient();

            if (o == null)
            {
                return res;
            }

            if (!string.IsNullOrWhiteSpace(o.ClientId) && !string.IsNullOrWhiteSpace(o.Secret))
            {
                res.DefaultRequestHeaders.Add("X-Client-ID", o.ClientId);
                res.DefaultRequestHeaders.Add("X-API-Key", o.Secret);
            }

            if (!string.IsNullOrWhiteSpace(o.UserName) && !string.IsNullOrWhiteSpace(o.Password))
            {
                var basic = Encoding.UTF8.GetBytes($"{o.UserName}:{o.Password}");
                res.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(basic));
            }

            return res;
        }

        /// <summary>
        /// Create client
        /// </summary>
        /// <param name="token">Authentication token</param>
        /// <param name="user">User encrypted information</param>
        /// <returns>Return the result</returns>
        private HttpClient CreateClient(string token, string user)
        {
            var res = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(Timeout)
            };

            res.DefaultRequestHeaders.Add(ZConst.HeaderAuth, token);
            res.DefaultRequestHeaders.Add(ZConst.HeaderUser, user);

            return res;
        }

        /// <summary>
        /// Create data
        /// </summary>
        /// <param name="data">Data</param>
        /// <returns>Return the result</returns>
        private static StringContent CreateData(object data)
        {
            var json = data.ToJson();
            var res = new StringContent(json, Encoding.UTF8, "application/json");
            res.Headers.ContentType = new MediaTypeHeaderValue(Type);
            return res;
        }

        /// <summary>
        /// Read content support Salesforce
        /// </summary>
        /// <param name="rsp">Response message</param>
        /// <returns>Return the result</returns>
        private async Task<SingleRsp> ReadContentSfx(HttpResponseMessage rsp)
        {
            var res = new SingleRsp();
            var s = await rsp.Content.ReadAsStringAsync();
            s = s.CorrectJson();

            var error = s.Value("errorCode").ToStr();
            if (!string.IsNullOrEmpty(error)) // has error
            {
                if (error == "INVALID_SESSION_ID")
                {
                    res.Data = new { TokenExpired = true };
                    return res;
                }

                var msg = s.Value("message").ToStr();
                res.SetError(msg);
            }
            res.Data = s;

            return res;
        }

        /// <summary>
        /// Read content support Rexsoftware
        /// </summary>
        /// <param name="rsp">Response message</param>
        /// <returns>Return the result</returns>
        private async Task<SingleRsp> ReadContentRex(HttpResponseMessage rsp)
        {
            var res = new SingleRsp();
            var s = await rsp.Content.ReadAsStringAsync();

            var error = s.Value("error").ToStr();
            if (!string.IsNullOrEmpty(error)) // has error
            {
                if (error.Value("type").ToStr() == "TokenException")
                {
                    res.Data = new { TokenExpired = true };
                    return res;
                }

                var msg = error.Value("message").ToStr();
                res.SetError(msg);
            }
            else
            {
                s = s.Value("result").ToStr();
            }
            res.Data = s;

            return res;
        }

        /// <summary>
        /// Set hash
        /// </summary>
        /// <param name="s">Site setting</param>
        private void SetHash(SiteDto s)
        {
            if (s != null)
            {
                if (s.ApiKey == null || s.ApiHash == null)
                {
                    _h = new ZHash(string.Empty);
                }
                else
                {
                    _h = new ZHash(s.ApiKey, s.ApiHash);
                }
            }
            else
            {
                _h = new ZHash(string.Empty);
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Salesforce setting
        /// </summary>
        public static bool HasSalesforce
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.HasSalesforce;
                }
                return res;
            }
        }

        /// <summary>
        /// Propertybase setting
        /// </summary>
        public static bool HasPropertybase
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.HasPropertybase;
                }
                return res;
            }
        }

        /// <summary>
        /// Rexsoftware setting
        /// </summary>
        public static bool HasRexsoftware
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.HasRexsoftware;
                }
                return res;
            }
        }

        /// <summary>
        /// Agentbox setting
        /// </summary>
        public static bool HasAgentbox
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.HasAgentbox;
                }
                return res;
            }
        }

        /// <summary>
        /// Mailchimp setting
        /// </summary>
        public static bool HasMailchimp
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.HasMailchimp;
                }
                return res;
            }
        }

        /// <summary>
        /// Clear auth CRM mode
        /// </summary>
        public static bool ClearAuthCrm
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.ClearAuthCrm;
                }
                return res;
            }
        }

        /// <summary>
        /// Clear auth EDM mode
        /// </summary>
        public static bool ClearAuthEdm
        {
            get
            {
                var res = false;
                if (_eAuth != null)
                {
                    res = _eAuth.ClearAuthEdm;
                }
                return res;
            }
        }

        /// <summary>
        /// Available CRM
        /// </summary>
        public static bool AvailableCrm
        {
            get
            {
                var a = HasSalesforce && !ZSetting.DisableSyncToSfx;
                var b = HasRexsoftware && !ZSetting.DisableSyncToRex;
                var c = HasAgentbox && !ZSetting.DisableSyncToAgb;
                var res = (a || b || c) && !ClearAuthCrm;
                return res;
            }
        }

        /// <summary>
        /// Available EDM
        /// </summary>
        public static bool AvailableEdm
        {
            get
            {
                var a = HasMailchimp && !ZSetting.DisableSyncToMch;
                var b = false;
                var c = false;
                var res = (a || b || c) && !ClearAuthEdm;
                return res;
            }
        }

        /// <summary>
        /// Auth expand
        /// </summary>
        public static ExpandAuthDto EAuth
        {
            get
            {
                return _eAuth;
            }
            set
            {
                _eAuth = value;
                if (_eAuth != null)
                {
                    _authSalesforce = _eAuth.Salesforce;
                    if (_authSalesforce != null)
                    {
                        var x = SfxToken;
                    }
                    _authRexsoftware = _eAuth.Rexsoftware;
                    if (_authRexsoftware != null)
                    {
                        var x = RexToken;
                    }

                    _authAgentbox = _eAuth.Agentbox;
                    _authMailchimp = _eAuth.Mailchimp;
                }
            }
        }

        /// <summary>
        /// Matcha token
        /// </summary>
        public string Token
        {
            get
            {
                if (_site == null)
                {
                    SiteNotConfig.LogError();
                    return null;
                }

                var res = _h.CreateToken(DateTime.Now, _site.SiteUid);
                return res;
            }
        }

        /// <summary>
        /// Salesforce token
        /// </summary>
        public static SfTokenDto SfxToken
        {
            get
            {
                if (_eAuth != null)
                {
                    var ea = _eAuth.Salesforce;
                    if (ea.HasSalesforce)
                    {
                        _authSalesforce = ea;
                    }
                }

                if (_authSalesforce == null)
                {
                    "Authentication is not config".LogError();
                    return _sToken;
                }
                if (string.IsNullOrWhiteSpace(_authSalesforce.Host))
                {
                    "Host is null or white space".LogError();
                    return _sToken;
                }

                if (_sToken == null)
                {
                    var t = new[] {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("client_id", _authSalesforce.ClientId),
                        new KeyValuePair<string, string>("client_secret", _authSalesforce.Secret),
                        new KeyValuePair<string, string>("username", _authSalesforce.UserName),
                        new KeyValuePair<string, string>("password", _authSalesforce.Password)
                    };
                    var data = new FormUrlEncodedContent(t);
                    var client = new HttpClient();

                    var r1 = client.PostAsync(_authSalesforce.Host, data).Result;
                    if (!r1.IsSuccessStatusCode)
                    {
                        return new SfTokenDto();
                    }

                    var r2 = r1.Content.ReadAsStringAsync().Result;
                    var jo = JObject.Parse(r2);

                    var error = jo.GetValue("error");
                    if (error == null)
                    {
                        _sToken = r2.ToInst<SfTokenDto>();
                    }
                    else
                    {
                        var msg = jo.GetValue("error_description").ToString();
                        Console.WriteLine(error.ToString() + " " + msg);
                    }
                }

                return _sToken;
            }
        }

        /// <summary>
        /// Rexsoftware token
        /// </summary>
        public static BaseTokenDto RexToken
        {
            get
            {
                if (_eAuth != null)
                {
                    var ea = _eAuth.Rexsoftware;
                    if (ea.HasRexsoftware)
                    {
                        _authRexsoftware = ea;
                    }
                }

                if (_authRexsoftware == null)
                {
                    "Authentication is not config".LogError();
                    return _rToken;
                }
                if (string.IsNullOrWhiteSpace(_authRexsoftware.Host))
                {
                    "Host is null or white space".LogError();
                    return _rToken;
                }

                if (_rToken == null)
                {
                    var req = new
                    {
                        email = _authRexsoftware.UserName,
                        password = _authRexsoftware.Password,
                        account_id = _authRexsoftware.AccountId
                    };

                    var data = CreateData(req);
                    var client = new HttpClient();

                    var r1 = client.PostAsync(_authRexsoftware.Host + "/Authentication/login", data).Result;
                    if (!r1.IsSuccessStatusCode)
                    {
                        return new BaseTokenDto();
                    }

                    var r2 = r1.Content.ReadAsStringAsync().Result;
                    var jo = JObject.Parse(r2);

                    var error = jo.GetValue("error");
                    if (error == null || string.IsNullOrEmpty(error.ToStr()))
                    {
                        var result = jo.GetValue("result");
                        _rToken = new BaseTokenDto(result.ToString())
                        {
                            InstanceUrl = _authRexsoftware.Host
                        };
                    }
                    else
                    {
                        var msg = jo.GetValue("message").ToString();
                        Console.WriteLine(error.ToString() + " " + msg);
                    }
                }

                return _rToken;
            }
        }

        /// <summary>
        /// API URL
        /// </summary>
        public string ApiUrl
        {
            get
            {
                if (_site == null)
                {
                    SiteNotConfig.LogError();
                    return null;
                }

                return _site.ApiUrl;
            }
            set
            {
                if (_site == null)
                {
                    SiteNotConfig.LogError();
                }
                else
                {
                    _site.ApiUrl = value;
                }
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Project
        /// </summary>
        public static SysCode _project;

        /// <summary>
        /// Site setting
        /// </summary>
        private SiteDto _site;

        /// <summary>
        /// Hash
        /// </summary>
        private ZHash _h;

        /// <summary>
        /// Authentication Salesforce setting
        /// </summary>
        private static AuthenticationDto _authSalesforce;

        /// <summary>
        /// Authentication Rexsoftware setting
        /// </summary>
        private static AuthenticationDto _authRexsoftware;

        /// <summary>
        /// Authentication Agentbox setting
        /// </summary>
        private static AuthenticationDto _authAgentbox;

        /// <summary>
        /// Authentication Mailchimp setting
        /// </summary>
        private static AuthenticationDto _authMailchimp;

        /// <summary>
        /// Auth expand
        /// </summary>
        private static ExpandAuthDto _eAuth;

        /// <summary>
        /// Salesforce token
        /// </summary>
        private static SfTokenDto _sToken;

        /// <summary>
        /// Rexsoftware token
        /// </summary>
        private static BaseTokenDto _rToken;

        #endregion

        #region -- Constants --

        /// <summary>
        /// APEX REST prefix
        /// </summary>
        public const string ApexRest = "apexrest";

        /// <summary>
        /// APEX data prefix
        /// </summary>
        public const string ApexData = "data";

        /// <summary>
        /// Type
        /// </summary>
        private const string Type = "application/json";

        /// <summary>
        /// Timeout (second)
        /// </summary>
        private const ushort Timeout = 25;

        /// <summary>
        /// Maximum of rows to return
        /// </summary>
        private const double MaxLimit = 100;

        /// <summary>
        /// Site is not config
        /// </summary>
        private const string SiteNotConfig = "Site is not config";

        #endregion
    }
}
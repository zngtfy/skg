﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Jun-14 16:38
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG
{
    /// <summary>
    /// Code
    /// </summary>
    public class ZCode
    {
        #region -- Error --

        /// <summary>
        /// Password is expired
        /// </summary>
        public const string EZ101 = "EZ101";

        /// <summary>
        /// Existing data
        /// </summary>
        public const string EZ102 = "EZ102";

        /// <summary>
        /// No data
        /// </summary>
        public const string EZ103 = "EZ103";

        /// <summary>
        /// Mandatory
        /// </summary>
        public const string EZ104 = "EZ104";

        /// <summary>
        /// Invalid user ID
        /// </summary>
        public const string EZ105 = "EZ105";

        /// <summary>
        /// Invalid password salt
        /// </summary>
        public const string EZ106 = "EZ106";

        /// <summary>
        /// Invalid password
        /// </summary>
        public const string EZ107 = "EZ107";

        /// <summary>
        /// Invalid password reminder token
        /// </summary>
        public const string EZ108 = "EZ108";

        /// <summary>
        /// Old password is incorrect
        /// </summary>
        public const string EZ109 = "EZ109";

        /// <summary>
        /// Password reminder token is expired
        /// </summary>
        public const string EZ110 = "EZ110";

        /// <summary>
        /// JWT is expired
        /// </summary>
        public const string EZ111 = "EZ111";

        /// <summary>
        /// New password was used previously
        /// </summary>
        public const string EZ112 = "EZ112";

        /// <summary>
        /// Account has been locked
        /// </summary>
        public const string EZ113 = "EZ113";

        /// <summary>
        /// JWT has logout
        /// </summary>
        public const string EZ114 = "EZ114";

        /// <summary>
        /// Password null or empty
        /// </summary>
        public const string EZ115 = "EZ115";

        /// <summary>
        /// JWT has logout (single session)
        /// </summary>
        public const string EZ116 = "EZ116";

        /// <summary>
        /// No permission create
        /// </summary>
        public const string EZ117 = "EZ117";

        /// <summary>
        /// No permission read
        /// </summary>
        public const string EZ118 = "EZ118";

        /// <summary>
        /// No permission update
        /// </summary>
        public const string EZ119 = "EZ119";

        /// <summary>
        /// No permission delete
        /// </summary>
        public const string EZ120 = "EZ120";

        /// <summary>
        /// Upload failed
        /// </summary>
        public const string EZ121 = "EZ121";

        /// <summary>
        /// Object null
        /// </summary>
        public const string EZ991 = "EZ991";

        /// <summary>
        /// Invalid access token or IP address not allowed
        /// </summary>
        public const string EZ992 = "EZ992";

        /// <summary>
        /// No delegator name
        /// </summary>
        public const string EZ993 = "EZ993";

        /// <summary>
        /// Server isn't responding
        /// </summary>
        public const string EZ994 = "EZ994";

        /// <summary>
        /// Sync is disabled
        /// </summary>
        public const string EZ995 = "EZ995";

        /// <summary>
        /// 401 Unauthorized
        /// </summary>
        public const string EZ996 = "EZ996";

        /// <summary>
        /// Form invalid
        /// </summary>
        public const string EZ997 = "EZ997";

        /// <summary>
        /// Maximum size of data list
        /// </summary>
        public const string EZ998 = "EZ998";

        /// <summary>
        /// Cannot install second time
        /// </summary>
        public const string EZ999 = "EZ999";

        #endregion

        #region -- Success --

        /// <summary>
        /// Create successfully
        /// </summary>
        public const string SZ101 = "SZ101";

        /// <summary>
        /// Update successfully
        /// </summary>
        public const string SZ102 = "SZ102";

        /// <summary>
        /// Upload successfully
        /// </summary>
        public const string SZ103 = "SZ103";

        /// <summary>
        /// Setup done
        /// </summary>
        public const string SZ999 = "SZ999";

        #endregion

        #region -- Information --

        /// <summary>
        /// Nothing to update
        /// </summary>
        public const string IZ101 = "IZ101";

        #endregion

        #region -- Message --

        /// <summary>
        /// Form invalid
        /// </summary>
        public const string EM997 = "Form invalid";

        /// <summary>
        /// 401 Unauthorized
        /// </summary>
        public const string EM996 = "401 Unauthorized";

        /// <summary>
        /// Server isn't responding
        /// </summary>
        public const string EM994 = "Server isn\'t responding";

        /// <summary>
        /// No delegator name
        /// </summary>
        public const string EM993 = "You don\'t have the permission to update data on behalf of the specified user";

        /// <summary>
        /// Invalid token
        /// </summary>
        public const string EM992 = "Invalid access token";

        #endregion
    }
}
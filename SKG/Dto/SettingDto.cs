﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-21 11:40
 * Update       : 2020-Jul-21 11:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;

namespace SKG.Dto
{
    /// <summary>
    /// Setting data transfer object
    /// </summary>
    public class SettingDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SettingDto()
        {
            ExpandAuth = new ExpandAuthDto();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Units of length
        /// </summary>
        public string LengthUnit { get; set; }

        /// <summary>
        /// Units of area
        /// </summary>
        public string AreaUnit { get; set; }

        /// <summary>
        /// Units of currency 
        /// </summary>
        public string CurrencyUnit { get; set; }

        /// <summary>
        /// Default email to
        /// </summary>
        public string DefaultTo { get; set; }

        /// <summary>
        /// Default email cc
        /// </summary>
        public string DefaultCc { get; set; }

        /// <summary>
        /// Default email bcc
        /// </summary>
        public string DefaultBcc { get; set; }

        /// <summary>
        /// Test email
        /// </summary>
        public string TestEmail { get; set; }

        /// <summary>
        /// Prevent crawler
        /// </summary>
        public string PreventCrawler { get; set; }

        /// <summary>
        /// Auth expand
        /// </summary>
        public ExpandAuthDto ExpandAuth { get; set; }

        /// <summary>
        /// Listing status
        /// </summary>
        public List<DictionDto> ListingStatuses { get; set; }

        #endregion
    }
}
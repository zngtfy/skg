﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-17 15:11
 * Update       : 2021-Apr-16 05:31
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;

namespace SKG.Dto
{
    using Ext;
    using static ZEnum;

    /// <summary>
    /// Auth expand data transfer object
    /// </summary>
    public class ExpandAuthDto : IdDto
    {
        #region -- Overrides --

        /// <summary>
        /// ID
        /// </summary>
        [JsonIgnore]
        public override uint Id { get => base.Id; set => base.Id = value; }

        /// <summary>
        /// Returns a string that represents the current object
        /// </summary>
        /// <returns>A string that represents the current object</returns>
        public override string ToString()
        {
            return this.ToJson();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ExpandAuthDto() : base() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        public ExpandAuthDto(uint i) : base(i) { }

        /// <summary>
        /// Set authentication
        /// </summary>
        /// <param name="o">Authentication</param>
        public void SetAuth(AuthenticationDto o)
        {
            if (o != null)
            {
                #region -- CRM --
                if (o.Type == External.Salesforce)
                {
                    Salesforce = o;
                }
                if (o.Type == External.Rexsoftware)
                {
                    Rexsoftware = o;
                }
                if (o.Type == External.Agentbox)
                {
                    Agentbox = o;
                }

                if (o.Type == External.Salesforce || o.Type == External.Rexsoftware || o.Type == External.Agentbox)
                {
                    ActiveCrm = o.Type;
                    ClearAuthCrm = false;
                }
                #endregion

                #region -- EDM --
                if (o.Type == External.Mailchimp)
                {
                    Mailchimp = o;
                }

                if (o.Type == External.Mailchimp)
                {
                    ActiveEdm = o.Type;
                    ClearAuthEdm = false;
                }
                #endregion
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Salesforce setting
        /// </summary>
        public AuthenticationDto Salesforce { get; set; }

        /// <summary>
        /// Rexsoftware setting
        /// </summary>
        public AuthenticationDto Rexsoftware { get; set; }

        /// <summary>
        /// Agentbox setting
        /// </summary>
        public AuthenticationDto Agentbox { get; set; }

        /// <summary>
        /// Mailchimp setting
        /// </summary>
        public AuthenticationDto Mailchimp { get; set; }

        /// <summary>
        /// Other setting
        /// </summary>
        public object Other { get; set; }

        /// <summary>
        /// Active CRM
        /// </summary>
        public External? ActiveCrm { get; set; }

        /// <summary>
        /// Active EDM
        /// </summary>
        public External? ActiveEdm { get; set; }

        /// <summary>
        /// Clear auth CRM mode
        /// </summary>
        public bool ClearAuthCrm { get; set; }

        /// <summary>
        /// Clear auth EDM mode
        /// </summary>
        public bool ClearAuthEdm { get; set; }

        /// <summary>
        /// Salesforce setting
        /// </summary>
        public bool HasSalesforce
        {
            get
            {
                if (Salesforce == null)
                {
                    return false;
                }

                var res = Salesforce.HasSalesforce && ActiveCrm == External.Salesforce;
                return res;
            }
        }

        /// <summary>
        /// Propertybase setting
        /// </summary>
        public bool HasPropertybase
        {
            get
            {
                if (Salesforce == null)
                {
                    return false;
                }

                var res = Salesforce.HasPropertybase && ActiveCrm == External.Salesforce;
                return res;
            }
        }

        /// <summary>
        /// Rexsoftware setting
        /// </summary>
        public bool HasRexsoftware
        {
            get
            {
                if (Rexsoftware == null)
                {
                    return false;
                }

                var res = Rexsoftware.HasRexsoftware && ActiveCrm == External.Rexsoftware;
                return res;
            }
        }

        /// <summary>
        /// Agentbox setting
        /// </summary>
        public bool HasAgentbox
        {
            get
            {
                if (Agentbox == null)
                {
                    return false;
                }

                var res = Agentbox.HasAgentbox && ActiveCrm == External.Agentbox;
                return res;
            }
        }

        /// <summary>
        /// Mailchimp setting
        /// </summary>
        public bool HasMailchimp
        {
            get
            {
                if (Mailchimp == null)
                {
                    return false;
                }

                var res = Mailchimp.HasMailchimp && ActiveEdm == External.Mailchimp;
                return res;
            }
        }

        #endregion
    }
}
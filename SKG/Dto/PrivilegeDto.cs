﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-06 08:54
 * Update       : 2020-Aug-14 10:25
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SKG.Dto
{
    using DAL.Models;
    using Ext;

    /// <summary>
    /// Privilege data transfer object
    /// </summary>
    public class PrivilegeDto
    {
        #region -- Overrides --

        /// <summary>
        /// Returns a string that represents the current object
        /// </summary>
        /// <returns>A string that represents the current object</returns>
        public override string ToString()
        {
            return this.ToJson();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public PrivilegeDto()
        {
            Apps = new List<AppSetting>();
            Tabs = new List<TabSetting>();
            Objs = new List<ObjSetting>();
            Stes = new List<SteSetting>();

            //TODO
            SessionSetting = string.Empty;
            PasswordPolicy = string.Empty;
            LoginHour = string.Empty;
            LoginIpRanges = string.Empty;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="a">App setting</param>
        /// <param name="b">Tab setting</param>
        /// <param name="c">Object setting</param>
        /// <param name="d">Site setting</param>
        public PrivilegeDto(List<AppSetting> a, List<TabSetting> b, List<ObjSetting> c, List<SteSetting> d) : this()
        {
            if (a == null)
            {
                a = new List<AppSetting>();
            }

            if (b == null)
            {
                b = new List<TabSetting>();
            }

            if (c == null)
            {
                c = new List<ObjSetting>();
            }

            if (d == null)
            {
                d = new List<SteSetting>();
            }

            Apps = a;
            Tabs = b;
            Objs = c;
            Stes = d;
        }

        #region -- Setting app --
        /// <summary>
        /// Add setting app
        /// </summary>
        /// <param name="s">Setting app data</param>
        public void AddApp(AppSetting s)
        {
            var res = new List<AppSetting>();

            foreach (var i in Apps)
            {
                res.Add(i);
            }
            res.Add(s);

            Apps = res;
        }

        /// <summary>
        /// Update setting app
        /// </summary>
        /// <param name="s">Setting app data</param>
        public void UpdateApp(AppSetting s)
        {
            var res = new List<AppSetting>();

            foreach (var i in Apps)
            {
                if (i.DevName == s.DevName)
                {
                    if (i.Visible == s.Visible)
                    {
                        res.Add(s);
                    }
                }
                else
                {
                    res.Add(i);
                }
            }

            Apps = res;
        }
        #endregion

        #region -- Setting tab --
        /// <summary>
        /// Add setting tab
        /// </summary>
        /// <param name="s">Setting tab data</param>
        public void AddTab(TabSetting s)
        {
            var res = new List<TabSetting>();

            foreach (var i in Tabs)
            {
                res.Add(i);
            }
            res.Add(s);

            Tabs = res;
        }

        /// <summary>
        /// Update setting tab
        /// </summary>
        /// <param name="s">Setting tab data</param>
        public void UpdateTab(TabSetting s)
        {
            var res = new List<TabSetting>();

            foreach (var i in Tabs)
            {
                if (i.DevName == s.DevName)
                {
                    if (i.Visible == s.Visible)
                    {
                        res.Add(s);
                    }
                }
                else
                {
                    res.Add(i);
                }
            }

            Tabs = res;
        }
        #endregion

        #region -- Setting object --
        /// <summary>
        /// Add setting object
        /// </summary>
        /// <param name="s">Setting object data</param>
        public void AddObj(ObjSetting s)
        {
            var res = new List<ObjSetting>();

            foreach (var i in Objs)
            {
                res.Add(i);
            }
            res.Add(s);

            Objs = res;
        }

        /// <summary>
        /// Update setting object
        /// </summary>
        /// <param name="s">Setting object data</param>
        public void UpdateObj(ObjSetting s)
        {
            var res = new List<ObjSetting>();

            foreach (var i in Objs)
            {
                if (i.DevName == s.DevName)
                {
                    if (i.AccessRight == s.AccessRight)
                    {
                        res.Add(s);
                    }
                }
                else
                {
                    res.Add(i);
                }
            }

            Objs = res;
        }
        #endregion

        #region -- Setting site --
        /// <summary>
        /// Add setting site
        /// </summary>
        /// <param name="s">Setting site data</param>
        public void AddObj(SteSetting s)
        {
            var res = new List<SteSetting>();

            foreach (var i in Stes)
            {
                res.Add(i);
            }
            res.Add(s);

            Stes = res;
        }

        /// <summary>
        /// Update setting site
        /// </summary>
        /// <param name="s">Setting site data</param>
        public void UpdateObj(SteSetting s)
        {
            var res = new List<SteSetting>();

            foreach (var i in Stes)
            {
                if (i.DevName == s.DevName)
                {
                    if (i.AccessRight == s.AccessRight)
                    {
                        res.Add(s);
                    }
                }
                else
                {
                    res.Add(i);
                }
            }

            Stes = res;
        }
        #endregion

        #endregion

        #region -- Properties --

        /// <summary>
        /// App setting
        /// </summary>
        public List<AppSetting> Apps { get; private set; }

        /// <summary>
        /// Tab setting
        /// </summary>
        public List<TabSetting> Tabs { get; private set; }

        /// <summary>
        /// Object setting
        /// </summary>
        public List<ObjSetting> Objs { get; private set; }

        /// <summary>
        /// Site setting
        /// </summary>
        public List<SteSetting> Stes { get; private set; }

        /// <summary>
        /// Session setting (session times out after 2 hours of inactivity)
        /// </summary>
        public string SessionSetting { get; set; }

        /// <summary>
        /// Password policy
        /// </summary>
        public string PasswordPolicy { get; set; }

        /// <summary>
        /// Login hour
        /// </summary>
        public string LoginHour { get; set; }

        /// <summary>
        /// Login IP range (use splitter ;)
        /// </summary>
        public string LoginIpRanges { get; set; }

        #endregion

        #region -- Classes --

        /// <summary>
        /// Zero setting
        /// </summary>
        public class Zero : IdDto
        {
            #region -- Overrides --

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>A string that represents the current object</returns>
            public override string ToString()
            {
                return this.ToJson();
            }

            #endregion

            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public Zero()
            {
                DevName = string.Empty;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Developer name</param>
            public Zero(uint i, string n)
            {
                Id = i;

                if (n == null)
                {
                    n = string.Empty;
                }

                DevName = n.Trim();
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Developer name</param>
            /// <param name="c">Is custom</param>
            public Zero(uint i, string n, bool c) : this(i, n)
            {
                Custom = c;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Developer name
            /// </summary>
            [JsonProperty("dn")]
            public string DevName { get; set; }

            /// <summary>
            /// Is custom
            /// </summary>
            [JsonProperty("ct")]
            public bool Custom { get; set; }

            #endregion
        }

        /// <summary>
        /// App setting
        /// </summary>
        public class AppSetting : Zero
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public AppSetting() : base() { }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            public AppSetting(uint i, string n) : base(i, n) { }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            /// <param name="c">Custom</param>
            public AppSetting(uint i, string n, bool c) : base(i, n, c) { }

            /// <summary>
            /// Convert list SysApp to list AppSetting
            /// </summary>
            /// <param name="l">List SysApp</param>
            /// <returns>Return the result</returns>
            public static List<AppSetting> Convert(List<SysApp> l)
            {
                if (l == null)
                {
                    l = new List<SysApp>();
                }

                var res = l.Select(p => new AppSetting(p.Id, p.DevName, p.Custom)).ToList();
                return res;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Visible
            /// </summary>
            [JsonProperty("v")]
            public bool Visible { get; set; }

            /// <summary>
            /// Default
            /// </summary>
            [JsonProperty("d")]
            public bool Default { get; set; }

            #endregion
        }

        /// <summary>
        /// Tab setting
        /// </summary>
        public class TabSetting : Zero
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public TabSetting() : base() { }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="n">Name</param>
            public TabSetting(string n) : base(0, n) { }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            public TabSetting(uint i, string n) : base(i, n) { }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            /// <param name="c">Custom</param>
            public TabSetting(uint i, string n, bool c) : base(i, n, c) { }

            /// <summary>
            /// Convert list SysTab to list TabSetting
            /// </summary>
            /// <param name="l">List SysTab</param>
            /// <returns>Return the result</returns>
            public static List<TabSetting> Convert(List<SysTab> l)
            {
                if (l == null)
                {
                    l = new List<SysTab>();
                }

                var res = l.Select(p => new TabSetting(p.Id, p.DevName, p.Custom)).ToList();
                return res;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Visible
            /// </summary>
            [JsonProperty("v")]
            public Visible Visible { get; set; }

            /// <summary>
            /// Default tab open
            /// </summary>
            [JsonProperty("d")]
            public bool Default { get; set; }

            #endregion
        }

        /// <summary>
        /// Object setting
        /// </summary>
        public class ObjSetting : Zero
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public ObjSetting() : base()
            {
                AccessRight = Sop.None;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            public ObjSetting(uint i, string n) : base(i, n)
            {
                AccessRight = Sop.None;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            /// <param name="c">Custom</param>
            public ObjSetting(uint i, string n, bool c) : base(i, n, c)
            {
                AccessRight = Sop.None;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            /// <param name="c">Custom</param>
            /// <param name="r">Access right</param>
            public ObjSetting(uint i, string n, bool c, Sop r) : base(i, n, c)
            {
                AccessRight = r;
            }

            /// <summary>
            /// Convert list SysCode to list ObjSetting
            /// </summary>
            /// <param name="l">List SysCode</param>
            /// <returns>Return the result</returns>
            public static List<ObjSetting> Convert(List<SysCode> l)
            {
                if (l == null)
                {
                    l = new List<SysCode>();
                }

                var res = l.Select(p => new ObjSetting(p.Id, p.DevName)).ToList();
                return res;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Access right
            /// </summary>
            [JsonProperty("ar")]
            public Sop AccessRight { get; set; }

            /// <summary>
            /// Option right
            /// </summary>
            [JsonProperty("or")]
            public object OptionRight { get; set; }

            /// <summary>
            /// Permission
            /// </summary>
            [JsonIgnore]
            public SopExt Permis
            {
                get { return AccessRight.ToPermis(DevName, OptionRight); }
            }

            #endregion
        }

        /// <summary>
        /// Site setting
        /// </summary>
        public class SteSetting : Zero
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public SteSetting() : base()
            {
                AccessRight = Ssp.None;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            public SteSetting(uint i, string n) : base(i, n)
            {
                AccessRight = Ssp.None;
            }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            /// <param name="c">Custom</param>
            public SteSetting(uint i, string n, bool c) : base(i, n, c) { }

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="n">Name</param>
            /// <param name="c">Custom</param>
            /// <param name="r">Access right</param>
            public SteSetting(uint i, string n, bool c, Ssp r) : base(i, n, c)
            {
                AccessRight = r;
            }

            /// <summary>
            /// Convert list SysCode to list SiteSetting
            /// </summary>
            /// <param name="l">List SysCode</param>
            /// <returns>Return the result</returns>
            public static List<SteSetting> Convert(List<SysSite> l)
            {
                if (l == null)
                {
                    l = new List<SysSite>();
                }

                var res = l.Select(p => new SteSetting(p.Id, p.DevName)).ToList();
                return res;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Access right
            /// </summary>
            [JsonProperty("ar")]
            public Ssp AccessRight { get; set; }

            /// <summary>
            /// Option right
            /// </summary>
            [JsonProperty("or")]
            public object OptionRight { get; set; }

            /// <summary>
            /// Permission
            /// </summary>
            [JsonIgnore]
            public SspExt Permis
            {
                get { return AccessRight.ToPermis(DevName, OptionRight); }
            }

            #endregion
        }

        /// <summary>
        /// Convert from enum to many item boolean
        /// </summary>
        public class SopExt
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="e">Standard object permissions</param>
            /// <param name="n">Object name</param>
            /// <param name="o">Options</param>
            public SopExt(Sop e, string n, object o)
            {
                if (n == null)
                {
                    n = string.Empty;
                }

                Name = n;

                Read = (e & Sop.Read) == Sop.Read;
                Create = (e & Sop.Create) == Sop.Create;
                Edit = (e & Sop.Edit) == Sop.Edit;
                Delete = (e & Sop.Delete) == Sop.Delete;

                ViewAll = (e & Sop.ViewAll) == Sop.ViewAll;
                ModifyAll = (e & Sop.ModifyAll) == Sop.ModifyAll;

                Options = o;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Object name
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// Options
            /// </summary>
            public object Options { get; set; }

            /// <summary>
            /// None
            /// </summary>
            public bool None
            {
                get
                {
                    var res = Read || Create || Edit || Delete || ViewAll || ModifyAll;
                    return !res;
                }
            }

            #region -- Basic Access --
            /// <summary>
            /// Read
            /// </summary>
            public bool Read { get; private set; }

            /// <summary>
            /// Create
            /// </summary>
            public bool Create { get; private set; }

            /// <summary>
            /// Edit
            /// </summary>
            public bool Edit { get; private set; }

            /// <summary>
            /// Delete
            /// </summary>
            public bool Delete { get; private set; }
            #endregion

            #region -- Data Administration --
            /// <summary>
            /// View all
            /// </summary>
            public bool ViewAll { get; private set; }

            /// <summary>
            /// Modify all
            /// </summary>
            public bool ModifyAll { get; private set; }
            #endregion

            #endregion
        }

        /// <summary>
        /// Convert from enum to many item boolean
        /// </summary>
        public class SspExt
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="e">Standard site permissions</param>
            /// <param name="n">Site name</param>
            /// <param name="o">Options</param>
            public SspExt(Ssp e, string n, object o)
            {
                if (n == null)
                {
                    n = string.Empty;
                }

                Name = n;

                Access = (e & Ssp.Access) == Ssp.Access;
                CreateUser = (e & Ssp.CreateUser) == Ssp.CreateUser;
                UpdateUser = (e & Ssp.UpdateUser) == Ssp.UpdateUser;
                ResetPassword = (e & Ssp.ResetPassword) == Ssp.ResetPassword;
                LoginAs = (e & Ssp.LoginAs) == Ssp.LoginAs;
                UseSiteProfile = (e & Ssp.UseSiteProfile) == Ssp.UseSiteProfile;
                SyncData = (e & Ssp.SyncData) == Ssp.SyncData;

                Options = o;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Site name
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// Options
            /// </summary>
            public object Options { get; set; }

            /// <summary>
            /// None
            /// </summary>
            public bool None
            {
                get
                {
                    var res = Access || CreateUser || UpdateUser || ResetPassword || LoginAs || UseSiteProfile || SyncData;
                    return !res;
                }
            }

            /// <summary>
            /// Access
            /// </summary>
            public bool Access { get; private set; }

            /// <summary>
            /// Create user
            /// </summary>
            public bool CreateUser { get; private set; }

            /// <summary>
            /// Update user
            /// </summary>
            public bool UpdateUser { get; private set; }

            /// <summary>
            /// Reset password
            /// </summary>
            public bool ResetPassword { get; private set; }

            /// <summary>
            /// Login as
            /// </summary>
            public bool LoginAs { get; private set; }

            /// <summary>
            /// Use site profile
            /// </summary>
            public bool UseSiteProfile { get; private set; }

            /// <summary>
            /// Sync data
            /// </summary>
            public bool SyncData { get; private set; }

            #endregion
        }

        #endregion

        #region -- Enums --

        /// <summary>
        /// Visible
        /// </summary>
        public enum Visible
        {
            /// <summary>
            /// Tab hidden
            /// </summary>
            [Display(Name = "Tab Hidden")]
            Hidden,

            /// <summary>
            /// Default off (not click)
            /// </summary>
            [Display(Name = "Default Off")]
            Off,

            /// <summary>
            /// Default on (can click)
            /// </summary>
            [Display(Name = "Default On")]
            On
        }

        /// <summary>
        /// Standard object permissions
        /// </summary>
        public enum Sop
        {
            /// <summary>
            /// None
            /// </summary>
            None = 0,

            #region -- Basic Access --

            /// <summary>
            /// Read
            /// </summary>
            Read = 1 << 0,

            /// <summary>
            /// Create
            /// </summary>
            Create = 1 << 1,

            /// <summary>
            /// Edit
            /// </summary>
            Edit = 1 << 2,

            /// <summary>
            /// Delete
            /// </summary>
            Delete = 1 << 3,

            #endregion

            #region -- Data Administration --

            /// <summary>
            /// View all
            /// </summary>
            ViewAll = Read,

            /// <summary>
            /// Modify all
            /// </summary>
            ModifyAll = Read | Edit | Delete,

            #endregion

            /// <summary>
            /// Full
            /// </summary>
            Full = Create | ModifyAll
        }

        /// <summary>
        /// Standard site permissions
        /// </summary>
        public enum Ssp
        {
            /// <summary>
            /// None
            /// </summary>
            None = 0,

            #region -- Basic Access --

            /// <summary>
            /// Access
            /// </summary>
            Access = 1 << 0,

            #endregion

            /// <summary>
            /// Create user
            /// </summary>
            CreateUser = 1 << 1,

            /// <summary>
            /// Update user
            /// </summary>
            UpdateUser = 1 << 2,

            /// <summary>
            /// Reset password
            /// </summary>
            ResetPassword = 1 << 3,

            /// <summary>
            /// Login as
            /// </summary>
            LoginAs = 1 << 4,

            /// <summary>
            /// Use site profile
            /// </summary>
            UseSiteProfile = 1 << 5,

            /// <summary>
            /// Sync data
            /// </summary>
            SyncData = 1 << 6,

            /// <summary>
            /// Full
            /// </summary>
            Full = Access | CreateUser | UpdateUser | ResetPassword | LoginAs | UseSiteProfile | SyncData
        }

        #endregion

        #region -- DataTest --

        /// <summary>
        /// Data test
        /// </summary>
        /// <param name="r">Objet access right</param>
        /// <param name="s">Site access right</param>
        /// <returns>Return the result</returns>
        public static PrivilegeDto DataTest(Sop r, Ssp s)
        {
            var res = new PrivilegeDto(DataApps, DataTabs, DataObjs(r), DataStes(s));
            return res;
        }

        #region -- For profile MatchaAnalystView --
        /// <summary>
        /// TestMatchaAnalystView
        /// </summary>
        /// <param name="r"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static PrivilegeDto TestMatchaAnalystView(Sop r, Ssp s)
        {
            var res = new PrivilegeDto(DaMatchaAnalystView, DtMatchaAnalystView, DataObjs(r), DataStes(s));
            return res;
        }

        /// <summary>
        /// DaMatchaAnalystView
        /// </summary>
        private static List<AppSetting> DaMatchaAnalystView
        {
            get
            {
                var res = new List<AppSetting>();

                var o = new AppSetting(1, "System") { Visible = false, Default = false };
                res.Add(o);

                o = new AppSetting(2, "CMS") { Visible = false, Default = false };
                res.Add(o);

                o = new AppSetting(3, "Analysis") { Visible = true, Default = true };
                res.Add(o);

                o = new AppSetting(4, "Test App") { Visible = false, Default = false, Custom = true };
                res.Add(o);

                return res;
            }
        }

        /// <summary>
        /// DtMatchaAnalystView
        /// </summary>
        private static List<TabSetting> DtMatchaAnalystView
        {
            get
            {
                var res = new List<TabSetting>();

                var o = new TabSetting(4, "Customer Journey Map") { Visible = Visible.On, Custom = true };
                res.Add(o);

                o = new TabSetting(5, "Dashboard") { Visible = Visible.On, Custom = true };
                res.Add(o);

                o = new TabSetting(6, "Site Traffic") { Visible = Visible.On };
                res.Add(o);

                o = new TabSetting(7, "Page Traffic") { Visible = Visible.On };
                res.Add(o);

                o = new TabSetting(8, "UserOverview") { Visible = Visible.On, Custom = true };
                res.Add(o);

                o = new TabSetting(9, "SaleOverview") { Visible = Visible.On, Custom = true };
                res.Add(o);

                o = new TabSetting(10, "Email Templates") { Visible = Visible.On, Custom = true };
                res.Add(o);

                return res;
            }
        }
        #endregion

        /// <summary>
        /// Data test app setting
        /// </summary>
        private static List<AppSetting> DataApps
        {
            get
            {
                var res = new List<AppSetting>();

                var o = new AppSetting(1, "System") { Visible = true, Default = false };
                res.Add(o);

                o = new AppSetting(2, "CMS") { Visible = true, Default = false };
                res.Add(o);

                o = new AppSetting(3, "Analysis") { Visible = true, Default = true };
                res.Add(o);

                o = new AppSetting(4, "Test") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                o = new AppSetting(5, "Developing") { Visible = true, Default = false };
                res.Add(o);

                o = new AppSetting(6, "Sandford") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                o = new AppSetting(7, "Bowline") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                o = new AppSetting(8, "Matcha") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                o = new AppSetting(9, "Demo") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                o = new AppSetting(10, "Sandford Analysis") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                o = new AppSetting(11, "Sandford CMS") { Visible = true, Default = false, Custom = true };
                res.Add(o);

                return res;
            }
        }

        /// <summary>
        /// Data test tab setting
        /// </summary>
        private static List<TabSetting> DataTabs
        {
            get
            {
                var res = new List<TabSetting>();

                var seq = 1u;
                var l = _tab.ToSet(ZConst.NewLine);
                foreach (var i in l)
                {
                    var o = new TabSetting(seq++, i) { Visible = Visible.On };
                    res.Add(o);
                }

                return res;
            }
        }

        /// <summary>
        /// Data test object setting
        /// </summary>
        /// <param name="r">Access right</param>
        /// <returns>Return the result</returns>
        private static List<ObjSetting> DataObjs(Sop r)
        {
            var res = new List<ObjSetting>();

            var l = _tab.ToSet(ZConst.NewLine);
            foreach (var i in l)
            {
                var o = new ObjSetting(0, i, false, r);
                res.Add(o);
            }

            return res;
        }

        /// <summary>
        /// Data test site setting
        /// </summary>
        /// <param name="r">Access right</param>
        /// <returns>Return the result</returns>
        private static List<SteSetting> DataStes(Ssp r)
        {
            var res = new List<SteSetting>();

            var o = new SteSetting(1, "Apartment", false, r);
            res.Add(o);

            o = new SteSetting(2, "Awaken", false, r);
            res.Add(o);

            o = new SteSetting(3, "The Times Vip", false, r);
            res.Add(o);

            o = new SteSetting(4, "Greenway", false, r);
            res.Add(o);

            o = new SteSetting(5, "Matcha", false, r);
            res.Add(o);

            o = new SteSetting(6, "Sandford", false, r);
            res.Add(o);

            o = new SteSetting(7, "Land and Home", false, r);
            res.Add(o);

            o = new SteSetting(8, "Bowline Wickham", false, r);
            res.Add(o);

            o = new SteSetting(9, "Noname", false, r);
            res.Add(o);

            o = new SteSetting(10, "Demo", false, r);
            res.Add(o);

            o = new SteSetting(11, "Core", true, r);
            res.Add(o);

            o = new SteSetting(12, "Creation Homes", true, r);
            res.Add(o);

            o = new SteSetting(13, "Arkcon", true, r);
            res.Add(o);

            o = new SteSetting(14, "Qantum", true, r);
            res.Add(o);

            return res;
        }

        /// <summary>
        /// Test data objs
        /// </summary>
        public static List<ObjSetting> TestDataObjs
        {
            get { return DataObjs(Sop.ModifyAll); }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// All tab
        /// </summary>
        private static string _tab = @" AnaAptSearchItem
                                        AnaAptSearch
                                        AnaCompareItem
                                        AnaCompare
                                        AnaHomeLotSearchItem
                                        AnaHomeLotSearche
                                        AnaPageInfo
                                        AnaPageLog
                                        AnaRatingConfig
                                        AnaRecentActivitie
                                        AnaReportLog
                                        AnaSessionInfo
                                        AnaUserSiteTemp
                                        AnaUserTemp
                                        HisCommon
                                        HisSiteEven
                                        HisSiteOdd
                                        SynAccountLog
                                        SynAgencie
                                        SynAgencyProperty
                                        SynApartmentLog
                                        SynApartment
                                        SynAppointment
                                        SynEmailAttachment
                                        SynEmailLog
                                        SynEmailTemplate
                                        SynEnquire
                                        SynFloorLevel
                                        SynGroupMember
                                        SynGroup
                                        SynHomeLotLog
                                        SynHomeLot
                                        SynHomePartner
                                        SynHomePlan
                                        SynHouseType
                                        SynMediaAlbum
                                        SynMediaFile
                                        SynNews
                                        SynNotificationJob
                                        SynPriceChange
                                        SynProfile
                                        SynUserMeta
                                        SynUserSite
                                        SysAccountLog
                                        SysApp
                                        SysAuth
                                        SysCode
                                        SysConfigSync
                                        SysErrorLog
                                        SysGroupMember
                                        SysGroup
                                        SysProfile
                                        SysRole
                                        SysSchedulerLog
                                        SysScheduler
                                        SysSiteManage
                                        SysSite
                                        SysTab
                                        SysType
                                        SysUser
                                        Dashboard
                                        SiteTraffic
                                        PageTraffic
                                        Overview
                                        JourneyMap
                                        Sales
                                        Opportunities
                                        CusMarketingConversions
                                        CusIndividualUsers
                                        CusAgentInsightOverview
                                        CusInventory
                                        CusPageDataOverview
                                        CusApartments
                                        CusFloorLevels
                                        CusBuildings
                                        CusAmenities
                                        CusMediaGallery
                                        CusSettings
                                        SalesMasterPlan
                                        OpportunityPlan";

        #endregion
    }
}
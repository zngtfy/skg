﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Label data transfer object
    /// </summary>
    public class LabelDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="title">Title</param>
        public LabelDto(string title)
        {
            if (title == null)
            {
                title = string.Empty;
            }

            Title = title;
            Filter = "Advanced Filters";
            Processing = "Processing...";
            Search = "Search...";
            GoBack = "Go Back";
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Label advanced filters
        /// </summary>
        public string Filter { get; private set; }

        /// <summary>
        /// Label processing
        /// </summary>
        public string Processing { get; private set; }

        /// <summary>
        /// Placeholder search
        /// </summary>
        public string Search { get; private set; }

        /// <summary>
        /// Label go back
        /// </summary>
        public string GoBack { get; set; }

        /// <summary>
        /// Label redirect to update page
        /// </summary>
        public string Update
        {
            get
            {
                return $"Update {Title}";
            }
        }

        /// <summary>
        /// Label redirect to create page
        /// </summary>
        public string Create
        {
            get
            {
                return $"Create {Title}";
            }
        }

        /// <summary>
        /// Label back to list page
        /// </summary>
        public string List
        {
            get
            {
                return $"{Title} List";
            }
        }

        /// <summary>
        /// Label detail
        /// </summary>
        public string Detail
        {
            get
            {
                return $"{Title} Detail";
            }
        }

        #endregion
    }
}
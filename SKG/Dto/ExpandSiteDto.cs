﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-17 15:11
 * Update       : 2020-Aug-17 15:11
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System.Collections.Generic;

namespace SKG.Dto
{
    using SKG.Ext;

    /// <summary>
    /// Site expand data transfer object
    /// </summary>
    public class ExpandSiteDto : IdDto
    {
        #region -- Overrides --

        /// <summary>
        /// ID
        /// </summary>
        [JsonIgnore]
        public override uint Id { get => base.Id; set => base.Id = value; }

        /// <summary>
        /// Returns a string that represents the current object
        /// </summary>
        /// <returns>A string that represents the current object</returns>
        public override string ToString()
        {
            return this.ToJson();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ExpandSiteDto() : base()
        {
            ListingStatuses = new List<DictionDto>();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        public ExpandSiteDto(uint i) : base(i) { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Listing status
        /// </summary>
        public List<DictionDto> ListingStatuses { get; set; }

        #endregion
    }
}
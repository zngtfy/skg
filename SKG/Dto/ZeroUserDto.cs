﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2022-May-12 02:40
 * Update       : 2022-May-12 02:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using static SKG.ZEnum;

namespace SKG.Dto
{
    /// <summary>
    /// ZeroUser data transfer object
    /// </summary>
    public class ZeroUserDto : IdDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroUserDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Site ID
        /// </summary>
        public uint SiteId { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Full ame
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Allow log as same user name beetwen MA and MS
        /// </summary>
        public bool LogAs { get; set; }

        /// <summary>
        /// User type
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// User type code
        /// </summary>
        public string UserTypeCode { get { return UserType.ToString(); } }

        /// <summary>
        /// User role
        /// </summary>
        public UserRole UserRole { get; set; }

        /// <summary>
        /// User role code
        /// </summary>
        public string UserRoleCode { get { return UserRole.ToString(); } }

        /// <summary>
        /// Master admin
        /// </summary>
        public bool MasterAdmin { get { return UserType == UserType.Master && UserRole == UserRole.Admin; } }

        /// <summary>
        /// Master user
        /// </summary>
        public bool MasterUser { get { return UserType == UserType.Master && UserRole == UserRole.Master; } }

        /// <summary>
        /// Lead agent
        /// </summary>
        public bool SuperHead { get { return UserType == UserType.Super && UserRole == UserRole.Head; } }

        /// <summary>
        /// Agent or super
        /// </summary>
        public bool SuperAgent { get { return UserType == UserType.Super && UserRole == UserRole.Super; } }

        /// <summary>
        /// Standard suite
        /// </summary>
        public bool StandardSuite { get { return UserType == UserType.Super && UserRole == UserRole.Sales; } }

        /// <summary>
        /// Standard user
        /// </summary>
        public bool StandardUser { get { return UserType == UserType.Standard && UserRole == UserRole.General; } }

        /// <summary>
        /// Standard subuser
        /// </summary>
        public bool StandardSub { get { return UserType == UserType.Standard && UserRole == UserRole.Sub; } }

        /// <summary>
        /// Is master user
        /// </summary>
        public bool IsMaster { get { return UserType == UserType.Master; } }

        /// <summary>
        /// Is agent user
        /// </summary>
        public bool IsAgent { get { return UserType == UserType.Super; } }

        /// <summary>
        /// Is standard user
        /// </summary>
        public bool IsStandard { get { return UserType == UserType.Standard; } }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-15 16:24
 * Update       : 2021-May-11 16:57
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;

namespace SKG.Dto
{
    /// <summary>
    /// Base token data transfer object
    /// </summary>
    public class BaseTokenDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public BaseTokenDto()
        {
            TokenType = "Bearer";
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="accessToken">Access token</param>
        public BaseTokenDto(string accessToken) : this()
        {
            AccessToken = accessToken;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Token type
        /// </summary>
        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }

        /// <summary>
        /// Access token
        /// </summary>
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Instance URL
        /// </summary>
        [JsonProperty(PropertyName = "instance_url")]
        public string InstanceUrl { get; set; }

        #endregion
    }
}
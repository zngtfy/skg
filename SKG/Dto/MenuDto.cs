﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-13 20:54
 * Update       : 2020-Aug-13 20:54
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    using DAL.Models;

    /// <summary>
    /// Menu data transfer object
    /// </summary>
    public class MenuDto : IdDto, IMenu
    {
        #region -- Implements --

        /// <summary>
        /// Icon
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Controller name
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Action name
        /// </summary>
        public string Action { get; set; }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public MenuDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Developer name
        /// </summary>
        public string DevName { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Parent ID
        /// </summary>
        public uint? ParentId { get; set; }

        /// <summary>
        /// App ID
        /// </summary>
        public uint AppId { get; set; }

        /// <summary>
        /// Can click
        /// </summary>
        public bool CanClick { get; set; }

        /// <summary>
        /// Default tab open
        /// </summary>
        public bool Default { get; set; }

        /// <summary>
        /// Hidden
        /// </summary>
        public bool Hidden { get; set; }

        /// <summary>
        /// Option right
        /// </summary>
        public object OptionRight { get; set; }

        #endregion
    }
}
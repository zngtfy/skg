﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Sep-30 15:40
 * Update       : 2020-Sep-30 15:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections.Generic;

namespace SKG.Dto
{
    /// <summary>
    /// Authentication site data transfer object
    /// </summary>
    public class AuthSiteDto : IdDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        public AuthSiteDto(uint i) : base(i) { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Site UID
        /// </summary>
        public Guid SiteUid { get; set; }

        /// <summary>
        /// URLs
        /// </summary>
        public List<string> Urls { get; set; }

        /// <summary>
        /// API key
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// API hash
        /// </summary>
        public string ApiHash { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-21 11:40
 * Update       : 2020-Jul-21 11:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Timeout data transfer object
    /// </summary>
    public class TimeoutDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public TimeoutDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Default session timeout (minute)
        /// </summary>
        public uint Session { get; set; }

        /// <summary>
        /// Default  cookie timeout (minute)
        /// </summary>
        public uint Cookie { get; set; }

        /// <summary>
        /// Session stores in cookie (minute)
        /// </summary>
        public uint SessionCookie { get; set; }

        #endregion
    }
}
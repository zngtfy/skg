﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-09 05:14
 * Update       : 2020-Aug-09 05:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;

namespace SKG.Dto
{
    /// <summary>
    /// Title data transfer object
    /// </summary>
    public class TitleDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="title">Title</param>
        public TitleDto(string title)
        {
            if (title == null)
            {
                title = string.Empty;
            }

            Label = title;
            Subtitles = new List<string>();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Label
        /// </summary>
        public string Label { get; private set; }

        /// <summary>
        /// Subtitles
        /// </summary>
        public List<string> Subtitles { get; set; }

        #endregion
    }
}
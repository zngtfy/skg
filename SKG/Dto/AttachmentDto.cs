﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-Feb-05 10:35
 * Update       : 2021-Feb-05 10:35
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Mail attachment data transfer object
    /// </summary>
    public class AttachmentDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public AttachmentDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="name">file name</param>
        /// <param name="content">file content (base64 string)</param>
        public AttachmentDto(string name, string content)
        {
            FileName = name;
            FileContent = content;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// File name
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// File content
        /// </summary>
        public string FileContent { get; set; }

        #endregion
    }
}
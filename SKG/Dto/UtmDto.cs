﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-08 15:08
 * Update       : 2020-Jul-10 14:41
 * Checklist    : 1.0
 * Status       : Done
 */
#endregion

using System.Collections.Generic;
using System.Linq;

namespace SKG.Dto
{
    using Ext;
    using static ZEnum;
    using Zhar = ZConst.Char;
    using Ztring = ZConst.String;

    /// <summary>
    /// https://en.wikipedia.org/wiki/UTM_parameters
    /// </summary>
    public class UtmDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public UtmDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="url">URL with query</param>
        public UtmDto(string url)
        {
            // Get query string
            var q = string.Empty;
            var l = url.ToSet(Ztring.Question);
            if (l.Count == 2)
            {
                q = l[1];
            }
            if (l.Count == 1)
            {
                q = l[0];
            }

            var dic = new Dictionary<string, string>();
            try
            {
                dic = q.Split(Zhar.Ampersand).Select(p => p.Split(Zhar.Equal)).Where(p => p.Length == 2).ToDictionary(p => p[0], sp => sp[1]);
            }
            catch { }

            #region -- UTM --
            dic.TryGetValue(Prefix + nameof(Source).ToLower(), out string val);
            Source = val;

            dic.TryGetValue(Prefix + nameof(Medium).ToLower(), out val);
            Medium = val;

            dic.TryGetValue(Prefix + nameof(Campaign).ToLower(), out val);
            Campaign = val;

            dic.TryGetValue(Prefix + nameof(Term).ToLower(), out val);
            Term = val;

            dic.TryGetValue(Prefix + nameof(Content).ToLower(), out val);
            Content = val;
            #endregion

            #region -- Traffic type --
            Traffic = Traffic.Direct;

            dic.TryGetValue(Google, out val);
            if (!string.IsNullOrEmpty(val))
            {
                Traffic = Traffic.Google;
            }

            dic.TryGetValue(GoogleAds, out val);
            if (!string.IsNullOrEmpty(val))
            {
                Traffic = Traffic.Google;
            }

            dic.TryGetValue(DoubleClick, out val);
            if (!string.IsNullOrEmpty(val))
            {
                Traffic = Traffic.DoubleClick;
            }

            dic.TryGetValue(Facebook, out val);
            if (!string.IsNullOrEmpty(val))
            {
                Traffic = Traffic.Facebook;
            }

            dic.TryGetValue(Zanox, out val);
            if (!string.IsNullOrEmpty(val))
            {
                Traffic = Traffic.Zanox;
            }
            #endregion
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="url">URL with query</param>
        /// <param name="ref">Referer</param>
        public UtmDto(string url, string @ref) : this(url)
        {
            Referer = @ref;

            if (!string.IsNullOrEmpty(@ref))
            {
                if (@ref.Contains(UtmDto.GoogleSearch))
                {
                    Traffic = Traffic.Google;
                }
                if (@ref.Contains(UtmDto.DuckDuckGoSearch))
                {
                    Traffic = Traffic.DuckDuckGo;
                }
                if (@ref.Contains(UtmDto.YandexSearch))
                {
                    Traffic = Traffic.Yandex;
                }
                if (@ref.Contains(UtmDto.BingSearch))
                {
                    Traffic = Traffic.Bing;
                }
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Source
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Medium
        /// </summary>
        public string Medium { get; set; }

        /// <summary>
        /// Campaign
        /// </summary>
        public string Campaign { get; set; }

        /// <summary>
        /// Term
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Referer
        /// </summary>
        public string Referer { get; set; }

        /// <summary>
        /// Traffic type
        /// </summary>
        public Traffic Traffic { get; set; }

        #endregion

        #region -- Constants --

        /// <summary>
        /// Google click identifier
        /// </summary>
        private const string Google = "gclid";

        /// <summary>
        /// Google Ads
        /// </summary>
        private const string GoogleAds = "gclsrc";

        /// <summary>
        /// DoubleClick click identifier, now Google
        /// </summary>
        private const string DoubleClick = "dclid";

        /// <summary>
        /// Facebook click identifier
        /// </summary>
        private const string Facebook = "fbclid";

        /// <summary>
        /// Zanox click identifier, now Awin
        /// </summary>
        private const string Zanox = "zanpid";

        /// <summary>
        /// Prefix
        /// </summary>
        private const string Prefix = "utm_";

        /// <summary>
        /// Google search
        /// </summary>
        private const string GoogleSearch = "google.com";

        /// <summary>
        /// DuckDuckGo search
        /// </summary>
        private const string DuckDuckGoSearch = "duckduckgo.com";

        /// <summary>
        /// Yandex search
        /// </summary>
        private const string YandexSearch = "yandex.com";

        /// <summary>
        /// Bing search
        /// </summary>
        private const string BingSearch = "bing.com";

        #endregion
    }
}
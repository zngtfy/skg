﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-05 22:44
 * Update       : 2020-Aug-13 14:22
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;
using static SKG.Dto.PrivilegeDto;

namespace SKG.Dto
{
    /// <summary>
    /// Navigation data transfer object
    /// </summary>
    public class NavDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="s">Sequence</param>
        public NavDto(string n, ushort s)
        {
            Name = n;
            Seq = s;
            Tabs = new List<Tab>();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Sequence
        /// </summary>
        public ushort Seq { get; set; }

        /// <summary>
        /// Tabs
        /// </summary>
        public List<Tab> Tabs { get; set; }

        #endregion

        #region -- Classes --

        /// <summary>
        /// Tab item
        /// </summary>
        public class Tab : IdDto
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="i">ID</param>
            /// <param name="s">Sequence</param>
            public Tab(uint i, ushort s) : base(i)
            {
                Seq = s;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Sequence
            /// </summary>
            public ushort Seq { get; set; }

            /// <summary>
            /// Visible
            /// </summary>
            public Visible? Visible { get; set; }

            /// <summary>
            /// Default tab open
            /// </summary>
            public bool? Default { get; set; }

            /// <summary>
            /// Option right
            /// </summary>
            public object OptionRight { get; set; }

            #endregion
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-15 16:24
 * Update       : 2021-May-11 16:57
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;

namespace SKG.Dto
{
    /// <summary>
    /// Salesforce token data transfer object
    /// </summary>
    public class SfTokenDto : BaseTokenDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SfTokenDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="accessToken">Access token</param>
        public SfTokenDto(string accessToken) : this()
        {
            AccessToken = accessToken;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Issued at
        /// </summary>
        [JsonProperty(PropertyName = "issued_at")]
        public string IssuedAt { get; set; }

        /// <summary>
        /// Signature
        /// </summary>
        [JsonProperty(PropertyName = "signature")]
        public string Signature { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-May-25 14:47
 * Update       : 2021-May-25 14:47
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;

namespace SKG.Dto
{
    /// <summary>
    /// Agentbox object data transfer object
    /// </summary>
    public class AgbObjectDto : ZeroDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public AgbObjectDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// ID
        /// </summary>
        [JsonProperty("id")]
        public new string ExternalId { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public new string Status { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        [JsonProperty("firstCreated")]
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        [JsonProperty("lastModified")]
        public DateTime? LastModifiedDate { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-05 22:44
 * Update       : 2020-Aug-13 14:22
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Id data transfer object
    /// </summary>
    public abstract class IdDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public IdDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        public IdDto(uint i)
        {
            Id = i;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// ID
        /// </summary>
        public virtual uint Id { get; set; }

        #endregion
    }
}
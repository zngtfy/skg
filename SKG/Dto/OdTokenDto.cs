﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-15 16:24
 * Update       : 2020-Jul-15 16:24
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;

namespace SKG.Dto
{
    /// <summary>
    /// OneDrive token data transfer object
    /// </summary>
    public class OdTokenDto : BaseTokenDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public OdTokenDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Scope
        /// </summary>
        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }

        /// <summary>
        /// Expires in
        /// </summary>
        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }

        /// <summary>
        /// Ext expires in
        /// </summary>
        [JsonProperty(PropertyName = "ext_expires_in")]
        public int ExtExpiresIn { get; set; }

        #endregion
    }
}
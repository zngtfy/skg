﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-01 12:02
 * Update       : 2020-Jul-01 12:02
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Diction data transfer object
    /// </summary>
    public class DictionDto : IdDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public DictionDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        public DictionDto(uint i) : base(i)
        {
            Key = i.ToString();
            Value = i;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="k">Key</param>
        /// <param name="v">Value</param>
        public DictionDto(string k, object v)
        {
            Key = k;
            Value = v;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="k">Key</param>
        /// <param name="v">Value</param>
        /// <param name="type">Data type: Apartment/Lot/Home</param>
        public DictionDto(string k, object v, string type)
        {
            Key = k;
            Value = v;
            DataType = type;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Data type
        /// </summary>
        public string DataType { get; set; } = string.Empty;

        #endregion
    }
}
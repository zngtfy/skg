﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-13 20:54
 * Update       : 2020-Aug-13 20:54
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.DAL.Models
{
    /// <summary>
    /// Interface user session model
    /// </summary>
    public interface IUserSession
    {
        #region -- Properties --

        /// <summary>
        /// Client ID
        /// </summary>
        Guid ClientId { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        Guid SessionId { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// User ID
        /// </summary>
        uint? UserId { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-05 22:44
 * Update       : 2020-Nov-04 17:39
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using SKG.Ext;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKG.Dto
{
    using static ZConst;
    using static ZEnum;

    /// <summary>
    /// Zero data transfer object
    /// </summary>
    public class ZeroDto : IdDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZeroDto() : base(0)
        {
            Name = string.Empty;
            StatusText = string.Empty;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        public ZeroDto(string n) : base(0)
        {
            Name = n;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroDto(string n, string d) : base(0)
        {
            Name = n;
            DevName = d;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="i">ID</param>
        /// <param name="n">Name</param>
        /// <param name="d">Developer name</param>
        public ZeroDto(uint i, string n, string d) : base(i)
        {
            Name = n;
            DevName = d;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(MaxSize.Name)]
        [DisplayName("Name")]
        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = string.Empty;
                }
                _name = value;
            }
        }

        /// <summary>
        /// Developer name
        /// </summary>
        [StringLength(MaxSize.DevName)]
        [DisplayName("Developer Name")]
        public virtual string DevName
        {
            get
            {
                return _devName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _devName = _name.ToPascalCase();
                }
                else
                {
                    _devName = value;
                }
            }
        }

        /// <summary>
        /// Status
        /// </summary>
        [DisplayName("Status")]
        public Status Status { get; set; }

        /// <summary>
        /// External ID
        /// </summary>
        [DisplayName("External ID")]
        public string ExternalId { get; set; }

        /// <summary>
        /// External source
        /// </summary>
        [DisplayName("External Source")]
        public string ExternalSrc { get; set; }

        /// <summary>
        /// Tag
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Status text with css
        /// </summary>
        [NotMapped]
        [DisplayName("Status")]
        public virtual string StatusText { get; set; }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Name
        /// </summary>
        private string _name;

        /// <summary>
        /// Developer name
        /// </summary>
        private string _devName;

        #endregion
    }
}
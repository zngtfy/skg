﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2022-Apr-15 10:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Sort data transfer object
    /// </summary>
    public sealed class SortDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SortDto()
        {
            Field = string.Empty;
            Direction = Ascending;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="field">Field name</param>
        public SortDto(string field)
        {
            Field = field;
            Direction = Ascending;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="field">Field name</param>
        /// <param name="direction">Direction code [ASC or DESC]</param>
        public SortDto(string field, string direction)
        {
            Field = field;
            Direction = direction;
        }

        /// <summary>
        /// Correct direction
        /// </summary>
        /// <param name="s">Input value</param>
        /// <returns>Return the result</returns>
        private string CorrectDirection(string s)
        {
            s = $"{s}".ToUpper();

            if (Ascending != s && Descending != s)
            {
                s = Ascending;
            }

            return s;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Field name
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// Direction code [ASC or DESC]
        /// </summary>
        public string Direction
        {
            get
            {
                return direction;
            }
            set
            {
                direction = CorrectDirection(value);
            }
        }

        /// <summary>
        /// Direction code [ASC or DESC] (support Kendo)
        /// </summary>
        public string Dir
        {
            get
            {
                return direction;
            }
            set
            {
                direction = CorrectDirection(value);
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Direction
        /// </summary>
        private string direction;

        #endregion

        #region -- Constants --

        /// <summary>
        /// Ascending
        /// </summary>
        public const string Ascending = "ASC";

        /// <summary>
        /// Descending
        /// </summary>
        public const string Descending = "DESC";

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-21 11:40
 * Update       : 2021-Apr-16 05:31
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    using DAL.Models;

    /// <summary>
    /// Authentication data transfer object
    /// </summary>
    public class AuthenticationDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public AuthenticationDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="m">The modal</param>
        public AuthenticationDto(SysAuth m)
        {
            if (m != null)
            {
                Host = m.ApiUrl;
                ClientId = m.ApiKey;
                Secret = m.ApiHash;
                UserName = m.UserName;
                Password = m.Password;
                AccountId = m.AccountId;
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Type
        /// </summary>
        public ZEnum.External? Type { get; set; }

        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Client ID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Secret
        /// </summary>
        public string Secret { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Account ID (for Rexsoftware)
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Salesforce setting
        /// </summary>
        public bool HasSalesforce
        {
            get
            {
                var a = string.IsNullOrWhiteSpace(Host);
                var b = string.IsNullOrWhiteSpace(ClientId);
                var c = string.IsNullOrWhiteSpace(Secret);
                var d = string.IsNullOrWhiteSpace(UserName);
                var e = string.IsNullOrWhiteSpace(Password);
                var f = a || b || c || d || e;
                var res = !f && Type == ZEnum.External.Salesforce;
                return res;
            }
        }

        /// <summary>
        /// Propertybase webhook
        /// </summary>
        public bool HasPropertybase
        {
            get
            {
                var a = string.IsNullOrWhiteSpace(Host);
                var e = string.IsNullOrWhiteSpace(Password);
                var f = a || e;
                var res = !f && Type == ZEnum.External.Salesforce;
                return res;
            }
        }

        /// <summary>
        /// Rexsoftware setting
        /// </summary>
        public bool HasRexsoftware
        {
            get
            {
                var a = string.IsNullOrWhiteSpace(Host);
                var b = string.IsNullOrWhiteSpace(AccountId);
                var d = string.IsNullOrWhiteSpace(UserName);
                var e = string.IsNullOrWhiteSpace(Password);
                var f = a || b || d || e;
                var res = !f && Type == ZEnum.External.Rexsoftware;
                return res;
            }
        }

        /// <summary>
        /// Agentbox setting
        /// </summary>
        public bool HasAgentbox
        {
            get
            {
                var a = string.IsNullOrWhiteSpace(Host);
                var b = string.IsNullOrWhiteSpace(ClientId);
                var c = string.IsNullOrWhiteSpace(Secret);
                var f = a || b || c;
                var res = !f && Type == ZEnum.External.Agentbox;
                return res;
            }
        }

        /// <summary>
        /// Mailchimp setting
        /// </summary>
        public bool HasMailchimp
        {
            get
            {
                var a = string.IsNullOrWhiteSpace(Host);
                var d = string.IsNullOrWhiteSpace(UserName);
                var e = string.IsNullOrWhiteSpace(Password);
                var f = a || d || e;
                var res = !f && Type == ZEnum.External.Mailchimp;
                return res;
            }
        }

        #endregion
    }
}
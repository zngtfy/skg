﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-May-07 09:39
 * Update       : 2021-May-07 09:39
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;

namespace SKG.Dto
{
    using Converter;
    using DAL.Models;
    using static ZConst;

    /// <summary>
    /// Rexsoftware object data transfer object
    /// </summary>
    public class RexObjectDto : ZeroDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public RexObjectDto() { }

        /// <summary>
        /// To model
        /// </summary>
        /// <returns>Return the result</returns>
        public virtual SysCode ToModel()
        {
            var res = new SysCode
            {
                Name = Name,
                ExternalId = ExternalId,
                ExternalSrc = "Rexsoftware",
                Tag = Tag,
                CreatedOn = CreatedDate == null ? DateTime.UtcNow : CreatedDate.Value,
                ModifiedOn = LastModifiedDate,
                Status = ZEnum.Status.Enabled
            };

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// ID
        /// </summary>
        [JsonProperty("id")]
        public new string ExternalId { get; set; }

        /// <summary>
        /// Entity tag
        /// </summary>
        [JsonProperty("etag")]
        public new string Tag { get; set; }

        /// <summary>
        /// System record state
        /// </summary>
        [JsonProperty("system_record_state")]
        public string SystemRecordState { get; set; }

        /// <summary>
        /// Related
        /// </summary>
        public object Related { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        [JsonProperty("system_ctime")]
        [JsonConverter(typeof(TimeFormatConverter), Format.Ymd)]
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        [JsonProperty("system_modtime")]
        [JsonConverter(typeof(TimeFormatConverter), Format.Ymd)]
        public DateTime? LastModifiedDate { get; set; }

        #endregion
    }
}
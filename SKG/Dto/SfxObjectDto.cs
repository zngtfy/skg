﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-May-07 09:39
 * Update       : 2021-May-07 09:39
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;

namespace SKG.Dto
{
    using DAL.Models;
    using static ZEnum;

    /// <summary>
    /// Salesforce object data transfer object
    /// </summary>
    public class SfxObjectDto : ZeroDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SfxObjectDto() { }

        /// <summary>
        /// To model
        /// </summary>
        /// <param name="typeId">Type ID</param>
        /// <param name="group">Group</param>
        /// <returns>Return the result</returns>
        public virtual SysCode ToModel(uint? typeId, string group)
        {
            var res = new SysCode
            {
                Name = Name,
                DevName = DevName,
                ExternalId = ExternalId,
                ExternalSrc = External.Salesforce.ToString(),
                Tag = Tag,
                CreatedOn = CreatedDate,
                ModifiedOn = LastModifiedDate,
                Status = ZEnum.Status.Enabled,

                TypeId = typeId,
                Group = group
            };

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// ID
        /// </summary>
        [JsonProperty("Id")]
        public new string ExternalId { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        #endregion
    }
}
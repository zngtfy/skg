﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-Sep-19 22:40
 * Update       : 2021-Sep-19 22:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// JWT data transfer object
    /// </summary>
    public class JwtDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public JwtDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// JWT signing
        /// </summary>
        public string Signing
        {
            get
            {
                if (string.IsNullOrEmpty(_signing))
                {
                    _signing = ZVariable.JwtSigning;
                }

                return _signing;
            }
            set
            {
                _signing = value;
            }
        }

        /// <summary>
        /// JWT issuer
        /// </summary>
        public string Issuer
        {
            get
            {
                if (string.IsNullOrEmpty(_issuer))
                {
                    _issuer = ZVariable.JwtIssuer;
                }

                return _issuer;
            }
            set
            {
                _issuer = value;
            }
        }

        /// <summary>
        /// JWT audience
        /// </summary>
        public string Audience
        {
            get
            {
                if (string.IsNullOrEmpty(_audience))
                {
                    _audience = ZVariable.JwtAudience;
                }

                return _audience;
            }
            set
            {
                _audience = value;
            }
        }

        /// <summary>
        /// Time to live of AccessToken (JWT) [2 - 1440] minutes
        /// </summary>
        public int Time
        {
            get
            {
                if (_time == 0)
                {
                    _time = ZVariable.JwtTime;
                }

                return _time;
            }
            set
            {
                if (value < 2)
                {
                    value = 2;
                }

                if (value > 1440)
                {
                    value = 1440;
                }

                _time = value;
            }
        }

        /// <summary>
        /// Time to live of RefreshToken [10 - 10080] minutes
        /// </summary>
        public int TimeRt
        {
            get
            {
                if (_timeRt == 0)
                {
                    _timeRt = 10080;
                }

                return _timeRt;
            }
            set
            {
                if (value < 10)
                {
                    value = 10;
                }

                if (value > 10080)
                {
                    value = 10080;
                }

                _timeRt = value;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// JWT signing
        /// </summary>
        private string _signing;

        /// <summary>
        /// JWT issuer
        /// </summary>
        private string _issuer;

        /// <summary>
        /// JWT audience
        /// </summary>
        private string _audience;

        /// <summary>
        /// Time to live of AccessToken (JWT)
        /// </summary>
        private int _time;

        /// <summary>
        /// Time to live of RefreshToken
        /// </summary>
        public int _timeRt;

        #endregion
    }
}
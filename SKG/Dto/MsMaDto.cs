﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Dec-10 08:03
 * Update       : 2020-Dec-10 08:03
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Diction data transfer object
    /// </summary>
    public class MsMaDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public MsMaDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="msId">Matcha site ID (Matcha.Home)</param>
        /// <param name="maId">Matcha analytics ID (Matcha.Analysis)</param>
        public MsMaDto(uint msId, uint maId)
        {
            MsId = msId;
            MaId = maId;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Matcha site ID (Matcha.Home)
        /// </summary>
        public uint MsId { get; set; }

        /// <summary>
        /// Matcha analytics ID (Matcha.Analysis)
        /// </summary>
        public uint MaId { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-05 22:44
 * Update       : 2022-May-12 02:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using static SKG.ZEnum;

namespace SKG.Dto
{
    using Ext;
    using static Dto.PrivilegeDto;

    /// <summary>
    /// Authority data transfer object
    /// </summary>
    public class AuthorityDto : ZeroUserDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public AuthorityDto()
        {
            SetDefault();
            Privilege = new PrivilegeDto();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="p">Privilege</param>
        public AuthorityDto(PrivilegeDto p)
        {
            SetDefault();

            if (p != null)
            {
                Privilege = p;
            }
            else
            {
                Privilege = new PrivilegeDto();
            }
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="p">Privilege</param>
        /// <param name="m">Menu (Item1 is menu, Item2 is default profile's app ID, Item3 is profile's app IDs)</param>
        public AuthorityDto(PrivilegeDto p, Tuple<List<MenuDto>, uint, List<uint>> m) : this(p)
        {
            if (m == null)
            {
                m = new Tuple<List<MenuDto>, uint, List<uint>>(new List<MenuDto>(), 0, new List<uint> { 0 });
            }

            Menus = m.Item1;
            AppId = m.Item2;
            AppIds = m.Item3;
        }

        /// <summary>
        /// Get privilege of object setting
        /// </summary>
        /// <param name="n">Object name</param>
        /// <returns>Return the result</returns>
        public Sop GetObjPrivilege(string n)
        {
            var res = Sop.None;

            if (Privilege == null)
            {
                return res;
            }

            var t = Privilege.Objs.FirstOrDefault(p => p.DevName == n);
            if (t != null)
            {
                res = t.AccessRight;
            }

            return res;
        }

        /// <summary>
        /// Get permission
        /// </summary>
        /// <param name="devName">Object developer name</param>
        /// <returns>Return the result</returns>
        public SopExt GetPermis(string devName)
        {
            var res = new SopExt(Sop.None, devName, null);

            var t = Privilege.Objs.FirstOrDefault(p => p.DevName == devName);
            if (t != null)
            {
                res = t.Permis;
                res.Options = t.OptionRight;
            }

            return res;
        }

        /// <summary>
        /// Set default data
        /// </summary>
        private void SetDefault()
        {
            var s = string.Empty;
            UserName = s;
            FullName = s;
            Email = s;
            Online = true;

            Menus = new List<MenuDto>();
            AppIds = new List<uint>();
            Sites = new List<SteSetting>();
            Delegators = new List<DelegationDto>();

            FunctionUsers = new List<ZeroDto>();
            FunctionSites = new List<ZeroDto>();
            FunctionApartments = new List<ZeroDto>();
            FunctionMasterplans = new List<ZeroDto>();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Privileges
        /// </summary>
        public PrivilegeDto Privilege { get; private set; }

        /// <summary>
        /// Menus of all app
        /// </summary>
        public List<MenuDto> Menus { get; set; }

        /// <summary>
        /// Current app ID selected
        /// </summary>
        public uint AppId { get; set; }

        /// <summary>
        /// App IDs
        /// </summary>
        public List<uint> AppIds { get; set; }

        /// <summary>
        /// Site UID
        /// </summary>
        public Guid SiteUid { get; set; }

        /// <summary>
        /// Site URL
        /// </summary>
        public string SiteUrl { get; set; }

        /// <summary>
        /// Site API URL
        /// </summary>
        public string SiteApiUrl { get; set; }

        /// <summary>
        /// Current site developer name selected
        /// </summary>
        public string SiteDevName { get; set; }

        /// <summary>
        /// Current site name selected
        /// </summary>
        public string SiteName
        {
            get { return _siteName.ToStr(); }
            set { _siteName = value; }
        }

        /// <summary>
        /// Type plan
        /// </summary>
        public TypePlan TypePlan { get; set; }

        /// <summary>
        /// CRM
        /// </summary>
        public External Crm { get; set; }

        /// <summary>
        /// Is administrator
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Is default administrator
        /// </summary>
        public bool IsDefaultAdmin
        {
            get
            {
                var un = UserName.ToStr(Format.Lower);
                var res = IsAdmin && un == ZConst.SaUn;
                return res;
            }
        }

        /// <summary>
        /// Agent ID
        /// </summary>
        public uint? AgentId { get; set; }

        /// <summary>
        /// Super ID
        /// </summary>
        public uint? SuperId { get; set; }

        /// <summary>
        /// Agency ID (for agent user)
        /// </summary>
        public uint? AgencyId { get; set; }

        /// <summary>
        /// Agency name
        /// </summary>
        public string AgencyName { get; set; }

        /// <summary>
        /// Agent full name
        /// </summary>
        public string AgentFullName { get; set; }

        /// <summary>
        /// Agent email
        /// </summary>
        public string AgentEmail { get; set; }

        /// <summary>
        /// Sites
        /// </summary>
        public List<SteSetting> Sites { get; set; }

        /// <summary>
        /// Delegators
        /// </summary>
        public List<DelegationDto> Delegators { get; set; }

        /// <summary>
        /// User functions
        /// </summary>
        public List<ZeroDto> FunctionUsers { get; set; }

        /// <summary>
        /// Site functions
        /// </summary>
        public List<ZeroDto> FunctionSites { get; set; }

        /// <summary>
        /// Apartment functions
        /// </summary>
        public List<ZeroDto> FunctionApartments { get; set; }

        /// <summary>
        /// Masterplan functions
        /// </summary>
        public List<ZeroDto> FunctionMasterplans { get; set; }

        /// <summary>
        /// Listing status
        /// </summary>
        public List<DictionDto> ListingStatuses { get; set; }

        /// <summary>
        /// Online
        /// </summary>
        public bool Online { get; set; }

        /// <summary>
        /// Last online
        /// </summary>
        public DateTime LastOnline { get; set; }

        /// <summary>
        /// Timezone offset (minute)
        /// </summary>
        public int TimezoneOffset { get; set; }

        /// <summary>
        /// Current time
        /// </summary>
        public DateTime CurrentTime
        {
            get
            {
                return DateTime.UtcNow.AddMinutes(-TimezoneOffset);
            }
        }

        /// <summary>
        /// Client ID
        /// </summary>
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public Guid? SessionId { get; set; }

        /// <summary>
        /// Apps
        /// </summary>
        [JsonIgnore]
        public List<DictionDto> Apps
        {
            get
            {
                var q = Menus.Where(p => p.ParentId == null);

                if (AppIds.Count > 0)
                {
                    q = q.Where(p => AppIds.Contains(p.Id));
                }

                var res = q.OrderBy(p => p.Name).Select(p => new DictionDto(p.Id.ToStr(), p.Name) { Id = p.Id }).ToList();
                return res;
            }
        }

        /// <summary>
        /// Object's privilege
        /// </summary>
        [JsonIgnore]
        public List<SopExt> PrivilegeObjs
        {
            get
            {
                if (Privilege == null)
                {
                    return null;
                }

                var res = Privilege.Objs.Select(p => new SopExt(p.AccessRight, p.DevName, p.OptionRight)).ToList();
                return res;
            }
        }

        /// <summary>
        /// Site's privilege
        /// </summary>
        [JsonIgnore]
        public List<SspExt> PrivilegeStes
        {
            get
            {
                if (Privilege == null)
                {
                    return null;
                }

                var res = Privilege.Stes.Select(p => new SspExt(p.AccessRight, p.DevName, p.OptionRight)).ToList();
                return res;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Site name
        /// </summary>
        private string _siteName;

        #endregion
    }
}
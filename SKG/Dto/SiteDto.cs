﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-21 11:40
 * Update       : 2020-Jul-21 11:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Site data transfer object
    /// </summary>
    public class SiteDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SiteDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Site title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Site UID
        /// </summary>
        public string SiteUid { get; set; }

        /// <summary>
        /// Base URL
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// API URL
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// API key
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// API hash
        /// </summary>
        public string ApiHash { get; set; }

        /// <summary>
        /// Static access token
        /// </summary>
        public string StaticToken { get; set; }

        /// <summary>
        /// IP allowed
        /// </summary>
        public string IpAllowed { get; set; }

        /// <summary>
        /// External admin
        /// </summary>
        public string ExternalAdmin { get; set; }

        #endregion
    }
}
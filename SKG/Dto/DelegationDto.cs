﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Sep-30 15:40
 * Update       : 2022-May-12 02:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SKG.Dto
{
    using Converter;
    using static ZConst;

    /// <summary>
    /// Delegation data transfer object
    /// </summary>
    public class DelegationDto : ZeroUserDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public DelegationDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Project owner ID has children
        /// </summary>
        public uint RootId { get; set; }

        /// <summary>
        /// Last modified on
        /// </summary>
        [JsonConverter(typeof(DateFormatConverter), Format.YmdHm)]
        public DateTime LastModifiedOn { get; set; }

        /// <summary>
        /// Start time
        /// </summary>
        [JsonConverter(typeof(DateFormatConverter), Format.YmdHm)]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// End time
        /// </summary>
        [JsonConverter(typeof(DateFormatConverter), Format.YmdHm)]
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Menus
        /// </summary>
        public List<string> Menus { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public ZEnum.Status Status { get; set; }

        #endregion
    }
}
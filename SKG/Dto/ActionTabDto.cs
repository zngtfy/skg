﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-18 16:34
 * Update       : 2020-Jun-18 16:34
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Dto
{
    /// <summary>
    /// Action tab data transfer object
    /// </summary>
    public class ActionTabDto : ActionBaseDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ActionTabDto() { }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Client ID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Last active
        /// </summary>
        public DateTime? LastActive { get; set; }

        /// <summary>
        /// Last inactive
        /// </summary>
        public DateTime? LastInactive { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-18 16:34
 * Update       : 2020-Jun-18 16:34
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Dto
{
    /// <summary>
    /// Action base data transfer object
    /// </summary>
    public class ActionBaseDto
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ActionBaseDto() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="o">Action tab DTO</param>
        public ActionBaseDto(ActionTabDto o)
        {
            if (o != null)
            {
                SessionId = o.SessionId;
                Path = o.Path;
                TimeActive = o.TimeActive;
                TimeInactive = o.TimeInactive;
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Session ID
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// Path
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Time active
        /// </summary>
        public uint TimeActive { get; set; }

        /// <summary>
        /// Time inactive
        /// </summary>
        public uint TimeInactive { get; set; }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Jun-04 09:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using SKG.Ext;
using System.Collections.Generic;

namespace SKG.Req
{
    using DAL.Models;

    /// <summary>
    /// Simple request
    /// </summary>
    public class SimpleReq : BaseReq<ZeroNorModel>
    {
        #region -- Overrides --

        /// <summary>
        /// Convert the request to the model
        /// </summary>
        /// <param name="createdBy">Created by</param>
        /// <returns>Return the result</returns>
        public override ZeroNorModel ToModel(string createdBy)
        {
            return new ZeroNorModel();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public SimpleReq() : base() { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="id">ID</param>
        public SimpleReq(uint id) : base(id) { }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="keyword">Keyword</param>
        public SimpleReq(string keyword) : base(keyword) { }

        /// <summary>
        /// Get value from dictionary
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Return the result</returns>
        public object GetObj(string key)
        {
            if (Dictionary == null)
            {
                return null;
            }

            return Dictionary.GetValueOrDefault(key);
        }

        /// <summary>
        /// Get value from dictionary
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Return the result</returns>
        public string GetStr(string key)
        {
            if (Dictionary == null)
            {
                return string.Empty;
            }

            var t = Dictionary.GetValueOrDefault(key);
            return t + string.Empty;
        }

        /// <summary>
        /// Exist key (include dictionary null or empty)
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="cache">Cache</param>
        /// <returns>Return the result</returns>
        public bool ExistKey(string key, bool cache = true)
        {
            if (!cache)
            {
                _rd = RspData;
            }

            var res = _rd == null || _rd.Count == 0 || _rd.Contains(key);
            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Dictionary
        /// </summary>
        public Dictionary<string, object> Dictionary { get; set; }

        /// <summary>
        /// Response data with key = rspData
        /// </summary>
        public List<string> RspData
        {
            get
            {
                var t = GetStr("rspData");
                return t.ToSet();
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Response data
        /// </summary>
        private static List<string> _rd;

        #endregion
    }
}
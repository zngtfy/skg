﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-04 06:09
 * Update       : 2020-Nov-11 14:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG.Req
{
    using DAL.Models;
    using static ZConst;

    /// <summary>
    /// Paged request for GET method (support paging on server)
    /// </summary>
    public class PagedReq : BaseReq<ZeroNorModel>
    {
        #region -- Overrides --

        /// <summary>
        /// Convert the request to the model
        /// </summary>
        /// <param name="createdBy">Created by</param>
        /// <returns>Return the result</returns>
        public override ZeroNorModel ToModel(string createdBy)
        {
            return new ZeroNorModel();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public PagedReq()
        {
            MaxResultCount = Default.PageSize;
            Paging = true;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Allow paging
        /// </summary>
        public bool Paging { get; set; }

        /// <summary>
        /// Current user ID
        /// </summary>
        public uint? UserId { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNum { get; set; }

        /// <summary>
        /// Page size (it is max result count also)
        /// </summary>
        public int PageSize { get { return MaxResultCount; } }

        /// <summary>
        /// Max result count (it is page size also)
        /// </summary>
        public virtual int MaxResultCount { get; set; }

        /// <summary>
        /// Sorting
        /// </summary>
        public virtual string Sorting { get; set; }

        /// <summary>
        /// Skip count
        /// </summary>
        public virtual int SkipCount
        {
            get
            {
                if (PageNum != null)
                {
                    _skipCount = (PageNum.Value - 1) * MaxResultCount;
                }

                return _skipCount;
            }
            set
            {
                if (PageNum == null && MaxResultCount > 0)
                {
                    PageNum = value / MaxResultCount + 1;
                }

                _skipCount = value;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Skip count
        /// </summary>
        private int _skipCount;

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2021-Mar-31 11:58
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.ComponentModel;

namespace SKG.Req
{
    using static ZEnum;

    /// <summary>
    /// Base request
    /// </summary>
    /// <typeparam name="T">Model class type</typeparam>
    public abstract class BaseReq<T>
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public BaseReq()
        {
            Search = string.Empty;
            Status = ZEnum.Status.Enabled;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="id">ID</param>
        public BaseReq(uint id) : this()
        {
            Id = id;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="search">Search</param>
        public BaseReq(string search)
        {
            Search = search;
        }

        /// <summary>
        /// Convert the request to the model
        /// </summary>
        /// <param name="createdBy">Created by</param>
        /// <returns>Return the result</returns>
        public abstract T ToModel(string createdBy);

        #endregion

        #region -- Properties --

        /// <summary>
        /// Search
        /// </summary>
        [DisplayName("Search")]
        public virtual string Search { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [DisplayName("Name")]
        public virtual string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [DisplayName("Description")]
        public virtual string Description { get; set; }

        /// <summary>
        /// Site ID
        /// </summary>
        [DisplayName("-- All Site --")]
        public virtual uint? SiteId { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [DisplayName("-- All Status --")]
        public virtual Status? Status { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// Request URL
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Dynamic
        /// </summary>
        public object Dynamic { get; set; }

        /// <summary>
        /// Timezone offset (minute)
        /// </summary>
        public int TimezoneOffset { get; set; }

        /// <summary>
        /// Action time
        /// </summary>
        public DateTime ActionTime
        {
            get
            {
                return DateTime.UtcNow.AddMinutes(-TimezoneOffset);
            }
        }

        /// <summary>
        /// Timezone
        /// </summary>
        public string Timezone
        {
            get
            {
                var t = TimeSpan.FromMinutes(TimezoneOffset);
                var sign = TimezoneOffset < 0 ? "+" : "-";
                var res = sign + t.ToString("hh':'mm");
                return res;
            }
        }

        #endregion
    }
}
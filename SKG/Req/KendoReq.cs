﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2022-Apr-15 10:14
 * Update       : 2022-Apr-15 10:14
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;

namespace SKG.Req
{
    using DAL.Models;
    using Dto;
    using static ZConst;

    /// <summary>
    /// Kendo request for GET method (support paging on server)
    /// </summary>
    public class KendoReq : BaseReq<ZeroNorModel>
    {
        #region -- Overrides --

        /// <summary>
        /// Convert the request to the model
        /// </summary>
        /// <param name="createdBy">Created by</param>
        /// <returns>Return the result</returns>
        public override ZeroNorModel ToModel(string createdBy)
        {
            return new ZeroNorModel();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public KendoReq()
        {
            PageSize = Default.PageSize;
            Paging = true;

            Sort = new List<SortDto>();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Allow paging
        /// </summary>
        public bool Paging { get; set; }

        /// <summary>
        /// Current user ID
        /// </summary>
        public uint? UserId { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? Page { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public virtual int PageSize { get; set; }

        /// <summary>
        /// Skip
        /// </summary>
        public virtual int Skip { get; set; }

        /// <summary>
        /// Take
        /// </summary>
        public virtual int Take { get; set; }

        /// <summary>
        /// Sort
        /// </summary>
        public virtual List<SortDto> Sort { get; set; }

        #endregion
    }
}
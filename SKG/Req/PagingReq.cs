﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Nov-11 14:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;

namespace SKG.Req
{
    using DAL.Models;
    using Dto;
    using static ZConst;

    /// <summary>
    /// Paging request for POST or PATCH method (support paging on server)
    /// </summary>
    public sealed class PagingReq : BaseReq<ZeroNorModel>
    {
        #region -- Overrides --

        /// <summary>
        /// Convert the request to the model
        /// </summary>
        /// <param name="createdBy">Created by</param>
        /// <returns>Return the result</returns>
        public override ZeroNorModel ToModel(string createdBy)
        {
            return new ZeroNorModel();
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public PagingReq()
        {
            PageNum = 1;
            PageSize = Default.PageSize;
            Filter = null;
            Sort = new List<SortDto>();
            Paging = true;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="sort">Sort</param>
        public PagingReq(object filter, List<SortDto> sort) : this()
        {
            Filter = filter;
            Sort = sort;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="sort">Sort</param>
        /// <param name="paging">Allow paging</param>
        public PagingReq(object filter, List<SortDto> sort, bool paging) : this(filter, sort)
        {
            Paging = paging;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Allow paging
        /// </summary>
        public bool Paging { get; set; }

        /// <summary>
        /// Current user ID
        /// </summary>
        public uint? UserId { get; set; }

        /// <summary>
        /// Filter
        /// </summary>
        public object Filter { get; set; }

        /// <summary>
        /// Sort
        /// </summary>
        public List<SortDto> Sort { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int PageNum
        {
            get
            {
                return pageNum;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }

                pageNum = value;
            }
        }

        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }

                pageSize = value;
            }
        }

        /// <summary>
        /// Offset
        /// </summary>
        public int Offset
        {
            get
            {
                var res = (PageNum - 1) * PageSize;
                return res;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Page number
        /// </summary>
        private int pageNum;

        /// <summary>
        /// Page size
        /// </summary>
        private int pageSize;

        #endregion
    }
}
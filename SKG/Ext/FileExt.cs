﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Dec-04 07:25
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace SKG.Ext
{
    using static ZEnum;

    /// <summary>
    /// Extend for the file
    /// </summary>
    public static class FileExt
    {
        #region -- Common --

        /// <summary>
        /// Exec bash
        /// https://stackoverflow.com/questions/45132081/file-permissions-on-linux-unix-with-net-core/47918132#47918132
        /// </summary>
        /// <param name="s">Command</param>
        public static void ExecBash(this string s)
        {
            var escapedArgs = s.Replace("\"", "\\\"");

            using var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\""
                }
            };

            process.Start();
            process.WaitForExit();
        }

        /// <summary>
        /// Convert file to array byte
        /// </summary>
        /// <param name="s">Full path file name</param>
        /// <returns>Return the result</returns>
        public static byte[] GetFile(this string s)
        {
            if (File.Exists(s))
            {
                var fs = File.OpenRead(s);
                var res = new byte[fs.Length];
                int br = fs.Read(res, 0, res.Length);

                if (br != fs.Length)
                {
                    throw new IOException(s);
                }

                return res;
            }
            else
            {
                return new byte[1];
            }
        }

        /// <summary>
        /// Convert file to array byte
        /// </summary>
        /// <param name="s">Full path file name without extension</param>
        /// <param name="e">File extension with dot</param>
        /// <returns>Return the result</returns>
        public static byte[] GetFile(this string s, string e)
        {
            if (File.Exists(s))
            {
                return s.GetFile();
            }

            s += e;
            if (File.Exists(s))
            {
                return s.GetFile();
            }

            return new byte[1];
        }

        /// <summary>
        /// Read all text
        /// </summary>
        /// <param name="s">Full path file name</param>
        /// <returns>Return the result</returns>
        public static string ReadAllText(this string s)
        {
            var res = string.Empty;

            if (!string.IsNullOrEmpty(s))
            {
                if (File.Exists(s))
                {
                    res = File.ReadAllText(s);
                }
            }

            return res;
        }

        /// <summary>
        /// Get absolute path if it exists, else return empty
        /// </summary>
        /// <param name="path">Root path</param>
        /// <param name="file">File path</param>
        /// <returns>Return the result</returns>
        public static string GetAbsolutePath(this string path, string file)
        {
            var t = path + file;

            var ok = File.Exists(t);
            var res = ok ? t : string.Empty;

            return res;
        }

        /// <summary>
        /// Get relative path if it exists, else return empty
        /// </summary>
        /// <param name="path">Root path</param>
        /// <param name="file">File path</param>
        /// <returns>Return the result</returns>
        public static string GetRelativePath(this string path, string file)
        {
            var t = path + file;

            var ok = File.Exists(t);
            var res = ok ? file : string.Empty;

            return res;
        }

        /// <summary>
        /// Convert size to string file size
        /// </summary>
        /// <param name="size">Size</param>
        /// <returns>Return string file size</returns>
        public static string ToFileSize(this decimal size)
        {
            if (size >= ZConst.Default.MB)
            {
                size = Math.Round(size / ZConst.Default.MB, 2);
                return string.Format("{0:0.##} MB", size);
            }
            else if (size >= ZConst.Default.KB)
            {
                size = Math.Round(size / ZConst.Default.KB, 2);
                return string.Format("{0:0.##} KB", size);
            }

            return string.Format("{0} bytes", size);
        }

        /// <summary>
        /// Convert to mime type
        /// </summary>
        /// <param name="s">File extension</param>
        /// <returns>Return the mime type</returns>
        public static string ToMimeType(this string s)
        {
            var res = s;

            s = s.ToStr(Format.Lower);
            var q = MimeType.Where(p => s.Equals(p.Suffixes));
            var t = q.Select(p => p.Mime).FirstOrDefault();

            if (!string.IsNullOrEmpty(t))
            {
                res = t;
            }

            return res;
        }

        #endregion

        /// <summary>
        /// Load file
        /// </summary>
        /// <typeparam name="T">JSON class type</typeparam>
        /// <param name="file">JSON file path</param>
        /// <param name="section">Section in JSON file</param>
        /// <returns>Return the result</returns>
        public static T Load<T>(this string file, string section = null) where T : new()
        {
            try
            {
                using var r = new StreamReader(file);
                var t1 = r.ReadToEnd();
                var t2 = JObject.Parse(t1);


                if (string.IsNullOrEmpty(section))
                {
                    return t2.ToObject<T>();
                }
                else
                {
                    var t3 = t2.GetValue(section);
                    return t3.ToObject<T>();
                }
            }
            catch { }

            return new T();
        }

        /// <summary>
        /// Save base64 image
        /// </summary>
        /// <param name="s">Base64 image</param>
        /// <param name="folder">folder name</param>
        /// <param name="name">File name</param>
        /// <returns>Return image name</returns>
        public static string SaveBase64Image(this string s, string folder, string name)
        {
            var res = string.Empty;

            try
            {
                var b64 = res;
                var t = s.ToSet(ZConst.Char.Comma);
                if (t.Count > 2)
                {
                    b64 = t[1];
                    res = t[2]; // get original file name
                }
                if (t.Count > 1)
                {
                    if (string.IsNullOrWhiteSpace(res))
                    {
                        var ext = t[0].Replace("data:image/", "").Replace(";base64", "");
                        res = $"{name}.{ext}";
                    }
                    else
                    {
                        t = res.ToSet(ZConst.Char.Dot);
                        if (!string.IsNullOrWhiteSpace(name))
                        {
                            if (t.Count > 1)
                            {
                                var ext = t.LastOrDefault();
                                res = $"{name}.{ext}";
                            }
                            else
                            {
                                res = name;
                            }
                        }
                    }

                    var path = Path.Combine(folder, res);
                    var bytes = Convert.FromBase64String(b64);
                    File.WriteAllBytes(path, bytes);
                }
                else
                {
                    "Not base64 image".LogInfor();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
            }

            return res;
        }

        /// <summary>
        /// Load base64 image
        /// </summary>
        /// <param name="path">Image path</param>
        /// <returns>Return base64 image</returns>
        public static string LoadBase64Image(this string path)
        {
            var bytes = File.ReadAllBytes(path);
            var res = bytes.To64();
            var ext = Path.GetExtension(path);

            res = $"data:image/{ext.Replace(".", "")};base64,{res}";
            return res;
        }

        /// <summary>
        /// Combine link
        /// </summary>
        /// <param name="path">Path name</param>
        /// <param name="file">File name</param>
        /// <returns>Return the result</returns>
        public static string CombineLink(this string path, string file)
        {
            if (path == null)
            {
                path = string.Empty;
            }

            if (file == null)
            {
                file = string.Empty;
            }

            var t = Path.Combine(path, file);
            var res = t.Replace("\\", "/");

            return res;
        }

        #region -- Files --

        /// <summary>
        /// Get list mime type from XML file
        /// </summary>
        private static List<Extension> MimeType
        {
            get
            {
                if (_mimeType != null && _mimeType.Count > 0)
                {
                    return _mimeType;
                }
                else
                {
                    try
                    {
                        var a = AppDomain.CurrentDomain.BaseDirectory;

                        // Check bin folder
                        var arr = AppDomain.CurrentDomain.BaseDirectory.Split(ZConst.String.BackSlash);
                        if (!arr.Contains("bin"))
                        {
                            a += "/bin/";
                        }

                        var xdoc = XDocument.Load(a + @"Xml\FileExtensions.xml");
                        var res = from lv1 in xdoc.Descendants("Extension")
                                  select new Extension
                                  {
                                      Suffixes = lv1.Attribute("Suffixes").Value,
                                      Mime = lv1.Attribute("Mime").Value
                                  };
                        _mimeType = res.ToList();
                    }
                    catch
                    {
                        _mimeType = new List<Extension>();
                    }
                    return _mimeType;
                }
            }
        }

        /// <summary>
        /// List mime type
        /// </summary>
        private static List<Extension> _mimeType;

        /// <summary>
        /// Extension
        /// </summary>
        private class Extension
        {
            /// <summary>
            /// Suffixes
            /// </summary>
            public string Suffixes { get; set; }

            /// <summary>
            /// Mime file type
            /// </summary>
            public string Mime { get; set; }
        }

        #endregion
    }
}
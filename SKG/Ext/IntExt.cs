﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Jun-14 16:38
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SKG.Ext
{
    /// <summary>
    /// Extend for the integer
    /// </summary>
    public static class IntExt
    {
        #region -- Common --

        /// <summary>
        /// Converts the specified nullable int representation of a number to an equivalent string 32-bit signed integer
        /// </summary>
        /// <param name="i">Nullable number</param>
        /// <returns>Return the string number</returns>
        public static string ToStrInt32(this int? i)
        {
            var t = i.ToInt();
            var res = t == 0 ? string.Empty : t.ToString();
            return res;
        }

        /// <summary>
        /// Convert Unix time value to a DateTime object
        /// </summary>
        /// <param name="i">The Unix time stamp to convert to DateTime object</param>
        /// <returns>Returns a DateTime object that represents value of the Unix time</returns>
        public static DateTime ToDateTime(this long i)
        {
            var t = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var res = t.AddSeconds(i);
            return res;
        }

        /// <summary>
        /// Random password
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Return the result</returns>
        public static string RandomPassword(this int length)
        {
            var chars = new byte[62];
            for (int i = 0; i < 10; i++)
            {
                chars[i] = (byte)(i + 48); // '0' to '9'
            }
            for (int i = 0; i < 26; i++)
            {
                chars[i + 10] = (byte)(i + 65); // 'A' to 'Z'
            }
            for (int i = 0; i < 26; i++)
            {
                chars[i + 36] = (byte)(i + 97); // 'a' to 'z'
            }

            var rdm = new Random();
            var res = new byte[length];
            for (int i = 0; i < length; i++)
            {
                res[i] = chars[rdm.Next(62)];
            }

            return Encoding.ASCII.GetString(res);
        }

        /// <summary>
        /// Fill range
        /// </summary>
        /// <param name="l">List</param>
        /// <returns>Return the result</returns>
        public static List<int> FillRange(this List<int> l)
        {
            var res = new List<int>();

            if (l == null || l.Count == 0)
            {
                return res;
            }

            var min = l.Min();
            var max = l.Max();
            for (var i = min; i <= max; i++)
            {
                res.Add(i);
            }

            return res;
        }

        /// <summary>
        /// Fill range
        /// </summary>
        /// <param name="l">List</param>
        /// <returns>Return the result</returns>
        public static List<double> FillRange(this List<double> l)
        {
            var res = new List<double>();

            if (l == null || l.Count == 0)
            {
                return res;
            }

            var min = l.Min();
            var max = l.Max();
            for (var i = min; i <= max; i++)
            {
                res.Add(i);
            }

            return res;
        }

        #endregion
    }
}
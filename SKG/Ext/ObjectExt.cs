﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Mar-31 11:58
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json.Linq;
using SKG.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SKG.Ext
{
    using Zormat = ZEnum.Format;

    /// <summary>
    /// Extend for the object
    /// </summary>
    public static class ObjectExt
    {
        #region -- Common --

        /// <summary>
        /// Value from object
        /// </summary>
        /// <param name="o">Input object</param>
        /// <param name="field">field name</param>
        /// <returns>Return the result</returns>
        public static JToken Value(this object o, string field)
        {
            var res = o.Value()[field];
            return res;
        }

        /// <summary>
        /// Value from object
        /// </summary>
        /// <param name="o">Input object</param>
        /// <returns>Return the result</returns>
        public static JObject Value(this object o)
        {
            var res = new JObject();

            if (o == null)
            {
                return res;
            }

            if (o is JObject)
            {
                res = (JObject)o;
            }
            else
            {
                var t = o.ToString();
                if (string.IsNullOrWhiteSpace(t))
                {
                    return res;
                }

                try
                {
                    if (o is string)
                    {
                        res = JObject.Parse(t);
                    }
                    else
                    {
                        res = JObject.Parse(o.ToJson());
                    }
                }
                catch (Exception ex)
                {
                    var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
                }
            }

            return res;
        }

        /// <summary>
        /// Compute SHA 1
        /// </summary>
        /// <param name="o">Input object</param>
        /// <returns>Return the result</returns>
        public static string ComputeSha(this object o)
        {
            if (o == null)
            {
                return string.Empty;
            }

            return ZHash.ComputeSha(o.ToString());
        }

        /// <summary>
        /// Check a property exist in object
        /// </summary>
        /// <param name="o">Object</param>
        /// <param name="p">Property name</param>
        /// <returns>Return the result</returns>
        public static bool HasProperty(this object o, string p)
        {
            var res = false;

            if (o != null)
            {
                var t = o.GetType().GetProperty(p);
                res = t != null;
            }

            return res;
        }

        /// <summary>
        /// Get value if a property exist in object
        /// </summary>
        /// <param name="o">Object</param>
        /// <param name="p">Property name</param>
        /// <returns>Return the result</returns>
        public static object GetPropertyValue(this object o, string p)
        {
            object res = null;

            if (o != null)
            {
                var t = o.GetType().GetProperty(p);
                if (t != null)
                {
                    res = t.GetValue(o);
                }
            }

            return res;
        }

        /// <summary>
        /// Set value if a property exist in object
        /// </summary>
        /// <param name="o">Object</param>
        /// <param name="p">Property name</param>
        /// <param name="value">Value need to set</param>
        public static void SetPropertyValue(this object o, string p, object value)
        {
            if (o != null)
            {
                var t = o.GetType().GetProperty(p);
                if (t != null)
                {
                    t.SetValue(o, value, null);
                }
            }
        }

        /// <summary>
        /// Compare all properties data from source to destination
        /// </summary>
        /// <param name="fr">Source</param>
        /// <param name="to">Destination</param>
        /// <returns>Return true if it is same, else false</returns>
        public static bool Same(this object fr, object to)
        {
            if (fr.GetType() != to.GetType())
            {
                return false;
            }

            var binding = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

            // Compare number of private and public methods
            var originalMethods = fr.GetType().GetMethods(binding);
            var comparedMethods = to.GetType().GetMethods(binding);
            if (comparedMethods.Length != originalMethods.Length)
            {
                return false;
            }

            // Compare number of private and public properties
            var originalProperties = fr.GetType().GetProperties(binding);
            var comparedProperties = to.GetType().GetProperties(binding);
            if (comparedProperties.Length != originalProperties.Length)
            {
                return false;
            }

            // Compare number of public and private fields
            var originalFields = fr.GetType().GetFields(binding);
            var comparedToFields = to.GetType().GetFields(binding);
            if (comparedToFields.Length != originalFields.Length)
            {
                return false;
            }

            // Compare each field values
            var t = BindingFlags.GetField | binding;
            foreach (var i in originalFields)
            {
                // Check to see if the object to contains the field
                var comparedObj = to.GetType().GetField(i.Name, binding);
                if (comparedObj == null)
                {
                    return false;
                }

                // Get the value of the field from the original object
                var value = fr.GetType().InvokeMember(i.Name, t, null, fr, null);

                // Get the value of the field
                var comparedValue = to.GetType().InvokeMember(comparedObj.Name, t, null, to, null);

                // Now compare the field values
                if (value != null && comparedValue != null)
                {
                    if (value.GetType() != comparedValue.GetType())
                    {
                        return false;
                    }

                    if (!value.ToString().Equals(comparedValue.ToString()))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            // Compare each property values
            t = BindingFlags.GetProperty | binding;
            foreach (var i in originalProperties)
            {
                // Check to see if the object to contains the field
                var comparedObj = to.GetType().GetProperty(i.Name, binding);
                if (comparedObj == null)
                {
                    return false;
                }

                // Get the value of the property from the original object
                var value = fr.GetType().InvokeMember(i.Name, t, null, fr, null);

                // Get the value of the property
                var comparedValue = to.GetType().InvokeMember(comparedObj.Name, t, null, to, null);

                // Now compare the property values
                if (value != null && comparedValue != null)
                {
                    if (value.GetType() != comparedValue.GetType())
                    {
                        return false;
                    }

                    if (!value.ToString().Equals(comparedValue.ToString()))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region -- Numeric --

        /// <summary>
        /// Can filter (not null and greater than zero)
        /// </summary>
        /// <param name="o">Object need to check</param>
        /// <returns>Return the result</returns>
        public static bool CanFilter(this object o)
        {
            return o.ToDecimal() > 0;
        }

        /// <summary>
        /// Check is numeric
        /// </summary>
        /// <param name="o">Object need to check</param>
        /// <returns>Return is is numeric or not</returns>
        public static bool IsNumeric(this object o)
        {
            if (o == null || o is DateTime || o is Boolean)
            {
                return false;
            }

            if (o is Int16 || o is Int32 || o is Int64)
            {
                return true;
            }

            if (o is Single || o is Double || o is Decimal)
            {
                return true;
            }

            try
            {
                decimal.Parse(o.ToString());
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// Stripping out non-numeric characters in string
        /// </summary>
        /// <param name="o">Object need to check</param>
        /// <returns>Return the numeric string</returns>
        public static string ToNumber(this object o)
        {
            var s = o + string.Empty;
            var res = Regex.Replace(s, "[^0-9]", string.Empty);
            //res = new string(s.Where(c => char.IsDigit(c)).ToArray());
            return res;
        }

        /// <summary>
        /// Format #,0.00
        /// </summary>
        /// <param name="o">Number object</param>
        /// <returns>Return the result</returns>
        public static string Formaz(this object o)
        {
            var t = o.ToDecimalN();
            var res = t == null ? "" : t.Value.ToString("#,0.00");
            return res;
        }

        #region -- Short --
        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 16-bit signed integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static short ToShort(this object o)
        {
            var t = o + string.Empty;
            short.TryParse(t, out short res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 16-bit unsigned integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static ushort ToShortU(this object o)
        {
            var t = o + string.Empty;
            ushort.TryParse(t, out ushort res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 16-bit signed integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static short? ToShortN(this object o)
        {
            short? res = null;

            if (o.IsNumeric())
            {
                res = o.ToShort();
            }

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 16-bit unsigned integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static ushort? ToShortUn(this object o)
        {
            ushort? res = null;

            if (o.IsNumeric())
            {
                res = o.ToShortU();
            }

            return res;
        }
        #endregion

        #region -- Int --
        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 32-bit signed integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static int ToInt(this object o)
        {
            var t = o + string.Empty;
            int.TryParse(t, out int res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 32-bit unsigned integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static uint ToIntU(this object o)
        {
            var t = o + string.Empty;
            uint.TryParse(t, out uint res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 32-bit signed integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static int? ToIntN(this object o)
        {
            int? res = null;

            if (o.IsNumeric())
            {
                res = o.ToInt();
            }

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 32-bit unsigned integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static uint? ToIntUn(this object o)
        {
            uint? res = null;

            if (o.IsNumeric())
            {
                res = o.ToIntU();
            }

            return res;
        }
        #endregion

        #region -- Long --
        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 64-bit signed integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static long ToLong(this object o)
        {
            var t = o + string.Empty;
            long.TryParse(t, out long res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 64-bit unsigned integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static ulong ToLongU(this object o)
        {
            var t = o + string.Empty;
            ulong.TryParse(t, out ulong res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 64-bit signed integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static long? ToLongN(this object o)
        {
            long? res = null;

            if (o.IsNumeric())
            {
                res = o.ToLong();
            }

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent 64-bit unsigned integer
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static ulong? ToLongUn(this object o)
        {
            ulong? res = null;

            if (o.IsNumeric())
            {
                res = o.ToLongU();
            }

            return res;
        }
        #endregion

        #region -- Float --
        /// <summary>
        /// Converts the specified object representation of a number to an equivalent float number
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static float ToFloat(this object o)
        {
            var t = o + string.Empty;
            float.TryParse(t, out float res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent float number
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static float? ToFloatN(this object o)
        {
            float? res = null;

            if (o.IsNumeric())
            {
                res = o.ToFloat();
            }

            return res;
        }
        #endregion

        #region -- Double --
        /// <summary>
        /// Converts the specified object representation of a number to an equivalent double number
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static double ToDouble(this object o)
        {
            var t = o + string.Empty;
            double.TryParse(t, out double res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent double number
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static double? ToDoubleN(this object o)
        {
            double? res = null;

            if (o.IsNumeric())
            {
                res = o.ToDouble();
            }

            return res;
        }
        #endregion

        #region -- Decimal --
        /// <summary>
        /// Converts the specified object representation of a number to an equivalent decimal number
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number</returns>
        public static decimal ToDecimal(this object o)
        {
            var t = o + string.Empty;
            decimal.TryParse(t, out decimal res);

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a number to an equivalent decimal number
        /// </summary>
        /// <param name="o">String number</param>
        /// <returns>Return the number (nullable)</returns>
        public static decimal? ToDecimalN(this object o)
        {
            decimal? res = null;

            if (o.IsNumeric())
            {
                res = o.ToDecimal();
            }

            return res;
        }
        #endregion

        #endregion

        #region -- Boolean --

        /// <summary>
        /// Converts the specified object representation of a boolean to an equivalent boolean
        /// </summary>
        /// <param name="o">String boolean</param>
        /// <returns>Return the boolean</returns>
        public static bool ToBool(this object o)
        {
            var t = (o + string.Empty).ToLower();

            if (t == "1" || t == "yes" || t == "y" || t == "true" || t == "t")
            {
                t = "true";
            }
            else
            {
                t = "false";
            }

            bool.TryParse(t, out bool i);
            bool res = i;
            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a boolean to an equivalent boolean
        /// </summary>
        /// <param name="o">String boolean</param>
        /// <returns>Return the boolean (nullable)</returns>
        public static bool? ToBoolNull(this object o)
        {
            bool? res = null;

            if (o != null)
            {
                var t = o + string.Empty;
                if (!string.IsNullOrEmpty(t))
                {
                    res = o.ToBool();
                }
            }

            return res;
        }

        #endregion

        #region -- DateTime --

        /// <summary>
        /// Converts the specified object representation of a DateTime to an equivalent DateTime
        /// </summary>
        /// <param name="o">String DateTime</param>
        /// <param name="f">Format</param>
        /// <returns>Return the DateTime</returns>
        public static DateTime ToDateTime(this object o, string f)
        {
            var res = o.ToDateTimeNull(f);

            if (res == null)
            {
                res = DateTime.UtcNow;
            }

            return res.Value;
        }

        /// <summary>
        /// Converts the specified object representation of a DateTime to an equivalent DateTime (nullable)
        /// </summary>
        /// <param name="o">String DateTime</param>
        /// <param name="f">Format</param>
        /// <returns>Return the DateTime (nullable)</returns>
        public static DateTime? ToDateTimeNull(this object o, string f)
        {
            DateTime? res = null;

            try
            {
                var t = o + string.Empty;
                var provider = CultureInfo.InvariantCulture;
                res = DateTime.ParseExact(t, new string[] { f }, provider, DateTimeStyles.None);
            }
            catch { }

            return res;
        }

        #endregion

        #region -- Guid --

        /// <summary>
        /// Converts the specified object representation of a Guid to an equivalent Guid
        /// </summary>
        /// <param name="o">String Guid</param>
        /// <returns>Return the Guid</returns>
        public static Guid ToGuid(this object o)
        {
            var t = o + string.Empty;
            Guid.TryParse(t, out Guid res);

            if (res == Guid.Empty)
            {
                res = Guid.NewGuid();
            }

            return res;
        }

        /// <summary>
        /// Converts the specified object representation of a Guid to an equivalent Guid
        /// </summary>
        /// <param name="o">String Guid</param>
        /// <returns>Return the Guid (nullable)</returns>
        public static Guid? ToGuidN(this object o)
        {
            Guid? res = null;

            if (o != null)
            {
                var t = o + string.Empty;
                if (!string.IsNullOrEmpty(t))
                {
                    res = o.ToGuid();
                }
            }

            return res;
        }

        #endregion

        #region -- String --

        /// <summary>
        /// Convert the object to string with format
        /// </summary>
        /// <param name="o">Object</param>
        /// <param name="f">Format type</param>
        /// <returns>Return the string</returns>
        public static string ToStr(this object o, Zormat f = Zormat.Original)
        {
            var t = o == null ? string.Empty : o.ToString();

            switch (f)
            {
                case Zormat.Sentence:
                    return t.ToUpperFirst();

                case Zormat.Lower:
                    return t.ToLower();

                case Zormat.Upper:
                    return t.ToUpper();

                case Zormat.Capitalized:
                    return t.ToUpperWords();

                default:
                    return t;
            }
        }

        /// <summary>
        /// Find and extract a number from a string
        /// </summary>
        /// <param name="n">String with number</param>
        /// <returns>Return the result</returns>
        public static string ExtractNumber(this string n)
        {
            var res = string.Empty;

            n += res;
            for (int i = 0; i < n.Length; i++)
            {
                if (char.IsDigit(n[i]))
                {
                    res += n[i];
                }
            }

            return res;
        }

        #endregion

        #region -- ToDisplayName --

        /// <summary>
        /// Get display name
        /// </summary>
        /// <typeparam name="T">Type of class</typeparam>
        /// <param name="o">Object</param>
        /// <param name="name">Property name</param>
        /// <param name="f">Format type</param>
        /// <returns>Return the result</returns>
        public static string ToDisplayName<T>(this T o, string name, Zormat f = Zormat.Sentence)
        {
            var pdc = TypeDescriptor.GetProperties(o);
            return pdc.ToDisplayName(name, f);
        }

        /// <summary>
        /// Get display name
        /// </summary>
        /// <param name="name">Property name</param>
        /// <param name="f">Format type</param>
        /// <returns>Return the result</returns>
        public static string ToDisplayName<T>(this string name, Zormat f = Zormat.Original)
        {
            var pdc = TypeDescriptor.GetProperties(typeof(T));
            return pdc.ToDisplayName(name, f);
        }

        /// <summary>
        /// Get display name
        /// </summary>
        /// <param name="pdc">Property descriptor collection</param>
        /// <param name="name">Property name</param>
        /// <param name="f">Format type</param>
        /// <returns>Return the result</returns>
        private static string ToDisplayName(this PropertyDescriptorCollection pdc, string name, Zormat f)
        {
            if (name == null)
            {
                return name;
            }

            var l = pdc.Cast<PropertyDescriptor>();
            var d = l.ToDictionary(p => p.Name, p => p.DisplayName);

            var res = d.GetValueOrDefault(name);
            if (res == null)
            {
                res = name;
            }
            res = res.ToStr(f);

            return res;
        }

        #endregion

        #region -- Salesforce --

        /// <summary>
        /// Salesforce InstanceUrl with ApexRest
        /// </summary>
        /// <param name="token">Salesforce token</param>
        /// <returns>Return the result</returns>
        public static string ApexRest(this SfTokenDto token)
        {
            if (token == null)
            {
                return string.Empty;
            }

            var res = $"{token.InstanceUrl}/services/{ZApi.ApexRest}/";
            return res;
        }

        /// <summary>
        /// Salesforce InstanceUrl with ApexData
        /// </summary>
        /// <param name="token">Salesforce token</param>
        /// <returns>Return the result</returns>
        public static string ApexData(this SfTokenDto token)
        {
            if (token == null)
            {
                return string.Empty;
            }

            var res = $"{token.InstanceUrl}/services/{ZApi.ApexData}/";
            return res;
        }

        /// <summary>
        /// Salesforce AccessToken
        /// </summary>
        /// <param name="token">Salesforce token</param>
        /// <returns>Return the result</returns>
        public static string ApexToken(this SfTokenDto token)
        {
            if (token == null)
            {
                return string.Empty;
            }

            var res = $"{token.TokenType} {token.AccessToken}";
            return res;
        }

        /// <summary>
        /// Salesforce update URL
        /// </summary>
        /// <param name="token">Salesforce token</param>
        /// <param name="objectName">Object name</param>
        /// <param name="v">API version </param>
        /// <returns>Return the result</returns>
        public static string ApexUpdateUrl(this SfTokenDto token, string objectName, string v = ZConst.ApexVersion)
        {
            var tt = ApexData(token);
            var res = $"{tt}{v}/sobjects/{objectName}/";
            return res;
        }

        #endregion

        /// <summary>
        /// Validate token with clear auth CRM mode
        /// </summary>
        /// <param name="token">Salesforce or Rexsoftware or Mailchimp token</param>
        /// <returns>Return the result</returns>
        public static bool Validate(this BaseTokenDto token)
        {
            var res = true;

            if (token == null)
            {
                res = false;
            }
            if (string.IsNullOrWhiteSpace(token.AccessToken))
            {
                res = false;
            }
            res = res || ZApi.ClearAuthCrm;

            return res;
        }
    }
}
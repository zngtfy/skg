﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Oct-04 08:49
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SKG.Ext
{
    using DAL.Models;
    using Dto;
    using Zhar = ZConst.Char;
    using Ztring = ZConst.String;

    /// <summary>
    /// Extend for the enumerable
    /// </summary>
    public static class ListExt
    {
        #region -- Common --

        /// <summary>
        /// Concatenates the members of a collection, using the specified separator between each member
        /// </summary>
        /// <typeparam name="T">The type of the members of values</typeparam>
        /// <param name="l">A collection that contains the objects to concatenate</param>
        /// <param name="as">Allow adding a separator to end of the return string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>A string that consists of the members of values delimited by the separator character</returns>
        public static string Zoin<T>(this IEnumerable<T> l, bool @as = false, string c = Ztring.Semicolon)
        {
            var res = string.Empty;

            if (l != null && l.Count() > 0)
            {
                res = string.Join(c, l);
            }

            if (res != c.ToString())
            {
                res = res.ToDistinct(false, c);
            }

            if (@as)
            {
                res = string.IsNullOrEmpty(res) ? c.ToString() : c + res + c;
            }

            return res;
        }

        /// <summary>
        /// Convert from IEnumerable (LINQ, List object) to DataTable
        /// </summary>
        /// <typeparam name="T">Class</typeparam>
        /// <param name="d">Data</param>
        /// <param name="s">Table name</param>
        /// <returns>Return a DataTable</returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> d, string s = ZConst.String.Blank)
        {
            var res = new DataTable(s);

            var props = typeof(T).GetProperties();
            foreach (var i in props)
            {
                var type = i.PropertyType;
                if ((type.IsGenericType) && (type.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    type = type.GetGenericArguments()[0];
                }
                res.Columns.Add(new DataColumn(i.Name, type));
            }

            foreach (var i in d)
            {
                var r = res.NewRow();

                foreach (var prop in props)
                {
                    r[prop.Name] = prop.GetValue(i, null) ?? DBNull.Value;
                }

                res.Rows.Add(r);
            }
            res.AcceptChanges();

            return res;
        }

        /// <summary>
        /// Create CSV file from list
        /// </summary>
        /// <typeparam name="T">Data type</typeparam>
        /// <param name="list">Data list</param>
        /// <param name="file">Full path file name</param>
        /// <returns>Return status can create or not</returns>
        public static bool ToCsv<T>(this List<T> list, string file)
        {
            try
            {
                if (list == null || list.Count == 0)
                {
                    var sw = new StreamWriter(file);
                    sw.Write("No data.");
                }

                // Get type from 0th member
                var type = list[0].GetType();
                var newLine = Environment.NewLine;

                using (var sw = new StreamWriter(file))
                {
                    // Make a new instance of the class name we figured out to get its props
                    var o = Activator.CreateInstance(type);

                    // Gets all properties
                    var props = o.GetType().GetProperties();

                    // Foreach of the properties in class above, write out properties
                    // this is the header row
                    foreach (var i in props)
                    {
                        sw.Write(i.Name.ToUpper() + ZConst.String.Comma);
                    }
                    sw.Write(newLine);

                    // This acts as datarow
                    foreach (T i in list)
                    {
                        // This acts as datacolumn
                        foreach (var j in props)
                        {
                            // This is the row+col intersection (the value)
                            var t = i.GetType().GetProperty(j.Name).GetValue(i, null) + string.Empty;
                            var res = t.Replace(ZConst.Char.Comma, ZConst.Char.Space) + ZConst.Char.Comma;

                            sw.Write(res);
                        }
                        sw.Write(newLine);
                    }
                }
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// Convert from List to JSON
        /// </summary>
        /// <typeparam name="T">Data type</typeparam>
        /// <param name="data">List data</param>
        /// <returns>Return to JSON data</returns>
        public static string ToJson<T>(this List<T> data)
        {
            var res = string.Empty;

            if (data != null)
            {
                res = JsonConvert.SerializeObject(data);
            }

            return res;
        }

        /// <summary>
        /// Replace element in list with new element
        /// </summary>
        /// <typeparam name="T">Class type</typeparam>
        /// <param name="l">List object</param>
        /// <param name="element">New element object</param>
        /// <param name="property">Property name</param>
        /// <returns>Return new list</returns>
        public static List<T> Update<T>(this List<T> l, T element, string property)
        {
            var res = new List<T>();

            foreach (var i in l)
            {
                var a = i.GetPropertyValue(property);
                var b = element.GetPropertyValue(property);

                if (a == null || b == null)
                {
                    continue;
                }

                if (a.Equals(b))
                {
                    res.Add(element);
                }
                else
                {
                    res.Add(i);
                }
            }

            return res;
        }

        /// <summary>
        /// Shuffle
        /// </summary>
        /// <typeparam name="T">Class type</typeparam>
        /// <param name="l">List data</param>
        public static void Shuffle<T>(this IList<T> l)
        {
            var rng = new Random();
            var n = l.Count;

            while (n > 1)
            {
                n--;
                var k = rng.Next(n + 1);
                T value = l[k];
                l[k] = l[n];
                l[n] = value;
            }
        }

        /// <summary>
        /// Convert list string to list with separator and distinct
        /// </summary>
        /// <param name="l">List string data</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return the result</returns>
        public static List<string> ToSet(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<string>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                if (string.IsNullOrEmpty(i))
                {
                    continue;
                }

                var arr = i.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries);
                res.AddRange(arr);
            }
            res = res.Distinct().ToList();

            return res;
        }

        #region -- Short --
        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list short</returns>
        public static List<short> ToListShort(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<short>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListShort(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list short</returns>
        public static List<ushort> ToListShortU(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<ushort>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListShortU(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list short</returns>
        public static List<short?> ToListShortN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<short?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListShortN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list short</returns>
        public static List<ushort?> ToListShortUn(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<ushort?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListShortUn(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        #region -- Int --
        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list int</returns>
        public static List<int> ToListInt(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<int>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListInt(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list int</returns>
        public static List<uint> ToListIntU(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<uint>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListIntU(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list int</returns>
        public static List<int?> ToListIntN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<int?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListIntN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list int</returns>
        public static List<uint?> ToListIntUn(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<uint?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListIntUn(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        #region -- Long --
        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list long</returns>
        public static List<long> ToListLong(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<long>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListLong(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list long</returns>
        public static List<ulong> ToListLongU(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<ulong>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListLongU(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list long</returns>
        public static List<long?> ToListLongN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<long?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListLongN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list long</returns>
        public static List<ulong?> ToListLongUn(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<ulong?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListLongUn(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        #region -- Float --
        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list float</returns>
        public static List<float> ToListFloat(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<float>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListFloat(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list float</returns>
        public static List<float?> ToListFloatN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<float?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListFloatN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        #region -- Double --
        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list double</returns>
        public static List<double> ToListDouble(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<double>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListDouble(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list double</returns>
        public static List<double?> ToListDoubleN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<double?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListDoubleN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        #region -- Decimal --
        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list decimal</returns>
        public static List<decimal> ToListDecimal(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<decimal>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListDecimal(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string number separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list decimal</returns>
        public static List<decimal?> ToListDecimalN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<decimal?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListDecimalN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        #region -- Guid --
        /// <summary>
        /// Convert list string separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list Guid</returns>
        public static List<Guid> ToListGuid(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<Guid>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListGuid(c));
            }
            res = res.Distinct().ToList();

            return res;
        }

        /// <summary>
        /// Convert list string separation with separator
        /// </summary>
        /// <param name="l">List string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return list Guid</returns>
        public static List<Guid?> ToListGuidN(this IEnumerable<string> l, char c = Zhar.Semicolon)
        {
            var res = new List<Guid?>();

            if (l == null)
            {
                return res;
            }

            foreach (var i in l)
            {
                res.AddRange(i.ToListGuidN(c));
            }
            res = res.Distinct().ToList();

            return res;
        }
        #endregion

        /// <summary>
        /// Convert ID to current system
        /// </summary>
        /// <param name="l">List ID</param>
        /// <param name="id">ID need to convert</param>
        /// <param name="isMa">Is MaID</param>
        /// <returns>Return the result</returns>
        public static uint ToMaId(this List<ZeroSyncModel> l, uint id, bool isMa = false)
        {
            var res = 0u;

            if (l == null)
            {
                l = new List<ZeroSyncModel>();
            }

            var t = l.FirstOrDefault(p => (isMa ? p.MaId : p.MsId) == id);
            if (t != null)
            {
                res = t.Id;
            }

            return res;
        }

        /// <summary>
        /// Convert ID to current system
        /// </summary>
        /// <param name="l">List ID</param>
        /// <param name="id">ID need to convert</param>
        /// <param name="isMa">Is MaID</param>
        /// <returns>Return the result</returns>
        public static uint? ToMaId(this List<ZeroSyncModel> l, uint? id, bool isMa = false)
        {
            uint? res = null;

            if (l == null)
            {
                l = new List<ZeroSyncModel>();
            }

            var t = l.FirstOrDefault(p => (isMa ? p.MaId : p.MsId) == id);
            if (t != null)
            {
                res = t.Id;
            }

            return res;
        }
        #endregion

        #region -- LINQ --

        /// <summary>
        /// Get And expression
        /// </summary>
        /// <typeparam name="T">Model class type</typeparam>
        /// <param name="o">Expression</param>
        /// <param name="e">Or expression</param>
        /// <returns>Return the Or expression</returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> o, Expression<Func<T, bool>> e)
        {
            if (o == null)
            {
                return e;
            }

            Replace(e, e.Parameters[0], o.Parameters[0]);
            var t = Expression.Or(o.Body, e.Body);
            var res = Expression.Lambda<Func<T, bool>>(t, o.Parameters);

            return res;
        }

        /// <summary>
        /// Get And expression
        /// </summary>
        /// <typeparam name="T">Model class type</typeparam>
        /// <param name="o">Expression</param>
        /// <param name="e">And expression</param>
        /// <returns>Return the And expression</returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> o, Expression<Func<T, bool>> e)
        {
            if (o == null)
            {
                return e;
            }

            Replace(e, e.Parameters[0], o.Parameters[0]);
            var t = Expression.And(o.Body, e.Body);
            var res = Expression.Lambda<Func<T, bool>>(t, o.Parameters);

            return res;
        }

        /// <summary>
        /// Sort query
        /// </summary>
        /// <typeparam name="T">Model class type</typeparam>
        /// <param name="o">Query</param>
        /// <param name="sorts">List field sort</param>
        /// <returns>Return the sort query</returns>
        public static IQueryable<T> Sort<T>(this IQueryable<T> o, List<SortDto> sorts)
        {
            var res = o;

            try
            {
                if (sorts == null)
                {
                    sorts = new List<SortDto>();
                }

                var exp = o.Expression;
                var param = Expression.Parameter(typeof(T));

                foreach (var i in sorts)
                {
                    var property = Expression.PropertyOrField(param, i.Field);
                    var sort = Expression.Lambda(property, param);

                    var direction = string.Empty;
                    if (i.Direction != SortDto.Ascending)
                    {
                        direction = ListSortDirection.Descending.ToString();
                    }

                    var method = sorts.IndexOf(i) == 0 ? "OrderBy" : "ThenBy";
                    method += direction;

                    var t1 = new[] { typeof(T), property.Type };
                    var t2 = Expression.Quote(sort);
                    var t3 = Expression.Call(typeof(Queryable), method, t1, exp, t2);

                    res = o.Provider.CreateQuery<T>(t3);
                    exp = res.Expression;
                }
            }
            catch { }

            return res;
        }

        /// <summary>
        /// Where if
        /// </summary>
        /// <typeparam name="T">Model class type</typeparam>
        /// <param name="q">Query</param>
        /// <param name="c">Condition</param>
        /// <param name="p">Predicate</param>
        /// <returns>Return the query</returns>
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> q, bool c, Expression<Func<T, bool>> p)
        {
            if (!c)
            {
                return q;
            }

            return q.Where(p);
        }

        /// <summary>
        /// Page by
        /// </summary>
        /// <typeparam name="T">Model class type</typeparam>
        /// <param name="q">Query</param>
        /// <param name="skip">The number of elements to skip before returning the remaining elements</param>
        /// <param name="take">The number of elements to return</param>
        /// <returns></returns>
        public static IQueryable<T> PageBy<T>(this IQueryable<T> q, int skip, int take)
        {
            return q.Skip(skip).Take(take);
        }

        /// <summary>
        /// Replace object support Or and And method
        /// </summary>
        /// <param name="o">Current instance object</param>
        /// <param name="old">Old object</param>
        /// <param name="new">New object</param>
        private static void Replace(object o, object old, object @new)
        {
            var flag = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            for (var i = o.GetType(); i != null; i = i.BaseType)
            {
                var t = i.GetFields(flag);
                foreach (var j in t)
                {
                    var val = j.GetValue(o);
                    if (val != null && val.GetType().Assembly == typeof(Expression).Assembly)
                    {
                        if (object.ReferenceEquals(val, old))
                        {
                            j.SetValue(o, @new);
                        }
                        else
                        {
                            Replace(val, old, @new);
                        }
                    }
                }
            }
        }

        #endregion

        #region -- Dictionary --

        /// <summary>
        /// Find ID
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="find">Find</param>
        /// <returns>Return the result</returns>
        public static List<uint> FindId(this Dictionary<uint, string> data, List<string> find)
        {
            var res = new List<uint>();

            if (data == null || find == null)
            {
                return res;
            }

            foreach (var i in data)
            {
                foreach (var j in find)
                {
                    var ok = i.Value.Contains(j);
                    if (ok)
                    {
                        res.Add(i.Key);
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Replace place holder by value in email template
        /// </summary>
        /// <param name="data">Key = PlaceHolder, Value = Value</param>
        /// <param name="s">Template string</param>
        /// <returns>Return the result</returns>
        public static string ReplacePlaceHolder(this Dictionary<string, string> data, string s)
        {
            var res = s + string.Empty;
            if (data == null)
            {
                return res;
            }

            foreach (var i in data)
            {
                res = res.Replace(i.Key, i.Value);
            }

            return res;
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Aug-07 16:47
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SKG.Ext
{
    using Att;
    using static Dto.PrivilegeDto;

    /// <summary>
    /// Extend for the enumeration
    /// </summary>
    public static class EnumExt
    {
        #region -- Common --

        /// <summary>
        /// Get enum description
        /// </summary>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the description</returns>
        public static string ToDescription(this Enum value)
        {
            return value.ToAtt<DescriptionAttribute>("Description");
        }

        /// <summary>
        /// Get enum display name
        /// </summary>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the display name</returns>
        public static string ToDisplay(this Enum value)
        {
            return value.ToAtt<DisplayAttribute>("Name");
        }

        /// <summary>
        /// Get enum attribute
        /// </summary>
        /// <typeparam name="T">Type of attribute</typeparam>
        /// <param name="value">Value of enum</param>
        /// <param name="field">Field need to get value</param>
        /// <returns>Return the field value</returns>
        public static string ToAtt<T>(this Enum value, string field) where T : Attribute
        {
            var res = string.Empty;

            if (value == null)
            {
                return res;
            }
            res = value.ToStr();

            var t1 = value.GetType().GetField(res);
            if (t1 == null)
            {
                return res;
            }

            var t2 = t1.GetCustomAttributes(typeof(T), false);
            var t3 = (T[])t2;

            if (t3 != null && t3.Length > 0)
            {
                res = t3[0].GetPropertyValue(field).ToStr();
            }

            return res;
        }

        /// <summary>
        /// Get enum attribute
        /// </summary>
        /// <typeparam name="T">Type of attribute</typeparam>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the result</returns>
        public static T ToAtt<T>(this Enum value) where T : Attribute
        {
            T res = null;

            var t1 = value.GetType().GetField(value.ToString());
            if (t1 == null)
            {
                return res;
            }

            var t2 = t1.GetCustomAttributes(typeof(T), false);
            var t3 = (T[])t2;

            if (t3 != null && t3.Length > 0)
            {
                res = t3[0];
            }

            return res;
        }

        /// <summary>
        /// Get enum attribute
        /// </summary>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the result</returns>
        public static CssAttribute ToAttCss(this Enum value)
        {
            var res = value.ToAtt<CssAttribute>();

            if (res == null)
            {
                res = new CssAttribute(string.Empty, string.Empty);
            }

            return res;
        }

        /// <summary>
        /// Get enum css name
        /// </summary>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the css name</returns>
        public static string ToCss(this Enum value)
        {
            return value.ToAtt<CssAttribute>("Name");
        }

        /// <summary>
        /// Get enum attribute
        /// </summary>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the result</returns>
        public static InfAttribute ToAttInf(this Enum value)
        {
            var res = value.ToAtt<InfAttribute>();

            if (res == null)
            {
                res = new InfAttribute(string.Empty, string.Empty);
            }

            return res;
        }

        /// <summary>
        /// Get enum inf name
        /// </summary>
        /// <param name="value">Value of enum</param>
        /// <returns>Return the css name</returns>
        public static string ToInf(this Enum value)
        {
            return value.ToAtt<InfAttribute>("Name");
        }

        /// <summary>
        /// Convert standard object permissions to permissions DTO
        /// </summary>
        /// <param name="e">Standard object permissions</param>
        /// <param name="f">Function name</param>
        /// <param name="o">Options</param>
        /// <returns>Return the result</returns>
        public static SopExt ToPermis(this Sop e, string f, object o)
        {
            return new SopExt(e, f, o);
        }

        /// <summary>
        /// Convert standard object permissions to permissions DTO
        /// </summary>
        /// <param name="e">Standard object permissions</param>
        /// <param name="f">Function name</param>
        /// <param name="o">Options</param>
        /// <returns>Return the result</returns>
        public static SspExt ToPermis(this Ssp e, string f, object o)
        {
            return new SspExt(e, f, o);
        }

        /// <summary>
        /// Get prefix of string AbCd to Ab
        /// </summary>
        /// <param name="e">Value of enum</param>
        /// <returns>Return the result</returns>
        public static string ToPrefix(this Enum e)
        {
            return e.ToString().ToPrefix();
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Jun-14 16:38
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Ext
{
    /// <summary>
    /// Extend for the date and time
    /// </summary>
    public static class TimeExt
    {
        #region -- Common --

        /// <summary>
        /// Start of day
        /// </summary>
        /// <param name="d">Date and time</param>
        /// <returns>Return first day at 00:00:00</returns>
        public static DateTime StartOfDay(this DateTime d)
        {
            var res = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
            return res;
        }

        /// <summary>
        /// End of day
        /// </summary>
        /// <param name="d">Date and time</param>
        /// <returns>Return end day at 23:59:59</returns>
        public static DateTime EndOfDay(this DateTime d)
        {
            var res = d.StartOfDay().AddDays(1).AddSeconds(-1);
            return res;
        }

        /// <summary>
        /// Start of month
        /// </summary>
        /// <param name="d">Date and time</param>
        /// <returns>Return first day of month at 00:00:00</returns>
        public static DateTime StartOfMonth(this DateTime d)
        {
            var res = new DateTime(d.Year, d.Month, 1);
            return res;
        }

        /// <summary>
        /// End of month
        /// </summary>
        /// <param name="d">Date and time</param>
        /// <returns>Return end day of month at 23:59:59</returns>
        public static DateTime EndOfMonth(this DateTime d)
        {
            var res = d.StartOfMonth().AddMonths(1).AddSeconds(-1);
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <param name="f">Format (default dd-MM-yyyy)</param>
        /// <returns>Return the string</returns>
        public static string ToStrDate(this DateTime d, string f = ZConst.Format.Dmy)
        {
            var res = d.ToString(f);
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <param name="f">Format (default dd-MM-yyyy)</param>
        /// <returns>Return the string</returns>
        public static string ToStrDate(this DateTime? d, string f = ZConst.Format.Dmy)
        {
            var res = d == null ? string.Empty : d.Value.ToStrDate(f);
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <returns>Return the string</returns>
        public static string ToStrYmdHms(this DateTime d)
        {
            var res = d.ToString(ZConst.Format.YmdHms);
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <returns>Return the string</returns>
        public static string ToStrSqlDateTime(this DateTime? d)
        {
            var res = d == null ? string.Empty : d.Value.ToStrYmdHms();
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format yyyy/MM/dd HH:mm:ss
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <returns>Return the string</returns>
        public static string ToStrIsoYmdHms(this DateTime d)
        {
            var res = d.ToString(ZConst.Format.IsoYmdHms);
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format yyyy/MM/dd HH:mm:ss
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <returns>Return the string</returns>
        public static string ToStrIsoDateTime(this DateTime? d)
        {
            var res = d == null ? string.Empty : d.Value.ToStrIsoYmdHms();
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format yyyy-MM-ddTHH:mm:ss.fffZ (ISO8601 with 3 decimal places)
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <param name="isUtc">Convert UTC</param>
        /// <returns>Return the string</returns>
        public static string ToStrIso8601(this DateTime d, bool isUtc = false)
        {
            if (isUtc)
            {
                d = d.ToUniversalTime();
            }

            var res = d.ToString(ZConst.Format.Iso8601);
            return res;
        }

        /// <summary>
        /// Convert DateTime to string with format yyyy-MM-ddTHH:mm:ss.fffZ (ISO8601 with 3 decimal places)
        /// </summary>
        /// <param name="d">DateTime object</param>
        /// <param name="isUtc">Convert UTC</param>
        /// <returns>Return the string</returns>
        public static string ToStrIso8601(this DateTime? d, bool isUtc = false)
        {
            var res = d == null ? string.Empty : d.Value.ToStrIso8601(isUtc);
            return res;
        }

        /// <summary>
        /// Convert a DateTime object to Unix time representation
        /// </summary>
        /// <param name="d">The DateTime object to convert to Unix time stamp</param>
        /// <returns>Returns a numerical representation (Unix time) of the DateTime object</returns>
        public static long ToUnixTime(this DateTime d)
        {
            var t = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var res = (long)(d - t).TotalSeconds;
            return res;
        }

        /// <summary>
        /// Minimum date
        /// </summary>
        /// <param name="d1">Date time 1</param>
        /// <param name="d2">Date time 2</param>
        /// <returns>Return the min date</returns>
        public static DateTime Min(this DateTime d1, DateTime d2)
        {
            var res = d1;

            if (d2 < d1)
            {
                res = d2;
            }

            return res;
        }

        /// <summary>
        /// Maximum date
        /// </summary>
        /// <param name="d1">Date time 1</param>
        /// <param name="d2">Date time 2</param>
        /// <returns>Return the max date</returns>
        public static DateTime Max(this DateTime d1, DateTime d2)
        {
            var res = d1;

            if (d2 > d1)
            {
                res = d2;
            }

            return res;
        }

        #endregion
    }
}
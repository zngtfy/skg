﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Oct-04 08:49
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace SKG.Ext
{
    using static ZEnum;
    using Zefault = ZConst.Default;
    using Zhar = ZConst.Char;
    using Zormat = ZConst.Format;
    using Ztring = ZConst.String;

    /// <summary>
    /// Extend for the string
    /// </summary>
    public static class StringExt
    {
        #region -- Common --

        /// <summary>
        /// Replace
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="old">Old value</param>
        /// <param name="new">New value</param>
        /// <returns>Return the result</returns>
        public static string Replacz(this string s, string old, string @new)
        {
            var res = s + "";

            old += ""; @new += "";

            if (res == old)
            {
                res = res.Replace(old, @new);
            }

            if (res.ToLower() == old.ToLower())
            {
                res = res.Replace(old.ToLower(), @new.ToLower());
            }

            if (res.ToUpper() == old.ToUpper())
            {
                res = res.Replace(old.ToUpper(), @new.ToUpper());
            }

            return res;
        }

        /// <summary>
        /// Get query parameters from a URL
        /// </summary>
        /// <param name="s">URL</param>
        /// <param name="param">Param name</param>
        /// <returns>Return the result</returns>
        public static string ToParam(this string s, string param)
        {
            var uri = new Uri(s);
            var res = HttpUtility.ParseQueryString(uri.Query).Get(param);
            return res;
        }

        /// <summary>
        /// Is valid email
        /// </summary>
        /// <param name="s">Email address</param>
        /// <returns>Return the result</returns>
        public static bool IsValidEmail(this string s)
        {
            var res = true;

            try
            {
                var m = new MailAddress(s);
            }
            catch
            {
                res = false;
            }

            return res;
        }

        /// <summary>
        /// Is valid phone
        /// </summary>
        /// <param name="s">Phone number</param>
        /// <returns>Return the result</returns>
        public static bool IsValidPhone(this string s)
        {
            bool res;
            try
            {
                res = Regex.IsMatch(s, ZConst.Regex.Phone);
            }
            catch
            {
                res = false;
            }
            return res;
        }

        /// <summary>
        /// Append message to current string
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="msg">Message</param>
        /// <param name="separation">separation</param>
        /// <returns>Return the result</returns>
        public static string Append(this string s, string msg, string separation)
        {
            var res = s;

            res += !string.IsNullOrWhiteSpace(res) ? separation : "";
            res += msg;

            return res;
        }

        /// <summary>
        /// Splits a string into substrings based on the characters in an array.
        /// You can specify whether the substrings include empty array elements.
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return array elements</returns>
        public static IEnumerable<string> Split(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return new List<string>();
            }

            var t = s.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries);
            var res = d ? t.Distinct() : t;

            return res;
        }

        /// <summary>
        /// Is base64 string
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static bool IsBase64(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return false;
            }

            var buffer = new Span<byte>(new byte[s.Length]);
            var res = Convert.TryFromBase64String(s, buffer, out int bytesParsed);

            if (!res)
            {
                var t = s.ToSet(ZConst.Char.Comma);
                if (t.Count > 1)
                {
                    res = Convert.TryFromBase64String(t[1], buffer, out bytesParsed);
                }
            }

            res = res || bytesParsed > 0;
            return res;
        }

        /// <summary>
        /// Find item in connection string
        /// </summary>
        /// <param name="s">Current MySQL connection string</param>
        /// <param name="find">Find</param>
        /// <returns>Return the result</returns>
        public static string FindItem(this string s, string find)
        {
            var l = s.ToSet();
            var res = l.FirstOrDefault(p => p.Contains(find)) + string.Empty;
            return res;
        }

        /// <summary>
        /// Find and extract a number from a string
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static string ToNumber(this string s)
        {
            return Regex.Match(s, @"\d+").Value;
        }

        /// <summary>
        /// Get correct port by environment
        /// </summary>
        /// <param name="s">Port number</param>
        /// <returns>Return the result</returns>
        public static string ToPort(this string s)
        {
            var res = s;

            var t1 = s == ZConst.MsPort || s == ZConst.MaPort;
            var t2 = s == "80" || s == "443";
            var t = t1 || t2;
            if (t && !ZVariable.DebugMode)
            {
                res = string.Empty;
            }

            if (!string.IsNullOrEmpty(res))
            {
                res = $":{res}";
            }

            return res;
        }

        /// <summary>
        /// Convert \r\n to br tag
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static string CrLf2Br(this string s)
        {
            return s.Replace("\r\n", "<br/>");
        }

        /// <summary>
        /// Create MySQL connection string with environment variable
        /// </summary>
        /// <param name="s">Current MySQL connection string</param>
        /// <param name="assembly">Assembly name</param>
        /// <param name="noPass">No password</param>
        /// <returns>Return the result</returns>
        public static string ToMySQL(this string s, string assembly, bool noPass = false)
        {
            var res = string.Empty;

            var equal = '=';
            var l = s.ToSet();
            foreach (var i in l)
            {
                var x = i.ToSet(equal);
                if (x.Count < 2)
                {
                    continue;
                }

                if ("server" == x[0].ToStr(Format.Lower))
                {
                    var t = ZVariable.DbServer;
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        x[1] = t;
                    }
                    res += $"{x[0]}={x[1]};";
                }

                if ("port" == x[0].ToStr(Format.Lower))
                {
                    var t = ZVariable.DbPort;
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        x[1] = t;
                    }
                    res += $"{x[0]}={x[1]};";
                }

                if ("database" == x[0].ToStr(Format.Lower))
                {
                    assembly += string.Empty;
                    var t = assembly.Contains("Analysis") ? ZVariable.DbDatabaseAnalysis : ZVariable.DbDatabaseHome;
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        x[1] = t;
                    }

                    res += $"{x[0]}={x[1]};";
                }

                if (x[0] == "Uid")
                {
                    var t = ZVariable.DbUser;
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        x[1] = t;
                    }
                    res += $"{x[0]}={x[1]};";
                }

                if ("pwd" == x[0].ToStr(Format.Lower))
                {
                    var t = ZVariable.DbPassword;
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        x[1] = t;
                    }

                    if (noPass)
                    {
                        x[1] = "******";
                    }

                    res += $"{x[0]}={x[1]};";
                }
            }

            return res;
        }

        /// <summary>
        /// Removes all leading and trailing white-space characters from the current string
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static string Trimz(this string s)
        {
            return (s + string.Empty).Trim();
        }

        /// <summary>
        /// Init letter (e.g: ToanNguyen -> TN, Nguyen -> NN)
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static string InitLetter(this string s)
        {
            s = s.ToAddSpace();
            var t = s.ToSet(ZConst.String.Space);

            var res = string.Empty;

            if (t.Count > 0)
            {
                // Get first letter
                var tx = t[0];
                var first = tx;
                if (first.Length > 0)
                {
                    first = first[0].ToString();
                }

                // Get last letter
                string last;
                var i = t.Count - 1;
                if (i > 0)
                {
                    last = t[i];
                    if (last.Length > 0)
                    {
                        last = last[0].ToString();
                    }
                }
                else
                {
                    i = tx.Length - 1;
                    last = tx[i].ToString();
                }

                // Result
                res = (first + last).ToUpper();
            }

            return res;
        }

        /// <summary>
        /// Get API URL
        /// </summary>
        /// <param name="a">Action name</param>
        /// <param name="c">Controller name</param>
        /// <param name="p">Prefix</param>
        /// <returns>Return the result</returns>
        public static string GetApiUrl(this string a, string c, string p = Zefault.ApiPrefix)
        {
            var res = $"{p}/{c}/{a}";
            return res;
        }

        /// <summary>
        /// Get plural
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="amount">Amount</param>
        /// <returns>Return the result</returns>
        public static string GetPlural(this string s, double amount)
        {
            var plural = amount > 1 ? "s" : string.Empty;
            var t = string.Format(Zormat.DisplayNumber, amount);
            var res = $"{t} {s}{plural}";
            return res;
        }

        /// <summary>
        /// https://alastaircrabtree.com/detecting-plurals-in-dot-net
        /// This is far from perfect but detects the majority of english language plurals.
        /// See https://en.wikipedia.org/wiki/English_plurals#Irregular_plurals
        /// and and http://grammar.yourdictionary.com/grammar-rules-and-tips/irregular-plurals.html
        /// for useful example cases.
        /// </summary>
        /// <param name="plural">Plural</param>
        /// <param name="singular">Singular</param>
        /// <returns>Return the result</returns>
        public static bool IsPlural(this string plural, string singular)
        {
            plural = (plural + string.Empty).ToLower();
            singular = (singular + string.Empty).ToLower();
            var commonPlurals = new Dictionary<string, string> {
                { "children", "child" },
                { "people", "person" },
                { "men", "man" }
            };

            // people => person and other "one off" edge cases
            if (commonPlurals.ContainsKey(plural) && commonPlurals[plural] == singular)
            {
                return true;
            }

            // series => series
            if (plural.EndsWith("ies") && plural == singular)
            {
                return true;
            }

            // dogs => dog
            if (plural.EndsWith("s") && singular == plural.ReplaceEnd("s"))
            {
                return true;
            }

            // babies => baby
            if (singular.EndsWith("y") && plural == singular.ReplaceEnd("y", "ies"))
            {
                return true;
            }

            // catches => catch
            if (plural.EndsWith("es") && singular == plural.ReplaceEnd("es"))
            {
                return true;
            }

            // statuses => status
            if (plural.EndsWith("uses") && singular == plural.ReplaceEnd("es"))
            {
                return true;
            }

            // synopses => synopsis
            if (plural.EndsWith("ses") && singular == plural.ReplaceEnd("es", "is"))
            {
                return true;
            }

            // wives => wife
            if (singular.EndsWith("fe") && plural == singular.ReplaceEnd("fe", "ves"))
            {
                return true;
            }

            // halves => half
            if (singular.EndsWith("f") && plural == singular.ReplaceEnd("f", "ves"))
            {
                return true;
            }

            // women => woman
            if (singular.EndsWith("man") && plural == singular.ReplaceEnd("man", "men"))
            {
                return true;
            }

            // vertices => vertex
            if (singular.EndsWith("ex") && plural == singular.ReplaceEnd("ex", "ices"))
            {
                return true;
            }

            // matricies => matrix
            if (singular.EndsWith("ix") && plural == singular.ReplaceEnd("ix", "ices"))
            {
                return true;
            }

            // indicies => index
            if (singular.EndsWith("ex") && plural == singular.ReplaceEnd("ex", "ices"))
            {
                return true;
            }

            // axes => axis
            if (singular.EndsWith("is") && plural == singular.ReplaceEnd("is", "es"))
            {
                return true;
            }

            // medium => media
            if (singular.EndsWith("um") && plural == singular.ReplaceEnd("um", "a"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Replace end
        /// </summary>
        /// <param name="word">word</param>
        /// <param name="trimString">Trim string</param>
        /// <param name="replacement">Replacement</param>
        /// <returns>Return the result</returns>
        public static string ReplaceEnd(this string word, string trimString, string replacement = "")
        {
            var res = Regex.Replace(word, $"{trimString}$", replacement);
            return res;
        }

        /// <summary>
        /// Normalize Vietnamese
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static string NormalizeVietnamese(this string s)
        {
            if (s == null)
            {
                return null;
            }

            var res = s.Normalize(NormalizationForm.FormD);
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");

            res = regex.Replace(res, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace('\u0020', '-');
            res = Regex.Replace(res, "[^0-9a-zA-Z-._@+]+", "");

            return res;
        }

        /// <summary>
        /// From base64 string
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return the result</returns>
        public static byte[] Fr64(this string s)
        {
            return string.IsNullOrWhiteSpace(s) ? null : Convert.FromBase64String(s);
        }

        /// <summary>
        /// Correct JSON data
        /// </summary>
        /// <param name="s">Invalid JSON</param>
        /// <returns>Return valid JSON</returns>
        public static string CorrectJson(this string s)
        {
            var res = s;

            if (string.IsNullOrWhiteSpace(res))
            {
                res = string.Empty;
            }

            // Remove first [
            if (res.Length > 0)
            {
                if (res[0] == '[')
                {
                    res = res[1..];
                }
            }

            // Remove last ]
            if (res.Length > 0)
            {
                if (res[^1] == ']')
                {
                    res = res[0..^1];
                }
            }

            // Remove "
            res = res.Replace("\"{", "{");
            res = res.Replace("}\"", "}");
            res = res.Replace("\"[", "[");
            res = res.Replace("]\"", "]");

            // Remove \
            res = res.Replace("\\\"", "\"");

            return res;
        }

        /// <summary>
        /// Distinct elements
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="as">Allow adding a separator to end of the return string</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <returns>Return the result</returns>
        public static string ToDistinct(this string s, bool @as = false, string c = Ztring.Semicolon)
        {
            var res = string.Empty;

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var t = s.Split(c, StringSplitOptions.RemoveEmptyEntries).Distinct();
            res = string.Join(c, t);

            if (@as)
            {
                res += c;
            }

            return res;
        }

        /// <summary>
        /// Return a copy of this string with first letter converted to uppercase
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return uppercase first letter</returns>
        public static string ToUpperFirst(this string s)
        {
            var res = string.Empty;

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            s = s.Trim();
            res = char.ToUpper(s[0]) + s.Substring(1).ToLower();

            return res;
        }

        /// <summary>
        /// Return a copy of this string with first letter of each word converted to uppercase
        /// </summary>
        /// <param name="s">String data</param>
        /// <returns>Return uppercase first letter of any words</returns>
        public static string ToUpperWords(this string s)
        {
            var res = string.Empty;

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            s = s.Trim().ToLower();
            var arr = s.ToCharArray();

            // First word
            if (arr.Length >= 1)
            {
                arr[0] = char.ToUpper(arr[0]);
            }

            // Next word after space
            for (var i = 1; i < arr.Length; i++)
            {
                if (arr[i - 1] == Zhar.Space)
                {
                    arr[i] = char.ToUpper(arr[i]);
                }
            }
            res = new string(arr);

            return res;
        }

        /// <summary>
        /// https://stackoverflow.com/questions/18627112/how-can-i-convert-text-to-pascal-case
        /// </summary>
        /// <param name="s">Original string</param>
        /// <returns>Return the text to PascalCase</returns>
        public static string ToPascalCase(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return string.Empty;
            }

            var invalidCharsRgx = new Regex("[^_a-zA-Z0-9]");
            var whiteSpace = new Regex(@"(?<=\s)");
            var startsWithLowerCaseChar = new Regex("^[a-z]");
            var firstCharFollowedByUpperCasesOnly = new Regex("(?<=[A-Z])[A-Z0-9]+$");
            var lowerCaseNextToNumber = new Regex("(?<=[0-9])[a-z]");
            var upperCaseInside = new Regex("(?<=[A-Z])[A-Z]+?((?=[A-Z][a-z])|(?=[0-9]))");

            // Replace white spaces with undescore, then replace all invalid chars with empty string
            var res = invalidCharsRgx.Replace(whiteSpace.Replace(s, "_"), string.Empty)
                // split by underscores
                .Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries)
                // set first letter to uppercase
                .Select(w => startsWithLowerCaseChar.Replace(w, m => m.Value.ToUpper()))
                // replace second and all following upper case letters to lower if there is no next lower (ABC -> Abc)
                .Select(w => firstCharFollowedByUpperCasesOnly.Replace(w, m => m.Value.ToLower()))
                // set upper case the first lower case following a number (Ab9cd -> Ab9Cd)
                .Select(w => lowerCaseNextToNumber.Replace(w, m => m.Value.ToUpper()))
                // lower second and next upper case letters except the last if it follows by any lower (ABcDEf -> AbcDef)
                .Select(w => upperCaseInside.Replace(w, m => m.Value.ToLower()));

            return string.Concat(res);
        }

        /// <summary>
        /// Convert the text to camelCase
        /// </summary>
        /// <param name="s">Original string</param>
        /// <returns>Return the text to camelCase</returns>
        public static string ToCamelCase(this string s)
        {
            var res = s.ToPascalCase();

            // First word
            var arr = res.ToCharArray();
            if (arr.Length >= 1)
            {
                arr[0] = char.ToLower(arr[0]);
            }

            res = new string(arr);

            return res;
        }

        #region -- Conheo --
        /// <summary>
        /// Add one space AbCd to Ab Cd
        /// </summary>
        /// <param name="s">Input string</param>
        /// <returns>Return string with space</returns>
        public static string ToAddSpace(this string s)
        {
            return s.Conheo(true);
        }

        /// <summary>
        /// Get prefix of string AbCd to Ab
        /// </summary>
        /// <param name="s">Input string</param>
        /// <returns>Return prefix of string</returns>
        public static string ToPrefix(this string s)
        {
            return s.Conheo(false);
        }

        /// <summary>
        /// Use for ToAddSpace and ToPrefix
        /// </summary>
        /// <param name="s">Input string</param>
        /// <param name="x">is ToAddSpace</param>
        /// <returns>Return the result</returns>
        private static string Conheo(this string s, bool x)
        {
            var res = string.Empty;

            s = (s + string.Empty).Trim();
            for (var i = 0; i < s.Length; i++)
            {
                if (i == 0)
                {
                    res = s[i].ToString();
                    continue;
                }

                var t = s[i];
                if ('A' <= t && t <= 'Z')
                {
                    if (x)
                    {
                        res += Ztring.Space;
                    }
                    else
                    {
                        break;
                    }
                }
                res += s[i];
            }
            res = res.Trim();

            return res;
        }
        #endregion

        /// <summary>
        /// Standardize string
        /// </summary>
        /// <param name="s">A string</param>
        /// <returns>Return the standardized string</returns>
        public static string Standardize(this string s)
        {
            var res = s + string.Empty;

            res = res.Replace(Ztring.Quotation, string.Empty);
            res = res.Replace(Ztring.LSquare, string.Empty);
            res = res.Replace(Ztring.RSquare, string.Empty);

            return res;
        }

        /// <summary>
        /// Remove string at last
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="remove">String need to remove</param>
        /// <param name="replace">String will replace</param>
        /// <returns>Return the string removed</returns>
        public static string RemoveLast(this string s, string remove, string replace = Ztring.Blank)
        {
            var res = string.Empty;

            try
            {
                var i = s.LastIndexOf(remove);
                if (i > 0 && s.Length >= i)
                {
                    res = s.Substring(0, i) + replace;
                }
            }
            catch
            {
                res = replace;
            }

            return res;
        }

        /// <summary>
        /// Get default value if input null or empty else trim
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="d">Default value</param>
        /// <returns>Return the string</returns>
        public static string ToDefault(this string s, string d = Ztring.Minus)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return d;
            }

            var res = s.Trim();

            return res;
        }

        /// <summary>
        /// Get full name
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <returns>Return the result</returns>
        public static string ToFullName(this string firstName, string lastName)
        {
            var first = string.Empty;
            if (!string.IsNullOrWhiteSpace(firstName))
            {
                first = firstName;
            }

            var last = string.Empty;
            if (!string.IsNullOrWhiteSpace(lastName))
            {
                last = lastName;
            }

            var fullName = first + Ztring.Space + last;

            return fullName.Trim();
        }

        /// <summary>
        /// Get first name
        /// </summary>
        /// <param name="fullName">Full name</param>
        /// <param name="lastName">Last name</param>
        /// <returns>Return the result</returns>
        public static string ToFirstName(this string fullName, out string lastName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                fullName = string.Empty;
            }

            var res = fullName;
            var index = fullName.IndexOf(ZConst.String.Space);
            if (index < 0)
            {
                lastName = string.Empty;
                return res;
            }

            lastName = fullName.Substring(index + 1).Trim();

            res = fullName.Substring(0, index).Trim();
            return res;
        }

        /// <summary>
        /// Convert string to list with separator and distinct
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct</param>
        /// <returns>Return the result</returns>
        public static List<string> ToSet(this string s, string c, bool d = true)
        {
            var res = new List<string>();

            if (string.IsNullOrWhiteSpace(s))
            {
                s = string.Empty;
            }

            var option = d ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;
            var arr = s.Split(new string[] { c }, option);
            var t = arr.Where(p => !string.IsNullOrWhiteSpace(p) || !d);

            if (d)
            {
                res = t.Select(p => p.Trim()).Distinct().ToList();
            }
            else
            {
                res = t.Select(p => p.Trim()).ToList();
            }

            return res;
        }

        /// <summary>
        /// Convert string to list with separator and distinct
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct</param>
        /// <returns>Return the result</returns>
        public static List<string> ToSet(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<string>();

            if (string.IsNullOrWhiteSpace(s))
            {
                s = string.Empty;
            }

            var arr = s.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries);
            var t = arr.Where(p => !string.IsNullOrWhiteSpace(p));

            if (d)
            {
                res = t.Select(p => p.Trim()).Distinct().ToList();
            }
            else
            {
                res = t.Select(p => p.Trim()).ToList();
            }

            return res;
        }

        /// <summary>
        /// Find value by keyword
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="keyword">Keyword</param>
        /// <param name="default">Default value if not found</param>
        /// <param name="c">Separator</param>
        /// <returns>Return the result</returns>
        public static string Find(this string s, string keyword, string @default, char c)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                s = string.Empty;
            }

            var t = s.ToSet(c);
            var res = t.FirstOrDefault(p => p.Contains(keyword));

            if (string.IsNullOrWhiteSpace(res))
            {
                res = @default;
            }
            else
            {
                res = res.Replace(keyword, string.Empty);
            }

            return res;
        }

        /// <summary>
        /// Get database from string connection
        /// </summary>
        /// <param name="s">String data</param>
        /// <param name="default">Default value if not found</param>
        /// <returns>Return the result</returns>
        public static string GetDatabase(this string s, string @default)
        {
            return s.Find("Database=", @default, Zhar.Semicolon);
        }

        #endregion

        #region -- Data --

        #region -- Short --
        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list short</returns>
        public static List<short> ToListShort(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<short>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToShort());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list short</returns>
        public static List<ushort> ToListShortU(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<ushort>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToShortU());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list short</returns>
        public static List<short?> ToListShortN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<short?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToShortN());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list short</returns>
        public static List<ushort?> ToListShortUn(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<ushort?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToShortUn());
            }

            return res;
        }
        #endregion

        #region -- Int --
        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list int</returns>
        public static List<int> ToListInt(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<int>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToInt());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list int</returns>
        public static List<uint> ToListIntU(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<uint>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToIntU());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list int</returns>
        public static List<int?> ToListIntN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<int?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToIntN());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list int</returns>
        public static List<uint?> ToListIntUn(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<uint?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToIntUn());
            }

            return res;
        }
        #endregion

        #region -- Long --
        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list long</returns>
        public static List<long> ToListLong(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<long>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToLong());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list long</returns>
        public static List<ulong> ToListLongU(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<ulong>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToLongU());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list long</returns>
        public static List<long?> ToListLongN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<long?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToLongN());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list long</returns>
        public static List<ulong?> ToListLongUn(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<ulong?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToLongUn());
            }

            return res;
        }
        #endregion

        #region -- Float --
        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list float</returns>
        public static List<float> ToListFloat(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<float>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToFloat());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list float</returns>
        public static List<float?> ToListFloatN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<float?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToFloatN());
            }

            return res;
        }
        #endregion

        #region -- Double --
        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list double</returns>
        public static List<double> ToListDouble(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<double>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToDouble());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list double</returns>
        public static List<double?> ToListDoubleN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<double?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToDoubleN());
            }

            return res;
        }
        #endregion

        #region -- Decimal --
        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list decimal</returns>
        public static List<decimal> ToListDecimal(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<decimal>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToDecimal());
            }

            return res;
        }

        /// <summary>
        /// Convert a string number separation with separator
        /// </summary>
        /// <param name="s">String number separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list decimal</returns>
        public static List<decimal?> ToListDecimalN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<decimal?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToDecimalN());
            }

            return res;
        }
        #endregion

        #region -- Guid --
        /// <summary>
        /// Convert a string separation with separator
        /// </summary>
        /// <param name="s">String separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list Guid</returns>
        public static List<Guid> ToListGuid(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<Guid>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToGuid());
            }

            return res;
        }

        /// <summary>
        /// Convert a string separation with separator
        /// </summary>
        /// <param name="s">String separation</param>
        /// <param name="c">Separator (default is semicolon)</param>
        /// <param name="d">Distinct data</param>
        /// <returns>Return list Guid</returns>
        public static List<Guid?> ToListGuidN(this string s, char c = Zhar.Semicolon, bool d = true)
        {
            var res = new List<Guid?>();

            if (string.IsNullOrWhiteSpace(s))
            {
                return res;
            }

            var arr = s.Split(c, d);
            foreach (var i in arr)
            {
                res.Add(i.ToGuidN());
            }

            return res;
        }
        #endregion

        /// <summary>
        /// Convert from JSON to instance of T
        /// </summary>
        /// <typeparam name="T">Data type</typeparam>
        /// <param name="s">JSON data</param>
        /// <returns>Return to instance of T</returns>
        public static T ToInst<T>(this string s) where T : new()
        {
            var res = new T();

            if (!string.IsNullOrWhiteSpace(s))
            {
                try
                {
                    res = JsonConvert.DeserializeObject<T>(s);
                }
                catch (Exception ex)
                {
                    s.LogInfor();
                    var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
                }
            }

            return res;
        }

        /// <summary>
        /// Convert from JSON to List
        /// </summary>
        /// <typeparam name="T">Data type</typeparam>
        /// <param name="s">JSON data</param>
        /// <returns>Return to List data</returns>
        public static List<T> ToList<T>(this string s)
        {
            var res = new List<T>();

            if (!string.IsNullOrWhiteSpace(s))
            {
                try
                {
                    res = JsonConvert.DeserializeObject<List<T>>(s);
                }
                catch (Exception ex)
                {
                    s.LogInfor();
                    var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
                }
            }

            return res;
        }

        #endregion

        #region -- RSA --

        /// <summary>
        /// Encrypt with public key
        /// </summary>
        /// <param name="data">Clear text</param>
        /// <param name="key">Public key</param>
        /// <returns>Return the result</returns>
        public static string Encrypt(this string data, string key)
        {
            var encrypt = Encoding.UTF8.GetBytes(data);
            var engine = new Pkcs1Encoding(new RsaEngine());

            key = string.Format(ZConst.PublicKey, key);

            using (var sr = new StringReader(key))
            {
                var par = (AsymmetricKeyParameter)new PemReader(sr).ReadObject();
                engine.Init(true, par);
            }

            var t = engine.ProcessBlock(encrypt, 0, encrypt.Length);
            var res = Convert.ToBase64String(t);

            return res;
        }

        /// <summary>
        /// Encrypt with private key
        /// </summary>
        /// <param name="data">Clear text</param>
        /// <param name="key">Private key</param>
        /// <returns>Return the result</returns>
        public static string EncryptWithPrivate(this string data, string key)
        {
            var encrypt = Encoding.UTF8.GetBytes(data);
            var engine = new Pkcs1Encoding(new RsaEngine());

            key = string.Format(ZConst.PrivateKey, key);
            using (var sr = new StringReader(key))
            {
                var par = (AsymmetricCipherKeyPair)new PemReader(sr).ReadObject();
                engine.Init(true, par.Public);
            }

            var t = engine.ProcessBlock(encrypt, 0, encrypt.Length);
            var res = Convert.ToBase64String(t);

            return res;
        }

        /// <summary>
        /// Decrypt with private key
        /// </summary>
        /// <param name="data">Encrypted text</param>
        /// <param name="key">Private key</param>
        /// <returns>Return the result</returns>
        public static string Decrypt(this string data, string key)
        {
            var res = data;

            try
            {
                var decrypt = Convert.FromBase64String(data);
                var engine = new Pkcs1Encoding(new RsaEngine());

                key = string.Format(ZConst.PrivateKey, key);
                using (var sr = new StringReader(key))
                {
                    var par = (AsymmetricCipherKeyPair)new PemReader(sr).ReadObject();
                    engine.Init(false, par.Private);
                }

                var t = engine.ProcessBlock(decrypt, 0, decrypt.Length);
                res = Encoding.UTF8.GetString(t);
            }
            catch { }

            return res;
        }

        #endregion

        #region -- Enums --

        /// <summary>
        /// Convert a string value to enum value
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="value">Description or value need to convert</param>
        /// <param name="default">Default value</param>
        /// <returns>Return the enum value</returns>
        public static T ToEnum<T>(this string value, T @default)
        {
            var res = @default;

            if (!typeof(T).IsEnum)
            {
                return res;
            }

            var type = typeof(T);
            var l = type.GetFields();

            foreach (var i in l)
            {
                if (Attribute.GetCustomAttribute(i, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (i.Name == value)
                    {
                        res = (T)i.GetValue(null);
                        break;
                    }

                    if (attribute.Description == value)
                    {
                        res = (T)i.GetValue(null);
                        break;
                    }
                }
                else
                {
                    if (i.Name == value)
                    {
                        res = (T)i.GetValue(null);
                        break;
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Convert a string value to enum value
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="value"></param>
        /// <param name="default">Description or value need to convert</param>
        /// <param name="sep">Separator character</param>
        /// <param name="skip">Skip convert this string</param>
        /// <returns>Return the enum value</returns>
        public static T ToEnum<T>(this string value, T @default, char sep, string skip = null)
        {
            var res = @default;

            if (string.IsNullOrWhiteSpace(value))
            {
                value = string.Empty;
            }

            if (string.IsNullOrWhiteSpace(skip))
            {
                skip = string.Empty;
            }

            var t1 = value.ToSet(sep);
            var t2 = skip.ToSet(sep);
            var l = t1.Except(t2);

            foreach (var i in l)
            {
                res = i.ToEnum<T>(@default);

                if (res.ToStr() != @default.ToStr())
                {
                    break;
                }
            }

            return res;
        }

        #endregion

        #region -- Salesforce --

        /// <summary>
        /// Get sobject URI
        /// </summary>
        /// <param name="s">Object name</param>
        /// <param name="id">ID</param>
        /// <param name="param">Param</param>
        /// <param name="ver">Apex version</param>
        /// <returns>Return the result</returns>
        public static string GetSobjectUri(this string s, string id, string param = null, string ver = ZConst.ApexVersion)
        {
            var res = string.Format(ZConst.Format.Sobject, ver, s, id, param);
            return res;
        }

        /// <summary>
        /// Get picklist URI
        /// </summary>
        /// <param name="s">Object name</param>
        /// <param name="recordType">Record type</param>
        /// <param name="fieldName">Field name</param>
        /// <param name="ver">Apex version</param>
        /// <returns>Return the result</returns>
        public static string GetPicklistUri(this string s, string recordType, string fieldName, string ver = ZConst.ApexVersion)
        {
            var res = string.Format(ZConst.Format.Picklist, ver, s, recordType, fieldName);
            return res;
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Jun-14 16:38
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;

namespace SKG.Ext
{
    using DAL.Models;
    using Rsp;

    /// <summary>
    /// Extend for the type
    /// </summary>
    public static class TypeExt
    {
        #region -- Common --

        /// <summary>
        /// Copy all properties data (skip null properties or ZConst.SkipProperty) from source to destination
        /// </summary>
        /// <typeparam name="T">The class type</typeparam>
        /// <param name="fr">Source</param>
        /// <param name="to">Destination</param>
        /// <param name="sp">Exclude unchanged columns (CreatedBy, CreatedOn)</param>
        /// <param name="sn">Allow skip null value</param>
        public static void Kopy<T>(this T fr, T to, bool sp = true, bool sn = true)
        {
            // Unchanged columns
            var t = fr.GetPropertyValue(nameof(ZeroNorModel.Skip)).ToStr();
            t += ZConst.String.Semicolon + ZConst.SkipProperty;
            var id = fr.GetPropertyValue(nameof(ZeroNorModel.Id)).ToIntU();
            if (sp && id > 0)
            {
                t += ZConst.String.Semicolon + ZConst.SkipPropertyCreated;
            }
            var skip = t.ToSet();

            // Filter
            var p = typeof(T).GetProperties().ToList();
            if (sn)
            {
                p = p.Where(x => x.GetValue(fr, null) != null).ToList();
            }

            // Clone data
            foreach (var i in p)
            {
                if (skip.Contains(i.Name))
                {
                    continue;
                }

                var v = i.GetValue(fr, null);
                if (i.CanWrite)
                {
                    i.SetValue(to, v, null);
                }
            }
        }

        /// <summary>
        /// Compare all properties data (skip null properties) from source to destination
        /// </summary>
        /// <typeparam name="T">The class type</typeparam>
        /// <param name="fr">Source</param>
        /// <param name="to">Destination</param>
        /// <returns>Return true if it is difference, else false</returns>
        public static bool Differ<T>(this T fr, T to)
        {
            if (fr == null || to == null)
            {
                return false;
            }

            var t1 = typeof(T).GetProperties();
            var t2 = t1.Where(x => x.GetValue(fr, null) != null);

            foreach (var i in t2)
            {
                if (i.Name == "Id")
                {
                    continue;
                }

                var t3 = i.GetValue(fr, null) + string.Empty;
                var t4 = i.GetValue(to, null) + string.Empty;

                if (t3 != t4)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Convert an object to JSON string
        /// </summary>
        /// <typeparam name="T">Target data type</typeparam>
        /// <param name="o">Target object</param>
        /// <param name="camelCase">Use camel case property name</param>
        /// <returns>Return to JSON data</returns>
        public static string ToJson<T>(this T o, bool camelCase = false)
        {
            var res = string.Empty;

            if (o != null)
            {
                var s = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                res = camelCase ? JsonConvert.SerializeObject(o, s) : JsonConvert.SerializeObject(o);
            }

            return res;
        }

        /// <summary>
        /// Convert object to multiple response
        /// </summary>
        /// <typeparam name="T">Target data type</typeparam>
        /// <param name="o">Target object</param>
        /// <returns>Return the result</returns>
        public static MultipleRsp ToMultipleRsp<T>(this T o)
        {
            var res = new MultipleRsp();

            var pros = o.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var i in pros)
            {
                var key = i.Name.ToCamelCase();
                var val = i.GetValue(o);

                var atts = i.GetCustomAttributes();
                if (atts.Count() > 0)
                {
                    var att = atts.First();
                    if (att is JsonIgnoreAttribute)
                    {
                        continue;
                    }

                    if (att is JsonConverterAttribute)
                    {
                        // Get format
                        var f = string.Empty;
                        var t = att as JsonConverterAttribute;
                        var param = t.ConverterParameters;
                        if (param.Length > 0)
                        {
                            f = param[0].ToString();
                        }

                        if (val is DateTime)
                        {
                            var dt = val as DateTime?;
                            val = dt?.ToString(f);
                        }
                    }
                }

                res.SetData(key, val);
            }

            return res;
        }

        /// <summary>
        /// Get display value
        /// </summary>
        /// <typeparam name="T">Target data type</typeparam>
        /// <param name="value">value</param>
        /// <returns>Return display value</returns>
        public static string GetDisplayValue<T>(this T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            if (!(fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) is DisplayAttribute[] attributes) || attributes.Length == 0)
            {
                return string.Empty;
            }

            if (attributes[0].ResourceType != null)
            {
                return LookupResource(attributes[0].ResourceType, attributes[0].Name);
            }

            return (attributes.Length > 0) ? attributes[0].Name : value.ToString();
        }

        /// <summary>
        /// Lookup resource
        /// </summary>
        /// <param name="rm">Resource manager</param>
        /// <param name="key">Resource key</param>
        /// <returns>Return string value</returns>
        private static string LookupResource(Type rm, string key)
        {
            foreach (var staticProperty in rm.GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public))
            {
                if (staticProperty.PropertyType == typeof(ResourceManager))
                {
                    var resourceManager = (ResourceManager)staticProperty.GetValue(null, null);
                    return resourceManager.GetString(key);
                }
            }

            return key;
        }

        #endregion

        /// <summary>
        /// Get property display name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p">Property expression</param>
        /// <returns>Return the result</returns>
        public static string GetDisplayName<T>(Expression<Func<T, object>> p)
        {
            var pi = GetPropertyInformation(p.Body);
            if (pi == null)
            {
                return p.ToString();
            }

            var att = pi.GetCustomAttribute<DisplayNameAttribute>(false);
            if (att == null)
            {
                return pi.Name;
            }

            return att.DisplayName;
        }

        /// <summary>
        /// Get property information
        /// </summary>
        /// <param name="p">Property expression</param>
        /// <returns>Return the result</returns>
        private static MemberInfo GetPropertyInformation(Expression p)
        {
            var me = p as MemberExpression;
            if (me == null)
            {
                var unary = p as UnaryExpression;
                if (unary != null && unary.NodeType == ExpressionType.Convert)
                {
                    me = unary.Operand as MemberExpression;
                }
            }

            if (me != null && me.Member.MemberType == MemberTypes.Property)
            {
                return me.Member;
            }

            return null;
        }
    }
}
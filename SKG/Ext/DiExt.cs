﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-16 13:11
 * Update       : 2020-Jul-16 13:11
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Microsoft.Extensions.DependencyInjection;
using System;

namespace SKG.Ext
{
    /// <summary>
    /// Extend for dependency injection
    /// </summary>
    public static class DiExt
    {
        #region -- Common --

        /// <summary>
        /// Add authentication message sender
        /// </summary>
        /// <param name="res">Services</param>
        /// <param name="co">Configure options</param>
        /// <returns></returns>
        public static IServiceCollection AddZmail(this IServiceCollection res, Action<ZEmail.AuthMessageSenderOptions> co)
        {
            res.Configure(co);
            res.AddSingleton<ZEmail, ZEmail>();

            return res;
        }

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-26 09:58
 * Update       : 2020-Oct-06 16:08
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Serilog;

namespace SKG.Ext
{
    using Rsp;
    using Ztring = ZConst.String;

    /// <summary>
    /// Extend for the logger
    /// </summary>
    public static class LogExt
    {
        #region -- Methods --

        /// <summary>
        /// Start logger to write log file<br/>
        /// https://github.com/serilog/serilog/wiki/Getting-Started
        /// </summary>
        /// <param name="name">Log file name</param>
        public static void StartLogger(this string name)
        {
            var file = $"logs/{name}{ZVariable.AspCustomer + "-"}.log";
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(file, rollingInterval: RollingInterval.Day)
                .CreateLogger();

            LogInfor($"{name} is started");
        }

        /// <summary>
        /// Write a log event information level
        /// </summary>
        /// <param name="rsp">Single response</param>
        /// <param name="s">Prefix</param>
        /// <param name="allow">Allow to write log independence variable environment</param>
        public static void LogInfor(this SingleRsp rsp, string s = Ztring.Blank, bool allow = false)
        {
            if (Allow || allow)
            {
                var msg = "data null";
                if (rsp != null)
                {
                    msg = rsp.Success ? rsp.Data.ToStr() : rsp.Message;
                }

                msg = string.Format(ZConst.FormatError, s + Ztring.Space + msg);
                Log.Information(msg);
            }
        }

        /// <summary>
        /// Write a log event information level
        /// </summary>
        /// <param name="msg">Message template describing the event</param>
        /// <param name="s">Prefix</param>
        /// <param name="allow">Allow to write log independence variable environment</param>
        public static void LogInfor(this string msg, string s = Ztring.Blank, bool allow = false)
        {
            if (Allow || allow)
            {
                msg = string.Format(ZConst.FormatError, s + Ztring.Space + msg);
                Log.Information(msg);
            }
        }

        /// <summary>
        /// Log information
        /// </summary>
        /// <param name="t">Data</param>
        /// <param name="path">Path</param>
        /// <param name="err">Error</param>
        public static void LogInfor(this MultipleRsp t, string path, string err)
        {
            if (t == null)
            {
                return;
            }

            string msg;
            if (t.Success)
            {
                msg = $"{path} {t.ToJson()}";
            }
            else
            {
                msg = $"{err} -> {path} {t.ToJson()}";
            }

            if (t.Data != null)
            {
                msg.LogInfor();
            }
        }

        /// <summary>
        /// Write a log event error level
        /// </summary>
        /// <param name="msg">Message template describing the event</param>
        /// <param name="s">Prefix</param>
        /// <param name="allow">Allow to write log independence variable environment</param>
        public static void LogError(this string msg, string s = Ztring.Blank, bool allow = false)
        {
            if (Allow || allow)
            {
                msg = string.Format(ZConst.FormatError, s + Ztring.Space + msg);
                Log.Error(msg);
            }
        }

        /// <summary>
        /// Write a log event debug level
        /// </summary>
        /// <param name="msg">Message template describing the event</param>
        /// <param name="s">Prefix</param>
        /// <param name="allow">Allow to write log independence variable environment</param>
        public static void LogDebug(this string msg, string s = Ztring.Blank, bool allow = false)
        {
            if (Allow || allow)
            {
                msg = string.Format(ZConst.FormatError, s + Ztring.Space + msg);
                Log.Debug(msg);
            }
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Allow to write log
        /// </summary>
        public static bool Allow
        {
            get
            {
                if (_allow == null)
                {
                    _allow = ZVariable.AllowWriteLog;
                }

                return _allow.Value;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Allow to write log
        /// </summary>
        private static bool? _allow;

        #endregion
    }
}
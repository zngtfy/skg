﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Jun-14 16:38
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;

namespace SKG.Ext
{
    /// <summary>
    /// Extend for the byte
    /// </summary>
    public static class ByteExt
    {
        #region -- Common --

        /// <summary>
        /// To base64 string
        /// </summary>
        /// <param name="o">Data</param>
        /// <returns>Return the result</returns>
        public static string To64(this byte[] o)
        {
            return o == null ? string.Empty : Convert.ToBase64String(o);
        }

        #endregion
    }
}
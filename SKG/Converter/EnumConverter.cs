﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Sep-07 21:15
 * Update       : 2020-Sep-07 21:15
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SKG.Ext;
using System;

namespace SKG.Converter
{
    /// <summary>
    /// Custom Enum JSON serializer/deserializer
    /// </summary>
    public class EnumConverter : JsonConverter
    {
        #region -- Overrides --

        /// <summary>
        /// Can convert
        /// </summary>
        /// <param name="objectType">Object type</param>
        /// <returns>Return the result</returns>
        public override bool CanConvert(Type objectType)
        {
            var res = objectType == typeof(Enum);
            return res;
        }

        /// <summary>
        /// Reads value from JSON
        /// </summary>
        /// <param name="reader">JSON reader</param>
        /// <param name="objectType">Target type</param>
        /// <param name="existingValue">Existing value</param>
        /// <param name="serializer">JSON serializer</param>
        /// <returns>Deserialized DateTime</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var t = JToken.Load(reader);
            var res = t.ToObject<Enum>();
            return res;
        }

        /// <summary>
        /// Writes value to JSON
        /// </summary>
        /// <param name="writer">JSON writer</param>
        /// <param name="value">Value to be written</param>
        /// <param name="serializer">JSON serializer</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteValue(string.Empty);
            }
            else
            {
                writer.WriteValue(value.ToString().ToAddSpace());
            }
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public EnumConverter() { }

        #endregion
    }
}
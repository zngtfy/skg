﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-02 08:12
 * Update       : 2020-Jul-02 08:12
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace SKG.Converter
{
    using Ext;

    /// <summary>
    /// Custom DateTime JSON serializer/deserializer
    /// </summary>
    public class TimeFormatConverter : DateTimeConverterBase
    {
        #region -- Overrides --

        /// <summary>
        /// Writes value to JSON
        /// </summary>
        /// <param name="writer">JSON writer</param>
        /// <param name="value">Value to be written</param>
        /// <param name="serializer">JSON serializer</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToString(_format));
        }

        /// <summary>
        /// Reads value from JSON
        /// </summary>
        /// <param name="reader">JSON reader</param>
        /// <param name="objectType">Target type</param>
        /// <param name="existingValue">Existing value</param>
        /// <param name="serializer">JSON serializer</param>
        /// <returns>Deserialized DateTime</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }

            var s = reader.Value.ToString();
            if (s.IsNumeric()) // convert Unix time
            {
                var l = s.ToLong();
                var res = l.ToDateTime();
                return res;
            }
            else
            {
                if (DateTime.TryParseExact(s, _format, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime res))
                {
                    return res;
                }
            }

            return DateTime.Now;
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public TimeFormatConverter()
        {
            _format = ZConst.Format.DmyHms;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="f">Format</param>
        public TimeFormatConverter(string f)
        {
            _format = f;
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Format
        /// </summary>
        private readonly string _format;

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-02 08:12
 * Update       : 2020-Jul-02 08:12
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json.Converters;

namespace SKG.Converter
{
    /// <summary>
    /// Date format converter
    /// </summary>
    public class DateFormatConverter : IsoDateTimeConverter
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public DateFormatConverter()
        {
            DateTimeFormat = ZConst.Format.DmyHms;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="f">Format</param>
        public DateFormatConverter(string f)
        {
            DateTimeFormat = f;
        }

        #endregion
    }
}
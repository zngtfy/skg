﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-02 08:12
 * Update       : 2020-Sep-07 21:15
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;

namespace SKG.Converter
{
    using Ext;
    using static ZConst;

    /// <summary>
    /// Decimal converter
    /// </summary>
    public class DecimalConverter : JsonConverter
    {
        #region -- Overrides --

        /// <summary>
        /// Can convert
        /// </summary>
        /// <param name="objectType">Object type</param>
        /// <returns>Return the result</returns>
        public override bool CanConvert(Type objectType)
        {
            var a = objectType == typeof(decimal) || objectType == typeof(decimal?);
            var b = objectType == typeof(double) || objectType == typeof(double?);
            var c = objectType == typeof(float) || objectType == typeof(float?);

            var res = a || b || c;
            return res;
        }

        /// <summary>
        /// Reads value from JSON
        /// </summary>
        /// <param name="reader">JSON reader</param>
        /// <param name="objectType">Target type</param>
        /// <param name="existingValue">Existing value</param>
        /// <param name="serializer">JSON serializer</param>
        /// <returns>Deserialized DateTime</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var token = JToken.Load(reader);
            if (token.Type == JTokenType.Float || token.Type == JTokenType.Integer)
            {
                return token.ToObject<decimal>();
            }

            if (token.Type == JTokenType.String)
            {
                // Customize this to suit your needs
                return Decimal.Parse(token.ToString(), CultureInfo.GetCultureInfo("es-ES"));
            }

            if (token.Type == JTokenType.Null && objectType == typeof(decimal?))
            {
                return null;
            }

            throw new JsonSerializationException("Unexpected token type: " + token.Type.ToString());
        }

        /// <summary>
        /// Writes value to JSON
        /// </summary>
        /// <param name="writer">JSON writer</param>
        /// <param name="value">Value to be written</param>
        /// <param name="serializer">JSON serializer</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var x = value.ToDecimalN();
            if (x == null)
            {
                writer.WriteValue(string.Empty);
            }
            else
            {
                var y = string.Format(_f, x.Value);
                writer.WriteValue(y);
            }
        }

        #endregion

        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public DecimalConverter()
        {
            _f = Default.FormatPercent;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="f">Format</param>
        public DecimalConverter(string f)
        {
            _f = f;
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Format
        /// </summary>
        private readonly string _f;

        #endregion
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Jul-03 09:15
 * Checklist    : 1.0
 * Status       : Done
 */
#endregion

using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;
using System.Text;

namespace SKG
{
    using Ext;

    /// <summary>
    /// Hash
    /// </summary>
    public class ZHash
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="s">Data</param>
        public ZHash(string s)
        {
            _dat = s + string.Empty;

            // Generate a 128-bit salt using a secure PRNG
            var salt = new byte[128 / 8];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(salt);
            rng.Dispose();

            // Derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            var hash = KeyDerivation.Pbkdf2(_dat, salt, KeyDerivationPrf.HMACSHA1, 10000, 256 / 8);

            Value = Convert.ToBase64String(hash);
            Salt = Convert.ToBase64String(salt);
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="value">Hash value</param>
        /// <param name="salt">Salt</param>
        public ZHash(string value, string salt)
        {
            Value = value;
            Salt = salt;
        }

        /// <summary>
        /// Create token, format: TOKEN;time;UID
        /// </summary>
        /// <param name="d">Date and time create</param>
        /// <param name="uid">Site UID</param>
        /// <param name="lower">Lowercase</param>
        /// <returns>Return the result</returns>
        public string CreateToken(DateTime d, string uid, bool lower = true)
        {
            var semi = ZConst.Format.Semicolon;
            var time = d.ToUnixTime();
            var f = string.Format(semi, Value, time);

            var token = HmacSha512(f, Salt, lower);
            var res = string.Format(semi, token, time);

            res = string.Format(semi, res, uid);
            return res;
        }

        /// <summary>
        /// Valid token
        /// </summary>
        /// <param name="s">Token needs to valid</param>
        /// <returns>Return the result</returns>
        public bool ValidToken(string s)
        {
            // Check token valid
            var l1 = s.ToSet();
            if (l1.Count < 3)
            {
                "Invalid token".LogError();
                return false;
            }

            // Check token live
            var time = l1[1].ToLong();
            var t1 = DateTime.Now.ToUnixTime();
            var t2 = t1 - time;
            if (t2 > TimeLive && TimeLive > 0)
            {
                "Token expired".LogError();
                return false;
            }

            // Compare token
            var d = time.ToDateTime();
            var token = CreateToken(d, string.Empty);
            var l2 = token.ToSet();
            var res = l1[0].ToLower() == l2[0].ToLower();

            return res;
        }

        /// <summary>
        /// Compute SHA 1
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="lower">Lowercase</param>
        /// <returns>Return the result</returns>
        public static string ComputeSha(string data, bool lower = true)
        {
            var sha = new SHA1CryptoServiceProvider();
            var bytes = Encoding.UTF8.GetBytes(data);
            var hash = sha.ComputeHash(bytes);
            var sb = new StringBuilder(hash.Length * 2);
            var f = lower ? "x2" : "X2";

            foreach (var i in hash)
            {
                sb.Append(i.ToString(f));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Compute HMAC SHA-256
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="key">Key</param>
        /// <param name="lower">Lowercase</param>
        /// <returns>Return the result</returns>
        public static string HmacSha256(string data, string key, bool lower = true)
        {
            var res = "";

            data += "";
            key += "";

            using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(key)))
            {
                var toan = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
                res = BitConverter.ToString(toan).Replace("-", "");
            }

            res = lower ? res.ToLower() : res.ToUpper();
            return res;
        }

        /// <summary>
        /// Compute HMAC SHA-512
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="key">Key</param>
        /// <param name="lower">Lowercase</param>
        /// <returns>Return the result</returns>
        public static string HmacSha512(string data, string key, bool lower = true)
        {
            var res = "";

            data += "";
            key += "";

            using (var hmac = new HMACSHA512(Encoding.UTF8.GetBytes(key)))
            {
                var toan = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
                res = BitConverter.ToString(toan).Replace("-", "");
            }

            res = lower ? res.ToLower() : res.ToUpper();
            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Hash value
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Salt
        /// </summary>
        public string Salt { get; private set; }

        /// <summary>
        /// Pepper
        /// </summary>
        public int Pepper
        {
            get
            {
                var res = Value.GetHashCode() + Salt.GetHashCode();
                return res;
            }
        }

        /// <summary>
        /// Time token live (seconds), if it is zero don't check time
        /// </summary>
        public ushort TimeLive
        {
            get
            {
                return _timeLive;
            }
            set
            {
                if (value <= 0)
                {
                    value = MinTimeLive;
                }

                if (value >= MaxTimeLive)
                {
                    value = MaxTimeLive;
                }

                _timeLive = value;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Data
        /// </summary>
        private readonly string _dat;

        /// <summary>
        /// Time token live (seconds)
        /// </summary>
        private ushort _timeLive;

        #endregion

        #region -- Constants --

        /// <summary>
        /// Min time token live (seconds)
        /// </summary>
        private const ushort MinTimeLive = 120;

        /// <summary>
        /// Max time token live (seconds)
        /// </summary>
        private const ushort MaxTimeLive = ushort.MaxValue;

        #endregion
    }
}
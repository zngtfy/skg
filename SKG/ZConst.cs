﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2021-Oct-04 08:49
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

namespace SKG
{
    /// <summary>
    /// Constant
    /// </summary>
    public class ZConst
    {
        #region -- Common field --

        /// <summary>
        /// Super admin user name
        /// </summary>
        public const string SaUn = "admin";

        /// <summary>
        /// Payload
        /// </summary>
        public const string Payload = "payload";

        /// <summary>
        /// Session
        /// </summary>
        public const string Session = "session";

        /// <summary>
        /// Public key format
        /// </summary>
        public const string PublicKey = "-----BEGIN PUBLIC KEY-----\n{0}\n-----END PUBLIC KEY-----";

        /// <summary>
        /// Private key format
        /// </summary>
        public const string PrivateKey = "-----BEGIN RSA PRIVATE KEY-----\n{0}\n-----END RSA PRIVATE KEY-----";

        /// <summary>
        /// Format error
        /// </summary>
        public const string FormatError = "~~~~~TOAN-LOG~~~~~ {0}";

        /// <summary>
        /// Version information file
        /// </summary>
        public const string VersionFile = "version.txt";

        /// <summary>
        /// Error synchronize
        /// </summary>
        public const string ErrorSync = "Cannot synchronize";

        /// <summary>
        /// New line
        /// </summary>
        public const string NewLine = "\r\n";

        /// <summary>
        /// Header Authorization
        /// </summary>
        public const string Authorization = "Authorization";

        /// <summary>
        /// Header Auth
        /// </summary>
        public const string HeaderAuth = "Auth";

        /// <summary>
        /// Header User
        /// </summary>
        public const string HeaderUser = "User";

        /// <summary>
        /// Cookie AccessToken
        /// </summary>
        public const string AccessToken = "AccessToken";

        /// <summary>
        /// Cookie RefreshToken
        /// </summary>
        public const string RefreshToken = "RefreshToken";

        /// <summary>
        /// MA API debug port
        /// </summary>
        public const string MaApiDebugPort = "44395";

        /// <summary>
        /// MA API production port
        /// </summary>
        public const string MaApiPort = "8095";

        /// <summary>
        /// MS API debug port
        /// </summary>
        public const string MsApiDebugPort = "44396";

        /// <summary>
        /// MS API production port
        /// </summary>
        public const string MsApiPort = "8096";

        /// <summary>
        /// MS API debug port
        /// </summary>
        public const string MtApiDebugPort = "44397";

        /// <summary>
        /// MS API production port
        /// </summary>
        public const string MtApiPort = "8097";

        /// <summary>
        /// MA production port
        /// </summary>
        public const string MaPort = "44398";

        /// <summary>
        /// MS production port
        /// </summary>
        public const string MsPort = "44399";

        /// <summary>
        /// Code using to testing
        /// </summary>
        public const string MyCode = "wuhan^virux-2019";

        /// <summary>
        /// Exclude unchanged columns (Id, DevName, SiteId, SyncStatus, SyncMessage, MsId, MaId)
        /// </summary>
        public const string SkipProperty = "Id;DevName;SiteId;SyncStatus;SyncMessage;MsId;MaId";

        /// <summary>
        /// Exclude unchanged columns (CreatedBy, CreatedOn)
        /// </summary>
        public const string SkipPropertyCreated = "CreatedBy;CreatedOn";

        /// <summary>
        /// Key ActionTab
        /// </summary>
        public const string KeyActionTab = "ActionTab";

        /// <summary>
        /// Key ClientId
        /// </summary>
        public const string KeyClientId = "ClientId";

        /// <summary>
        /// Key SessionId
        /// </summary>
        public const string KeySessionId = "SessionId";

        /// <summary>
        /// Key TabId
        /// </summary>
        public const string KeyTabId = "TabId";

        /// <summary>
        /// ZLog UI
        /// </summary>
        public const string ZLogUi = "ZLogUi";

        /// <summary>
        /// Apex version
        /// </summary>
        public const string ApexVersion = "v53.0";

        #endregion

        #region -- Common class --

        /// <summary>
        /// String
        /// </summary>
        public class String
        {
            /// <summary>
            /// Empty
            /// </summary>
            public const string Empty = "";

            /// <summary>
            /// Space
            /// </summary>
            public const string Space = " ";

            /// <summary>
            /// Grave accent
            /// </summary>
            public const string GraveAccent = "`";

            /// <summary>
            /// Tilde
            /// </summary>
            public const string Tilde = "~";

            /// <summary>
            /// Exclamation
            /// </summary>
            public const string Exclamation = "!";

            /// <summary>
            /// At sign
            /// </summary>
            public const string AtSign = "@";

            /// <summary>
            /// Sharp
            /// </summary>
            public const string Sharp = "#";

            /// <summary>
            /// Dollar
            /// </summary>
            public const string Dollar = "$";

            /// <summary>
            /// Percent
            /// </summary>
            public const string Percent = "%";

            /// <summary>
            /// Caret
            /// </summary>
            public const string Caret = "^";

            /// <summary>
            /// Ampersand
            /// </summary>
            public const string Ampersand = "&";

            /// <summary>
            /// Asterisk
            /// </summary>
            public const string Asterisk = "*";

            /// <summary>
            /// Left parenthesis
            /// </summary>
            public const string LParenthesis = "(";

            /// <summary>
            /// Right parenthesis
            /// </summary>
            public const string RParenthesis = ")";

            /// <summary>
            /// Minus
            /// </summary>
            public const string Minus = "-";

            /// <summary>
            /// Underscore
            /// </summary>
            public const string Underscore = "_";

            /// <summary>
            /// Plus
            /// </summary>
            public const string Plus = "+";

            /// <summary>
            /// Equal
            /// </summary>
            public const string Equal = "=";

            /// <summary>
            /// Left square
            /// </summary>
            public const string LSquare = "[";

            /// <summary>
            /// Left curly
            /// </summary>
            public const string LCurly = "{";

            /// <summary>
            /// Right square
            /// </summary>
            public const string RSquare = "]";

            /// <summary>
            /// Right curly
            /// </summary>
            public const string RCurly = "}";

            /// <summary>
            /// Back slash
            /// </summary>
            public const string BackSlash = "\\";

            /// <summary>
            /// Vertical bar
            /// </summary>
            public const string VBar = "|";

            /// <summary>
            /// Semicolon
            /// </summary>
            public const string Semicolon = ";";

            /// <summary>
            /// Colon
            /// </summary>
            public const string Colon = ":";

            /// <summary>
            /// Apostrophe
            /// </summary>
            public const string Apostrophe = "'";

            /// <summary>
            /// Quotation
            /// </summary>
            public const string Quotation = "\"";

            /// <summary>
            /// Comma
            /// </summary>
            public const string Comma = ",";

            /// <summary>
            /// Less-than
            /// </summary>
            public const string Less = "<";

            /// <summary>
            /// Dot
            /// </summary>
            public const string Dot = ".";

            /// <summary>
            /// Greater-than
            /// </summary>
            public const string Greater = ">";

            /// <summary>
            /// Question
            /// </summary>
            public const string Question = "?";

            /// <summary>
            /// Slash
            /// </summary>
            public const string Slash = "/";

            /// <summary>
            /// Blank
            /// </summary>
            public const string Blank = "";
        }

        /// <summary>
        /// Char
        /// </summary>
        public class Char
        {
            /// <summary>
            /// Space
            /// </summary>
            public const char Space = ' ';

            /// <summary>
            /// Grave accent
            /// </summary>
            public const char GraveAccent = '`';

            /// <summary>
            /// Tilde
            /// </summary>
            public const char Tilde = '~';

            /// <summary>
            /// Exclamation
            /// </summary>
            public const char Exclamation = '!';

            /// <summary>
            /// At sign
            /// </summary>
            public const char AtSign = '@';

            /// <summary>
            /// Sharp
            /// </summary>
            public const char Sharp = '#';

            /// <summary>
            /// Dollar
            /// </summary>
            public const char Dollar = '$';

            /// <summary>
            /// Percent
            /// </summary>
            public const char Percent = '%';

            /// <summary>
            /// Caret
            /// </summary>
            public const char Caret = '^';

            /// <summary>
            /// Ampersand
            /// </summary>
            public const char Ampersand = '&';

            /// <summary>
            /// Asterisk
            /// </summary>
            public const char Asterisk = '*';

            /// <summary>
            /// Left parenthesis
            /// </summary>
            public const char LParenthesis = '(';

            /// <summary>
            /// Right parenthesis
            /// </summary>
            public const char RParenthesis = ')';

            /// <summary>
            /// Minus
            /// </summary>
            public const char Minus = '-';

            /// <summary>
            /// Underscore
            /// </summary>
            public const char Underscore = '_';

            /// <summary>
            /// Plus
            /// </summary>
            public const char Plus = '+';

            /// <summary>
            /// Equal
            /// </summary>
            public const char Equal = '=';

            /// <summary>
            /// Left square
            /// </summary>
            public const char LSquare = '[';

            /// <summary>
            /// Left curly
            /// </summary>
            public const char LCurly = '{';

            /// <summary>
            /// Right square
            /// </summary>
            public const char RSquare = ']';

            /// <summary>
            /// Right curly
            /// </summary>
            public const char RCurly = '}';

            /// <summary>
            /// Back slash
            /// </summary>
            public const char BackSlash = '\\';

            /// <summary>
            /// Vertical bar
            /// </summary>
            public const char VBar = '|';

            /// <summary>
            /// Semicolon
            /// </summary>
            public const char Semicolon = ';';

            /// <summary>
            /// Colon
            /// </summary>
            public const char Colon = ':';

            /// <summary>
            /// Apostrophe
            /// </summary>
            public const char Apostrophe = '\'';

            /// <summary>
            /// Quotation
            /// </summary>
            public const char Quotation = '\'';

            /// <summary>
            /// Comma
            /// </summary>
            public const char Comma = ',';

            /// <summary>
            /// Less-than
            /// </summary>
            public const char Less = '<';

            /// <summary>
            /// Dot
            /// </summary>
            public const char Dot = '.';

            /// <summary>
            /// Greater-than
            /// </summary>
            public const char Greater = '>';

            /// <summary>
            /// Question
            /// </summary>
            public const char Question = '?';

            /// <summary>
            /// Slash
            /// </summary>
            public const char Slash = '/';
        }

        /// <summary>
        /// Date and time format string
        /// </summary>
        public class Format
        {
            /// <summary>
            /// dd-MM-yyyy
            /// </summary>
            public const string Dmy = "dd-MM-yyyy";

            /// <summary>
            /// yyyy-MM-dd
            /// </summary>
            public const string Ymd = "yyyy-MM-dd";

            /// <summary>
            /// HH:mm:ss
            /// </summary>
            public const string Hms = "HH:mm:ss";

            /// <summary>
            /// HH:mm
            /// </summary>
            public const string Hm = "HH:mm";

            /// <summary>
            /// dd-MM-yyyy HH:mm:ss
            /// </summary>
            public const string DmyHms = Dmy + String.Space + Hms;

            /// <summary>
            /// yyyy-MM-dd HH:mm:ss
            /// </summary>
            public const string YmdHms = Ymd + String.Space + Hms;

            /// <summary>
            /// yyyy-MM-dd HH:mm:ss
            /// </summary>
            public const string YmdHm = Ymd + String.Space + Hm;

            /// <summary>
            /// yyyy/MM/dd HH:mm:ss
            /// </summary>
            public const string IsoYmdHms = "yyyy/MM/dd " + Hms;

            /// <summary>
            /// yyyy-MM-ddTHH:mm:ss.fffZ (ISO8601 with 3 decimal places)
            /// </summary>
            public const string Iso8601 = Ymd + "'T'" + Hms + ".fff'Z'";

            /// <summary>
            /// {0}/{1}
            /// </summary>
            public const string Slash = "{0}" + String.Slash + "{1}";

            /// <summary>
            /// {0};{1}
            /// </summary>
            public const string Semicolon = "{0}" + String.Semicolon + "{1}";

            /// <summary>
            /// {0}^{1}
            /// </summary>
            public const string Caret = "{0}" + String.Caret + "{1}";

            /// <summary>
            /// Format {0:#,0}
            /// </summary>
            public const string DisplayNumber = "{0:#,0}";

            /// <summary>
            /// All
            /// </summary>
            public const string PicklistAll = "-- All {0} --";

            /// <summary>
            /// Select
            /// </summary>
            public const string PicklistSelect = "-- Select {0} --";

            /// <summary>
            /// $"{ApexVersion}/sobjects/{objectName}/{id}{param}"
            /// </summary>
            public const string Sobject = "{0}/sobjects/{1}/{2}{3}";

            /// <summary>
            /// $"{ApexVersion}/ui-api/object-info/{objectName}/picklist-values/{recordType}/{fieldName}"
            /// </summary>
            public const string Picklist = "{0}/ui-api/object-info/{1}/picklist-values/{2}/{3}";
        }

        /// <summary>
        /// Minimum size for configuration
        /// </summary>
        public class MinSize
        {
            /// <summary>
            /// Amount
            /// </summary>
            public const ushort Amount = 0;

            /// <summary>
            /// Password
            /// </summary>
            public const ushort Password = 8;
        }

        /// <summary>
        /// Maximum size for configuration
        /// </summary>
        public class MaxSize
        {
            /// <summary>
            /// No
            /// </summary>
            public const ushort No = 16;

            /// <summary>
            /// Code
            /// </summary>
            public const ushort Code = 32;

            /// <summary>
            /// Password
            /// </summary>
            public const ushort Password = 48;

            /// <summary>
            /// Developer name
            /// </summary>
            public const ushort DevName = 64;

            /// <summary>
            /// Name
            /// </summary>
            public const ushort Name = 128;

            /// <summary>
            /// CreatedBy or ModifiedBy
            /// </summary>
            public const ushort UserName = 256;

            /// <summary>
            /// Description
            /// </summary>
            public const ushort Description = 512;

            /// <summary>
            /// String length 4
            /// </summary>
            public const int L4 = 4;

            /// <summary>
            /// String length 6
            /// </summary>
            public const int L6 = 6;

            /// <summary>
            /// String length 8
            /// </summary>
            public const int L8 = 8;

            /// <summary>
            /// String length 12
            /// </summary>
            public const int L12 = 12;

            /// <summary>
            /// String length 16
            /// </summary>
            public const int L16 = 16;

            /// <summary>
            /// String length 32
            /// </summary>
            public const int L32 = 32;

            /// <summary>
            /// String length 64
            /// </summary>
            public const int L64 = 64;

            /// <summary>
            /// String length 128
            /// </summary>
            public const int L128 = 128;

            /// <summary>
            /// String length 256
            /// </summary>
            public const int L256 = 256;

            /// <summary>
            /// String length 512
            /// </summary>
            public const int L512 = 512;

            /// <summary>
            /// String length 1024
            /// </summary>
            public const int L1024 = 1024;

            /// <summary>
            /// String length 2048
            /// </summary>
            public const int L2048 = 2048;

            /// <summary>
            /// String length 3072
            /// </summary>
            public const int L3072 = 3072;

            /// <summary>
            /// Data list
            /// </summary>
            public const ushort DataList = 1000;
        }

        /// <summary>
        /// Setting code
        /// </summary>
        public class Code
        {
            /// <summary>
            /// Language
            /// </summary>
            public const string Language = "LANG";

            /// <summary>
            /// Project
            /// </summary>
            public const string Project = "Project";

            /// <summary>
            /// Expand auth
            /// </summary>
            public const string ExpandAuth = "ExpandAuth";
        }

        /// <summary>
        /// Default value
        /// </summary>
        public class Default
        {
            /// <summary>
            /// Language code
            /// </summary>
            public const string Language = "en-us";

            /// <summary>
            /// AES passphase
            /// </summary>
            public const string Passphase = "~<*>~";

            /// <summary>
            /// API prefix
            /// </summary>
            public const string ApiPrefix = "api";

            /// <summary>
            /// Separator |^|
            /// </summary>
            public const string Separator = String.VBar + String.Caret + String.VBar;

            /// <summary>
            /// Format percent 1% (without decimal digit)
            /// </summary>
            public const string FormatPercent = "{0:P0}";

            /// <summary>
            /// Format separator {0}|^|{1}
            /// </summary>
            public const string FormatSeparator = "{0}" + Separator + "{1}";

            /// <summary>
            /// Max number of record to sync
            /// </summary>
            public const ushort MaxSizeSync = 3000;

            /// <summary>
            /// Page size
            /// </summary>
            public const ushort PageSize = 10;

            /// <summary>
            /// Serial number format {0:0000#}
            /// </summary>
            public const string SerialFormat = "{0:0000#}";

            #region -- File --
            /// <summary>
            /// Maximum file size allow upload (bytes = 2MB)
            /// </summary>
            public const int FileMaxSize = 2 * MB;

            /// <summary>
            /// Byte size
            /// </summary>
            public const int KB = 1024, MB = KB * 1024;

            /// <summary>
            /// Maximum of files allow upload
            /// </summary>
            public const int FileMaxCount = 10;

            /// <summary>
            /// Maximum file's name characters
            /// </summary>
            public const int FileMaxName = 200;
            #endregion

            #region -- Path --
            /// <summary>
            /// Private path (private)
            /// </summary>
            public const string PrivatePath = "/private";

            /// <summary>
            /// Media path (public)
            /// </summary>
            public const string MediaPath = "/media";

            /// <summary>
            /// Avatar path
            /// </summary>
            public const string AvatarPath = "/Avatar";
            #endregion
        }

        /// <summary>
        /// Message value
        /// </summary>
        public class Message
        {
            /// <summary>
            /// All
            /// </summary>
            public const string PicklistAll = "-- All --";

            /// <summary>
            /// Select
            /// </summary>
            public const string PicklistSelect = "-- Select --";

            /// <summary>
            /// No email template
            /// </summary>
            public const string NoEmailTemplate = "There is no email template, please setup it.";

            /// <summary>
            /// Invalid email
            /// </summary>
            public const string InvalidEmail = "Email is not valid";

            /// <summary>
            /// Invalid phone
            /// </summary>
            public const string InvalidPhone = "Phone number is not valid";
        }

        /// <summary>
        /// Regular expression
        /// </summary>
        public class Regex
        {
            /// <summary>
            /// Email
            /// </summary>
            public const string Email = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            /// <summary>
            /// Phone
            /// </summary>
            public const string Phone = @"^[\+]?[(]?[\+]?[0-9]{2,}[)]?[-\s\. 0-9]{6,16}$";
        }

        /// <summary>
        /// Scope
        /// </summary>
        public class Scope
        {
            /// <summary>
            /// Site
            /// </summary>
            public const string Site = "Site";

            /// <summary>
            /// User
            /// </summary>
            public const string User = "User";

            /// <summary>
            /// Home lot
            /// </summary>
            public const string Lot = "Lot";

            /// <summary>
            /// Apartment
            /// </summary>
            public const string Apt = "Apt";
        }

        /// <summary>
        /// Special function
        /// </summary>
        public class FunctionZ
        {
            /// <summary>
            /// Allow agent update lots status without selected seller and buyer
            /// </summary>
            public const string F00 = "QuickChangeLot";

            /// <summary>
            /// Allow auto refresh session when current session expired
            /// </summary>
            public const string F01 = "AutoRefreshSession";

            /// <summary>
            /// Default buyer
            /// </summary>
            public const string F02 = "DefaultBuyer";

            /// <summary>
            /// VIP launch
            /// </summary>
            public const string F03 = "VipLaunch";

            /// <summary>
            /// Hidden price
            /// </summary>
            public const string F04 = "HiddenPrice";

            /// <summary>
            /// Hide in agent list
            /// </summary>
            public const string F05 = "HideAgentList";

            /// <summary>
            /// Default seller
            /// </summary>
            public const string F06 = "DefaultSeller";

            /// <summary>
            /// Allow enquiry login
            /// </summary>
            public const string F07 = "EnquiryLogin";
        }

        #endregion
    }
}
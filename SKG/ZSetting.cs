﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-15 10:11
 * Update       : 2021-Sep-19 22:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Security.Claims;

namespace SKG
{
    using Dto;
    using Ext;
    using Redit;
    using static ZConst;

    /// <summary>
    /// Setting
    /// </summary>
    public class ZSetting
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ZSetting()
        {
            Salesforce = new AuthenticationDto();
            Rexsoftware = new AuthenticationDto();
            Agentbox = new AuthenticationDto();
            Mailchimp = new AuthenticationDto();

            Setting = new SettingDto();
            Site = new SiteDto();
            Timeout = new TimeoutDto();

            AesPassphase = Default.Passphase;
        }

        /// <summary>
        /// Get media path
        /// </summary>
        /// <param name="site">Site name</param>
        /// <returns>Return the result</returns>
        public string GetMediaPath(string site)
        {
            var res = Default.MediaPath;

            if (!string.IsNullOrEmpty(site))
            {
                site = $"/{site}";
            }

            res = $"{WebRootPath}{res}{site}";
            return res;
        }

        /// <summary>
        /// Get media
        /// </summary>
        /// <param name="path">Path name</param>
        /// <param name="file">File name</param>
        /// <param name="isUrl">Is URL or site DevName</param>
        /// <param name="name">Base URL or site DevName</param>
        /// <param name="default">Default file name</param>
        /// <returns>Return the result</returns>
        public static string GetMedia(string path, string file, bool isUrl, string name, string @default = "default.jpg")
        {
            if (@default == null)
            {
                @default = "default";
            }
            file = !string.IsNullOrEmpty(file) ? file : @default;

            name += string.Empty;
            var devName = isUrl ? string.Empty : name;

            path += string.Empty;
            if (!path.Contains(Default.MediaPath))
            {
                path = $"{Default.MediaPath}/{devName}{path}/";
            }
            else
            {
                path = $"{path}/";
            }

            if (file.Contains(Default.MediaPath))
            {
                path = string.Empty;
            }

            var res = $"{path}/{file}".Replace("//", "/").Replace("//", "/");
            if (isUrl)
            {
                res = $"{name}{res}";
            }

            return res;
        }

        /// <summary>
        /// Get media
        /// </summary>
        /// <param name="path">Path name</param>
        /// <param name="file">File name</param>
        /// <param name="default">Default file name</param>
        /// <returns>Return the result</returns>
        public static string GetMedia(string path, string file, string @default = "default.jpg")
        {
            if (@default == null)
            {
                @default = "default";
            }
            file = !string.IsNullOrEmpty(file) ? file : @default;

            path += string.Empty;
            if (!path.Contains(Default.MediaPath))
            {
                path = $"{Default.MediaPath}/{path}/";
            }
            else
            {
                path = $"{path}/";
            }

            if (file.Contains(Default.MediaPath))
            {
                path = string.Empty;
            }

            var res = $"{path}/{file}".Replace("//", "/").Replace("//", "/");
            return res;
        }

        /// <summary>
        /// Get API URL
        /// </summary>
        /// <param name="an">Assembly name</param>
        /// <param name="debugMode">Debug mode</param>
        /// <param name="isDevelopment">Is development</param>
        /// <param name="isTest">Is test</param>
        /// <param name="isQa">Is quality assurance</param>
        /// <param name="isStaging">Is staging</param>
        /// <returns>Return the result</returns>
        public static string GetApiUrl(string an, bool debugMode, bool isDevelopment, bool isTest, bool isQa, bool isStaging)
        {
            string res;
            if (debugMode)
            {
                res = "https://localhost";
            }
            else
            {
                res = ZVariable.ApiUrl;

                if (isDevelopment)
                {
                    if (string.IsNullOrEmpty(res))
                    {
                        res = "https://analytics-dev.immex-staging.com";
                    }
                }
                else if (isTest)
                {
                    if (string.IsNullOrEmpty(res))
                    {
                        res = "https://analytics-test.immex-staging.com";
                    }
                }
                else if (isQa)
                {
                    if (string.IsNullOrEmpty(res))
                    {
                        res = "https://analytics-qa.immex-staging.com";
                    }
                }
                else if (isStaging)
                {
                    if (string.IsNullOrEmpty(res))
                    {
                        res = "https://analytics.immex-staging.com";
                    }
                }
            }

            var port = an == "Matcha.Home" ? ZVariable.MaPort : ZVariable.MaApiPort;
            port = port.ToPort();
            res += port;

            return res;
        }

        /// <summary>
        /// Get user ID
        /// </summary>
        /// <param name="claim">Claims principal</param>
        /// <returns>Return the result</returns>
        public uint? GetUserId(ClaimsPrincipal claim)
        {
            uint? res = null;

            var t = claim.FindFirst(ClaimTypes.NameIdentifier);
            if (t != null)
            {
                res = t.Value.ToIntUn();
            }

            return res;
        }

        /// <summary>
        /// Get authority
        /// </summary>
        /// <param name="claim">Claims principal</param>
        /// <returns>Return the result</returns>
        public AuthorityDto GetAuthority(ClaimsPrincipal claim)
        {
            var id = GetUserId(claim);
            if (id == null)
            {
                return new AuthorityDto();
            }

            if (_rs == null)
            {
                var st = RSetting;
                if (st != null)
                {
                    _rs = new RedisStore(st);
                }
            }

            var res = _rs.GetAuthority(id);
            return res;
        }

        /// <summary>
        /// Validate header
        /// </summary>
        /// <param name="auth">Auth</param>
        /// <param name="debugMode">Allow to use debug mode</param>
        /// <returns>Return the result</returns>
        public bool Validate(string auth, bool debugMode = true)
        {
            bool res;

            if (Site == null)
            {
                "Site is null".LogError();
                res = false;
            }
            else
            {
                var debug = false;
                if (debugMode)
                {
                    debug = DebugMode;
                }

                var hash = new ZHash(Site.ApiKey, Site.ApiHash);
                res = hash.ValidToken(auth) || debug;

                if (!res)
                {
                    if (!Production && auth == ZConst.MyCode)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Disable sync to MA
        /// </summary>
        public static bool DisableSyncToMa { get; set; }

        /// <summary>
        /// Disable sync to Salesforce
        /// </summary>
        public static bool DisableSyncToSfx { get; set; }

        /// <summary>
        /// Disable sync to Rexsoftware
        /// </summary>
        public static bool DisableSyncToRex { get; set; }

        /// <summary>
        /// Disable sync to Agentbox
        /// </summary>
        public static bool DisableSyncToAgb { get; set; }

        /// <summary>
        /// Disable sync to Mailchimp
        /// </summary>
        public static bool DisableSyncToMch { get; set; }

        /// <summary>
        /// Assembly name
        /// </summary>
        public string Assembly { get; set; }

        /// <summary>
        /// Connection string
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// RSA mode
        /// </summary>
        public bool RsaMode { get; set; }

        /// <summary>
        /// RSA public key
        /// </summary>
        public string RsaPublicKey { get; set; }

        /// <summary>
        /// RSA private key
        /// </summary>
        public string RsaPrivateKey { get; set; }

        /// <summary>
        /// Salesforce setting
        /// </summary>
        public AuthenticationDto Salesforce { get; set; }

        /// <summary>
        /// Rexsoftware setting
        /// </summary>
        public AuthenticationDto Rexsoftware { get; set; }

        /// <summary>
        /// Agentbox setting
        /// </summary>
        public AuthenticationDto Agentbox { get; set; }

        /// <summary>
        /// Mailchimp setting
        /// </summary>
        public AuthenticationDto Mailchimp { get; set; }

        /// <summary>
        /// Setting
        /// </summary>
        public SettingDto Setting { get; set; }

        /// <summary>
        /// Site
        /// </summary>
        public SiteDto Site { get; set; }

        /// <summary>
        /// Session timeout
        /// </summary>
        public TimeoutDto Timeout { get; set; }

        /// <summary>
        /// JSON Web Token
        /// </summary>
        public JwtDto Jwt { get; set; }

        /// <summary>
        /// Redis setting
        /// </summary>
        public RedisStore.RSetting RSetting { get; set; }

        /// <summary>
        /// Development mode
        /// </summary>
        public bool DebugMode { get; set; }

        /// <summary>
        /// AES passphase
        /// </summary>
        public string AesPassphase { get; set; }

        /// <summary>
        /// System administrator user name
        /// </summary>
        public string SaUn { get; set; }

        /// <summary>
        /// Base path
        /// </summary>
        public string BasePath { get { return ZVariable.AspBasepath; } }

        /// <summary>
        /// Web root path
        /// </summary>
        public string WebRootPath { get; set; }

        /// <summary>
        /// Media path without site name
        /// </summary>
        public string MediaPath { get { return GetMediaPath(null); } }

        /// <summary>
        /// Environment
        /// </summary>
        public static string Environment
        {
            get
            {
                var t = ZVariable.AspCustomer;
                return string.IsNullOrWhiteSpace(t) ? "local" : t.Replace("-", "");
            }
        }

        /// <summary>
        /// Customer
        /// </summary>
        public string Customer { get; set; }

        /// <summary>
        /// Database name
        /// </summary>
        public string DatabaseName
        {
            get
            {
                if (string.IsNullOrEmpty(_databaseName))
                {
                    var a = "Database=";
                    var b = ConnectionString.FindItem(a);
                    _databaseName = b.Replace(a, "");
                }

                return _databaseName;
            }
            set
            {
                _databaseName = value;
            }
        }

        /// <summary>
        /// Development
        /// </summary>
        public bool Development
        {
            get
            {
                var res = string.IsNullOrEmpty(Customer);

                if (!res)
                {
                    res = Customer.Contains("dev") || Customer.Contains("toan");
                }

                return res;
            }
        }

        /// <summary>
        /// Test
        /// </summary>
        public bool Test
        {
            get
            {
                var res = false;

                if (Customer != null)
                {
                    res = Customer.Contains("test");
                }

                return res;
            }
        }

        /// <summary>
        /// Quality assurance
        /// </summary>
        public bool Qa
        {
            get
            {
                var res = false;

                if (Customer != null)
                {
                    res = Customer.Contains("qa");
                }

                return res;
            }
        }

        /// <summary>
        /// Staging
        /// </summary>
        public bool Staging
        {
            get
            {
                var res = false;

                if (Customer != null)
                {
                    res = Customer.Contains("stg");
                }

                return res;
            }
        }

        /// <summary>
        /// User acceptance testing
        /// </summary>
        public bool Uat
        {
            get
            {
                var res = false;

                if (Customer != null)
                {
                    res = Customer.Contains("uat");
                }

                return res;
            }
        }

        /// <summary>
        /// Production
        /// </summary>
        public bool Production
        {
            get
            {
                var res = !Development && !Test && !Qa && !Staging && !Uat;
                res = res && !ZVariable.DebugMode;
                return res;
            }
        }

        /// <summary>
        /// Functions
        /// </summary>
        public static StringMap Functions { get; set; }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Database name
        /// </summary>
        private string _databaseName;

        /// <summary>
        /// Redis store
        /// </summary>
        private RedisStore _rs;

        #endregion
    }
}
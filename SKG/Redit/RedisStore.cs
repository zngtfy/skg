﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Dec-09 06:12
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using SKG.Dto;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SKG.Redit
{
    using DAL.Models;
    using Ext;

    /// <summary>
    /// Redis store
    /// </summary>
    public class RedisStore
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="s">Setting connection</param>
        public RedisStore(RSetting s)
        {
            _s = s;

            if (_s == null)
            {
                "Redis setting is not config".LogError();
            }

            if (HasRedis && _s != null)
            {
                var options = new ConfigurationOptions
                {
                    EndPoints = { _s.ConnectionString },
                    AllowAdmin = _s.AllowAdmin,
                    Password = _s.Password
                };
                _lazy = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(options), LazyThreadSafetyMode.PublicationOnly);
            }
        }

        #region -- Authority --
        /// <summary>
        /// Set authority
        /// </summary>
        /// <param name="o">Authority</param>
        public void SetAuthority(AuthorityDto o)
        {
            if (o == null)
            {
                "Authority is null".LogInfor();
                return;
            }

            if (!HasRedis)
            {
                NoRedis.LogInfor();
                return;
            }

            try
            {
                var key = $"{_s.Prefix}_{ZConst.Payload}_{o.Id}";
                var json = o.ToJson();
                var lastOnline = o.LastOnline.ToStrIso8601();

                HashEntry[] hash = {
                    new HashEntry(nameof(json), json),
                    new HashEntry(nameof(lastOnline), lastOnline)
                };

                RedisCache.HashSet(key, hash);
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
            }
        }

        /// <summary>
        /// Get authority
        /// </summary>
        /// <param name="id">User ID</param>
        /// <returns>Return the result</returns>
        public AuthorityDto GetAuthority(uint? id)
        {
            var res = new AuthorityDto();

            if (id == null)
            {
                UserNull.LogInfor();
                return res;
            }

            if (!HasRedis)
            {
                NoRedis.LogInfor();
                return res;
            }

            try
            {
                var key = $"{_s.Prefix}_{ZConst.Payload}_{id}";
                var json = string.Empty;

                var db = Connection.GetDatabase(_s.Database);
                json = (string)db.HashGet(key, nameof(json));
                var lastOnline = string.Empty;
                lastOnline = (string)db.HashGet(key, nameof(lastOnline));

                res = json.ToInst<AuthorityDto>();
                res.LastOnline = lastOnline.ToDateTime(ZConst.Format.Iso8601);
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
            }

            return res;
        }

        /// <summary>
        /// Clear authority
        /// </summary>
        /// <param name="id">User ID</param>
        public void ClsAuthority(uint? id)
        {
            if (id == null)
            {
                UserNull.LogInfor();
                return;
            }

            if (!HasRedis)
            {
                NoRedis.LogInfor();
                return;
            }

            try
            {
                var key = $"{_s.Prefix}_{ZConst.Payload}_{id}";
                var json = string.Empty;
                var lastOnline = string.Empty;

                HashEntry[] hash = {
                    new HashEntry(nameof(json), json),
                    new HashEntry(nameof(lastOnline), lastOnline)
                };

                RedisCache.HashSet(key, hash);
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
            }
        }
        #endregion

        /// <summary>
        /// Update client information and store in Redis
        /// </summary>
        /// <param name="at">Action tab</param>
        /// <param name="isUp">True is increment else decrement</param>
        public void Update(ref ActionTab at, bool? isUp)
        {
            if (!HasRedis)
            {
                NoRedis.LogInfor();
                return;
            }

            if (at == null)
            {
                "Action tab is null".LogError();
                return;
            }

            try
            {
                if (at.SessionId == null || at.SessionId == Guid.Empty)
                {
                    "Session ID is null".LogError();
                    return;
                }

                Guid? uid = null;
                var newTab = at.Tabs.LastOrDefault();
                if (newTab == null)
                {
                    newTab = new ActionTab.TabInfo();
                }
                else
                {
                    uid = newTab.Uid;
                }

                var key = $"{_s.Prefix}_{ZConst.Session}_{at.SessionId}";
                var tabOpened = CountingTabOpened(key, isUp);

                #region -- Update tab info --
                var json = RedisCache.HashGet(key, KeyTabs).ToString();
                var tabs = json.ToList<ActionTab.TabInfo>();

                var oldTab = tabs.LastOrDefault(p => p.Uid == uid);
                if (oldTab == null)
                {
                    if (isUp == true)
                    {
                        tabs.Add(newTab); // append to
                    }
                    else
                    {
                        oldTab = tabs.LastOrDefault(p => p.Path == newTab.Path);
                        if (oldTab != null)
                        {
                            newTab.Uid = oldTab.Uid;
                        }
                        tabs = tabs.Update(newTab, nameof(newTab.Uid)); // update to
                    }
                }
                else
                {
                    tabs = tabs.Update(newTab, nameof(newTab.Uid)); // update to
                }

                // Update tabs
                at.Tabs = tabs;
                json = tabs.ToJson();
                #endregion

                #region -- Update hash set --
                var online = isUp == false ? 0 : 1;
                var lastOnline = DateTime.UtcNow.ToStrIso8601();

                HashEntry[] hash = {
                    new HashEntry(nameof(at.ClientId).ToCamelCase(), at.ClientId.ToStr()),
                    new HashEntry(nameof(at.SessionId).ToCamelCase(), at.SessionId.ToStr()),
                    new HashEntry(nameof(at.UserName).ToCamelCase(), at.UserName.ToStr()),
                    new HashEntry(nameof(at.ConnectionId).ToCamelCase(), at.ConnectionId.ToStr()),
                    new HashEntry(nameof(at.UserId).ToCamelCase(), at.UserId.ToStr()),
                    new HashEntry(nameof(at.Anonymous).ToCamelCase(), at.Anonymous),
                    new HashEntry(nameof(at.SessionExpired).ToCamelCase(), at.SessionExpired),
                    new HashEntry(nameof(at.SessionClosed).ToCamelCase(), at.SessionClosed),
                    new HashEntry(nameof(at.ScreenSize).ToCamelCase(), at.ScreenSize.ToJson()),
                    new HashEntry(nameof(at.MouseInfos).ToCamelCase(), at.MouseInfos.ToJson()),
                    new HashEntry(nameof(at.IpAddress).ToCamelCase(), at.IpAddress),
                    new HashEntry("online", online),
                    new HashEntry("lastOnline", lastOnline),
                    new HashEntry(KeyTabs, json),
                    new HashEntry(KeyTabOpened, tabOpened)
                };

                // Browser information
                var browser = at.Browser;
                if (browser.OsName != null)
                {
                    hash = hash.Append(new HashEntry("osName", browser.OsName)).ToArray();
                    hash = hash.Append(new HashEntry("osVersion", browser.OsVersion)).ToArray();
                    hash = hash.Append(new HashEntry("browserName", browser.BrowserName)).ToArray();
                    hash = hash.Append(new HashEntry("browserVersion", browser.BrowserVersion)).ToArray();
                }

                RedisCache.HashSet(key, hash);
                #endregion
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message; msg.LogError(); ex.StackTrace.LogError();
            }
        }

        /// <summary>
        /// Set offline user inactive during
        /// </summary>
        /// <param name="sec">Second</param>
        public void SetOffline(uint sec)
        {
            var x = ActiveUserInfos;
            var be = DateTime.Now.AddSeconds(-sec);
            var willOff = x.Where(p => p.LastOnline < be).ToList();

            var s = "";
            foreach (var i in willOff)
            {
                s += i.ToJson() + "\n";
            }

            //s.LogInfor();
        }

        /// <summary>
        /// Get active session information
        /// </summary>
        /// <param name="key">Hash key</param>
        /// <returns>Return the result</returns>
        public ActiveSessionInfo GetActiveSessionInfo(string key)
        {
            ActiveSessionInfo res;

            var db = Connection.GetDatabase(_s.Database);
            var clientId = (string)db.HashGet(key, nameof(res.ClientId).ToCamelCase());
            var sessionId = (string)db.HashGet(key, nameof(res.SessionId).ToCamelCase());
            var userName = (string)db.HashGet(key, nameof(res.UserName).ToCamelCase());
            var connectionId = (string)db.HashGet(key, nameof(res.ConnectionId).ToCamelCase());
            var userId = (string)db.HashGet(key, nameof(res.UserId).ToCamelCase());
            var ipAddress = (string)db.HashGet(key, nameof(res.IpAddress).ToCamelCase());
            var anonymous = (bool)db.HashGet(key, nameof(res.Anonymous).ToCamelCase());
            var online = (bool)db.HashGet(key, nameof(res.Online).ToCamelCase());
            var t = (string)db.HashGet(key, "lastOnline");
            var lastOnline = t.ToDateTime(ZConst.Format.Iso8601);

            res = new ActiveSessionInfo
            {
                ClientId = clientId.ToGuid(),
                SessionId = sessionId.ToGuid(),
                UserName = userName,
                ConnectionId = connectionId,
                UserId = userId.ToIntUn(),
                IpAddress = ipAddress,
                Anonymous = anonymous,
                Online = online,
                LastOnline = lastOnline
            };

            return res;
        }

        /// <summary>
        /// Get active user information
        /// </summary>
        /// <param name="key">Hash key</param>
        /// <returns>Return the result</returns>
        public AuthorityDto GetActiveUserInfo(string key)
        {
            AuthorityDto res;

            var db = Connection.GetDatabase(_s.Database);
            var json = (string)db.HashGet(key, "json");
            res = json.ToInst<AuthorityDto>();

            return res;
        }

        /// <summary>
        /// Get key from database
        /// </summary>
        /// <param name="pattern">Pattern to seach</param>
        /// <returns>Return the result</returns>
        public List<string> GetKeyFrDb(string pattern = ZConst.String.Asterisk)
        {
            var sv = Connection.GetServer(_s.ConnectionString);
            var keys = sv.Keys(_s.Database, pattern: pattern);
            var res = keys.Select(key => (string)key).ToList();

            return res;
        }

        /// <summary>
        /// Get value from database
        /// </summary>
        /// <param name="key">Hash key</param>
        /// <param name="field">Hash field</param>
        /// <returns>Return the result</returns>
        public RedisValue GetValFrDb(string key, string field)
        {
            var db = Connection.GetDatabase(_s.Database);
            var res = db.HashGet(key, field);

            return res;
        }

        /// <summary>
        /// Delete all the keys of the database
        /// </summary>
        public void FlushDatabase()
        {
            if (HasRedis)
            {
                var sv = Connection.GetServer(_s.ConnectionString);
                sv.FlushDatabase(_s.Database);
            }
        }

        /// <summary>
        /// Counting tab opened
        /// </summary>
        /// <param name="key">Client ID</param>
        /// <param name="isUp">True is increment else decrement</param>
        /// <returns>Return number of tab opened</returns>
        private long CountingTabOpened(string key, bool? isUp)
        {
            var res = 1L;

            if (isUp == null)
            {
                res = RedisCache.HashIncrement(key, KeyTabOpened, 0);
                return res;
            }

            var ok = RedisCache.HashExists(key, KeyTabOpened);
            if (ok)
            {
                if (isUp.Value)
                {
                    res = RedisCache.HashIncrement(key, KeyTabOpened, 1);
                }
                else
                {
                    res = RedisCache.HashDecrement(key, KeyTabOpened, 1);

                    if (res < 0)
                    {
                        res = 0;
                    }
                }
            }

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Connection
        /// </summary>
        public ConnectionMultiplexer Connection => _lazy.Value;

        /// <summary>
        /// Redis cache
        /// </summary>
        public IDatabase RedisCache => Connection.GetDatabase(_s.Database);

        /// <summary>
        /// Has Redis (set appsettings-noredis.json -> don't have Redis server)
        /// </summary>
        private bool HasRedis
        {
            get
            {
                if (_hasRedis != null)
                {
                    return _hasRedis.Value;
                }

                var t = "-noredis;".ToSet();
                _hasRedis = !t.Contains(ZVariable.AspCustomer);
                return _hasRedis.Value;
            }
        }

        /// <summary>
        /// Active session information
        /// </summary>
        public List<ActiveSessionInfo> ActiveSessionInfos
        {
            get
            {
                var res = new List<ActiveSessionInfo>();

                if (!HasRedis)
                {
                    return res;
                }

                var key = $"{_s.Prefix}_{ZConst.Session}_*";
                var arr = GetKeyFrDb(key);
                foreach (var i in arr)
                {
                    var t = GetActiveSessionInfo(i);
                    res.Add(t);
                }

                return res;
            }
        }

        /// <summary>
        /// Active user information
        /// </summary>
        public List<AuthorityDto> ActiveUserInfos
        {
            get
            {
                var res = new List<AuthorityDto>();

                if (!HasRedis)
                {
                    return res;
                }

                var key = $"{_s.Prefix}_{ZConst.Payload}_*";
                var arr = GetKeyFrDb(key);
                foreach (var i in arr)
                {
                    var t = GetActiveUserInfo(i);

                    if (string.IsNullOrWhiteSpace(t.UserName))
                    {
                        continue;
                    }

                    res.Add(t);
                }

                return res;
            }
        }

        #endregion

        #region -- Fields --

        /// <summary>
        /// Lazy connection
        /// </summary>
        private readonly Lazy<ConnectionMultiplexer> _lazy;

        /// <summary>
        /// Setting connection
        /// </summary>
        private readonly RSetting _s;

        /// <summary>
        /// Has Redis (set appsettings-noredis.json -> dont have Redis server)
        /// </summary>
        private static bool? _hasRedis;

        #endregion

        #region -- Constants --

        /// <summary>
        /// Key tabs
        /// </summary>
        private const string KeyTabs = "tabs";

        /// <summary>
        /// Key tabOpened
        /// </summary>
        private const string KeyTabOpened = "tabOpened";

        /// <summary>
        /// User null
        /// </summary>
        private const string UserNull = "User ID is null";

        /// <summary>
        /// No Redis
        /// </summary>
        private const string NoRedis = "Don't have Redis";

        #endregion

        #region -- Classes --

        /// <summary>
        /// Active session information
        /// </summary>
        public class ActiveSessionInfo : IUserSession
        {
            #region -- Implements --

            /// <summary>
            /// Client ID
            /// </summary>
            public Guid ClientId { get; set; }

            /// <summary>
            /// Session ID
            /// </summary>
            public Guid SessionId { get; set; }

            /// <summary>
            /// User name
            /// </summary>
            public string UserName { get; set; }

            /// <summary>
            /// User ID
            /// </summary>
            public uint? UserId { get; set; }

            #endregion

            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public ActiveSessionInfo() { }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Remote IP address
            /// </summary>
            public string IpAddress { get; set; }

            /// <summary>
            /// Connection ID
            /// </summary>
            public string ConnectionId { get; set; }

            /// <summary>
            /// Anonymous
            /// </summary>
            public bool Anonymous { get; set; }

            /// <summary>
            /// Online
            /// </summary>
            public bool Online { get; set; }

            /// <summary>
            /// Last online
            /// </summary>
            public DateTime LastOnline { get; set; }

            #endregion
        }

        /// <summary>
        /// Setting
        /// </summary>
        public class RSetting
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public RSetting()
            {
                Hosts = new List<RServer>();
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Allow admin
            /// </summary>
            public bool AllowAdmin { get; set; }

            /// <summary>
            /// SSL
            /// </summary>
            public bool Ssl { get; set; }

            /// <summary>
            /// Connect timeout
            /// </summary>
            public int ConnectTimeout { get; set; }

            /// <summary>
            /// Connect retry
            /// </summary>
            public int ConnectRetry { get; set; }

            /// <summary>
            /// Database name [0 - 15]
            /// </summary>
            public int Database
            {
                get
                {
                    return _database;
                }
                set
                {
                    if (value < 0)
                    {
                        value = 0;
                    }

                    if (value > 15)
                    {
                        value = 15;
                    }

                    _database = value;
                }
            }

            /// <summary>
            /// Prefix
            /// </summary>
            public string Prefix { get; set; }

            /// <summary>
            /// Hosts
            /// </summary>
            public List<RServer> Hosts { get; set; }

            /// <summary>
            /// Default connection string
            /// </summary>
            public string ConnectionString
            {
                get
                {
                    var host = ZVariable.RdServer;
                    var port = ZVariable.RdPort;

                    if (Hosts.Count > 0)
                    {
                        if (string.IsNullOrWhiteSpace(host))
                        {
                            host = Hosts[0].Host;
                        }
                        if (string.IsNullOrWhiteSpace(port))
                        {
                            port = Hosts[0].Port;
                        }
                    }

                    var res = $"{host}:{port}";
                    return res;
                }
            }

            /// <summary>
            /// Password
            /// </summary>
            public string Password
            {
                get
                {
                    var res = ZVariable.RdPassword;
                    if (string.IsNullOrWhiteSpace(res))
                    {
                        res = _password;
                    }

                    return res;
                }

                set
                {
                    _password = value;
                }
            }

            #endregion

            #region -- Fields --

            /// <summary>
            /// Database name [0 - 15]
            /// </summary>
            private int _database;

            /// <summary>
            /// Password
            /// </summary>
            private string _password;

            #endregion
        }

        /// <summary>
        /// Server
        /// </summary>
        public class RServer
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public RServer() { }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Host
            /// </summary>
            public string Host { get; set; }

            /// <summary>
            /// Port
            /// </summary>
            public string Port { get; set; }

            #endregion
        }

        #endregion
    }
}
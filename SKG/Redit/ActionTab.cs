﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-18 16:34
 * Update       : 2020-Jun-18 16:34
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections.Generic;
using System.Net;

namespace SKG.Redit
{
    /// <summary>
    /// Action tab of browser
    /// </summary>
    public class ActionTab
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        public ActionTab()
        {
            ClientId = Guid.Empty;
            SessionId = Guid.Empty;
            UserName = string.Empty;
            UserId = 0;

            Anonymous = true;
            SetDefault();
        }

        /// <summary>
        /// Add user
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="userName">User name</param>
        /// <param name="ip">Remote IP address</param>
        public void AddUser(uint? userId, string userName, IPAddress ip)
        {
            if (userId > 0)
            {
                Anonymous = false;
                UserName = userName;
            }

            UserId = userId;
            IpAddress = ip == null ? string.Empty : ip.ToString();
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="clientId">Client ID</param>
        /// <param name="sessionId">Session ID</param>
        /// <param name="userName">User name</param>
        /// <param name="userId">User ID</param>
        public ActionTab(Guid clientId, Guid sessionId, string userName, uint? userId) : this()
        {
            ClientId = clientId;
            SessionId = sessionId;
            UserName = userName;
            UserId = userId;

            Anonymous = userId == null;
            SetDefault();
        }

        /// <summary>
        /// Add tab
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>Return the result</returns>
        public TabInfo AddTab(string path)
        {
            var res = new TabInfo { Path = path };
            Tabs.Add(res);
            return res;
        }

        /// <summary>
        /// Set default
        /// </summary>
        private void SetDefault()
        {
            Tabs = new List<TabInfo>();
            IpAddress = string.Empty;
            Browser = new BrowserInfo();

            ScreenSize = new List<int>();
            MouseInfos = new List<MouseInfo>();
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Client ID
        /// </summary>
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public Guid? SessionId { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User ID
        /// </summary>
        public uint? UserId { get; set; }

        /// <summary>
        /// Connection ID
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// Anonymous
        /// </summary>
        public bool Anonymous { get; set; }

        /// <summary>
        /// Session expired
        /// </summary>
        public bool SessionExpired { get; set; }

        /// <summary>
        /// Session closed
        /// </summary>
        public bool SessionClosed { get; set; }

        /// <summary>
        /// Tabs
        /// </summary>
        public List<TabInfo> Tabs { get; set; }

        /// <summary>
        /// Remote IP address
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Browser version
        /// </summary>
        public BrowserInfo Browser { get; set; }

        /// <summary>
        /// Screen height,width
        /// </summary>
        public List<int> ScreenSize { get; set; }

        /// <summary>
        /// Mouse information
        /// </summary>
        public List<MouseInfo> MouseInfos { get; set; }

        #endregion

        #region -- Classes --

        /// <summary>
        /// Tab information
        /// </summary>
        public class TabInfo
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public TabInfo()
            {
                Uid = Guid.NewGuid();
                StartActive = DateTime.Now;
                TimeActive = 1;
                TimeInactive = 0;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// UID
            /// </summary>
            public Guid? Uid { get; set; }

            /// <summary>
            /// Path
            /// </summary>
            public string Path { get; set; }

            /// <summary>
            /// Start active
            /// </summary>
            public DateTime StartActive { get; set; }

            /// <summary>
            /// Start inactive
            /// </summary>
            public DateTime? StartInactive { get; set; }

            /// <summary>
            /// Last active
            /// </summary>
            public DateTime? LastActive { get; set; }

            /// <summary>
            /// Last inactive
            /// </summary>
            public DateTime? LastInactive { get; set; }

            /// <summary>
            /// Time active (second)
            /// </summary>
            public uint TimeActive { get; set; }

            /// <summary>
            /// Time inactive (second)
            /// </summary>
            public uint TimeInactive { get; set; }

            #endregion
        }

        /// <summary>
        /// Browser information
        /// </summary>
        public class BrowserInfo
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public BrowserInfo() { }

            #endregion

            #region -- Properties --

            /// <summary>
            /// OS name
            /// </summary>
            public string OsName { get; set; }

            /// <summary>
            /// OS version
            /// </summary>
            public string OsVersion { get; set; }

            /// <summary>
            /// Browser name
            /// </summary>
            public string BrowserName { get; set; }

            /// <summary>
            /// Browser version
            /// </summary>
            public string BrowserVersion { get; set; }

            /// <summary>
            /// User agent
            /// </summary>
            public string UserAgent { get; set; }

            /// <summary>
            /// App version
            /// </summary>
            public string AppVersion { get; set; }

            /// <summary>
            /// Platform
            /// </summary>
            public string Platform { get; set; }

            /// <summary>
            /// Vendor
            /// </summary>
            public string Vendor { get; set; }

            #endregion
        }

        /// <summary>
        /// Mouse information
        /// </summary>
        public class MouseInfo
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public MouseInfo()
            {
                WindowSize = new List<int>();
                DocumentSize = new List<int>();
                MousePosition = new List<List<int>>();
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// Screen height,width
            /// </summary>
            public List<int> WindowSize { get; set; }

            /// <summary>
            /// Screen height,width
            /// </summary>
            public List<int> DocumentSize { get; set; }

            /// <summary>
            /// Mouse position x,y
            /// </summary>
            public List<List<int>> MousePosition { get; set; }

            #endregion
        }

        #endregion
    }
}
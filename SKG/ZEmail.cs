﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-14 16:38
 * Update       : 2020-Jun-14 16:38
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SKG
{
    using Dto;
    using Ext;
    using Zhar = ZConst.Char;

    /// <summary>
    /// Email
    /// </summary>
    public class ZEmail
    {
        #region -- Methods --

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="optionsAccessor"></param>
        public ZEmail(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;

            var path = AppDomain.CurrentDomain.BaseDirectory;
            Folder = path + "Email/";
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="host">Host</param>
        /// <param name="port">Port</param>
        /// <param name="userName">User name</param>
        /// <param name="password">Encrypted password</param>
        /// <param name="private">RSA private key</param>
        public ZEmail(string host, int port, string userName, string password, string @private)
        {
            Host = host;
            Port = port;
            UserName = userName;
            Password = password;
            Private = @private;

            var path = AppDomain.CurrentDomain.BaseDirectory;
            Folder = path + "Email/";
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="message">Mail message</param>
        public void SendEmail(MailMessage message)
        {
            var t = Password.Decrypt(Private);
            var cred = new NetworkCredential(UserName, t);

            var smtp = new SmtpClient(Host, Port)
            {
                Credentials = cred,
                EnableSsl = true
            };

            smtp.Send(message);
            smtp.Dispose();
        }

        /// <summary>
        /// Get mail with image
        /// </summary>
        /// <param name="i">Email information</param>
        /// <returns>Return the result</returns>
        public MailMessage GetMailWithImage(Info i)
        {
            var htm = (Folder + "index.html").ReadAllText();

            var i1 = new LinkedResource(Folder + "images/app.png")
            {
                ContentId = Guid.NewGuid().ToString()
            };
            htm = htm.Replace("cid:0", "cid:" + i1.ContentId + "\"");

            var i2 = new LinkedResource(Folder + "images/cws.png")
            {
                ContentId = Guid.NewGuid().ToString()
            };
            htm = htm.Replace("cid:1", "cid:" + i2.ContentId + "\"");

            htm = htm.Replace("Z-SUBJECT", i.Subject);
            htm = htm.Replace("Z-BODY1", "Dear " + i.FullName + ",");
            htm = htm.Replace("Z-BODY2", "Your User ID for ePortal is");
            htm = htm.Replace("Z-BODY3", "Please click the here to setup your password and continue");

            var alt = AlternateView.CreateAlternateViewFromString(htm, null, MediaTypeNames.Text.Html);
            alt.LinkedResources.Add(i1);
            alt.LinkedResources.Add(i2);

            var msg = new MailMessage
            {
                IsBodyHtml = true
            };
            msg.AlternateViews.Add(alt);

            msg.To.Add(i.To);
            msg.From = new MailAddress(i.Fr, i.Display);
            msg.Subject = i.Subject;

            if (!string.IsNullOrEmpty(i.Cc))
            {
                msg.CC.Add(i.Cc);
            }

            return msg;
        }

        /// <summary>
        /// Get email with image
        /// </summary>
        /// <param name="i">Email information</param>
        /// <returns>Return the result</returns>
        public MailMessage GetEmailWithImage(Info i)
        {
            var htm = (Folder + "index.html").ReadAllText();

            var avatar = new LinkedResource(Folder + "images/avatar.jpg")
            {
                ContentId = Guid.NewGuid().ToString()
            };
            htm = htm.Replace($"cid:{nameof(avatar)}", "cid:" + avatar.ContentId + "\"");

            var logo = new LinkedResource(Folder + "images/logo.png")
            {
                ContentId = Guid.NewGuid().ToString()
            };
            htm = htm.Replace($"cid:{nameof(avatar)}", "cid:" + logo.ContentId + "\"");

            var facebook = new LinkedResource(Folder + "images/facebook.png")
            {
                ContentId = Guid.NewGuid().ToString()
            };
            htm = htm.Replace($"cid:{nameof(avatar)}", "cid:" + facebook.ContentId + "\"");

            var instagram = new LinkedResource(Folder + "images/instagram.png")
            {
                ContentId = Guid.NewGuid().ToString()
            };
            htm = htm.Replace($"cid:{nameof(avatar)}", "cid:" + instagram.ContentId + "\"");

            var alt = AlternateView.CreateAlternateViewFromString(htm, null, MediaTypeNames.Text.Html);
            alt.LinkedResources.Add(avatar);
            alt.LinkedResources.Add(logo);
            alt.LinkedResources.Add(facebook);
            alt.LinkedResources.Add(instagram);

            var msg = new MailMessage
            {
                IsBodyHtml = true
            };
            msg.AlternateViews.Add(alt);

            msg.To.Add(i.To);
            msg.From = new MailAddress(i.Fr, i.Display);
            msg.Subject = i.Subject;

            if (!string.IsNullOrEmpty(i.Cc))
            {
                msg.CC.Add(i.Cc);
            }

            return msg;
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="to">Multiple emails with a comma separator</param>
        /// <param name="cc">Multiple emails with a comma separator</param>
        /// <param name="bcc">Multiple emails with a comma separator</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Message</param>
        /// <param name="att">Attachments</param>
        /// <returns>Return the result</returns>
        public Task SendEmailAsync(string to, string cc, string bcc, string subject, string message, List<AttachmentDto> att = null)
        {
            return SendEmailAsync(to, cc, bcc, subject, message, null, null, att);
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="to">Multiple emails with a comma separator</param>
        /// <param name="cc">Multiple emails with a comma separator</param>
        /// <param name="bcc">Multiple emails with a comma separator</param>
        /// <param name="sub">Subject</param>
        /// <param name="msg">Message</param>
        /// <param name="ab">Action before email sent</param>
        /// <param name="aa">Action after email sent</param>
        /// <param name="att">Attachments</param>
        /// <returns>Return the result</returns>
        public Task SendEmailAsync(string to, string cc, string bcc, string sub, string msg, Action<SendGridMessage> ab, Action<Task<Response>> aa, List<AttachmentDto> att = null)
        {
            var se = ZVariable.ServerEnvironment;
            if (!string.IsNullOrWhiteSpace(se))
            {
                sub = $"[{se.ToUpper()}] {sub}";
            }

            var client = new SendGridClient(Options.SendGridApiKey);
            var fr = new EmailAddress(Options.SendGridSenderEmail, Options.SendGridSenderName);

            #region -- Check duplicate email --
            var comma = ZConst.String.Comma;
            var sTo = to.ToSet(comma);
            var sCc = cc.ToSet(comma);
            var sBcc = bcc.ToSet(comma);

            var eTo = sTo.Except(sCc).Except(sBcc);
            var eCc = sCc.Except(sBcc);
            var eBcc = sBcc.Except(sTo);

            var tt = eTo.Zoin(false, comma);
            cc = eCc.Zoin(false, comma);

            if (string.IsNullOrEmpty(tt))
            {
                bcc = eBcc.Zoin(false, comma);
            }
            else
            {
                to = tt;
            }
            #endregion

            var lTo = CreateEmailAddress(to);
            var lCc = CreateEmailAddress(cc);
            var lBcc = CreateEmailAddress(bcc);
            var mail = MailHelper.CreateSingleEmailToMultipleRecipients(fr, lTo, sub, msg, msg);

            if (lCc.Count > 0)
            {
                mail.AddCcs(lCc);
            }
            if (lBcc.Count > 0)
            {
                mail.AddBccs(lBcc);
            }

            if (att != null)
            {
                foreach (var i in att)
                {
                    mail.AddAttachment(i.FileName, i.FileContent);
                }
            }

            if (ab != null)
            {
                ab(mail);
            }
            var res = client.SendEmailAsync(mail);
            if (aa != null)
            {
                aa(res);
            }

            return res;
        }

        /// <summary>
        /// Create email address
        /// </summary>
        /// <param name="emails">Multiple emails with a comma separator</param>
        /// <returns>Return list EmailAddress</returns>
        private List<EmailAddress> CreateEmailAddress(string emails)
        {
            var res = new List<EmailAddress>();

            var arr = emails.ToSet(Zhar.Comma);
            foreach (var i in arr)
            {
                var mailTo = new EmailAddress(i);
                res.Add(mailTo);
            }

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; private set; }

        /// <summary>
        /// Port
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// RSA private key
        /// </summary>
        public string Private { get; private set; }

        /// <summary>
        /// Email folder
        /// </summary>
        public string Folder { get; private set; }

        /// <summary>
        /// Options
        /// </summary>
        public AuthMessageSenderOptions Options { get; }

        #endregion

        #region -- Classes --

        /// <summary>
        /// Email information
        /// </summary>
        public class Info
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            /// <param name="fr">From</param>
            /// <param name="to">To</param>
            public Info(string fr, string to)
            {
                Fr = fr;
                To = to;
            }

            #endregion

            #region -- Properties --

            /// <summary>
            /// From
            /// </summary>
            public string Fr { get; set; }

            /// <summary>
            /// To
            /// </summary>
            public string To { get; set; }

            /// <summary>
            /// Carbon copy
            /// </summary>
            public string Cc { get; set; }

            /// <summary>
            /// Subject
            /// </summary>
            public string Subject { get; set; }

            /// <summary>
            /// Full name
            /// </summary>
            public string FullName { get; set; }

            /// <summary>
            /// Display name associated with address
            /// </summary>
            public string Display { get; set; }

            #endregion
        }

        /// <summary>
        /// AuthMessageSenderOptions
        /// </summary>
        public class AuthMessageSenderOptions
        {
            #region -- Methods --

            /// <summary>
            /// Initialize
            /// </summary>
            public AuthMessageSenderOptions() { }

            #endregion

            #region -- Properties --

            /// <summary>
            /// SendGridApiKey
            /// </summary>
            public string SendGridApiKey { get; set; }

            /// <summary>
            /// SendGridSenderEmail
            /// </summary>
            public string SendGridSenderEmail { get; set; }

            /// <summary>
            /// SendGridSenderName
            /// </summary>
            public string SendGridSenderName { get; set; }

            #endregion
        }

        #endregion
    }
}
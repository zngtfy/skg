﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2021-Oct-10 22:40
 * Update       : 2021-Oct-10 22:40
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Linq;
using System.Reflection;

namespace SKG.SchemaFilter
{
    using Attributes;

    /// <summary>
    /// Skip property schema filter
    /// </summary>
    public class SkipPropertySchemaFilter : ISchemaFilter
    {
        #region -- Implements --

        /// <summary>
        /// Apply
        /// </summary>
        /// <param name="schema">Schema</param>
        /// <param name="context">Context</param>
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (schema?.Properties == null)
            {
                return;
            }

            var properties = context.Type.GetProperties().Where(t => t.GetCustomAttribute<SkipPropertyAttribute>() != null);
            foreach (var i in properties)
            {
                var propertyToRemove = schema.Properties.Keys.SingleOrDefault(p => string.Equals(p, i.Name, StringComparison.OrdinalIgnoreCase));

                if (propertyToRemove != null)
                {
                    schema.Properties.Remove(propertyToRemove);
                }
            }
        }

        #endregion

    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Nov-04 14:08
 * Update       : 2020-Nov-04 14:08
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Linq;

namespace SKG.BLL
{
    /// <summary>
    /// Interface extend service
    /// </summary>
    /// <typeparam name="T">Model class type</typeparam>
    public interface IExtendSvc<T>
    {
        #region -- Methods --

        /// <summary>
        /// Query
        /// </summary>
        /// <param name="siteId">Site ID</param>
        /// <returns>Return the result</returns>
        IQueryable<T> Query(uint siteId);

        #endregion
    }
}
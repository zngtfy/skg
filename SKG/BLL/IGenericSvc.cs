﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jun-04 09:48
 * Update       : 2020-Aug-06 06:58
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System.Collections.Generic;
using System.Linq;

namespace SKG.BLL
{
    using Req;
    using Rsp;

    /// <summary>
    /// Interface generic service
    /// </summary>
    /// <typeparam name="T">Model class type</typeparam>
    /// <typeparam name="TID">Model ID type</typeparam>
    public interface IGenericSvc<T, TID> where T : class
    {
        #region -- Methods --

        /// <summary>
        /// Create the model
        /// </summary>
        /// <param name="m">The model</param>
        /// <returns>Return the result</returns>
        SingleRsp CreateRsp(T m);

        /// <summary>
        /// Create the models
        /// </summary>
        /// <param name="l">List model</param>
        /// <returns>Return the result</returns>
        MultipleRsp CreateRsp(List<T> l);

        /// <summary>
        /// Read by
        /// </summary>
        /// <param name="req">Paging request</param>
        /// <returns>Return the result</returns>
        SearchRsp ReadRsp(PagingReq req);

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <returns>Return the object</returns>
        SingleRsp ReadRsp(TID id);

        /// <summary>
        /// Read single object
        /// </summary>
        /// <param name="code">Secondary key</param>
        /// <returns>Return the object</returns>
        SingleRsp ReadRsp(string code);

        /// <summary>
        /// Update the model
        /// </summary>
        /// <param name="m">The model</param>
        /// <returns>Return the result</returns>
        SingleRsp UpdateRsp(T m);

        /// <summary>
        /// Update the models
        /// </summary>
        /// <param name="l">List model</param>
        /// <returns>Return the result</returns>
        MultipleRsp UpdateRsp(List<T> l);

        /// <summary>
        /// Delete single object
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <returns>Return the result</returns>
        SingleRsp DeleteRsp(TID id);

        /// <summary>
        /// Delete single object
        /// </summary>
        /// <param name="code">Secondary key</param>
        /// <returns>Return the result</returns>
        SingleRsp DeleteRsp(string code);

        /// <summary>
        /// Restore the model
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <returns>Return the result</returns>
        SingleRsp RestoreRsp(TID id);

        /// <summary>
        /// Restore the model
        /// </summary>
        /// <param name="code">Secondary key</param>
        /// <returns>Return the result</returns>
        SingleRsp RestoreRsp(string code);

        /// <summary>
        /// Remove and not restore
        /// </summary>
        /// <param name="id">Primary key</param>
        /// <returns>Return the result</returns>
        SingleRsp RemoveRsp(TID id);

        #endregion

        #region -- Properties --

        /// <summary>
        /// Return query all undeleted data
        /// </summary>
        IQueryable<T> Availabilities { get; }

        /// <summary>
        /// Return query all enabled data
        /// </summary>
        IQueryable<T> Enablements { get; }

        #endregion
    }
}
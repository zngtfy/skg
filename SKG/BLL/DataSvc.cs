﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : nvt87x@gmail.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-12 15:48
 * Update       : 2020-Aug-12 15:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using System;
using System.Collections.Generic;

namespace SKG.BLL
{
    using DAL.Models;
    using Dto;
    using Ext;
    using static ZEnum;

    /// <summary>
    /// Data service
    /// </summary>
    public static class DataSvc
    {
        #region -- Methods --

        /// <summary>
        /// Create SysCode
        /// </summary>
        /// <param name="type">SysType</param>
        /// <returns>Return the list</returns>
        public static List<SysCode> CreateSysCode(SysType type)
        {
            var res = new List<SysCode>();

            // All system objects
            var t = @"  SysAccountLogs
                        SysApps
                        SysAuths
                        SysCodes
                        SysConfigSyncs
                        SysErrorLogs
                        SysGroupMembers
                        SysGroups
                        SysProfiles
                        SysRoles
                        SysSchedulerLogs
                        SysSchedulers
                        SysSiteManages
                        SysSites
                        SysTabs
                        SysTypes
                        SysUsers";

            var l = t.ToSet(ZConst.NewLine);
            foreach (var i in l)
            {
                var m = new SysCode(i) { Type = type };
                res.Add(m);
            }

            return res;
        }

        #endregion

        #region -- Properties --

        /// <summary>
        /// All SysType
        /// </summary>
        /// <returns>Return the list</returns>
        public static List<SysType> SysTypes
        {
            get
            {
                var res = new List<SysType>();

                foreach (var i in (TypeZ[])Enum.GetValues(typeof(TypeZ)))
                {
                    res.Add(new SysType(i));
                }

                return res;
            }
        }

        /// <summary>
        /// All SysApp
        /// </summary>
        /// <returns>Return the list</returns>
        public static List<SysApp> SysApps
        {
            get
            {
                #region -- System --
                var l = new List<NavDto>();
                var nav = new NavDto("SETTINGS", 0);
                nav.Tabs.Add(new NavDto.Tab(1, 0));
                nav.Tabs.Add(new NavDto.Tab(2, 2));
                nav.Tabs.Add(new NavDto.Tab(3, 3));
                nav.Tabs.Add(new NavDto.Tab(11, 1));
                l.Add(nav);
                var a = new SysApp("System")
                {
                    Navigation = l.ToJson(),
                    Profile = "1;2;3;4;"
                };
                #endregion

                #region -- CMS --
                l = new List<NavDto>();
                nav = new NavDto("SETTINGS", 0);
                nav.Tabs.Add(new NavDto.Tab(10, 0));
                l.Add(nav);
                var b = new SysApp("CMS")
                {
                    Navigation = l.ToJson(),
                    Profile = "1;2;3;"
                };
                #endregion

                #region -- Analysis --
                l = new List<NavDto>();
                nav = new NavDto("INSIGHTS / DATA", 0);
                nav.Tabs.Add(new NavDto.Tab(4, 2));
                nav.Tabs.Add(new NavDto.Tab(6, 0));
                nav.Tabs.Add(new NavDto.Tab(7, 1));
                l.Add(nav);
                nav = new NavDto("USER INSIGHTS", 1);
                nav.Tabs.Add(new NavDto.Tab(8, 0));
                nav.Tabs.Add(new NavDto.Tab(3, 1));
                l.Add(nav);
                nav = new NavDto("SALES & OPPORTUNITIES", 2);
                nav.Tabs.Add(new NavDto.Tab(9, 0));
                l.Add(nav);
                nav = new NavDto("SETTINGS", 2);
                nav.Tabs.Add(new NavDto.Tab(10, 0));
                l.Add(nav);
                var c = new SysApp("Analysis")
                {
                    Navigation = l.ToJson(),
                    Profile = "4;"
                };
                #endregion

                #region -- Test --
                l = new List<NavDto>();
                nav = new NavDto("Demo", 0);
                nav.Tabs.Add(new NavDto.Tab(4, 0));
                nav.Tabs.Add(new NavDto.Tab(10, 1));
                l.Add(nav);
                var d = new SysApp("Test")
                {
                    Navigation = l.ToJson(),
                    Profile = "3;"
                };
                #endregion

                var res = new List<SysApp> { a, b, c, d };
                return res;
            }
        }

        /// <summary>
        /// All SysTabs
        /// </summary>
        /// <returns>Return the list</returns>
        public static List<SysTab> SysTabs
        {
            get
            {
                var res = new List<SysTab> {
                    new SysTab("User"),
                    new SysTab("Profile"),
                    new SysTab("Role")
                };

                return res;
            }
        }

        #endregion
    }
}
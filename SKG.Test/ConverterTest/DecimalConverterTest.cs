﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-09 16:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json;
using NUnit.Framework;

namespace SKG.Test.ConverterTest
{
    using Converter;
    using Ext;

    public class DecimalConverterTest
    {
        [SetUp]
        public void Setup() { }

        [Test]
        public void Test1()
        {
            var o = new Poco();
            var s = o.ToJson();
            var exp = "{\"Number1\":\"0%\",\"Number1N\":null,\"Number2\":\"0%\",\"Number2N\":null,\"Number3\":\"0%\",\"Number3N\":null}";
            Assert.AreEqual(exp, s);
        }

        [Test]
        public void Test2()
        {
            var s = "{\"Number1\":\"0%\",\"Number1N\":null,\"Number2\":\"0%\",\"Number2N\":null,\"Number3\":\"0%\",\"Number3N\":null}";
            var o = s.ToInst<Poco>();
        }
    }

    public class Poco
    {
        [JsonConverter(typeof(DecimalConverter))]
        public decimal Number1 { get; set; }

        [JsonConverter(typeof(DecimalConverter))]
        public decimal? Number1N { get; set; }

        [JsonConverter(typeof(DecimalConverter))]
        public double Number2 { get; set; }

        [JsonConverter(typeof(DecimalConverter))]
        public double? Number2N { get; set; }

        [JsonConverter(typeof(DecimalConverter))]
        public float Number3 { get; set; }

        [JsonConverter(typeof(DecimalConverter))]
        public float? Number3N { get; set; }
    }
}
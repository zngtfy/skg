#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-10 14:41
 * Checklist    : 1.0
 * Status       : Done
 */
#endregion

using NUnit.Framework;
using System;

namespace SKG.Test
{
    public class ZHashTest
    {
        [SetUp]
        public void Setup()
        {
            _h = new ZHash("Toan");
        }

        [Test]
        public void Test1()
        {
            var h1 = new ZHash(_h.Value, _h.Salt);
            var token = h1.CreateToken(DateTime.Now, "UID");

            var h2 = new ZHash(_h.Value, _h.Salt);
            var t = h2.ValidToken(token);
            Assert.IsTrue(t);

            t = h2.ValidToken("a;b;c");
            Assert.IsFalse(t);
        }

        [Test]
        public void Test2()
        {
            ZHash.ComputeSha("a");

            _h.TimeLive = 1;
            var t = _h.ValidToken("a;1594391587");
            Assert.IsFalse(t);

            _h.TimeLive = 1;
            t = _h.ValidToken("a;1594391587;b");
            Assert.IsFalse(t);

            _h.TimeLive = 0;
            _h.TimeLive = ushort.MaxValue;
            Assert.IsNotNull(_h.Pepper);
        }

        private ZHash _h;
    }
}
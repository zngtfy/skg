﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-10 14:41
 * Checklist    : 1.0
 * Status       : Done
 */
#endregion

using NUnit.Framework;
using System;

namespace SKG.Test.DALTest.ModelsTest
{
    using DAL.Models;
    using static ZEnum;

    public class SysAuthTest
    {
        [SetUp]
        public void Setup()
        {
            _m = SysAuth.Create("Matcha.Admin", Guid.NewGuid(), "https://localhost:44395");
        }

        [Test]
        public void Test1()
        {
            var token = _m.CreateToken(DateTime.Now, "UID");
            _m.Status = Status.Enabled;
            var res = _m.ValidToken(token);

            Assert.IsTrue(res);
            Assert.IsNotNull(_m.Name);
            Assert.IsNotNull(_m.ApiUrl);
            Assert.IsNotNull(_m.Status);
            Assert.IsNotNull(_m.SiteUid);
        }

        private SysAuth _m;
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-06 08:54
 * Update       : 2020-Aug-06 08:54
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;
using SKG.Ext;

namespace SKG.Test.DtoTest
{
    using Dto;

    public class AuthorityDtoTest
    {
        [SetUp]
        public void Setup() { }

        [Test]
        public void Test1()
        {
            var o = new AuthorityDto(new PrivilegeDto());
            Assert.AreEqual("", o.Email);
            Assert.AreEqual(0, o.Id);

            var x = o.GetObjPrivilege("SysProfiles");
            var y = x.ToPermis(null, null);
            Assert.IsFalse(y.Read);
            Assert.IsFalse(y.Create);
            Assert.IsFalse(y.Edit);
            Assert.IsFalse(y.Delete);
            Assert.IsFalse(y.ViewAll);
            Assert.IsFalse(y.ModifyAll);
        }

        [Test]
        public void Test2()
        {
            var o = new AuthorityDto(null);
            Assert.AreEqual("", o.Email);
            Assert.AreEqual(0, o.Id);

            var x = o.GetObjPrivilege(null);
            Assert.IsTrue(x.ToPermis(null, null).None);

            x = o.GetObjPrivilege("");
            Assert.IsTrue(x.ToPermis(null, null).None);

            x = o.GetObjPrivilege("Toan");
            Assert.IsTrue(x.ToPermis(null, null).None);
        }
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Aug-06 08:54
 * Update       : 2020-Aug-06 08:54
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;

namespace SKG.Test.DtoTest
{
    using Dto;
    using static Dto.PrivilegeDto;

    public class PrivilegeDtoTest
    {
        [SetUp]
        public void Setup()
        {
            _o = new PrivilegeDto();
            Assert.IsNotNull(_o.Apps);
            Assert.IsNotNull(_o.Tabs);
            Assert.IsNotNull(_o.Objs);
        }

        [Test]
        public void Test1()
        {
            _o.AddApp(new AppSetting());
            _o.AddApp(new AppSetting(0, "CMS"));
            Assert.AreEqual(2, _o.Apps.Count);

            _o.AddTab(new TabSetting());
            _o.AddTab(new TabSetting(0, "Home"));
            Assert.AreEqual(2, _o.Tabs.Count);

            _o.AddObj(new ObjSetting());
            _o.AddObj(new ObjSetting(0, "SynUserSites"));
            Assert.AreEqual(2, _o.Objs.Count);
        }

        [Test]
        public void Test2()
        {
            var a = new AppSetting(0, "CMS");
            Assert.AreEqual("CMS", a.DevName);
            Assert.IsFalse(a.Visible);
            Assert.IsFalse(a.Default);

            var b = new TabSetting(0, "Home");
            Assert.AreEqual("Home", b.DevName);
            Assert.AreEqual(Visible.Hidden, b.Visible);

            var c = new ObjSetting(0, "SynUserSite");
            Assert.AreEqual("SynUserSite", c.DevName);
            Assert.AreEqual(Sop.None, c.AccessRight);

            c = new ObjSetting(0, "SynUserSite", true, Sop.ModifyAll);
            Assert.AreEqual("SynUserSite", c.DevName);
            Assert.AreEqual(Sop.ModifyAll, c.AccessRight);

            var t = c.ToString();
            Assert.AreEqual("{\"ar\":13,\"dn\":\"SynUserSite\",\"ct\":true,\"Id\":0}", t);
        }

        [Test]
        public void Test3()
        {
            _o.AddApp(new AppSetting(0, "Toan"));
            _o.AddTab(new TabSetting(0, "Dashboard"));
            _o.AddObj(new ObjSetting(0, "Profile"));

            _o.UpdateApp(new AppSetting(0, "Toan"));
            _o.UpdateTab(new TabSetting(0, "Dashboard"));
            _o.UpdateObj(new ObjSetting(0, "Profile"));

            Assert.AreEqual(80, TestDataObjs.Count);
        }

        [Test]
        public void Test4()
        {
            #region -- AppSetting --
            var a = new AppSetting();
            Assert.IsEmpty(a.DevName);
            Assert.IsFalse(a.Visible);

            a = new AppSetting(0, null);
            Assert.IsEmpty(a.DevName);
            Assert.IsFalse(a.Visible);

            a = new AppSetting(0, "");
            Assert.IsEmpty(a.DevName);
            Assert.IsFalse(a.Visible);
            #endregion

            #region -- TabSetting --
            var b = new TabSetting();
            Assert.IsEmpty(b.DevName);
            Assert.AreEqual(Visible.Hidden, b.Visible);

            b = new TabSetting(0, null);
            Assert.IsEmpty(b.DevName);
            Assert.AreEqual(Visible.Hidden, b.Visible);

            b = new TabSetting(0, "");
            Assert.IsEmpty(b.DevName);
            Assert.AreEqual(Visible.Hidden, b.Visible);
            #endregion

            #region -- ObjSetting --
            var c = new ObjSetting();
            Assert.IsEmpty(c.DevName);
            Assert.AreEqual(Sop.None, c.AccessRight);

            c = new ObjSetting(0, null);
            Assert.IsEmpty(c.DevName);
            Assert.AreEqual(Sop.None, c.AccessRight);

            c = new ObjSetting(0, "");
            Assert.IsEmpty(c.DevName);
            Assert.AreEqual(Sop.None, c.AccessRight);
            #endregion
        }

        private PrivilegeDto _o;
    }
}
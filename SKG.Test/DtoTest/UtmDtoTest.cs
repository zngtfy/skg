﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-10 14:41
 * Checklist    : 1.0
 * Status       : Done
 */
#endregion

using NUnit.Framework;

namespace SKG.Test.DtoTest
{
    using Dto;
    using Zraffic = ZEnum.Traffic;

    public class UtmDtoTest
    {
        [SetUp]
        public void Setup() { }

        [Test]
        public void Test1()
        {
            var s = "https://localhost:44399/?utm_source=google&utm_medium=email&utm_campaign=spring_sale&utm_term=toan&utm_content=ton-ten&fbclid=IwAR0qimvXpuxSW9ReF5cb953jZ6NKYVBURVpKvlm7GRRS3-Yxfr0MTRfEyec";
            var o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Facebook);
            Assert.IsNotNull(o.Source);
            Assert.IsNotNull(o.Medium);
            Assert.IsNotNull(o.Campaign);
            Assert.IsNotNull(o.Term);
            Assert.IsNotNull(o.Content);

            s = "https://localhost:44399/?utm_source=facebook&utm_medium=toan&utm_campaign=toan-ten&utm_term=hello&utm_content=ok";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Direct);

            s = "https://localhost:44399/?fbclid=IwAR3lXUpY7p5iIEqkFn1cddP1qKOl6cRf5D514UrN_HUP0YLtHA6mWfiwT8o";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Facebook);

            s = "https://localhost:44399/?gclid=CjwKEAiApOq2BRDoo8SVjZHV7TkSJABLe2iDnbfXanR14L-Di0IaoXGFTRx_uF_SGJ6QrmMstGDQChoCbBzw_wcB";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Google);

            s = "https://localhost:44399/?gclsrc=CjwKEAiApOq2BRDoo8SVjZHV7TkSJABLe2iDnbfXanR14L-Di0IaoXGFTRx_uF_SGJ6QrmMstGDQChoCbBzw_wcB";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Google);

            s = "https://localhost:44399/?dclid=CjwKEAiApOq2BRDoo8SVjZHV7TkSJABLe2iDnbfXanR14L-Di0IaoXGFTRx_uF_SGJ6QrmMstGDQChoCbBzw_wcB";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.DoubleClick);

            s = "https://localhost:44399/?zanpid=CjwKEAiApOq2BRDoo8SVjZHV7TkSJABLe2iDnbfXanR14L-Di0IaoXGFTRx_uF_SGJ6QrmMstGDQChoCbBzw_wcB";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Zanox);
        }

        [Test]
        public void Test2()
        {
            var s = "?utm_source=google&utm_medium=email&utm_campaign=spring_sale&utm_term=toan&utm_content=ton-ten&fbclid=IwAR0qimvXpuxSW9ReF5cb953jZ6NKYVBURVpKvlm7GRRS3-Yxfr0MTRfEyec";
            var o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Facebook);
            Assert.IsNotNull(o.Source);

            s = "utm_source=google&utm_medium=email&utm_campaign=spring_sale&utm_term=toan&utm_content=ton-ten&fbclid=IwAR0qimvXpuxSW9ReF5cb953jZ6NKYVBURVpKvlm7GRRS3-Yxfr0MTRfEyec";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Facebook);
            Assert.IsNotNull(o.Source);

            s = "/utm_source=google&utm_medium=email&utm_campaign=spring_sale&utm_term=toan&utm_content=ton-ten&fbclid=IwAR0qimvXpuxSW9ReF5cb953jZ6NKYVBURVpKvlm7GRRS3-Yxfr0MTRfEyec";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Facebook);
            Assert.IsNull(o.Source);
        }

        [Test]
        public void Test3()
        {
            var s = string.Empty;
            var o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Direct);

            s = null;
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Direct);

            s = "toan";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Direct);

            s = "?";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Direct);

            s = "=";
            o = new UtmDto(s);
            Assert.AreEqual(o.Traffic, Zraffic.Direct);
        }
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-13 10:28
 * Update       : 2020-Jul-13 10:28
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;

namespace SKG.Test
{
    using static Dto.PrivilegeDto;

    public class ZEnumTest
    {
        [SetUp]
        public void Setup() { }

        [Test]
        public void Test1()
        {
            var t = (int)Sop.Read;
            Assert.AreEqual(1, t);

            t = (int)Sop.Create;
            Assert.AreEqual(2, t);

            t = (int)Sop.Edit;
            Assert.AreEqual(4, t);

            t = (int)Sop.Delete;
            Assert.AreEqual(8, t);

            t = (int)Sop.ViewAll;
            Assert.AreEqual(1, t);

            t = (int)Sop.ModifyAll;
            Assert.AreEqual(13, t);
        }


        [Test]
        public void Test2()
        {
            var x = (Sop.ViewAll & Sop.Read) == Sop.Read;
            Assert.IsTrue(x);

            x = (Sop.ModifyAll & Sop.Read) == Sop.Read;
            Assert.IsTrue(x);

            x = (Sop.ModifyAll & Sop.Create) == Sop.Create;
            Assert.IsFalse(x);

            x = (Sop.ModifyAll & Sop.Edit) == Sop.Edit;
            Assert.IsTrue(x);

            x = (Sop.ModifyAll & Sop.Delete) == Sop.Delete;
            Assert.IsTrue(x);
        }
    }
}
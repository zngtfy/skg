﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-09 16:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;

namespace SKG.Test.ExtTest
{
    using Ext;

    public class TypeExtTest
    {
        [SetUp]
        public void Setup() { }

        #region -- Kopy --

        [Test]
        public void Kopy1()
        {
            var a = new Poco("a") { Id = "1" };
            var b = new Poco("b");
            a.Kopy(b);
            Assert.AreEqual(a.Name, b.Name);
            Assert.IsNull(b.Id);
        }

        #endregion

        #region -- Differ --

        [Test]
        public void Differ1()
        {
            var a = new Poco("a") { Id = "1" };
            var b = new Poco("b");
            var t = a.Differ(b);
            Assert.IsTrue(t);
        }

        #endregion

        #region -- ToJson OK --

        [Test]
        public void ToJson1()
        {
            var o = new Poco("a");
            var t = o.ToJson();
            var exp = "{\"Id\":null,\"Name\":\"a\"}";
            Assert.AreEqual(exp, t);

            o = null;
            t = o.ToJson();
            exp = "";
            Assert.AreEqual(exp, t);
        }

        #endregion
    }
}
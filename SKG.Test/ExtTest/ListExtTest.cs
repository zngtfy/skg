﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-09 16:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;
using System.Collections.Generic;

namespace SKG.Test.ExtTest
{
    using Ext;
    using Ztring = ZConst.String;

    public class ListExtTest
    {
        [SetUp]
        public void Setup() { }

        #region -- Zoin OK --

        [Test]
        public void Zoin1()
        {
            var o = new Poco("a");
            var l = new List<Poco> { o };
            var t = l.Zoin();
            Assert.AreEqual("a", t);

            t = l.Zoin(true);
            Assert.AreEqual(";a;", t);

            l = new List<Poco> { o, o };
            t = l.Zoin();
            Assert.AreEqual("a", t);

            t = l.Zoin(true);
            Assert.AreEqual(";a;", t);
        }

        [Test]
        public void Zoin2()
        {
            List<Poco> l = null;
            var t = l.Zoin();
            Assert.AreEqual("", t);

            t = l.Zoin(true);
            Assert.AreEqual(Ztring.Semicolon, t);

            t = l.Zoin(true, Ztring.Ampersand);
            Assert.AreEqual(Ztring.Ampersand, t);
        }

        #endregion

        #region -- ToSet OK --

        [Test]
        public void ToSet1()
        {
            var l = new List<string>() { "a", "b" };
            var t = l.ToSet();
            Assert.AreEqual(2, t.Count);

            l = new List<string>() { "a;c", "b;a" };
            t = l.ToSet();
            Assert.AreEqual(3, t.Count);

            l = new List<string>() { "a;c", "", "b;a" };
            t = l.ToSet();
            Assert.AreEqual(3, t.Count);

            l = new List<string>() { "a;c", ";;", "b;a" };
            t = l.ToSet();
            Assert.AreEqual(3, t.Count);
        }

        [Test]
        public void ToSet2()
        {
            var l = new List<string>();
            var t = l.ToSet();
            Assert.AreEqual(0, t.Count);

            l = null;
            t = l.ToSet();
            Assert.AreEqual(0, t.Count);
        }

        #endregion

        #region -- ToListInt OK --

        [Test]
        public void ToListInt()
        {
            var s = new List<string> { "a;b;a;b;c" };
            var t = s.ToListInt();
            Assert.AreEqual(1, t.Count);
            Assert.AreEqual(0, t[0]);

            s = new List<string> { "a;1;a;b;c;" };
            t = s.ToListInt();
            Assert.AreEqual(2, t.Count);
            Assert.AreEqual(0, t[0]);
            Assert.AreEqual(1, t[1]);

            s = new List<string> { "a;b;;;;a;4;c;" };
            t = s.ToListInt();
            Assert.AreEqual(2, t.Count);
            Assert.AreEqual(0, t[0]);
            Assert.AreEqual(4, t[1]);

            s = null;
            t = s.ToListInt();
            Assert.AreEqual(0, t.Count);

            s = new List<string> { "" };
            t = s.ToListInt();
            Assert.AreEqual(0, t.Count);
        }

        #endregion

        #region -- ToListIntN OK --

        [Test]
        public void ToListIntN()
        {
            var s = new List<string> { "a;b;a;b;c" };
            var t = s.ToListIntN();
            Assert.AreEqual(1, t.Count);
            Assert.IsNull(t[0]);

            s = new List<string> { "a;1;a;b;c;" };
            t = s.ToListIntN();
            Assert.AreEqual(2, t.Count);
            Assert.IsNull(t[0]);
            Assert.AreEqual(1, t[1]);

            s = new List<string> { "a;b;;;;a;4;c;" };
            t = s.ToListIntN();
            Assert.AreEqual(2, t.Count);
            Assert.IsNull(t[0]);
            Assert.AreEqual(4, t[1]);

            s = null;
            t = s.ToListIntN();
            Assert.AreEqual(0, t.Count);

            s = new List<string> { "" };
            t = s.ToListIntN();
            Assert.AreEqual(0, t.Count);
        }

        #endregion
    }

    public class Poco
    {
        public override string ToString()
        {
            return Name;
        }

        public Poco() { }

        public Poco(string name)
        {
            Name = name;
        }

        public string Id { get; set; }

        public string Name { get; private set; }
    }
}
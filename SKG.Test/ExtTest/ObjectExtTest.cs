﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-09 16:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;

namespace SKG.Test.ExtTest
{
    using Ext;
    using Zormat = ZEnum.Format;

    public class ObjectExtTest
    {
        [SetUp]
        public void Setup()
        {
            _upper = "TOAN NGUYEN";
            _lower = "toan nguyen";
        }

        #region -- Value --

        [Test]
        public void Value1()
        {
            var o = new { id = "1" };
            var t = o.Value("id").ToString();
            Assert.AreEqual("1", t);
        }

        [Test]
        public void Value2()
        {
            var o = "{ \"criteria\": [ { \"name\": \"project.name\", \"type\": \"in\", \"value\": [ \"Estateville\" ] } ], \"offset\": 0, \"limit\": 20, \"order_by\": { \"system_ctime\": \"asc\" } }";
            var t = o.Value("limit").ToString();
            Assert.AreEqual("20", t);
        }

        [Test]
        public void Value3()
        {
            var x = "{ \"id\": \"1\" }";
            var o = JObject.Parse(x);
            var t = o.Value("id").ToString();
            Assert.AreEqual("1", t);
        }

        [Test]
        public void Value4()
        {
            object o = null;
            var t = o.Value("id");
            Assert.AreEqual(null, t);
        }

        [Test]
        public void Value5()
        {
            DateTime? o = null;
            var t = o.Value("id");
            Assert.AreEqual(null, t);
        }

        [Test]
        public void Value6()
        {
            string o = "";
            var t = o.Value("id");
            Assert.AreEqual(null, t);

            o = "toan";
            t = o.Value("id");
            Assert.AreEqual(null, t);
        }

        #endregion

        #region -- ToStr --

        [Test]
        public void ToStr1()
        {
            var o = _lower;
            var t = o.ToStr();
            Assert.AreEqual(o, t);

            t = o.ToStr(Zormat.Capitalized);
            Assert.AreEqual("Toan Nguyen", t);

            t = o.ToStr(Zormat.Lower);
            Assert.AreEqual(_lower, t);

            t = o.ToStr(Zormat.Original);
            Assert.AreEqual(o, t);

            t = o.ToStr(Zormat.Sentence);
            Assert.AreEqual("Toan nguyen", t);

            t = o.ToStr(Zormat.Upper);
            Assert.AreEqual(_upper, t);
        }

        [Test]
        public void ToStr2()
        {
            var o = _upper;
            var t = o.ToStr();
            Assert.AreEqual(o, t);

            t = o.ToStr(Zormat.Capitalized);
            Assert.AreEqual("Toan Nguyen", t);

            t = o.ToStr(Zormat.Lower);
            Assert.AreEqual(_lower, t);

            t = o.ToStr(Zormat.Original);
            Assert.AreEqual(o, t);

            t = o.ToStr(Zormat.Sentence);
            Assert.AreEqual("Toan nguyen", t);

            t = o.ToStr(Zormat.Upper);
            Assert.AreEqual(_upper, t);
        }

        [Test]
        public void ToStr3()
        {
            object o = null;
            var t = o.ToStr();
            Assert.AreEqual(string.Empty, t);

            o = string.Empty;
            t = o.ToStr();
            Assert.AreEqual(string.Empty, t);
        }

        [Test]
        public void ToStr4()
        {
            object o = new Poco("hello");
            var t = o.ToStr();
            Assert.AreEqual("hello", t);

            o = null;
            t = o.ToStr();
            Assert.AreEqual(string.Empty, t);
        }

        #endregion

        #region -- IsNumeric --

        [Test]
        public void IsNumeric1()
        {
            var o = _lower;
            var t = o.IsNumeric();
            Assert.IsFalse(t);

            o = null;
            t = o.IsNumeric();
            Assert.IsFalse(t);

            o = new DateTime();
            t = o.IsNumeric();
            Assert.IsFalse(t);

            o = true;
            t = o.IsNumeric();
            Assert.IsFalse(t);
        }

        [Test]
        public void IsNumeric2()
        {
            object o = 1;
            var t = o.IsNumeric();
            Assert.IsTrue(t);

            o = 1L;
            t = o.IsNumeric();
            Assert.IsTrue(t);

            o = 1.1;
            t = o.IsNumeric();
            Assert.IsTrue(t);
        }

        #endregion

        #region -- ToDecimal --

        [Test]
        public void ToInt32()
        {
            var o = _lower;
            var t = o.ToDecimal();
            Assert.AreEqual(0, t);

            o = null;
            t = o.ToDecimal();
            Assert.AreEqual(0, t);

            o = "1,1";
            t = o.ToDecimal();
            Assert.AreEqual(11, t);

            o = "1.1";
            t = o.ToDecimal();
            Assert.AreEqual(1.1, t);

            o = "11";
            t = o.ToDecimal();
            Assert.AreEqual(11, t);
        }

        #endregion

        #region -- ToDecimalN --

        [Test]
        public void ToDecimalN()
        {
            var o = _lower;
            var t = o.ToDecimalN();
            Assert.IsNull(t);

            o = null;
            t = o.ToDecimalN();
            Assert.IsNull(t);

            o = "1,1";
            t = o.ToDecimalN();
            Assert.AreEqual(11, t);

            o = "11";
            t = o.ToDecimalN();
            Assert.AreEqual(11, t);
        }

        #endregion

        #region -- ToGuid --

        [Test]
        public void ToGuid1()
        {
            var o = "efd28e75-9d66-4adb-a573-b4f7c584ce06";
            var e = new Guid(o);
            var t = o.ToGuid();
            Assert.AreEqual(e, t);

            o = "toan";
            e = Guid.Empty;
            t = o.ToGuid();
            Assert.AreNotEqual(e, t);
        }

        [Test]
        public void ToGuid2()
        {
            var o = "";
            var e = Guid.Empty;
            var t = o.ToGuid();
            Assert.AreNotEqual(e, t);

            o = null;
            t = o.ToGuid();
            Assert.AreNotEqual(e, t);
        }

        #endregion

        #region -- ToGuidN --

        [Test]
        public void ToGuidN1()
        {
            var o = "efd28e75-9d66-4adb-a573-b4f7c584ce06";
            var e = new Guid(o);
            var t = o.ToGuidN();
            Assert.AreEqual(e, t);
        }

        [Test]
        public void ToGuidN2()
        {
            var o = "";
            Guid? e = null;
            var t = o.ToGuidN();
            Assert.AreEqual(e, t);

            o = null;
            t = o.ToGuidN();
            Assert.AreEqual(e, t);

            o = "toan";
            t = o.ToGuidN();
            Assert.AreNotEqual(e, t);
        }

        #endregion

        #region -- ToDateTime --

        [Test]
        public void ToDateTime()
        {
            var o = "efd28e75-9d66-4adb-a573-b4f7c584ce06";
            var t = o.ToDateTime("dd/MM/yyyy");
            Assert.AreEqual(DateTime.Now.Year, t.Year);
            Assert.AreEqual(DateTime.Now.Month, t.Month);
            Assert.AreEqual(DateTime.Now.Day, t.Day);

            o = "11/09/2020";
            t = o.ToDateTime("dd/MM/yyyy");
            Assert.AreEqual(2020, t.Year);
            Assert.AreEqual(9, t.Month);
            Assert.AreEqual(11, t.Day);
        }

        #endregion

        #region -- ToDateTimeNull --

        [Test]
        public void ToDateTimeNull()
        {
            var o = "efd28e75-9d66-4adb-a573-b4f7c584ce06";
            var t = o.ToDateTimeNull("dd/MM/yyyy");
            Assert.IsNull(t);

            o = "11/09/2020";
            t = o.ToDateTime("dd/MM/yyyy");
            Assert.AreEqual(2020, t.Value.Year);
            Assert.AreEqual(9, t.Value.Month);
            Assert.AreEqual(11, t.Value.Day);
        }

        #endregion

        private object _upper;
        private object _lower;
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-09 16:48
 * Update       : 2020-Jul-09 16:48
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;

namespace SKG.Test.ExtTest
{
    using Ext;
    using Ztring = ZConst.String;

    public class StringExtTest
    {
        [SetUp]
        public void Setup()
        {
            _upper = "TOAN NGUYEN";
            _lower = "toan nguyen";
        }

        #region -- ToUpperFirst OK --

        [Test]
        public void ToUpperFirst1()
        {
            var s = _lower;
            var t = s.ToUpperFirst();
            Assert.AreEqual("Toan nguyen", t);

            s = "a";
            t = s.ToUpperFirst();
            Assert.AreEqual("A", t);

            s = "ab";
            t = s.ToUpperFirst();
            Assert.AreEqual("Ab", t);
        }

        [Test]
        public void ToUpperFirst2()
        {
            var s = _upper;
            var t = s.ToUpperFirst();
            Assert.AreEqual("Toan nguyen", t);
        }

        [Test]
        public void ToUpperFirst13()
        {
            var s = "";
            var t = s.ToUpperFirst();
            Assert.AreEqual("", t);

            s = null;
            t = s.ToUpperFirst();
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- ToUpperWords OK --

        [Test]
        public void ToUpperWords1()
        {
            var s = _lower;
            var t = s.ToUpperWords();
            Assert.AreEqual("Toan Nguyen", t);

            s = "a";
            t = s.ToUpperWords();
            Assert.AreEqual("A", t);

            s = "ab";
            t = s.ToUpperWords();
            Assert.AreEqual("Ab", t);
        }

        [Test]
        public void ToUpperWords2()
        {
            var s = _upper;
            var t = s.ToUpperWords();
            Assert.AreEqual("Toan Nguyen", t);
        }

        [Test]
        public void ToUpperWords3()
        {
            var s = "";
            var t = s.ToUpperWords();
            Assert.AreEqual("", t);

            s = null;
            t = s.ToUpperWords();
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- ToPascalCase OK --

        [Test]
        public void ToPascalCase1()
        {
            var s = _lower;
            var t = s.ToPascalCase();
            Assert.AreEqual("ToanNguyen", t);

            s = _upper;
            t = s.ToPascalCase();
            Assert.AreEqual("ToanNguyen", t);

            s = "a";
            t = s.ToPascalCase();
            Assert.AreEqual("A", t);

            s = "ab";
            t = s.ToPascalCase();
            Assert.AreEqual("Ab", t);
        }

        [Test]
        public void ToPascalCase2()
        {
            var s = "WARD_VS_VITAL_SIGNS";
            var t = s.ToPascalCase();
            Assert.AreEqual("WardVsVitalSigns", t);

            s = "Who am I?";
            t = s.ToPascalCase();
            Assert.AreEqual("WhoAmI", t);

            s = "I ate before you got here";
            t = s.ToPascalCase();
            Assert.AreEqual("IAteBeforeYouGotHere", t);

            s = "Hello|Who|Am|I?";
            t = s.ToPascalCase();
            Assert.AreEqual("HelloWhoAmI", t);

            s = "Live long and prosper";
            t = s.ToPascalCase();
            Assert.AreEqual("LiveLongAndProsper", t);

            s = "Lorem ipsum dolor...";
            t = s.ToPascalCase();
            Assert.AreEqual("LoremIpsumDolor", t);

            s = "CoolSP";
            t = s.ToPascalCase();
            Assert.AreEqual("CoolSp", t);

            s = "AB9CD";
            t = s.ToPascalCase();
            Assert.AreEqual("Ab9Cd", t);
        }

        [Test]
        public void ToPascalCase3()
        {
            var s = "";
            var t = s.ToPascalCase();
            Assert.AreEqual("", t);

            s = null;
            t = s.ToPascalCase();
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- ToDefault OK --

        [Test]
        public void ToDefault1()
        {
            var s = _upper;
            var t = s.ToDefault();
            Assert.AreEqual(_upper, t);
        }

        [Test]
        public void ToDefault2()
        {
            var s = "";
            var t = s.ToDefault();
            Assert.AreEqual(Ztring.Minus, t);

            s = null;
            t = s.ToDefault();
            Assert.AreEqual(Ztring.Minus, t);

            s = "";
            t = s.ToDefault(_upper);
            Assert.AreEqual(_upper, t);
        }

        #endregion

        #region -- ToDistinct OK --

        [Test]
        public void ToDistinct1()
        {
            var s = "a;b;a;b;c";
            var t = s.ToDistinct();
            Assert.AreEqual("a;b;c", t);

            s = "a;b;a;b;c;";
            t = s.ToDistinct();
            Assert.AreEqual("a;b;c", t);

            s = "a;b;a;b;c;";
            t = s.ToDistinct(true);
            Assert.AreEqual("a;b;c;", t);
        }

        [Test]
        public void ToDistinct2()
        {
            var s = "";
            var t = s.ToDistinct();
            Assert.AreEqual("", t);

            s = null;
            t = s.ToDistinct();
            Assert.AreEqual("", t);
        }

        [Test]
        public void ToDistinct3()
        {
            var s = "a;b;;a;b;c";
            var t = s.ToDistinct();
            Assert.AreEqual("a;b;c", t);

            s = "a;b;a;b;;;c;";
            t = s.ToDistinct(true);
            Assert.AreEqual("a;b;c;", t);
        }

        #endregion

        #region -- ToSet --

        [Test]
        public void ToSet1()
        {
            var s = "a;b;a;b;c";
            var t = s.ToSet();
            Assert.AreEqual(3, t.Count);

            s = "a;b;a;b;c;";
            t = s.ToSet();
            Assert.AreEqual(3, t.Count);

            s = "a;b;;;;a;b;c;";
            t = s.ToSet();
            Assert.AreEqual(3, t.Count);
        }

        [Test]
        public void ToSet2()
        {
            var s = "";
            var t = s.ToSet();
            Assert.AreEqual(0, t.Count);

            s = null;
            t = s.ToSet();
            Assert.AreEqual(0, t.Count);

            s = ",";
            t = s.ToSet();
            Assert.AreEqual(1, t.Count);
        }

        [Test]
        public void ToSet3()
        {
            var s = "a;b;a;b;c";
            var t = s.ToSet(";;");
            Assert.AreEqual(1, t.Count);

            s = "a;b;a::;b;c;";
            t = s.ToSet("::");
            Assert.AreEqual(2, t.Count);

            s = "a;b;;;;a;b;c;";
            t = s.ToSet(";;");
            Assert.AreEqual(2, t.Count);
        }

        [Test]
        public void ToSet4()
        {
            var s = "";
            var t = s.ToSet(";;");
            Assert.AreEqual(0, t.Count);

            s = null;
            t = s.ToSet(";;");
            Assert.AreEqual(0, t.Count);

            s = ",";
            t = s.ToSet(";;");
            Assert.AreEqual(1, t.Count);
        }

        #endregion

        #region -- ToListInt OK --

        [Test]
        public void ToListInt()
        {
            var s = "a;b;a;b;c";
            var t = s.ToListInt();
            Assert.AreEqual(3, t.Count);
            Assert.AreEqual(0, t[0]);
            Assert.AreEqual(0, t[1]);
            Assert.AreEqual(0, t[2]);

            s = "a;1;a;b;c;";
            t = s.ToListInt();
            Assert.AreEqual(4, t.Count);
            Assert.AreEqual(0, t[0]);
            Assert.AreEqual(1, t[1]);
            Assert.AreEqual(0, t[2]);

            s = "a;b;;;;a;4;c;";
            t = s.ToListInt();
            Assert.AreEqual(4, t.Count);
            Assert.AreEqual(0, t[0]);
            Assert.AreEqual(0, t[1]);
            Assert.AreEqual(4, t[2]);

            s = null;
            t = s.ToListInt();
            Assert.AreEqual(0, t.Count);

            s = "";
            t = s.ToListInt();
            Assert.AreEqual(0, t.Count);
        }

        #endregion

        #region -- ToListIntN OK --

        [Test]
        public void ToListIntN()
        {
            var s = "a;b;a;b;c";
            var t = s.ToListIntN();
            Assert.AreEqual(3, t.Count);
            Assert.IsNull(t[0]);
            Assert.IsNull(t[1]);
            Assert.IsNull(t[2]);

            s = "a;1;a;b;c;";
            t = s.ToListIntN();
            Assert.AreEqual(4, t.Count);
            Assert.IsNull(t[0]);
            Assert.AreEqual(1, t[1]);
            Assert.IsNull(t[2]);

            s = "a;b;;;;a;4;c;";
            t = s.ToListIntN();
            Assert.AreEqual(4, t.Count);
            Assert.IsNull(t[0]);
            Assert.IsNull(t[1]);
            Assert.AreEqual(4, t[2]);

            s = null;
            t = s.ToListIntN();
            Assert.AreEqual(0, t.Count);

            s = "";
            t = s.ToListIntN();
            Assert.AreEqual(0, t.Count);
        }

        #endregion

        #region -- ToInst OK --

        [Test]
        public void ToInst()
        {
            var s = "{\"Id\":1,\"Name\":\"a\"}";
            var t = s.ToInst<Poco>();
            Assert.IsNotNull(t);
            Assert.AreEqual("1", t.Id);

            s = "{\"Id\":nulme\":\"a\"}";
            t = s.ToInst<Poco>();
            Assert.IsNotNull(t);
            Assert.IsNull(t.Id);
        }

        #endregion

        #region -- ToList OK --

        [Test]
        public void ToList()
        {
            var s = "[{\"Id\":1,\"Name\":\"a\"}]";
            var t = s.ToList<Poco>();
            Assert.AreEqual(1, t.Count);
            Assert.AreEqual("1", t[0].Id);

            s = "[{\"Id\":nulme\":\"a\"}]";
            t = s.ToList<Poco>();
            Assert.AreEqual(0, t.Count);
        }

        #endregion

        #region -- ToAddSpace OK --

        [Test]
        public void ToAddSpace()
        {
            var s = "NguyenVanToan";
            var t = s.ToAddSpace();
            Assert.AreEqual("Nguyen Van Toan", t);

            s = "topten";
            t = s.ToAddSpace();
            Assert.AreEqual("topten", t);

            s = "topTen";
            t = s.ToAddSpace();
            Assert.AreEqual("top Ten", t);

            s = "";
            t = s.ToAddSpace();
            Assert.AreEqual("", t);

            s = null;
            t = s.ToAddSpace();
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- ToPrefix OK --

        [Test]
        public void ToPrefix()
        {
            var s = "NguyenVanToan";
            var t = s.ToPrefix();
            Assert.AreEqual("Nguyen", t);

            s = "nguyenVanToan";
            t = s.ToPrefix();
            Assert.AreEqual("nguyen", t);

            s = "topten";
            t = s.ToPrefix();
            Assert.AreEqual("topten", t);

            s = "topTen";
            t = s.ToPrefix();
            Assert.AreEqual("top", t);

            s = "";
            t = s.ToPrefix();
            Assert.AreEqual("", t);

            s = null;
            t = s.ToPrefix();
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- InitLetter --

        [Test]
        public void InitLetter1()
        {
            var s = "NguyenVanToan";
            var t = s.InitLetter();
            Assert.AreEqual("NT", t);

            s = "Nguyen";
            t = s.InitLetter();
            Assert.AreEqual("NN", t);

            s = "ToanNguyen";
            t = s.InitLetter();
            Assert.AreEqual("TN", t);
        }

        [Test]
        public void InitLetter2()
        {
            var s = "";
            var t = s.InitLetter();
            Assert.AreEqual("", t);

            s = null;
            t = s.InitLetter();
            Assert.AreEqual("", t);

            s = " ";
            t = s.InitLetter();
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- ToMySQL --

        [Test]
        public void ToMySQL1()
        {
            var s = "NguyenVanToan";
            var t = s.ToMySQL("a");
            Assert.AreEqual("", t);

            s = "Server=192.168.1.99;Port=3306;Database=matcha-analysis-dev;Uid=root;Pwd=P@SSnhucu6969;";
            t = s.ToMySQL("analysis");
            Assert.IsNotNull(t);

            t = s.ToMySQL(null, true);
            Assert.IsNotNull(t);
        }

        [Test]
        public void ToMySQL2()
        {
            var s = "";
            var t = s.ToMySQL(s);
            Assert.AreEqual("", t);

            s = null;
            t = s.ToMySQL(s);
            Assert.AreEqual("", t);

            s = " ";
            t = s.ToMySQL(s);
            Assert.AreEqual("", t);
        }

        #endregion

        #region -- ToFirstName --

        [Test]
        public void ToFirstName1()
        {
            var s = "Toan Nguyen";
            var t = s.ToFirstName(out string lastName);
            Assert.AreEqual("Toan", t);
            Assert.AreEqual("Nguyen", lastName);

            s = "Toan Nguyen Van";
            t = s.ToFirstName(out lastName);
            Assert.AreEqual("Toan", t);
            Assert.AreEqual("Nguyen Van", lastName);
        }

        [Test]
        public void ToFirstName2()
        {
            var s = "";
            var t = s.ToFirstName(out string lastName);
            Assert.AreEqual("", t);
            Assert.AreEqual("", lastName);

            s = null;
            t = s.ToFirstName(out lastName);
            Assert.AreEqual("", t);
            Assert.AreEqual("", lastName);
        }

        #endregion

        #region -- IsPlural --

        [Test]
        public void IsPlural1()
        {
            Assert.IsTrue("Synopses".IsPlural("Synopsis"));
            Assert.IsTrue("Men".IsPlural("Man"));
            Assert.IsTrue("Women".IsPlural("Woman"));
            Assert.IsTrue("People".IsPlural("Person"));
            Assert.IsTrue("Children".IsPlural("Child"));
            Assert.IsFalse("Synopses".IsPlural("NotSynopses"));
        }

        #endregion

        private string _upper;
        private string _lower;
    }
}
﻿#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Dec-04 07:25
 * Update       : 2020-Dec-04 07:25
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;

namespace SKG.Test.ExtTest
{
    using Ext;

    public class FileExtTest
    {
        [SetUp]
        public void Setup() { }

        #region -- ToFileSize --

        [Test]
        public void ToFileSize1()
        {
            var a = 10001m.ToFileSize();
            Assert.AreEqual("9.77 KB", a);

            a = 2000001m.ToFileSize();
            Assert.AreEqual("1.91 MB", a);
        }

        #endregion

        #region -- ToMimeType --

        [Test]
        public void ToMimeType1()
        {
            var ext = "";
            var a = ext.ToMimeType();
            Assert.AreEqual(ext, a);

            ext = ".";
            a = ext.ToMimeType();
            Assert.AreEqual(ext, a);

            ext = ".pdf";
            a = ext.ToMimeType();
            Assert.AreEqual("application/pdf", a);
        }

        #endregion
    }
}
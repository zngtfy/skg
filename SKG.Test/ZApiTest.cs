#region Information
/*
 * Author       : Zng Tfy
 * Email        : toan@immexgroup.com
 * Phone        : +84 345 515 010
 * ------------------------------- *
 * Create       : 2020-Jul-15 08:38
 * Update       : 2021-May-11 18:58
 * Checklist    : 1.0
 * Status       : New
 */
#endregion

using NUnit.Framework;

namespace SKG.Test
{
    using Dto;
    using Ext;
    using static ZConst;

    public class ZApiTest
    {
        [SetUp]
        public void Setup()
        {
            var s = new SiteDto
            {
                SiteUid = "efd28e75-9d66-4adb-a573-b4f7c584ce06",
                ApiUrl = "https://localhost:44395",
                ApiKey = "TqgAsTj8VIpA3VoQUgJweqjShrMD4D+zZA5Qb/l60nA=",
                ApiHash = "CFTwZpTTfgMW8K39IF7hew=="
            };

            _h = new ZApi(s);
            _expandData = "{\"Salesforce\":{\"Host\":\"https://test.salesforce.com/services/oauth2/token\",\"ClientId\":\"3MVG9pcaEGrGRoTIT.r78JgvslTUmkEgy5Vh3GZRAMNSeBR73MvTfGL4tzfyJBoexlDXzCuyWxZJnmdkErV36\",\"Secret\":\"CA49412F38FED6E7C5901F5A41B39352344DF727641F5AA2663F3BCC4877D93A\",\"UserName\":\"yao@microrentals.com.au.beauimmer\",\"Password\":\"P@ss3883\"},\"Other\":null}";
        }

        [Test]
        public void Test1()
        {
            var d = new
            {
                userName = "toan",
                apartmentCode = "A001"
            };

            var rsp = _h.PostAsync("api/CusSync/add-favorite", d, null);
            rsp.Wait();
            var t = rsp.Result;
            Assert.IsFalse(t.Success);

            var text = string.Format(Format.Semicolon, 1, "toan@gmail.com");
            var encrypted = ZEncryption.UrlEncrypt(text);
            rsp = _h.PostAsync("api/CusSync/add-favorite", d, encrypted);
            rsp.Wait();
            t = rsp.Result;
            Assert.IsFalse(t.Success);
        }

        [Test]
        public void Test2()
        {
            var rsp = _h.PostSfxAsync("MatchaUser", "toan");
            rsp.Wait();
            var t = rsp.Result;
            Assert.IsFalse(t.Success);
        }

        [Test]
        public void Test3()
        {
            var s = new AuthenticationDto
            {
                Host = "https://login.salesforce.com/services/oauth2/token",
                ClientId = "3MVG9n_HvETGhr3AVQkpxxWah..kDFdZ46bwh4YQHaqpQU6VzE8UV9.u_G1OWQ0s_2._tnAZ8ICq.akxtqmxc",
                Secret = "F00EC1AEA02BD19310ACB87712F5C8F519187944D45A723A38D1448C682DDDDB",
                UserName = "toan@immexgroup.com",
                Password = "#MelSgn2021@vn2s8e"
            };

            var o = new ExpandAuthDto
            {
                Salesforce = s
            };
            var json = o.ToJson();
            Assert.NotNull(json);

            var api = new ZApi(o);
            var rsp = api.PostSfxAsync("MatchaUser", "toan");
            rsp.Wait();
            var t = rsp.Result;
            Assert.True(t.Success);
        }

        [Test]
        public void Test4()
        {
            var s = new SiteDto
            {
                ApiKey = "uLy8+4XEPTtpyfP2LdKGeV573bp3OjcdLIKFXvIpsC0=",
                ApiHash = "CrCTdrcR8N6dw9xgPI9N8A==",
                ApiUrl = "https://analytics.immex-staging.com",
                SiteUid = "9a3f9285-759b-4916-af70-fda463bc3291"
            };

            var api = new ZApi(s);
            var token = api.Token;
            Assert.IsNotNull(token);
        }

        [Test]
        public void Test5()
        {
            var o = _expandData.ToInst<ExpandAuthDto>();
            var api = new ZApi(o);
            var rsp = api.PostSfxAsync("ApiTest", "toan");
            rsp.Wait();
            var t = rsp.Result;
            Assert.IsFalse(t.Success);
        }

        [Test]
        public void Test6()
        {
            var o = _expandData.ToInst<ExpandAuthDto>();
            var api = new ZApi(o);
            var rsp = api.GetSfxAsync("ApiRequest");
            rsp.Wait();
            var t = rsp.Result;
            Assert.True(t.Success);
        }

        private ZApi _h;

        private string _expandData;
    }
}